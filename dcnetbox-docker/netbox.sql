--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO netbox;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO netbox;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO netbox;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO netbox;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO netbox;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO netbox;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO netbox;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO netbox;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO netbox;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO netbox;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO netbox;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO netbox;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: circuits_circuit; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.circuits_circuit (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    cid character varying(50) NOT NULL,
    install_date date,
    commit_rate integer,
    comments text NOT NULL,
    provider_id integer NOT NULL,
    type_id integer NOT NULL,
    tenant_id integer,
    description character varying(100) NOT NULL,
    status smallint NOT NULL,
    CONSTRAINT circuits_circuit_commit_rate_check CHECK ((commit_rate >= 0)),
    CONSTRAINT circuits_circuit_status_check CHECK ((status >= 0))
);


ALTER TABLE public.circuits_circuit OWNER TO netbox;

--
-- Name: circuits_circuit_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.circuits_circuit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.circuits_circuit_id_seq OWNER TO netbox;

--
-- Name: circuits_circuit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.circuits_circuit_id_seq OWNED BY public.circuits_circuit.id;


--
-- Name: circuits_circuittermination; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.circuits_circuittermination (
    id integer NOT NULL,
    term_side character varying(1) NOT NULL,
    port_speed integer NOT NULL,
    upstream_speed integer,
    xconnect_id character varying(50) NOT NULL,
    pp_info character varying(100) NOT NULL,
    circuit_id integer NOT NULL,
    site_id integer NOT NULL,
    connected_endpoint_id integer,
    connection_status boolean,
    cable_id integer,
    description character varying(100) NOT NULL,
    CONSTRAINT circuits_circuittermination_port_speed_check CHECK ((port_speed >= 0)),
    CONSTRAINT circuits_circuittermination_upstream_speed_check CHECK ((upstream_speed >= 0))
);


ALTER TABLE public.circuits_circuittermination OWNER TO netbox;

--
-- Name: circuits_circuittermination_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.circuits_circuittermination_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.circuits_circuittermination_id_seq OWNER TO netbox;

--
-- Name: circuits_circuittermination_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.circuits_circuittermination_id_seq OWNED BY public.circuits_circuittermination.id;


--
-- Name: circuits_circuittype; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.circuits_circuittype (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.circuits_circuittype OWNER TO netbox;

--
-- Name: circuits_circuittype_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.circuits_circuittype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.circuits_circuittype_id_seq OWNER TO netbox;

--
-- Name: circuits_circuittype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.circuits_circuittype_id_seq OWNED BY public.circuits_circuittype.id;


--
-- Name: circuits_provider; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.circuits_provider (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    asn bigint,
    account character varying(30) NOT NULL,
    portal_url character varying(200) NOT NULL,
    noc_contact text NOT NULL,
    admin_contact text NOT NULL,
    comments text NOT NULL
);


ALTER TABLE public.circuits_provider OWNER TO netbox;

--
-- Name: circuits_provider_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.circuits_provider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.circuits_provider_id_seq OWNER TO netbox;

--
-- Name: circuits_provider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.circuits_provider_id_seq OWNED BY public.circuits_provider.id;


--
-- Name: dcim_cable; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_cable (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    termination_a_id integer NOT NULL,
    termination_b_id integer NOT NULL,
    type smallint,
    status boolean NOT NULL,
    label character varying(100) NOT NULL,
    color character varying(6) NOT NULL,
    length smallint,
    length_unit smallint,
    _abs_length numeric(10,4),
    termination_a_type_id integer NOT NULL,
    termination_b_type_id integer NOT NULL,
    CONSTRAINT dcim_cable_length_check CHECK ((length >= 0)),
    CONSTRAINT dcim_cable_length_unit_check CHECK ((length_unit >= 0)),
    CONSTRAINT dcim_cable_termination_a_id_check CHECK ((termination_a_id >= 0)),
    CONSTRAINT dcim_cable_termination_b_id_check CHECK ((termination_b_id >= 0)),
    CONSTRAINT dcim_cable_type_check CHECK ((type >= 0))
);


ALTER TABLE public.dcim_cable OWNER TO netbox;

--
-- Name: dcim_cable_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_cable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_cable_id_seq OWNER TO netbox;

--
-- Name: dcim_cable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_cable_id_seq OWNED BY public.dcim_cable.id;


--
-- Name: dcim_consoleport; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_consoleport (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    connection_status boolean,
    connected_endpoint_id integer,
    device_id integer NOT NULL,
    cable_id integer,
    description character varying(100) NOT NULL
);


ALTER TABLE public.dcim_consoleport OWNER TO netbox;

--
-- Name: dcim_consoleport_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_consoleport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_consoleport_id_seq OWNER TO netbox;

--
-- Name: dcim_consoleport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_consoleport_id_seq OWNED BY public.dcim_consoleport.id;


--
-- Name: dcim_consoleporttemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_consoleporttemplate (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_type_id integer NOT NULL
);


ALTER TABLE public.dcim_consoleporttemplate OWNER TO netbox;

--
-- Name: dcim_consoleporttemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_consoleporttemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_consoleporttemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_consoleporttemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_consoleporttemplate_id_seq OWNED BY public.dcim_consoleporttemplate.id;


--
-- Name: dcim_consoleserverport; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_consoleserverport (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_id integer NOT NULL,
    cable_id integer,
    connection_status boolean,
    description character varying(100) NOT NULL
);


ALTER TABLE public.dcim_consoleserverport OWNER TO netbox;

--
-- Name: dcim_consoleserverport_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_consoleserverport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_consoleserverport_id_seq OWNER TO netbox;

--
-- Name: dcim_consoleserverport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_consoleserverport_id_seq OWNED BY public.dcim_consoleserverport.id;


--
-- Name: dcim_consoleserverporttemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_consoleserverporttemplate (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_type_id integer NOT NULL
);


ALTER TABLE public.dcim_consoleserverporttemplate OWNER TO netbox;

--
-- Name: dcim_consoleserverporttemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_consoleserverporttemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_consoleserverporttemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_consoleserverporttemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_consoleserverporttemplate_id_seq OWNED BY public.dcim_consoleserverporttemplate.id;


--
-- Name: dcim_device; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_device (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(64),
    serial character varying(50) NOT NULL,
    "position" smallint,
    face smallint,
    status smallint NOT NULL,
    comments text NOT NULL,
    device_role_id integer NOT NULL,
    device_type_id integer NOT NULL,
    platform_id integer,
    rack_id integer,
    primary_ip4_id integer,
    primary_ip6_id integer,
    tenant_id integer,
    asset_tag character varying(50),
    site_id integer NOT NULL,
    cluster_id integer,
    virtual_chassis_id integer,
    vc_position smallint,
    vc_priority smallint,
    local_context_data jsonb,
    CONSTRAINT dcim_device_face_check CHECK ((face >= 0)),
    CONSTRAINT dcim_device_position_check CHECK (("position" >= 0)),
    CONSTRAINT dcim_device_status_4f698226_check CHECK ((status >= 0)),
    CONSTRAINT dcim_device_vc_position_check CHECK ((vc_position >= 0)),
    CONSTRAINT dcim_device_vc_priority_check CHECK ((vc_priority >= 0))
);


ALTER TABLE public.dcim_device OWNER TO netbox;

--
-- Name: dcim_device_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_device_id_seq OWNER TO netbox;

--
-- Name: dcim_device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_device_id_seq OWNED BY public.dcim_device.id;


--
-- Name: dcim_devicebay; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_devicebay (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_id integer NOT NULL,
    installed_device_id integer,
    description character varying(100) NOT NULL
);


ALTER TABLE public.dcim_devicebay OWNER TO netbox;

--
-- Name: dcim_devicebay_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_devicebay_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_devicebay_id_seq OWNER TO netbox;

--
-- Name: dcim_devicebay_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_devicebay_id_seq OWNED BY public.dcim_devicebay.id;


--
-- Name: dcim_devicebaytemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_devicebaytemplate (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_type_id integer NOT NULL
);


ALTER TABLE public.dcim_devicebaytemplate OWNER TO netbox;

--
-- Name: dcim_devicebaytemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_devicebaytemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_devicebaytemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_devicebaytemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_devicebaytemplate_id_seq OWNED BY public.dcim_devicebaytemplate.id;


--
-- Name: dcim_devicerole; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_devicerole (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    color character varying(6) NOT NULL,
    vm_role boolean NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.dcim_devicerole OWNER TO netbox;

--
-- Name: dcim_devicerole_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_devicerole_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_devicerole_id_seq OWNER TO netbox;

--
-- Name: dcim_devicerole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_devicerole_id_seq OWNED BY public.dcim_devicerole.id;


--
-- Name: dcim_devicetype; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_devicetype (
    id integer NOT NULL,
    model character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    u_height smallint NOT NULL,
    is_full_depth boolean NOT NULL,
    manufacturer_id integer NOT NULL,
    subdevice_role boolean,
    part_number character varying(50) NOT NULL,
    comments text NOT NULL,
    created date,
    last_updated timestamp with time zone,
    CONSTRAINT dcim_devicetype_u_height_check CHECK ((u_height >= 0))
);


ALTER TABLE public.dcim_devicetype OWNER TO netbox;

--
-- Name: dcim_devicetype_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_devicetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_devicetype_id_seq OWNER TO netbox;

--
-- Name: dcim_devicetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_devicetype_id_seq OWNED BY public.dcim_devicetype.id;


--
-- Name: dcim_frontport; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_frontport (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    rear_port_position smallint NOT NULL,
    description character varying(100) NOT NULL,
    device_id integer NOT NULL,
    rear_port_id integer NOT NULL,
    cable_id integer,
    CONSTRAINT dcim_frontport_rear_port_position_check CHECK ((rear_port_position >= 0)),
    CONSTRAINT dcim_frontport_type_check CHECK ((type >= 0))
);


ALTER TABLE public.dcim_frontport OWNER TO netbox;

--
-- Name: dcim_frontport_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_frontport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_frontport_id_seq OWNER TO netbox;

--
-- Name: dcim_frontport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_frontport_id_seq OWNED BY public.dcim_frontport.id;


--
-- Name: dcim_frontporttemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_frontporttemplate (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    rear_port_position smallint NOT NULL,
    device_type_id integer NOT NULL,
    rear_port_id integer NOT NULL,
    CONSTRAINT dcim_frontporttemplate_rear_port_position_check CHECK ((rear_port_position >= 0)),
    CONSTRAINT dcim_frontporttemplate_type_check CHECK ((type >= 0))
);


ALTER TABLE public.dcim_frontporttemplate OWNER TO netbox;

--
-- Name: dcim_frontporttemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_frontporttemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_frontporttemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_frontporttemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_frontporttemplate_id_seq OWNED BY public.dcim_frontporttemplate.id;


--
-- Name: dcim_interface; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_interface (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    mgmt_only boolean NOT NULL,
    description character varying(100) NOT NULL,
    device_id integer,
    mac_address macaddr,
    lag_id integer,
    enabled boolean NOT NULL,
    mtu integer,
    virtual_machine_id integer,
    mode smallint,
    untagged_vlan_id integer,
    _connected_circuittermination_id integer,
    _connected_interface_id integer,
    connection_status boolean,
    cable_id integer,
    CONSTRAINT dcim_interface_mode_check CHECK ((mode >= 0)),
    CONSTRAINT dcim_interface_mtu_check CHECK ((mtu >= 0)),
    CONSTRAINT dcim_interface_type_b8044832_check CHECK ((type >= 0))
);


ALTER TABLE public.dcim_interface OWNER TO netbox;

--
-- Name: dcim_interface_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_interface_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_interface_id_seq OWNER TO netbox;

--
-- Name: dcim_interface_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_interface_id_seq OWNED BY public.dcim_interface.id;


--
-- Name: dcim_interface_tagged_vlans; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_interface_tagged_vlans (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    vlan_id integer NOT NULL
);


ALTER TABLE public.dcim_interface_tagged_vlans OWNER TO netbox;

--
-- Name: dcim_interface_tagged_vlans_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_interface_tagged_vlans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_interface_tagged_vlans_id_seq OWNER TO netbox;

--
-- Name: dcim_interface_tagged_vlans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_interface_tagged_vlans_id_seq OWNED BY public.dcim_interface_tagged_vlans.id;


--
-- Name: dcim_interfacetemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_interfacetemplate (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    mgmt_only boolean NOT NULL,
    device_type_id integer NOT NULL,
    CONSTRAINT dcim_interfacetemplate_type_ebd08d49_check CHECK ((type >= 0))
);


ALTER TABLE public.dcim_interfacetemplate OWNER TO netbox;

--
-- Name: dcim_interfacetemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_interfacetemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_interfacetemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_interfacetemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_interfacetemplate_id_seq OWNED BY public.dcim_interfacetemplate.id;


--
-- Name: dcim_inventoryitem; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_inventoryitem (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    part_id character varying(50) NOT NULL,
    serial character varying(50) NOT NULL,
    discovered boolean NOT NULL,
    device_id integer NOT NULL,
    parent_id integer,
    manufacturer_id integer,
    asset_tag character varying(50),
    description character varying(100) NOT NULL
);


ALTER TABLE public.dcim_inventoryitem OWNER TO netbox;

--
-- Name: dcim_manufacturer; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_manufacturer (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.dcim_manufacturer OWNER TO netbox;

--
-- Name: dcim_manufacturer_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_manufacturer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_manufacturer_id_seq OWNER TO netbox;

--
-- Name: dcim_manufacturer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_manufacturer_id_seq OWNED BY public.dcim_manufacturer.id;


--
-- Name: dcim_module_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_module_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_module_id_seq OWNER TO netbox;

--
-- Name: dcim_module_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_module_id_seq OWNED BY public.dcim_inventoryitem.id;


--
-- Name: dcim_platform; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_platform (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    napalm_driver character varying(50) NOT NULL,
    manufacturer_id integer,
    created date,
    last_updated timestamp with time zone,
    napalm_args jsonb
);


ALTER TABLE public.dcim_platform OWNER TO netbox;

--
-- Name: dcim_platform_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_platform_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_platform_id_seq OWNER TO netbox;

--
-- Name: dcim_platform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_platform_id_seq OWNED BY public.dcim_platform.id;


--
-- Name: dcim_powerfeed; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_powerfeed (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(50) NOT NULL,
    status smallint NOT NULL,
    type smallint NOT NULL,
    supply smallint NOT NULL,
    phase smallint NOT NULL,
    voltage smallint NOT NULL,
    amperage smallint NOT NULL,
    max_utilization smallint NOT NULL,
    available_power smallint NOT NULL,
    comments text NOT NULL,
    cable_id integer,
    power_panel_id integer NOT NULL,
    rack_id integer,
    connected_endpoint_id integer,
    connection_status boolean,
    CONSTRAINT dcim_powerfeed_amperage_check CHECK ((amperage >= 0)),
    CONSTRAINT dcim_powerfeed_available_power_check CHECK ((available_power >= 0)),
    CONSTRAINT dcim_powerfeed_max_utilization_check CHECK ((max_utilization >= 0)),
    CONSTRAINT dcim_powerfeed_phase_check CHECK ((phase >= 0)),
    CONSTRAINT dcim_powerfeed_status_check CHECK ((status >= 0)),
    CONSTRAINT dcim_powerfeed_supply_check CHECK ((supply >= 0)),
    CONSTRAINT dcim_powerfeed_type_check CHECK ((type >= 0)),
    CONSTRAINT dcim_powerfeed_voltage_check CHECK ((voltage >= 0))
);


ALTER TABLE public.dcim_powerfeed OWNER TO netbox;

--
-- Name: dcim_powerfeed_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_powerfeed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_powerfeed_id_seq OWNER TO netbox;

--
-- Name: dcim_powerfeed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_powerfeed_id_seq OWNED BY public.dcim_powerfeed.id;


--
-- Name: dcim_poweroutlet; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_poweroutlet (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_id integer NOT NULL,
    cable_id integer,
    connection_status boolean,
    description character varying(100) NOT NULL,
    feed_leg smallint,
    power_port_id integer,
    CONSTRAINT dcim_poweroutlet_feed_leg_check CHECK ((feed_leg >= 0))
);


ALTER TABLE public.dcim_poweroutlet OWNER TO netbox;

--
-- Name: dcim_poweroutlet_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_poweroutlet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_poweroutlet_id_seq OWNER TO netbox;

--
-- Name: dcim_poweroutlet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_poweroutlet_id_seq OWNED BY public.dcim_poweroutlet.id;


--
-- Name: dcim_poweroutlettemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_poweroutlettemplate (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_type_id integer NOT NULL,
    feed_leg smallint,
    power_port_id integer,
    CONSTRAINT dcim_poweroutlettemplate_feed_leg_check CHECK ((feed_leg >= 0))
);


ALTER TABLE public.dcim_poweroutlettemplate OWNER TO netbox;

--
-- Name: dcim_poweroutlettemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_poweroutlettemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_poweroutlettemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_poweroutlettemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_poweroutlettemplate_id_seq OWNED BY public.dcim_poweroutlettemplate.id;


--
-- Name: dcim_powerpanel; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_powerpanel (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(50) NOT NULL,
    rack_group_id integer,
    site_id integer NOT NULL
);


ALTER TABLE public.dcim_powerpanel OWNER TO netbox;

--
-- Name: dcim_powerpanel_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_powerpanel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_powerpanel_id_seq OWNER TO netbox;

--
-- Name: dcim_powerpanel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_powerpanel_id_seq OWNED BY public.dcim_powerpanel.id;


--
-- Name: dcim_powerport; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_powerport (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    connection_status boolean,
    device_id integer NOT NULL,
    _connected_poweroutlet_id integer,
    cable_id integer,
    description character varying(100) NOT NULL,
    _connected_powerfeed_id integer,
    allocated_draw smallint,
    maximum_draw smallint,
    CONSTRAINT dcim_powerport_allocated_draw_check CHECK ((allocated_draw >= 0)),
    CONSTRAINT dcim_powerport_maximum_draw_check CHECK ((maximum_draw >= 0))
);


ALTER TABLE public.dcim_powerport OWNER TO netbox;

--
-- Name: dcim_powerport_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_powerport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_powerport_id_seq OWNER TO netbox;

--
-- Name: dcim_powerport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_powerport_id_seq OWNED BY public.dcim_powerport.id;


--
-- Name: dcim_powerporttemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_powerporttemplate (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    device_type_id integer NOT NULL,
    allocated_draw smallint,
    maximum_draw smallint,
    CONSTRAINT dcim_powerporttemplate_allocated_draw_check CHECK ((allocated_draw >= 0)),
    CONSTRAINT dcim_powerporttemplate_maximum_draw_check CHECK ((maximum_draw >= 0))
);


ALTER TABLE public.dcim_powerporttemplate OWNER TO netbox;

--
-- Name: dcim_powerporttemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_powerporttemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_powerporttemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_powerporttemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_powerporttemplate_id_seq OWNED BY public.dcim_powerporttemplate.id;


--
-- Name: dcim_rack; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_rack (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(50) NOT NULL,
    facility_id character varying(50),
    u_height smallint NOT NULL,
    comments text NOT NULL,
    group_id integer,
    site_id integer NOT NULL,
    tenant_id integer,
    type smallint,
    width smallint NOT NULL,
    role_id integer,
    desc_units boolean NOT NULL,
    serial character varying(50) NOT NULL,
    status smallint NOT NULL,
    asset_tag character varying(50),
    outer_depth smallint,
    outer_unit smallint,
    outer_width smallint,
    CONSTRAINT dcim_rack_outer_depth_check CHECK ((outer_depth >= 0)),
    CONSTRAINT dcim_rack_outer_unit_check CHECK ((outer_unit >= 0)),
    CONSTRAINT dcim_rack_outer_width_check CHECK ((outer_width >= 0)),
    CONSTRAINT dcim_rack_status_check CHECK ((status >= 0)),
    CONSTRAINT dcim_rack_type_check CHECK ((type >= 0)),
    CONSTRAINT dcim_rack_u_height_check CHECK ((u_height >= 0)),
    CONSTRAINT dcim_rack_width_check CHECK ((width >= 0))
);


ALTER TABLE public.dcim_rack OWNER TO netbox;

--
-- Name: dcim_rack_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_rack_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_rack_id_seq OWNER TO netbox;

--
-- Name: dcim_rack_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_rack_id_seq OWNED BY public.dcim_rack.id;


--
-- Name: dcim_rackgroup; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_rackgroup (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    site_id integer NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.dcim_rackgroup OWNER TO netbox;

--
-- Name: dcim_rackgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_rackgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_rackgroup_id_seq OWNER TO netbox;

--
-- Name: dcim_rackgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_rackgroup_id_seq OWNED BY public.dcim_rackgroup.id;


--
-- Name: dcim_rackreservation; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_rackreservation (
    id integer NOT NULL,
    units smallint[] NOT NULL,
    created date,
    description character varying(100) NOT NULL,
    rack_id integer NOT NULL,
    user_id integer NOT NULL,
    tenant_id integer,
    last_updated timestamp with time zone
);


ALTER TABLE public.dcim_rackreservation OWNER TO netbox;

--
-- Name: dcim_rackreservation_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_rackreservation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_rackreservation_id_seq OWNER TO netbox;

--
-- Name: dcim_rackreservation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_rackreservation_id_seq OWNED BY public.dcim_rackreservation.id;


--
-- Name: dcim_rackrole; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_rackrole (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    color character varying(6) NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.dcim_rackrole OWNER TO netbox;

--
-- Name: dcim_rackrole_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_rackrole_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_rackrole_id_seq OWNER TO netbox;

--
-- Name: dcim_rackrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_rackrole_id_seq OWNED BY public.dcim_rackrole.id;


--
-- Name: dcim_rearport; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_rearport (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    positions smallint NOT NULL,
    description character varying(100) NOT NULL,
    device_id integer NOT NULL,
    cable_id integer,
    CONSTRAINT dcim_rearport_positions_check CHECK ((positions >= 0)),
    CONSTRAINT dcim_rearport_type_check CHECK ((type >= 0))
);


ALTER TABLE public.dcim_rearport OWNER TO netbox;

--
-- Name: dcim_rearport_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_rearport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_rearport_id_seq OWNER TO netbox;

--
-- Name: dcim_rearport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_rearport_id_seq OWNED BY public.dcim_rearport.id;


--
-- Name: dcim_rearporttemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_rearporttemplate (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    positions smallint NOT NULL,
    device_type_id integer NOT NULL,
    CONSTRAINT dcim_rearporttemplate_positions_check CHECK ((positions >= 0)),
    CONSTRAINT dcim_rearporttemplate_type_check CHECK ((type >= 0))
);


ALTER TABLE public.dcim_rearporttemplate OWNER TO netbox;

--
-- Name: dcim_rearporttemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_rearporttemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_rearporttemplate_id_seq OWNER TO netbox;

--
-- Name: dcim_rearporttemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_rearporttemplate_id_seq OWNED BY public.dcim_rearporttemplate.id;


--
-- Name: dcim_region; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_region (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    parent_id integer,
    created date,
    last_updated timestamp with time zone,
    CONSTRAINT dcim_region_level_check CHECK ((level >= 0)),
    CONSTRAINT dcim_region_lft_check CHECK ((lft >= 0)),
    CONSTRAINT dcim_region_rght_check CHECK ((rght >= 0)),
    CONSTRAINT dcim_region_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.dcim_region OWNER TO netbox;

--
-- Name: dcim_region_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_region_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_region_id_seq OWNER TO netbox;

--
-- Name: dcim_region_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_region_id_seq OWNED BY public.dcim_region.id;


--
-- Name: dcim_site; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_site (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    facility character varying(50) NOT NULL,
    asn bigint,
    physical_address character varying(200) NOT NULL,
    shipping_address character varying(200) NOT NULL,
    comments text NOT NULL,
    tenant_id integer,
    contact_email character varying(254) NOT NULL,
    contact_name character varying(50) NOT NULL,
    contact_phone character varying(20) NOT NULL,
    region_id integer,
    description character varying(100) NOT NULL,
    status smallint NOT NULL,
    time_zone character varying(63) NOT NULL,
    latitude numeric(8,6),
    longitude numeric(9,6),
    CONSTRAINT dcim_site_status_check CHECK ((status >= 0))
);


ALTER TABLE public.dcim_site OWNER TO netbox;

--
-- Name: dcim_site_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_site_id_seq OWNER TO netbox;

--
-- Name: dcim_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_site_id_seq OWNED BY public.dcim_site.id;


--
-- Name: dcim_virtualchassis; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.dcim_virtualchassis (
    id integer NOT NULL,
    domain character varying(30) NOT NULL,
    master_id integer NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.dcim_virtualchassis OWNER TO netbox;

--
-- Name: dcim_virtualchassis_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.dcim_virtualchassis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcim_virtualchassis_id_seq OWNER TO netbox;

--
-- Name: dcim_virtualchassis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.dcim_virtualchassis_id_seq OWNED BY public.dcim_virtualchassis.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO netbox;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO netbox;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO netbox;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO netbox;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO netbox;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO netbox;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO netbox;

--
-- Name: extras_configcontext; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_configcontext (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    weight smallint NOT NULL,
    description character varying(100) NOT NULL,
    is_active boolean NOT NULL,
    data jsonb NOT NULL,
    CONSTRAINT extras_configcontext_weight_check CHECK ((weight >= 0))
);


ALTER TABLE public.extras_configcontext OWNER TO netbox;

--
-- Name: extras_configcontext_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_configcontext_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_configcontext_id_seq OWNER TO netbox;

--
-- Name: extras_configcontext_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_configcontext_id_seq OWNED BY public.extras_configcontext.id;


--
-- Name: extras_configcontext_platforms; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_configcontext_platforms (
    id integer NOT NULL,
    configcontext_id integer NOT NULL,
    platform_id integer NOT NULL
);


ALTER TABLE public.extras_configcontext_platforms OWNER TO netbox;

--
-- Name: extras_configcontext_platforms_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_configcontext_platforms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_configcontext_platforms_id_seq OWNER TO netbox;

--
-- Name: extras_configcontext_platforms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_configcontext_platforms_id_seq OWNED BY public.extras_configcontext_platforms.id;


--
-- Name: extras_configcontext_regions; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_configcontext_regions (
    id integer NOT NULL,
    configcontext_id integer NOT NULL,
    region_id integer NOT NULL
);


ALTER TABLE public.extras_configcontext_regions OWNER TO netbox;

--
-- Name: extras_configcontext_regions_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_configcontext_regions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_configcontext_regions_id_seq OWNER TO netbox;

--
-- Name: extras_configcontext_regions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_configcontext_regions_id_seq OWNED BY public.extras_configcontext_regions.id;


--
-- Name: extras_configcontext_roles; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_configcontext_roles (
    id integer NOT NULL,
    configcontext_id integer NOT NULL,
    devicerole_id integer NOT NULL
);


ALTER TABLE public.extras_configcontext_roles OWNER TO netbox;

--
-- Name: extras_configcontext_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_configcontext_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_configcontext_roles_id_seq OWNER TO netbox;

--
-- Name: extras_configcontext_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_configcontext_roles_id_seq OWNED BY public.extras_configcontext_roles.id;


--
-- Name: extras_configcontext_sites; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_configcontext_sites (
    id integer NOT NULL,
    configcontext_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.extras_configcontext_sites OWNER TO netbox;

--
-- Name: extras_configcontext_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_configcontext_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_configcontext_sites_id_seq OWNER TO netbox;

--
-- Name: extras_configcontext_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_configcontext_sites_id_seq OWNED BY public.extras_configcontext_sites.id;


--
-- Name: extras_configcontext_tenant_groups; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_configcontext_tenant_groups (
    id integer NOT NULL,
    configcontext_id integer NOT NULL,
    tenantgroup_id integer NOT NULL
);


ALTER TABLE public.extras_configcontext_tenant_groups OWNER TO netbox;

--
-- Name: extras_configcontext_tenant_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_configcontext_tenant_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_configcontext_tenant_groups_id_seq OWNER TO netbox;

--
-- Name: extras_configcontext_tenant_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_configcontext_tenant_groups_id_seq OWNED BY public.extras_configcontext_tenant_groups.id;


--
-- Name: extras_configcontext_tenants; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_configcontext_tenants (
    id integer NOT NULL,
    configcontext_id integer NOT NULL,
    tenant_id integer NOT NULL
);


ALTER TABLE public.extras_configcontext_tenants OWNER TO netbox;

--
-- Name: extras_configcontext_tenants_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_configcontext_tenants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_configcontext_tenants_id_seq OWNER TO netbox;

--
-- Name: extras_configcontext_tenants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_configcontext_tenants_id_seq OWNED BY public.extras_configcontext_tenants.id;


--
-- Name: extras_customfield; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_customfield (
    id integer NOT NULL,
    type smallint NOT NULL,
    name character varying(50) NOT NULL,
    label character varying(50) NOT NULL,
    description character varying(100) NOT NULL,
    required boolean NOT NULL,
    "default" character varying(100) NOT NULL,
    weight smallint NOT NULL,
    filter_logic smallint NOT NULL,
    CONSTRAINT extras_customfield_filter_logic_check CHECK ((filter_logic >= 0)),
    CONSTRAINT extras_customfield_type_check CHECK ((type >= 0)),
    CONSTRAINT extras_customfield_weight_check CHECK ((weight >= 0))
);


ALTER TABLE public.extras_customfield OWNER TO netbox;

--
-- Name: extras_customfield_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_customfield_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_customfield_id_seq OWNER TO netbox;

--
-- Name: extras_customfield_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_customfield_id_seq OWNED BY public.extras_customfield.id;


--
-- Name: extras_customfield_obj_type; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_customfield_obj_type (
    id integer NOT NULL,
    customfield_id integer NOT NULL,
    contenttype_id integer NOT NULL
);


ALTER TABLE public.extras_customfield_obj_type OWNER TO netbox;

--
-- Name: extras_customfield_obj_type_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_customfield_obj_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_customfield_obj_type_id_seq OWNER TO netbox;

--
-- Name: extras_customfield_obj_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_customfield_obj_type_id_seq OWNED BY public.extras_customfield_obj_type.id;


--
-- Name: extras_customfieldchoice; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_customfieldchoice (
    id integer NOT NULL,
    value character varying(100) NOT NULL,
    weight smallint NOT NULL,
    field_id integer NOT NULL,
    CONSTRAINT extras_customfieldchoice_weight_check CHECK ((weight >= 0))
);


ALTER TABLE public.extras_customfieldchoice OWNER TO netbox;

--
-- Name: extras_customfieldchoice_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_customfieldchoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_customfieldchoice_id_seq OWNER TO netbox;

--
-- Name: extras_customfieldchoice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_customfieldchoice_id_seq OWNED BY public.extras_customfieldchoice.id;


--
-- Name: extras_customfieldvalue; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_customfieldvalue (
    id integer NOT NULL,
    obj_id integer NOT NULL,
    serialized_value character varying(255) NOT NULL,
    field_id integer NOT NULL,
    obj_type_id integer NOT NULL,
    CONSTRAINT extras_customfieldvalue_obj_id_check CHECK ((obj_id >= 0))
);


ALTER TABLE public.extras_customfieldvalue OWNER TO netbox;

--
-- Name: extras_customfieldvalue_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_customfieldvalue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_customfieldvalue_id_seq OWNER TO netbox;

--
-- Name: extras_customfieldvalue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_customfieldvalue_id_seq OWNED BY public.extras_customfieldvalue.id;


--
-- Name: extras_customlink; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_customlink (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    text character varying(500) NOT NULL,
    url character varying(500) NOT NULL,
    weight smallint NOT NULL,
    group_name character varying(50) NOT NULL,
    button_class character varying(30) NOT NULL,
    new_window boolean NOT NULL,
    content_type_id integer NOT NULL,
    CONSTRAINT extras_customlink_weight_check CHECK ((weight >= 0))
);


ALTER TABLE public.extras_customlink OWNER TO netbox;

--
-- Name: extras_customlink_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_customlink_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_customlink_id_seq OWNER TO netbox;

--
-- Name: extras_customlink_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_customlink_id_seq OWNED BY public.extras_customlink.id;


--
-- Name: extras_exporttemplate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_exporttemplate (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    template_code text NOT NULL,
    mime_type character varying(50) NOT NULL,
    file_extension character varying(15) NOT NULL,
    content_type_id integer NOT NULL,
    description character varying(200) NOT NULL,
    template_language smallint NOT NULL,
    CONSTRAINT extras_exporttemplate_template_language_check CHECK ((template_language >= 0))
);


ALTER TABLE public.extras_exporttemplate OWNER TO netbox;

--
-- Name: extras_exporttemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_exporttemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_exporttemplate_id_seq OWNER TO netbox;

--
-- Name: extras_exporttemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_exporttemplate_id_seq OWNED BY public.extras_exporttemplate.id;


--
-- Name: extras_graph; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_graph (
    id integer NOT NULL,
    type smallint NOT NULL,
    weight smallint NOT NULL,
    name character varying(100) NOT NULL,
    source character varying(500) NOT NULL,
    link character varying(200) NOT NULL,
    CONSTRAINT extras_graph_type_check CHECK ((type >= 0)),
    CONSTRAINT extras_graph_weight_check CHECK ((weight >= 0))
);


ALTER TABLE public.extras_graph OWNER TO netbox;

--
-- Name: extras_graph_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_graph_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_graph_id_seq OWNER TO netbox;

--
-- Name: extras_graph_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_graph_id_seq OWNED BY public.extras_graph.id;


--
-- Name: extras_imageattachment; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_imageattachment (
    id integer NOT NULL,
    object_id integer NOT NULL,
    image character varying(100) NOT NULL,
    image_height smallint NOT NULL,
    image_width smallint NOT NULL,
    name character varying(50) NOT NULL,
    created timestamp with time zone NOT NULL,
    content_type_id integer NOT NULL,
    CONSTRAINT extras_imageattachment_image_height_check CHECK ((image_height >= 0)),
    CONSTRAINT extras_imageattachment_image_width_check CHECK ((image_width >= 0)),
    CONSTRAINT extras_imageattachment_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE public.extras_imageattachment OWNER TO netbox;

--
-- Name: extras_imageattachment_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_imageattachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_imageattachment_id_seq OWNER TO netbox;

--
-- Name: extras_imageattachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_imageattachment_id_seq OWNED BY public.extras_imageattachment.id;


--
-- Name: extras_objectchange; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_objectchange (
    id integer NOT NULL,
    "time" timestamp with time zone NOT NULL,
    user_name character varying(150) NOT NULL,
    request_id uuid NOT NULL,
    action smallint NOT NULL,
    changed_object_id integer NOT NULL,
    related_object_id integer,
    object_repr character varying(200) NOT NULL,
    object_data jsonb NOT NULL,
    changed_object_type_id integer NOT NULL,
    related_object_type_id integer,
    user_id integer,
    CONSTRAINT extras_objectchange_action_check CHECK ((action >= 0)),
    CONSTRAINT extras_objectchange_changed_object_id_check CHECK ((changed_object_id >= 0)),
    CONSTRAINT extras_objectchange_related_object_id_check CHECK ((related_object_id >= 0))
);


ALTER TABLE public.extras_objectchange OWNER TO netbox;

--
-- Name: extras_objectchange_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_objectchange_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_objectchange_id_seq OWNER TO netbox;

--
-- Name: extras_objectchange_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_objectchange_id_seq OWNED BY public.extras_objectchange.id;


--
-- Name: extras_reportresult; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_reportresult (
    id integer NOT NULL,
    report character varying(255) NOT NULL,
    created timestamp with time zone NOT NULL,
    failed boolean NOT NULL,
    data jsonb NOT NULL,
    user_id integer
);


ALTER TABLE public.extras_reportresult OWNER TO netbox;

--
-- Name: extras_reportresult_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_reportresult_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_reportresult_id_seq OWNER TO netbox;

--
-- Name: extras_reportresult_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_reportresult_id_seq OWNED BY public.extras_reportresult.id;


--
-- Name: extras_tag; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_tag (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    slug character varying(100) NOT NULL,
    color character varying(6) NOT NULL,
    comments text NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.extras_tag OWNER TO netbox;

--
-- Name: extras_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_tag_id_seq OWNER TO netbox;

--
-- Name: extras_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_tag_id_seq OWNED BY public.extras_tag.id;


--
-- Name: extras_taggeditem; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_taggeditem (
    id integer NOT NULL,
    object_id integer NOT NULL,
    content_type_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.extras_taggeditem OWNER TO netbox;

--
-- Name: extras_taggeditem_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_taggeditem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_taggeditem_id_seq OWNER TO netbox;

--
-- Name: extras_taggeditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_taggeditem_id_seq OWNED BY public.extras_taggeditem.id;


--
-- Name: extras_topologymap; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_topologymap (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    device_patterns text NOT NULL,
    description character varying(100) NOT NULL,
    site_id integer,
    type smallint NOT NULL,
    CONSTRAINT extras_topologymap_type_check CHECK ((type >= 0))
);


ALTER TABLE public.extras_topologymap OWNER TO netbox;

--
-- Name: extras_topologymap_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_topologymap_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_topologymap_id_seq OWNER TO netbox;

--
-- Name: extras_topologymap_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_topologymap_id_seq OWNED BY public.extras_topologymap.id;


--
-- Name: extras_webhook; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_webhook (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    type_create boolean NOT NULL,
    type_update boolean NOT NULL,
    type_delete boolean NOT NULL,
    payload_url character varying(500) NOT NULL,
    http_content_type smallint NOT NULL,
    secret character varying(255) NOT NULL,
    enabled boolean NOT NULL,
    ssl_verification boolean NOT NULL,
    CONSTRAINT extras_webhook_http_content_type_check CHECK ((http_content_type >= 0))
);


ALTER TABLE public.extras_webhook OWNER TO netbox;

--
-- Name: extras_webhook_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_webhook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_webhook_id_seq OWNER TO netbox;

--
-- Name: extras_webhook_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_webhook_id_seq OWNED BY public.extras_webhook.id;


--
-- Name: extras_webhook_obj_type; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.extras_webhook_obj_type (
    id integer NOT NULL,
    webhook_id integer NOT NULL,
    contenttype_id integer NOT NULL
);


ALTER TABLE public.extras_webhook_obj_type OWNER TO netbox;

--
-- Name: extras_webhook_obj_type_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.extras_webhook_obj_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extras_webhook_obj_type_id_seq OWNER TO netbox;

--
-- Name: extras_webhook_obj_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.extras_webhook_obj_type_id_seq OWNED BY public.extras_webhook_obj_type.id;


--
-- Name: ipam_aggregate; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_aggregate (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    family smallint NOT NULL,
    prefix cidr NOT NULL,
    date_added date,
    description character varying(100) NOT NULL,
    rir_id integer NOT NULL,
    CONSTRAINT ipam_aggregate_family_check CHECK ((family >= 0))
);


ALTER TABLE public.ipam_aggregate OWNER TO netbox;

--
-- Name: ipam_aggregate_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_aggregate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_aggregate_id_seq OWNER TO netbox;

--
-- Name: ipam_aggregate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_aggregate_id_seq OWNED BY public.ipam_aggregate.id;


--
-- Name: ipam_ipaddress; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_ipaddress (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    family smallint NOT NULL,
    address inet NOT NULL,
    description character varying(100) NOT NULL,
    interface_id integer,
    nat_inside_id integer,
    vrf_id integer,
    tenant_id integer,
    status smallint NOT NULL,
    role smallint,
    dns_name character varying(255) NOT NULL,
    CONSTRAINT ipam_ipaddress_family_check CHECK ((family >= 0)),
    CONSTRAINT ipam_ipaddress_role_check CHECK ((role >= 0)),
    CONSTRAINT ipam_ipaddress_status_check CHECK ((status >= 0))
);


ALTER TABLE public.ipam_ipaddress OWNER TO netbox;

--
-- Name: ipam_ipaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_ipaddress_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_ipaddress_id_seq OWNER TO netbox;

--
-- Name: ipam_ipaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_ipaddress_id_seq OWNED BY public.ipam_ipaddress.id;


--
-- Name: ipam_prefix; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_prefix (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    family smallint NOT NULL,
    prefix cidr NOT NULL,
    status smallint NOT NULL,
    description character varying(100) NOT NULL,
    role_id integer,
    site_id integer,
    vlan_id integer,
    vrf_id integer,
    tenant_id integer,
    is_pool boolean NOT NULL,
    CONSTRAINT ipam_prefix_family_check CHECK ((family >= 0)),
    CONSTRAINT ipam_prefix_status_check CHECK ((status >= 0))
);


ALTER TABLE public.ipam_prefix OWNER TO netbox;

--
-- Name: ipam_prefix_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_prefix_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_prefix_id_seq OWNER TO netbox;

--
-- Name: ipam_prefix_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_prefix_id_seq OWNED BY public.ipam_prefix.id;


--
-- Name: ipam_rir; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_rir (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    is_private boolean NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.ipam_rir OWNER TO netbox;

--
-- Name: ipam_rir_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_rir_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_rir_id_seq OWNER TO netbox;

--
-- Name: ipam_rir_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_rir_id_seq OWNED BY public.ipam_rir.id;


--
-- Name: ipam_role; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_role (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    weight smallint NOT NULL,
    created date,
    last_updated timestamp with time zone,
    CONSTRAINT ipam_role_weight_check CHECK ((weight >= 0))
);


ALTER TABLE public.ipam_role OWNER TO netbox;

--
-- Name: ipam_role_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_role_id_seq OWNER TO netbox;

--
-- Name: ipam_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_role_id_seq OWNED BY public.ipam_role.id;


--
-- Name: ipam_service; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_service (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(30) NOT NULL,
    protocol smallint NOT NULL,
    port integer NOT NULL,
    description character varying(100) NOT NULL,
    device_id integer,
    virtual_machine_id integer,
    CONSTRAINT ipam_service_port_check CHECK ((port >= 0)),
    CONSTRAINT ipam_service_protocol_check CHECK ((protocol >= 0))
);


ALTER TABLE public.ipam_service OWNER TO netbox;

--
-- Name: ipam_service_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_service_id_seq OWNER TO netbox;

--
-- Name: ipam_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_service_id_seq OWNED BY public.ipam_service.id;


--
-- Name: ipam_service_ipaddresses; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_service_ipaddresses (
    id integer NOT NULL,
    service_id integer NOT NULL,
    ipaddress_id integer NOT NULL
);


ALTER TABLE public.ipam_service_ipaddresses OWNER TO netbox;

--
-- Name: ipam_service_ipaddresses_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_service_ipaddresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_service_ipaddresses_id_seq OWNER TO netbox;

--
-- Name: ipam_service_ipaddresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_service_ipaddresses_id_seq OWNED BY public.ipam_service_ipaddresses.id;


--
-- Name: ipam_vlan; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_vlan (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    vid smallint NOT NULL,
    name character varying(64) NOT NULL,
    status smallint NOT NULL,
    role_id integer,
    site_id integer,
    group_id integer,
    description character varying(100) NOT NULL,
    tenant_id integer,
    CONSTRAINT ipam_vlan_status_check CHECK ((status >= 0)),
    CONSTRAINT ipam_vlan_vid_check CHECK ((vid >= 0))
);


ALTER TABLE public.ipam_vlan OWNER TO netbox;

--
-- Name: ipam_vlan_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_vlan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_vlan_id_seq OWNER TO netbox;

--
-- Name: ipam_vlan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_vlan_id_seq OWNED BY public.ipam_vlan.id;


--
-- Name: ipam_vlangroup; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_vlangroup (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    site_id integer,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.ipam_vlangroup OWNER TO netbox;

--
-- Name: ipam_vlangroup_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_vlangroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_vlangroup_id_seq OWNER TO netbox;

--
-- Name: ipam_vlangroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_vlangroup_id_seq OWNED BY public.ipam_vlangroup.id;


--
-- Name: ipam_vrf; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.ipam_vrf (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(50) NOT NULL,
    rd character varying(21),
    description character varying(100) NOT NULL,
    enforce_unique boolean NOT NULL,
    tenant_id integer
);


ALTER TABLE public.ipam_vrf OWNER TO netbox;

--
-- Name: ipam_vrf_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.ipam_vrf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ipam_vrf_id_seq OWNER TO netbox;

--
-- Name: ipam_vrf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.ipam_vrf_id_seq OWNED BY public.ipam_vrf.id;


--
-- Name: secrets_secret; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.secrets_secret (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(100) NOT NULL,
    ciphertext bytea NOT NULL,
    hash character varying(128) NOT NULL,
    device_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.secrets_secret OWNER TO netbox;

--
-- Name: secrets_secret_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.secrets_secret_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.secrets_secret_id_seq OWNER TO netbox;

--
-- Name: secrets_secret_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.secrets_secret_id_seq OWNED BY public.secrets_secret.id;


--
-- Name: secrets_secretrole; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.secrets_secretrole (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.secrets_secretrole OWNER TO netbox;

--
-- Name: secrets_secretrole_groups; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.secrets_secretrole_groups (
    id integer NOT NULL,
    secretrole_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.secrets_secretrole_groups OWNER TO netbox;

--
-- Name: secrets_secretrole_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.secrets_secretrole_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.secrets_secretrole_groups_id_seq OWNER TO netbox;

--
-- Name: secrets_secretrole_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.secrets_secretrole_groups_id_seq OWNED BY public.secrets_secretrole_groups.id;


--
-- Name: secrets_secretrole_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.secrets_secretrole_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.secrets_secretrole_id_seq OWNER TO netbox;

--
-- Name: secrets_secretrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.secrets_secretrole_id_seq OWNED BY public.secrets_secretrole.id;


--
-- Name: secrets_secretrole_users; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.secrets_secretrole_users (
    id integer NOT NULL,
    secretrole_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.secrets_secretrole_users OWNER TO netbox;

--
-- Name: secrets_secretrole_users_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.secrets_secretrole_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.secrets_secretrole_users_id_seq OWNER TO netbox;

--
-- Name: secrets_secretrole_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.secrets_secretrole_users_id_seq OWNED BY public.secrets_secretrole_users.id;


--
-- Name: secrets_sessionkey; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.secrets_sessionkey (
    id integer NOT NULL,
    cipher bytea NOT NULL,
    hash character varying(128) NOT NULL,
    created timestamp with time zone NOT NULL,
    userkey_id integer NOT NULL
);


ALTER TABLE public.secrets_sessionkey OWNER TO netbox;

--
-- Name: secrets_sessionkey_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.secrets_sessionkey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.secrets_sessionkey_id_seq OWNER TO netbox;

--
-- Name: secrets_sessionkey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.secrets_sessionkey_id_seq OWNED BY public.secrets_sessionkey.id;


--
-- Name: secrets_userkey; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.secrets_userkey (
    id integer NOT NULL,
    created date NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    public_key text NOT NULL,
    master_key_cipher bytea,
    user_id integer NOT NULL
);


ALTER TABLE public.secrets_userkey OWNER TO netbox;

--
-- Name: secrets_userkey_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.secrets_userkey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.secrets_userkey_id_seq OWNER TO netbox;

--
-- Name: secrets_userkey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.secrets_userkey_id_seq OWNED BY public.secrets_userkey.id;


--
-- Name: taggit_tag; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.taggit_tag (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    slug character varying(100) NOT NULL
);


ALTER TABLE public.taggit_tag OWNER TO netbox;

--
-- Name: taggit_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.taggit_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taggit_tag_id_seq OWNER TO netbox;

--
-- Name: taggit_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.taggit_tag_id_seq OWNED BY public.taggit_tag.id;


--
-- Name: taggit_taggeditem; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.taggit_taggeditem (
    id integer NOT NULL,
    object_id integer NOT NULL,
    content_type_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.taggit_taggeditem OWNER TO netbox;

--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.taggit_taggeditem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taggit_taggeditem_id_seq OWNER TO netbox;

--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.taggit_taggeditem_id_seq OWNED BY public.taggit_taggeditem.id;


--
-- Name: tenancy_tenant; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.tenancy_tenant (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(30) NOT NULL,
    slug character varying(50) NOT NULL,
    description character varying(100) NOT NULL,
    comments text NOT NULL,
    group_id integer
);


ALTER TABLE public.tenancy_tenant OWNER TO netbox;

--
-- Name: tenancy_tenant_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.tenancy_tenant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tenancy_tenant_id_seq OWNER TO netbox;

--
-- Name: tenancy_tenant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.tenancy_tenant_id_seq OWNED BY public.tenancy_tenant.id;


--
-- Name: tenancy_tenantgroup; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.tenancy_tenantgroup (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.tenancy_tenantgroup OWNER TO netbox;

--
-- Name: tenancy_tenantgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.tenancy_tenantgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tenancy_tenantgroup_id_seq OWNER TO netbox;

--
-- Name: tenancy_tenantgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.tenancy_tenantgroup_id_seq OWNED BY public.tenancy_tenantgroup.id;


--
-- Name: users_token; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.users_token (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    expires timestamp with time zone,
    key character varying(40) NOT NULL,
    write_enabled boolean NOT NULL,
    description character varying(100) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.users_token OWNER TO netbox;

--
-- Name: users_token_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.users_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_token_id_seq OWNER TO netbox;

--
-- Name: users_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.users_token_id_seq OWNED BY public.users_token.id;


--
-- Name: virtualization_cluster; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.virtualization_cluster (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(100) NOT NULL,
    comments text NOT NULL,
    group_id integer,
    type_id integer NOT NULL,
    site_id integer
);


ALTER TABLE public.virtualization_cluster OWNER TO netbox;

--
-- Name: virtualization_cluster_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.virtualization_cluster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.virtualization_cluster_id_seq OWNER TO netbox;

--
-- Name: virtualization_cluster_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.virtualization_cluster_id_seq OWNED BY public.virtualization_cluster.id;


--
-- Name: virtualization_clustergroup; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.virtualization_clustergroup (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.virtualization_clustergroup OWNER TO netbox;

--
-- Name: virtualization_clustergroup_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.virtualization_clustergroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.virtualization_clustergroup_id_seq OWNER TO netbox;

--
-- Name: virtualization_clustergroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.virtualization_clustergroup_id_seq OWNED BY public.virtualization_clustergroup.id;


--
-- Name: virtualization_clustertype; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.virtualization_clustertype (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(50) NOT NULL,
    created date,
    last_updated timestamp with time zone
);


ALTER TABLE public.virtualization_clustertype OWNER TO netbox;

--
-- Name: virtualization_clustertype_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.virtualization_clustertype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.virtualization_clustertype_id_seq OWNER TO netbox;

--
-- Name: virtualization_clustertype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.virtualization_clustertype_id_seq OWNED BY public.virtualization_clustertype.id;


--
-- Name: virtualization_virtualmachine; Type: TABLE; Schema: public; Owner: netbox
--

CREATE TABLE public.virtualization_virtualmachine (
    id integer NOT NULL,
    created date,
    last_updated timestamp with time zone,
    name character varying(64) NOT NULL,
    vcpus smallint,
    memory integer,
    disk integer,
    comments text NOT NULL,
    cluster_id integer NOT NULL,
    platform_id integer,
    primary_ip4_id integer,
    primary_ip6_id integer,
    tenant_id integer,
    status smallint NOT NULL,
    role_id integer,
    local_context_data jsonb,
    CONSTRAINT virtualization_virtualmachine_disk_check CHECK ((disk >= 0)),
    CONSTRAINT virtualization_virtualmachine_memory_check CHECK ((memory >= 0)),
    CONSTRAINT virtualization_virtualmachine_status_check CHECK ((status >= 0)),
    CONSTRAINT virtualization_virtualmachine_vcpus_check CHECK ((vcpus >= 0))
);


ALTER TABLE public.virtualization_virtualmachine OWNER TO netbox;

--
-- Name: virtualization_virtualmachine_id_seq; Type: SEQUENCE; Schema: public; Owner: netbox
--

CREATE SEQUENCE public.virtualization_virtualmachine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.virtualization_virtualmachine_id_seq OWNER TO netbox;

--
-- Name: virtualization_virtualmachine_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: netbox
--

ALTER SEQUENCE public.virtualization_virtualmachine_id_seq OWNED BY public.virtualization_virtualmachine.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: circuits_circuit id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuit ALTER COLUMN id SET DEFAULT nextval('public.circuits_circuit_id_seq'::regclass);


--
-- Name: circuits_circuittermination id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination ALTER COLUMN id SET DEFAULT nextval('public.circuits_circuittermination_id_seq'::regclass);


--
-- Name: circuits_circuittype id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittype ALTER COLUMN id SET DEFAULT nextval('public.circuits_circuittype_id_seq'::regclass);


--
-- Name: circuits_provider id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_provider ALTER COLUMN id SET DEFAULT nextval('public.circuits_provider_id_seq'::regclass);


--
-- Name: dcim_cable id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_cable ALTER COLUMN id SET DEFAULT nextval('public.dcim_cable_id_seq'::regclass);


--
-- Name: dcim_consoleport id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleport ALTER COLUMN id SET DEFAULT nextval('public.dcim_consoleport_id_seq'::regclass);


--
-- Name: dcim_consoleporttemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleporttemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_consoleporttemplate_id_seq'::regclass);


--
-- Name: dcim_consoleserverport id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverport ALTER COLUMN id SET DEFAULT nextval('public.dcim_consoleserverport_id_seq'::regclass);


--
-- Name: dcim_consoleserverporttemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverporttemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_consoleserverporttemplate_id_seq'::regclass);


--
-- Name: dcim_device id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device ALTER COLUMN id SET DEFAULT nextval('public.dcim_device_id_seq'::regclass);


--
-- Name: dcim_devicebay id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebay ALTER COLUMN id SET DEFAULT nextval('public.dcim_devicebay_id_seq'::regclass);


--
-- Name: dcim_devicebaytemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebaytemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_devicebaytemplate_id_seq'::regclass);


--
-- Name: dcim_devicerole id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicerole ALTER COLUMN id SET DEFAULT nextval('public.dcim_devicerole_id_seq'::regclass);


--
-- Name: dcim_devicetype id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicetype ALTER COLUMN id SET DEFAULT nextval('public.dcim_devicetype_id_seq'::regclass);


--
-- Name: dcim_frontport id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontport ALTER COLUMN id SET DEFAULT nextval('public.dcim_frontport_id_seq'::regclass);


--
-- Name: dcim_frontporttemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontporttemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_frontporttemplate_id_seq'::regclass);


--
-- Name: dcim_interface id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface ALTER COLUMN id SET DEFAULT nextval('public.dcim_interface_id_seq'::regclass);


--
-- Name: dcim_interface_tagged_vlans id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface_tagged_vlans ALTER COLUMN id SET DEFAULT nextval('public.dcim_interface_tagged_vlans_id_seq'::regclass);


--
-- Name: dcim_interfacetemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interfacetemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_interfacetemplate_id_seq'::regclass);


--
-- Name: dcim_inventoryitem id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_inventoryitem ALTER COLUMN id SET DEFAULT nextval('public.dcim_module_id_seq'::regclass);


--
-- Name: dcim_manufacturer id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_manufacturer ALTER COLUMN id SET DEFAULT nextval('public.dcim_manufacturer_id_seq'::regclass);


--
-- Name: dcim_platform id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_platform ALTER COLUMN id SET DEFAULT nextval('public.dcim_platform_id_seq'::regclass);


--
-- Name: dcim_powerfeed id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed ALTER COLUMN id SET DEFAULT nextval('public.dcim_powerfeed_id_seq'::regclass);


--
-- Name: dcim_poweroutlet id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlet ALTER COLUMN id SET DEFAULT nextval('public.dcim_poweroutlet_id_seq'::regclass);


--
-- Name: dcim_poweroutlettemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlettemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_poweroutlettemplate_id_seq'::regclass);


--
-- Name: dcim_powerpanel id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerpanel ALTER COLUMN id SET DEFAULT nextval('public.dcim_powerpanel_id_seq'::regclass);


--
-- Name: dcim_powerport id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport ALTER COLUMN id SET DEFAULT nextval('public.dcim_powerport_id_seq'::regclass);


--
-- Name: dcim_powerporttemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerporttemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_powerporttemplate_id_seq'::regclass);


--
-- Name: dcim_rack id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack ALTER COLUMN id SET DEFAULT nextval('public.dcim_rack_id_seq'::regclass);


--
-- Name: dcim_rackgroup id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackgroup ALTER COLUMN id SET DEFAULT nextval('public.dcim_rackgroup_id_seq'::regclass);


--
-- Name: dcim_rackreservation id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackreservation ALTER COLUMN id SET DEFAULT nextval('public.dcim_rackreservation_id_seq'::regclass);


--
-- Name: dcim_rackrole id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackrole ALTER COLUMN id SET DEFAULT nextval('public.dcim_rackrole_id_seq'::regclass);


--
-- Name: dcim_rearport id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearport ALTER COLUMN id SET DEFAULT nextval('public.dcim_rearport_id_seq'::regclass);


--
-- Name: dcim_rearporttemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearporttemplate ALTER COLUMN id SET DEFAULT nextval('public.dcim_rearporttemplate_id_seq'::regclass);


--
-- Name: dcim_region id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_region ALTER COLUMN id SET DEFAULT nextval('public.dcim_region_id_seq'::regclass);


--
-- Name: dcim_site id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_site ALTER COLUMN id SET DEFAULT nextval('public.dcim_site_id_seq'::regclass);


--
-- Name: dcim_virtualchassis id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_virtualchassis ALTER COLUMN id SET DEFAULT nextval('public.dcim_virtualchassis_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: extras_configcontext id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext ALTER COLUMN id SET DEFAULT nextval('public.extras_configcontext_id_seq'::regclass);


--
-- Name: extras_configcontext_platforms id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_platforms ALTER COLUMN id SET DEFAULT nextval('public.extras_configcontext_platforms_id_seq'::regclass);


--
-- Name: extras_configcontext_regions id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_regions ALTER COLUMN id SET DEFAULT nextval('public.extras_configcontext_regions_id_seq'::regclass);


--
-- Name: extras_configcontext_roles id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_roles ALTER COLUMN id SET DEFAULT nextval('public.extras_configcontext_roles_id_seq'::regclass);


--
-- Name: extras_configcontext_sites id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_sites ALTER COLUMN id SET DEFAULT nextval('public.extras_configcontext_sites_id_seq'::regclass);


--
-- Name: extras_configcontext_tenant_groups id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenant_groups ALTER COLUMN id SET DEFAULT nextval('public.extras_configcontext_tenant_groups_id_seq'::regclass);


--
-- Name: extras_configcontext_tenants id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenants ALTER COLUMN id SET DEFAULT nextval('public.extras_configcontext_tenants_id_seq'::regclass);


--
-- Name: extras_customfield id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield ALTER COLUMN id SET DEFAULT nextval('public.extras_customfield_id_seq'::regclass);


--
-- Name: extras_customfield_obj_type id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield_obj_type ALTER COLUMN id SET DEFAULT nextval('public.extras_customfield_obj_type_id_seq'::regclass);


--
-- Name: extras_customfieldchoice id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldchoice ALTER COLUMN id SET DEFAULT nextval('public.extras_customfieldchoice_id_seq'::regclass);


--
-- Name: extras_customfieldvalue id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldvalue ALTER COLUMN id SET DEFAULT nextval('public.extras_customfieldvalue_id_seq'::regclass);


--
-- Name: extras_customlink id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customlink ALTER COLUMN id SET DEFAULT nextval('public.extras_customlink_id_seq'::regclass);


--
-- Name: extras_exporttemplate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_exporttemplate ALTER COLUMN id SET DEFAULT nextval('public.extras_exporttemplate_id_seq'::regclass);


--
-- Name: extras_graph id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_graph ALTER COLUMN id SET DEFAULT nextval('public.extras_graph_id_seq'::regclass);


--
-- Name: extras_imageattachment id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_imageattachment ALTER COLUMN id SET DEFAULT nextval('public.extras_imageattachment_id_seq'::regclass);


--
-- Name: extras_objectchange id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_objectchange ALTER COLUMN id SET DEFAULT nextval('public.extras_objectchange_id_seq'::regclass);


--
-- Name: extras_reportresult id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_reportresult ALTER COLUMN id SET DEFAULT nextval('public.extras_reportresult_id_seq'::regclass);


--
-- Name: extras_tag id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_tag ALTER COLUMN id SET DEFAULT nextval('public.extras_tag_id_seq'::regclass);


--
-- Name: extras_taggeditem id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_taggeditem ALTER COLUMN id SET DEFAULT nextval('public.extras_taggeditem_id_seq'::regclass);


--
-- Name: extras_topologymap id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_topologymap ALTER COLUMN id SET DEFAULT nextval('public.extras_topologymap_id_seq'::regclass);


--
-- Name: extras_webhook id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook ALTER COLUMN id SET DEFAULT nextval('public.extras_webhook_id_seq'::regclass);


--
-- Name: extras_webhook_obj_type id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook_obj_type ALTER COLUMN id SET DEFAULT nextval('public.extras_webhook_obj_type_id_seq'::regclass);


--
-- Name: ipam_aggregate id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_aggregate ALTER COLUMN id SET DEFAULT nextval('public.ipam_aggregate_id_seq'::regclass);


--
-- Name: ipam_ipaddress id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_ipaddress ALTER COLUMN id SET DEFAULT nextval('public.ipam_ipaddress_id_seq'::regclass);


--
-- Name: ipam_prefix id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_prefix ALTER COLUMN id SET DEFAULT nextval('public.ipam_prefix_id_seq'::regclass);


--
-- Name: ipam_rir id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_rir ALTER COLUMN id SET DEFAULT nextval('public.ipam_rir_id_seq'::regclass);


--
-- Name: ipam_role id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_role ALTER COLUMN id SET DEFAULT nextval('public.ipam_role_id_seq'::regclass);


--
-- Name: ipam_service id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service ALTER COLUMN id SET DEFAULT nextval('public.ipam_service_id_seq'::regclass);


--
-- Name: ipam_service_ipaddresses id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service_ipaddresses ALTER COLUMN id SET DEFAULT nextval('public.ipam_service_ipaddresses_id_seq'::regclass);


--
-- Name: ipam_vlan id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan ALTER COLUMN id SET DEFAULT nextval('public.ipam_vlan_id_seq'::regclass);


--
-- Name: ipam_vlangroup id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlangroup ALTER COLUMN id SET DEFAULT nextval('public.ipam_vlangroup_id_seq'::regclass);


--
-- Name: ipam_vrf id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vrf ALTER COLUMN id SET DEFAULT nextval('public.ipam_vrf_id_seq'::regclass);


--
-- Name: secrets_secret id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secret ALTER COLUMN id SET DEFAULT nextval('public.secrets_secret_id_seq'::regclass);


--
-- Name: secrets_secretrole id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole ALTER COLUMN id SET DEFAULT nextval('public.secrets_secretrole_id_seq'::regclass);


--
-- Name: secrets_secretrole_groups id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_groups ALTER COLUMN id SET DEFAULT nextval('public.secrets_secretrole_groups_id_seq'::regclass);


--
-- Name: secrets_secretrole_users id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_users ALTER COLUMN id SET DEFAULT nextval('public.secrets_secretrole_users_id_seq'::regclass);


--
-- Name: secrets_sessionkey id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_sessionkey ALTER COLUMN id SET DEFAULT nextval('public.secrets_sessionkey_id_seq'::regclass);


--
-- Name: secrets_userkey id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_userkey ALTER COLUMN id SET DEFAULT nextval('public.secrets_userkey_id_seq'::regclass);


--
-- Name: taggit_tag id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_tag ALTER COLUMN id SET DEFAULT nextval('public.taggit_tag_id_seq'::regclass);


--
-- Name: taggit_taggeditem id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_taggeditem ALTER COLUMN id SET DEFAULT nextval('public.taggit_taggeditem_id_seq'::regclass);


--
-- Name: tenancy_tenant id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenant ALTER COLUMN id SET DEFAULT nextval('public.tenancy_tenant_id_seq'::regclass);


--
-- Name: tenancy_tenantgroup id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenantgroup ALTER COLUMN id SET DEFAULT nextval('public.tenancy_tenantgroup_id_seq'::regclass);


--
-- Name: users_token id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.users_token ALTER COLUMN id SET DEFAULT nextval('public.users_token_id_seq'::regclass);


--
-- Name: virtualization_cluster id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_cluster ALTER COLUMN id SET DEFAULT nextval('public.virtualization_cluster_id_seq'::regclass);


--
-- Name: virtualization_clustergroup id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustergroup ALTER COLUMN id SET DEFAULT nextval('public.virtualization_clustergroup_id_seq'::regclass);


--
-- Name: virtualization_clustertype id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustertype ALTER COLUMN id SET DEFAULT nextval('public.virtualization_clustertype_id_seq'::regclass);


--
-- Name: virtualization_virtualmachine id; Type: DEFAULT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine ALTER COLUMN id SET DEFAULT nextval('public.virtualization_virtualmachine_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	8	add_logentry
2	Can change log entry	8	change_logentry
3	Can delete log entry	8	delete_logentry
4	Can view log entry	8	view_logentry
5	Can add permission	9	add_permission
6	Can change permission	9	change_permission
7	Can delete permission	9	delete_permission
8	Can view permission	9	view_permission
9	Can add group	10	add_group
10	Can change group	10	change_group
11	Can delete group	10	delete_group
12	Can view group	10	view_group
13	Can add user	11	add_user
14	Can change user	11	change_user
15	Can delete user	11	delete_user
16	Can view user	11	view_user
17	Can add content type	12	add_contenttype
18	Can change content type	12	change_contenttype
19	Can delete content type	12	delete_contenttype
20	Can view content type	12	view_contenttype
21	Can add session	13	add_session
22	Can change session	13	change_session
23	Can delete session	13	delete_session
24	Can view session	13	view_session
25	Can add Tag	14	add_tag
26	Can change Tag	14	change_tag
27	Can delete Tag	14	delete_tag
28	Can view Tag	14	view_tag
29	Can add Tagged Item	15	add_taggeditem
30	Can change Tagged Item	15	change_taggeditem
31	Can delete Tagged Item	15	delete_taggeditem
32	Can view Tagged Item	15	view_taggeditem
33	Can add provider	16	add_provider
34	Can change provider	16	change_provider
35	Can delete provider	16	delete_provider
36	Can view provider	16	view_provider
37	Can add circuit type	17	add_circuittype
38	Can change circuit type	17	change_circuittype
39	Can delete circuit type	17	delete_circuittype
40	Can view circuit type	17	view_circuittype
41	Can add circuit	18	add_circuit
42	Can change circuit	18	change_circuit
43	Can delete circuit	18	delete_circuit
44	Can view circuit	18	view_circuit
45	Can add circuit termination	7	add_circuittermination
46	Can change circuit termination	7	change_circuittermination
47	Can delete circuit termination	7	delete_circuittermination
48	Can view circuit termination	7	view_circuittermination
49	Can add console port	1	add_consoleport
50	Can change console port	1	change_consoleport
51	Can delete console port	1	delete_consoleport
52	Can view console port	1	view_consoleport
53	Can add console port template	19	add_consoleporttemplate
54	Can change console port template	19	change_consoleporttemplate
55	Can delete console port template	19	delete_consoleporttemplate
56	Can view console port template	19	view_consoleporttemplate
57	Can add console server port	2	add_consoleserverport
58	Can change console server port	2	change_consoleserverport
59	Can delete console server port	2	delete_consoleserverport
60	Can view console server port	2	view_consoleserverport
61	Can add console server port template	20	add_consoleserverporttemplate
62	Can change console server port template	20	change_consoleserverporttemplate
63	Can delete console server port template	20	delete_consoleserverporttemplate
64	Can view console server port template	20	view_consoleserverporttemplate
65	Can add device	21	add_device
66	Can change device	21	change_device
67	Can delete device	21	delete_device
68	Can view device	21	view_device
69	Read-only access to devices via NAPALM	21	napalm_read
70	Read/write access to devices via NAPALM	21	napalm_write
71	Can add device role	22	add_devicerole
72	Can change device role	22	change_devicerole
73	Can delete device role	22	delete_devicerole
74	Can view device role	22	view_devicerole
75	Can add device type	23	add_devicetype
76	Can change device type	23	change_devicetype
77	Can delete device type	23	delete_devicetype
78	Can view device type	23	view_devicetype
79	Can add interface	5	add_interface
80	Can change interface	5	change_interface
81	Can delete interface	5	delete_interface
82	Can view interface	5	view_interface
83	Can add interface template	24	add_interfacetemplate
84	Can change interface template	24	change_interfacetemplate
85	Can delete interface template	24	delete_interfacetemplate
86	Can view interface template	24	view_interfacetemplate
87	Can add manufacturer	25	add_manufacturer
88	Can change manufacturer	25	change_manufacturer
89	Can delete manufacturer	25	delete_manufacturer
90	Can view manufacturer	25	view_manufacturer
91	Can add platform	26	add_platform
92	Can change platform	26	change_platform
93	Can delete platform	26	delete_platform
94	Can view platform	26	view_platform
95	Can add power outlet	4	add_poweroutlet
96	Can change power outlet	4	change_poweroutlet
97	Can delete power outlet	4	delete_poweroutlet
98	Can view power outlet	4	view_poweroutlet
99	Can add power outlet template	27	add_poweroutlettemplate
100	Can change power outlet template	27	change_poweroutlettemplate
101	Can delete power outlet template	27	delete_poweroutlettemplate
102	Can view power outlet template	27	view_poweroutlettemplate
103	Can add power port	3	add_powerport
104	Can change power port	3	change_powerport
105	Can delete power port	3	delete_powerport
106	Can view power port	3	view_powerport
107	Can add power port template	28	add_powerporttemplate
108	Can change power port template	28	change_powerporttemplate
109	Can delete power port template	28	delete_powerporttemplate
110	Can view power port template	28	view_powerporttemplate
111	Can add rack	29	add_rack
112	Can change rack	29	change_rack
113	Can delete rack	29	delete_rack
114	Can view rack	29	view_rack
115	Can add rack group	30	add_rackgroup
116	Can change rack group	30	change_rackgroup
117	Can delete rack group	30	delete_rackgroup
118	Can view rack group	30	view_rackgroup
119	Can add site	31	add_site
120	Can change site	31	change_site
121	Can delete site	31	delete_site
122	Can view site	31	view_site
123	Can add device bay	32	add_devicebay
124	Can change device bay	32	change_devicebay
125	Can delete device bay	32	delete_devicebay
126	Can view device bay	32	view_devicebay
127	Can add device bay template	33	add_devicebaytemplate
128	Can change device bay template	33	change_devicebaytemplate
129	Can delete device bay template	33	delete_devicebaytemplate
130	Can view device bay template	33	view_devicebaytemplate
131	Can add rack role	34	add_rackrole
132	Can change rack role	34	change_rackrole
133	Can delete rack role	34	delete_rackrole
134	Can view rack role	34	view_rackrole
135	Can add rack reservation	35	add_rackreservation
136	Can change rack reservation	35	change_rackreservation
137	Can delete rack reservation	35	delete_rackreservation
138	Can view rack reservation	35	view_rackreservation
139	Can add region	36	add_region
140	Can change region	36	change_region
141	Can delete region	36	delete_region
142	Can view region	36	view_region
143	Can add inventory item	37	add_inventoryitem
144	Can change inventory item	37	change_inventoryitem
145	Can delete inventory item	37	delete_inventoryitem
146	Can view inventory item	37	view_inventoryitem
147	Can add virtual chassis	38	add_virtualchassis
148	Can change virtual chassis	38	change_virtualchassis
149	Can delete virtual chassis	38	delete_virtualchassis
150	Can view virtual chassis	38	view_virtualchassis
151	Can add front port	39	add_frontport
152	Can change front port	39	change_frontport
153	Can delete front port	39	delete_frontport
154	Can view front port	39	view_frontport
155	Can add front port template	40	add_frontporttemplate
156	Can change front port template	40	change_frontporttemplate
157	Can delete front port template	40	delete_frontporttemplate
158	Can view front port template	40	view_frontporttemplate
159	Can add rear port	41	add_rearport
160	Can change rear port	41	change_rearport
161	Can delete rear port	41	delete_rearport
162	Can view rear port	41	view_rearport
163	Can add rear port template	42	add_rearporttemplate
164	Can change rear port template	42	change_rearporttemplate
165	Can delete rear port template	42	delete_rearporttemplate
166	Can view rear port template	42	view_rearporttemplate
167	Can add cable	43	add_cable
168	Can change cable	43	change_cable
169	Can delete cable	43	delete_cable
170	Can view cable	43	view_cable
171	Can add power feed	44	add_powerfeed
172	Can change power feed	44	change_powerfeed
173	Can delete power feed	44	delete_powerfeed
174	Can view power feed	44	view_powerfeed
175	Can add power panel	45	add_powerpanel
176	Can change power panel	45	change_powerpanel
177	Can delete power panel	45	delete_powerpanel
178	Can view power panel	45	view_powerpanel
179	Can add aggregate	46	add_aggregate
180	Can change aggregate	46	change_aggregate
181	Can delete aggregate	46	delete_aggregate
182	Can view aggregate	46	view_aggregate
183	Can add IP address	47	add_ipaddress
184	Can change IP address	47	change_ipaddress
185	Can delete IP address	47	delete_ipaddress
186	Can view IP address	47	view_ipaddress
187	Can add prefix	48	add_prefix
188	Can change prefix	48	change_prefix
189	Can delete prefix	48	delete_prefix
190	Can view prefix	48	view_prefix
191	Can add RIR	49	add_rir
192	Can change RIR	49	change_rir
193	Can delete RIR	49	delete_rir
194	Can view RIR	49	view_rir
195	Can add role	50	add_role
196	Can change role	50	change_role
197	Can delete role	50	delete_role
198	Can view role	50	view_role
199	Can add VLAN	51	add_vlan
200	Can change VLAN	51	change_vlan
201	Can delete VLAN	51	delete_vlan
202	Can view VLAN	51	view_vlan
203	Can add VRF	52	add_vrf
204	Can change VRF	52	change_vrf
205	Can delete VRF	52	delete_vrf
206	Can view VRF	52	view_vrf
207	Can add VLAN group	53	add_vlangroup
208	Can change VLAN group	53	change_vlangroup
209	Can delete VLAN group	53	delete_vlangroup
210	Can view VLAN group	53	view_vlangroup
211	Can add service	54	add_service
212	Can change service	54	change_service
213	Can delete service	54	delete_service
214	Can view service	54	view_service
215	Can add export template	55	add_exporttemplate
216	Can change export template	55	change_exporttemplate
217	Can delete export template	55	delete_exporttemplate
218	Can view export template	55	view_exporttemplate
219	Can add graph	56	add_graph
220	Can change graph	56	change_graph
221	Can delete graph	56	delete_graph
222	Can view graph	56	view_graph
223	Can add topology map	57	add_topologymap
224	Can change topology map	57	change_topologymap
225	Can delete topology map	57	delete_topologymap
226	Can view topology map	57	view_topologymap
227	Can add custom field	58	add_customfield
228	Can change custom field	58	change_customfield
229	Can delete custom field	58	delete_customfield
230	Can view custom field	58	view_customfield
231	Can add custom field choice	59	add_customfieldchoice
232	Can change custom field choice	59	change_customfieldchoice
233	Can delete custom field choice	59	delete_customfieldchoice
234	Can view custom field choice	59	view_customfieldchoice
235	Can add custom field value	60	add_customfieldvalue
236	Can change custom field value	60	change_customfieldvalue
237	Can delete custom field value	60	delete_customfieldvalue
238	Can view custom field value	60	view_customfieldvalue
239	Can add image attachment	61	add_imageattachment
240	Can change image attachment	61	change_imageattachment
241	Can delete image attachment	61	delete_imageattachment
242	Can view image attachment	61	view_imageattachment
243	Can add report result	62	add_reportresult
244	Can change report result	62	change_reportresult
245	Can delete report result	62	delete_reportresult
246	Can view report result	62	view_reportresult
247	Can add webhook	63	add_webhook
248	Can change webhook	63	change_webhook
249	Can delete webhook	63	delete_webhook
250	Can view webhook	63	view_webhook
251	Can add object change	64	add_objectchange
252	Can change object change	64	change_objectchange
253	Can delete object change	64	delete_objectchange
254	Can view object change	64	view_objectchange
255	Can add config context	65	add_configcontext
256	Can change config context	65	change_configcontext
257	Can delete config context	65	delete_configcontext
258	Can view config context	65	view_configcontext
259	Can add tag	66	add_tag
260	Can change tag	66	change_tag
261	Can delete tag	66	delete_tag
262	Can view tag	66	view_tag
263	Can add tagged item	67	add_taggeditem
264	Can change tagged item	67	change_taggeditem
265	Can delete tagged item	67	delete_taggeditem
266	Can view tagged item	67	view_taggeditem
267	Can add custom link	68	add_customlink
268	Can change custom link	68	change_customlink
269	Can delete custom link	68	delete_customlink
270	Can view custom link	68	view_customlink
271	Can add secret role	69	add_secretrole
272	Can change secret role	69	change_secretrole
273	Can delete secret role	69	delete_secretrole
274	Can view secret role	69	view_secretrole
275	Can add secret	70	add_secret
276	Can change secret	70	change_secret
277	Can delete secret	70	delete_secret
278	Can view secret	70	view_secret
279	Can add user key	71	add_userkey
280	Can change user key	71	change_userkey
281	Can delete user key	71	delete_userkey
282	Can view user key	71	view_userkey
283	Can activate user keys for decryption	71	activate_userkey
284	Can add session key	72	add_sessionkey
285	Can change session key	72	change_sessionkey
286	Can delete session key	72	delete_sessionkey
287	Can view session key	72	view_sessionkey
288	Can add tenant	73	add_tenant
289	Can change tenant	73	change_tenant
290	Can delete tenant	73	delete_tenant
291	Can view tenant	73	view_tenant
292	Can add tenant group	74	add_tenantgroup
293	Can change tenant group	74	change_tenantgroup
294	Can delete tenant group	74	delete_tenantgroup
295	Can view tenant group	74	view_tenantgroup
296	Can add token	75	add_token
297	Can change token	75	change_token
298	Can delete token	75	delete_token
299	Can view token	75	view_token
300	Can add cluster	76	add_cluster
301	Can change cluster	76	change_cluster
302	Can delete cluster	76	delete_cluster
303	Can view cluster	76	view_cluster
304	Can add cluster group	77	add_clustergroup
305	Can change cluster group	77	change_clustergroup
306	Can delete cluster group	77	delete_clustergroup
307	Can view cluster group	77	view_clustergroup
308	Can add cluster type	78	add_clustertype
309	Can change cluster type	78	change_clustertype
310	Can delete cluster type	78	delete_clustertype
311	Can view cluster type	78	view_clustertype
312	Can add virtual machine	79	add_virtualmachine
313	Can change virtual machine	79	change_virtualmachine
314	Can delete virtual machine	79	delete_virtualmachine
315	Can view virtual machine	79	view_virtualmachine
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 315, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$150000$XZiHqkrczAd2$tR7LsBHvGNy/CNf2dyaXXXHtnyl6t2rjcQAhJP2vAx4=	2019-08-26 05:35:20.446327-04	t	admin			non-reply@saigonbpo.vn	t	t	2019-08-26 04:23:15.552247-04
2	!fJoA0ZnNM89Dr0TEyK5DKsHGvudm3vH0n7z7pJcX	2019-08-27 03:08:28.229858-04	t	vuongnx	Vuong	Nguyen Xuan	vuong.nx@saigonbpo.vn	t	t	2019-08-27 03:07:22-04
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: circuits_circuit; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.circuits_circuit (id, created, last_updated, cid, install_date, commit_rate, comments, provider_id, type_id, tenant_id, description, status) FROM stdin;
\.


--
-- Name: circuits_circuit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.circuits_circuit_id_seq', 1, false);


--
-- Data for Name: circuits_circuittermination; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.circuits_circuittermination (id, term_side, port_speed, upstream_speed, xconnect_id, pp_info, circuit_id, site_id, connected_endpoint_id, connection_status, cable_id, description) FROM stdin;
\.


--
-- Name: circuits_circuittermination_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.circuits_circuittermination_id_seq', 1, false);


--
-- Data for Name: circuits_circuittype; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.circuits_circuittype (id, name, slug, created, last_updated) FROM stdin;
1	Internet	internet	\N	\N
2	Private WAN	private-wan	\N	\N
3	Out-of-Band	out-of-band	\N	\N
\.


--
-- Name: circuits_circuittype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.circuits_circuittype_id_seq', 3, true);


--
-- Data for Name: circuits_provider; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.circuits_provider (id, created, last_updated, name, slug, asn, account, portal_url, noc_contact, admin_contact, comments) FROM stdin;
\.


--
-- Name: circuits_provider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.circuits_provider_id_seq', 1, false);


--
-- Data for Name: dcim_cable; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_cable (id, created, last_updated, termination_a_id, termination_b_id, type, status, label, color, length, length_unit, _abs_length, termination_a_type_id, termination_b_type_id) FROM stdin;
\.


--
-- Name: dcim_cable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_cable_id_seq', 1, false);


--
-- Data for Name: dcim_consoleport; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_consoleport (id, name, connection_status, connected_endpoint_id, device_id, cable_id, description) FROM stdin;
\.


--
-- Name: dcim_consoleport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_consoleport_id_seq', 1, false);


--
-- Data for Name: dcim_consoleporttemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_consoleporttemplate (id, name, device_type_id) FROM stdin;
\.


--
-- Name: dcim_consoleporttemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_consoleporttemplate_id_seq', 1, false);


--
-- Data for Name: dcim_consoleserverport; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_consoleserverport (id, name, device_id, cable_id, connection_status, description) FROM stdin;
\.


--
-- Name: dcim_consoleserverport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_consoleserverport_id_seq', 1, false);


--
-- Data for Name: dcim_consoleserverporttemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_consoleserverporttemplate (id, name, device_type_id) FROM stdin;
\.


--
-- Name: dcim_consoleserverporttemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_consoleserverporttemplate_id_seq', 1, false);


--
-- Data for Name: dcim_device; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_device (id, created, last_updated, name, serial, "position", face, status, comments, device_role_id, device_type_id, platform_id, rack_id, primary_ip4_id, primary_ip6_id, tenant_id, asset_tag, site_id, cluster_id, virtual_chassis_id, vc_position, vc_priority, local_context_data) FROM stdin;
1	2019-08-26	2019-08-27 06:37:57.680567-04	VPS01		\N	0	1		8	1	11	1	175	\N	\N	Server001	1	4	\N	\N	\N	\N
2	2019-08-27	2019-08-27 06:40:39.353928-04	VPS05	SGH850TH90	\N	0	1		8	2	11	1	197	\N	\N	Server006	1	5	\N	\N	\N	\N
5	2019-08-28	2019-08-27 23:04:55.398454-04	VPS03	SGH708YBTY	\N	0	1		8	1	11	1	201	\N	\N	Server004	1	\N	\N	\N	\N	\N
4	2019-08-27	2019-08-27 23:06:48.285221-04	VPS02		\N	\N	1		8	1	11	1	199	\N	\N	Server002	1	\N	\N	\N	\N	\N
6	2019-08-28	2019-08-27 23:08:33.561041-04	VPS04	SGH829WLKQ	\N	\N	1		8	1	\N	1	203	\N	\N	Server005	1	\N	\N	\N	\N	\N
7	2019-08-28	2019-08-27 23:10:05.374422-04	VPS06	SGH849SDD5	\N	0	1		8	2	\N	1	205	\N	\N	Server007	1	\N	\N	\N	\N	\N
3	2019-08-27	2019-08-27 23:13:52.458245-04	Firewall001	S300067841CE287	\N	0	1		6	3	\N	2	174	\N	\N	Firewall001	1	\N	\N	\N	\N	\N
8	2019-08-28	2019-08-27 23:14:20.021471-04	Firewall002	S30055199EAED8F	\N	\N	1		6	3	\N	2	\N	\N	\N	Firewall002	1	\N	\N	\N	\N	\N
9	2019-08-28	2019-08-27 23:18:24.497012-04	Database	SGH651SFPK	\N	\N	1		8	1	8	1	207	\N	\N	Server003	1	\N	\N	\N	\N	\N
10	2019-08-28	2019-08-27 23:24:40.437646-04	Swich Border		\N	0	1		4	4	\N	2	208	\N	\N	\N	1	\N	\N	\N	\N	\N
11	2019-08-28	2019-08-27 23:28:30.358725-04	ho-sw-core-01		\N	0	1		2	5	\N	2	209	\N	\N	\N	1	\N	\N	\N	\N	\N
12	2019-08-28	2019-08-27 23:30:27.131093-04	ho-sw-central-01		\N	\N	1		3	6	\N	2	210	\N	\N	\N	1	\N	\N	\N	\N	\N
\.


--
-- Name: dcim_device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_device_id_seq', 12, true);


--
-- Data for Name: dcim_devicebay; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_devicebay (id, name, device_id, installed_device_id, description) FROM stdin;
\.


--
-- Name: dcim_devicebay_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_devicebay_id_seq', 1, false);


--
-- Data for Name: dcim_devicebaytemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_devicebaytemplate (id, name, device_type_id) FROM stdin;
\.


--
-- Name: dcim_devicebaytemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_devicebaytemplate_id_seq', 1, false);


--
-- Data for Name: dcim_devicerole; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_devicerole (id, name, slug, color, vm_role, created, last_updated) FROM stdin;
2	Core Switch	core-switch	2196f3	t	\N	\N
3	Distribution Switch	distribution-switch	2196f3	t	\N	\N
4	Access Switch	access-switch	2196f3	t	\N	\N
5	Management Switch	management-switch	ff9800	t	\N	\N
6	Firewall	firewall	f44336	t	\N	\N
7	Router	router	9c27b0	t	\N	\N
9	PDU	pdu	607d8b	t	\N	\N
1	Console Server	console-server	cddc39	t	\N	2019-08-27 02:33:57.106766-04
8	Server	server	c0c0c0	t	\N	2019-08-27 03:19:18.24816-04
\.


--
-- Name: dcim_devicerole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_devicerole_id_seq', 9, true);


--
-- Data for Name: dcim_devicetype; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_devicetype (id, model, slug, u_height, is_full_depth, manufacturer_id, subdevice_role, part_number, comments, created, last_updated) FROM stdin;
1	DL380 G9	dl380-g9	2	t	4	\N			2019-08-26	2019-08-26 06:36:54.897289-04
2	DL380 G10	dl380-g10	2	t	4	\N			2019-08-27	2019-08-26 21:00:08.045525-04
3	SG310	sg310	1	t	10	\N			2019-08-27	2019-08-27 02:32:39.453373-04
4	HP 1820-24G-PoE+ (185W) Switch	hp-1820-24g-poe-185w-switch	1	t	4	\N	J9983A		2019-08-28	2019-08-27 23:20:13.941576-04
5	HP J9850A Switch 5406Rzl2	hp-j9850a-switch-5406rzl2	4	t	4	\N	J9850A		2019-08-28	2019-08-27 23:26:53.459724-04
6	HP J9726A 2920-24G Switch	hp-j9726a-2920-24g-switch	1	t	4	\N	J9726A		2019-08-28	2019-08-27 23:29:11.763871-04
\.


--
-- Name: dcim_devicetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_devicetype_id_seq', 6, true);


--
-- Data for Name: dcim_frontport; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_frontport (id, name, type, rear_port_position, description, device_id, rear_port_id, cable_id) FROM stdin;
\.


--
-- Name: dcim_frontport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_frontport_id_seq', 1, false);


--
-- Data for Name: dcim_frontporttemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_frontporttemplate (id, name, type, rear_port_position, device_type_id, rear_port_id) FROM stdin;
\.


--
-- Name: dcim_frontporttemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_frontporttemplate_id_seq', 1, false);


--
-- Data for Name: dcim_interface; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_interface (id, name, type, mgmt_only, description, device_id, mac_address, lag_id, enabled, mtu, virtual_machine_id, mode, untagged_vlan_id, _connected_circuittermination_id, _connected_interface_id, connection_status, cable_id) FROM stdin;
2	Interface1	0	f		\N	\N	\N	t	\N	3	\N	\N	\N	\N	\N	\N
3	Interface1	0	f		\N	\N	\N	t	\N	4	\N	\N	\N	\N	\N	\N
4	Interface1	0	f		\N	\N	\N	t	\N	5	\N	\N	\N	\N	\N	\N
5	Interface1	0	f		\N	\N	\N	t	\N	6	\N	\N	\N	\N	\N	\N
6	Interface1	0	f		\N	\N	\N	t	\N	7	\N	\N	\N	\N	\N	\N
7	Interface1	0	f		\N	\N	\N	t	\N	8	\N	\N	\N	\N	\N	\N
8	Interface1	0	f		\N	\N	\N	t	\N	9	\N	\N	\N	\N	\N	\N
9	Interface1	0	f		\N	\N	\N	t	\N	10	\N	\N	\N	\N	\N	\N
10	Interface1	0	f		\N	\N	\N	t	\N	11	\N	\N	\N	\N	\N	\N
11	Interface1	0	f		\N	\N	\N	t	\N	12	\N	\N	\N	\N	\N	\N
12	Interface1	0	f		\N	\N	\N	t	\N	13	\N	\N	\N	\N	\N	\N
13	Interface1	1000	f		3	\N	\N	t	\N	\N	100	\N	\N	\N	\N	\N
14	Management	800	f		1	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
15	Interface1	0	f		\N	\N	\N	t	\N	14	\N	\N	\N	\N	\N	\N
16	Interface1	0	f		\N	\N	\N	t	\N	15	\N	\N	\N	\N	\N	\N
17	Interface1	0	f		\N	\N	\N	t	\N	16	\N	\N	\N	\N	\N	\N
18	Interface1	0	f		\N	\N	\N	t	\N	17	\N	\N	\N	\N	\N	\N
19	Interface1	0	f		\N	\N	\N	t	\N	18	\N	\N	\N	\N	\N	\N
20	Interface1	0	f		\N	\N	\N	t	\N	19	\N	\N	\N	\N	\N	\N
21	Interface1	0	f		\N	\N	\N	t	\N	20	\N	\N	\N	\N	\N	\N
22	Interface1	0	f		\N	\N	\N	t	\N	21	\N	\N	\N	\N	\N	\N
23	Interface1	0	f		\N	\N	\N	t	\N	22	\N	\N	\N	\N	\N	\N
24	Interface1	0	f		\N	\N	\N	t	\N	23	\N	\N	\N	\N	\N	\N
25	Interface1	0	f		\N	\N	\N	t	\N	24	\N	\N	\N	\N	\N	\N
26	Interface1	0	f		\N	\N	\N	t	\N	25	\N	\N	\N	\N	\N	\N
27	Interface1	0	f		\N	\N	\N	t	\N	26	\N	\N	\N	\N	\N	\N
28	Interface1	0	f		\N	\N	\N	t	\N	27	\N	\N	\N	\N	\N	\N
29	Interface1	0	f		\N	\N	\N	t	\N	28	\N	\N	\N	\N	\N	\N
30	Interface1	0	f		\N	\N	\N	t	\N	29	\N	\N	\N	\N	\N	\N
31	Interface1	0	f		\N	\N	\N	t	\N	30	\N	\N	\N	\N	\N	\N
32	Interface1	0	f		\N	\N	\N	t	\N	31	\N	\N	\N	\N	\N	\N
33	Interface1	0	f		\N	\N	\N	t	\N	32	\N	\N	\N	\N	\N	\N
34	Interface1	0	f		\N	\N	\N	t	\N	33	\N	\N	\N	\N	\N	\N
35	iLo	0	f		1	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
37	iLo	0	f		2	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
36	Management	1000	f		2	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
38	Management	1000	f		4	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
39	iLo	0	f		4	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
40	Management	1000	f		5	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
41	iLo	0	f		5	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
42	Management	1000	f		6	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
43	iLo	0	f		6	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
44	Management	1000	f		7	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
45	iLo	0	f		7	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
46	Interface1	1000	f		9	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
47	Management	1000	f		10	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
48	Management	1000	f		11	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
49	Management	1000	f		12	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Name: dcim_interface_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_interface_id_seq', 49, true);


--
-- Data for Name: dcim_interface_tagged_vlans; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_interface_tagged_vlans (id, interface_id, vlan_id) FROM stdin;
\.


--
-- Name: dcim_interface_tagged_vlans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_interface_tagged_vlans_id_seq', 1, false);


--
-- Data for Name: dcim_interfacetemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_interfacetemplate (id, name, type, mgmt_only, device_type_id) FROM stdin;
\.


--
-- Name: dcim_interfacetemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_interfacetemplate_id_seq', 1, false);


--
-- Data for Name: dcim_inventoryitem; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_inventoryitem (id, name, part_id, serial, discovered, device_id, parent_id, manufacturer_id, asset_tag, description) FROM stdin;
\.


--
-- Data for Name: dcim_manufacturer; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_manufacturer (id, name, slug, created, last_updated) FROM stdin;
1	APC	apc	\N	\N
2	Cisco	cisco	\N	\N
3	Dell	dell	\N	\N
5	Juniper	juniper	\N	\N
9	Microsoft	microsoft	2019-08-26	2019-08-26 04:51:10.607267-04
10	Sophos	sophos	2019-08-27	2019-08-27 02:31:54.386823-04
4	HPE	hpe	\N	2019-08-27 23:20:27.077257-04
\.


--
-- Name: dcim_manufacturer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_manufacturer_id_seq', 10, true);


--
-- Name: dcim_module_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_module_id_seq', 3, true);


--
-- Data for Name: dcim_platform; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_platform (id, name, slug, napalm_driver, manufacturer_id, created, last_updated, napalm_args) FROM stdin;
5	Linux	linux		\N	\N	\N	\N
7	Window Server 2012 R2	window-server-2012-r2		9	2019-08-26	2019-08-26 04:51:18.837008-04	\N
8	Centos 7	centos-7		\N	2019-08-26	2019-08-26 04:53:45.634689-04	\N
10	Window 7 Pro	window-7-pro		9	2019-08-27	2019-08-27 00:33:42.567019-04	\N
11	ESXi 6.0	esxi-6-0		\N	2019-08-27	2019-08-27 02:17:17.554894-04	\N
9	Ubuntu Server	ubuntu-server		\N	2019-08-26	2019-08-27 23:15:01.399616-04	\N
\.


--
-- Name: dcim_platform_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_platform_id_seq', 11, true);


--
-- Data for Name: dcim_powerfeed; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_powerfeed (id, created, last_updated, name, status, type, supply, phase, voltage, amperage, max_utilization, available_power, comments, cable_id, power_panel_id, rack_id, connected_endpoint_id, connection_status) FROM stdin;
\.


--
-- Name: dcim_powerfeed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_powerfeed_id_seq', 1, false);


--
-- Data for Name: dcim_poweroutlet; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_poweroutlet (id, name, device_id, cable_id, connection_status, description, feed_leg, power_port_id) FROM stdin;
\.


--
-- Name: dcim_poweroutlet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_poweroutlet_id_seq', 1, false);


--
-- Data for Name: dcim_poweroutlettemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_poweroutlettemplate (id, name, device_type_id, feed_leg, power_port_id) FROM stdin;
\.


--
-- Name: dcim_poweroutlettemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_poweroutlettemplate_id_seq', 1, false);


--
-- Data for Name: dcim_powerpanel; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_powerpanel (id, created, last_updated, name, rack_group_id, site_id) FROM stdin;
\.


--
-- Name: dcim_powerpanel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_powerpanel_id_seq', 1, false);


--
-- Data for Name: dcim_powerport; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_powerport (id, name, connection_status, device_id, _connected_poweroutlet_id, cable_id, description, _connected_powerfeed_id, allocated_draw, maximum_draw) FROM stdin;
\.


--
-- Name: dcim_powerport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_powerport_id_seq', 1, false);


--
-- Data for Name: dcim_powerporttemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_powerporttemplate (id, name, device_type_id, allocated_draw, maximum_draw) FROM stdin;
\.


--
-- Name: dcim_powerporttemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_powerporttemplate_id_seq', 1, false);


--
-- Data for Name: dcim_rack; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_rack (id, created, last_updated, name, facility_id, u_height, comments, group_id, site_id, tenant_id, type, width, role_id, desc_units, serial, status, asset_tag, outer_depth, outer_unit, outer_width) FROM stdin;
1	2019-08-26	2019-08-26 06:34:48.885035-04	Rack01	\N	42		1	1	\N	\N	19	\N	f		3	\N	\N	\N	\N
2	2019-08-26	2019-08-26 06:35:28.035425-04	Rack02	\N	42		1	1	\N	\N	19	\N	f		3	\N	\N	\N	\N
\.


--
-- Name: dcim_rack_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_rack_id_seq', 2, true);


--
-- Data for Name: dcim_rackgroup; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_rackgroup (id, name, slug, site_id, created, last_updated) FROM stdin;
1	Server Room F4	server-room-f4	1	2019-08-26	2019-08-26 06:34:02.554559-04
\.


--
-- Name: dcim_rackgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_rackgroup_id_seq', 1, true);


--
-- Data for Name: dcim_rackreservation; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_rackreservation (id, units, created, description, rack_id, user_id, tenant_id, last_updated) FROM stdin;
\.


--
-- Name: dcim_rackreservation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_rackreservation_id_seq', 1, false);


--
-- Data for Name: dcim_rackrole; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_rackrole (id, name, slug, color, created, last_updated) FROM stdin;
\.


--
-- Name: dcim_rackrole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_rackrole_id_seq', 1, false);


--
-- Data for Name: dcim_rearport; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_rearport (id, name, type, positions, description, device_id, cable_id) FROM stdin;
\.


--
-- Name: dcim_rearport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_rearport_id_seq', 1, false);


--
-- Data for Name: dcim_rearporttemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_rearporttemplate (id, name, type, positions, device_type_id) FROM stdin;
\.


--
-- Name: dcim_rearporttemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_rearporttemplate_id_seq', 1, false);


--
-- Data for Name: dcim_region; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_region (id, name, slug, lft, rght, tree_id, level, parent_id, created, last_updated) FROM stdin;
\.


--
-- Name: dcim_region_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_region_id_seq', 1, false);


--
-- Data for Name: dcim_site; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_site (id, created, last_updated, name, slug, facility, asn, physical_address, shipping_address, comments, tenant_id, contact_email, contact_name, contact_phone, region_id, description, status, time_zone, latitude, longitude) FROM stdin;
1	2019-08-26	2019-08-26 22:05:01.401107-04	H3	h3		\N	4th Floor, H3 Building, 384 Hoang Dieu, Ward 6, District 4, Ho Chi Minh City, Vietnam			\N	info@saigonbpo.vn		+84 28 7300 8184	\N		1	Asia/Ho_Chi_Minh	\N	\N
\.


--
-- Name: dcim_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_site_id_seq', 1, true);


--
-- Data for Name: dcim_virtualchassis; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.dcim_virtualchassis (id, domain, master_id, created, last_updated) FROM stdin;
\.


--
-- Name: dcim_virtualchassis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.dcim_virtualchassis_id_seq', 1, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2019-08-26 05:23:14.893924-04	1	a97c71 (admin)	1	[{"added": {}}]	75	1
2	2019-08-26 05:58:49.531265-04	1	Field1	1	[{"added": {}}]	58	1
3	2019-08-26 06:01:04.129071-04	1	Field1	2	[{"changed": {"fields": ["obj_type"]}}]	58	1
4	2019-08-26 06:40:26.626187-04	1	Network	1	[{"added": {}}]	57	1
5	2019-08-27 02:18:11.298419-04	1	Field1	3		58	1
6	2019-08-27 03:08:00.690736-04	2	vuongnx	2	[{"changed": {"fields": ["is_staff", "is_superuser"]}}]	11	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 6, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	dcim	consoleport
2	dcim	consoleserverport
3	dcim	powerport
4	dcim	poweroutlet
5	dcim	interface
7	circuits	circuittermination
8	admin	logentry
9	auth	permission
10	auth	group
11	auth	user
12	contenttypes	contenttype
13	sessions	session
14	taggit	tag
15	taggit	taggeditem
16	circuits	provider
17	circuits	circuittype
18	circuits	circuit
19	dcim	consoleporttemplate
20	dcim	consoleserverporttemplate
21	dcim	device
22	dcim	devicerole
23	dcim	devicetype
24	dcim	interfacetemplate
25	dcim	manufacturer
26	dcim	platform
27	dcim	poweroutlettemplate
28	dcim	powerporttemplate
29	dcim	rack
30	dcim	rackgroup
31	dcim	site
32	dcim	devicebay
33	dcim	devicebaytemplate
34	dcim	rackrole
35	dcim	rackreservation
36	dcim	region
37	dcim	inventoryitem
38	dcim	virtualchassis
39	dcim	frontport
40	dcim	frontporttemplate
41	dcim	rearport
42	dcim	rearporttemplate
43	dcim	cable
44	dcim	powerfeed
45	dcim	powerpanel
46	ipam	aggregate
47	ipam	ipaddress
48	ipam	prefix
49	ipam	rir
50	ipam	role
51	ipam	vlan
52	ipam	vrf
53	ipam	vlangroup
54	ipam	service
55	extras	exporttemplate
56	extras	graph
57	extras	topologymap
58	extras	customfield
59	extras	customfieldchoice
60	extras	customfieldvalue
61	extras	imageattachment
62	extras	reportresult
63	extras	webhook
64	extras	objectchange
65	extras	configcontext
66	extras	tag
67	extras	taggeditem
68	extras	customlink
69	secrets	secretrole
70	secrets	secret
71	secrets	userkey
72	secrets	sessionkey
73	tenancy	tenant
74	tenancy	tenantgroup
75	users	token
76	virtualization	cluster
77	virtualization	clustergroup
78	virtualization	clustertype
79	virtualization	virtualmachine
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 79, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-08-26 04:21:46.556425-04
2	auth	0001_initial	2019-08-26 04:21:46.614225-04
3	admin	0001_initial	2019-08-26 04:21:46.735926-04
4	admin	0002_logentry_remove_auto_add	2019-08-26 04:21:46.757687-04
5	admin	0003_logentry_add_action_flag_choices	2019-08-26 04:21:46.769106-04
6	contenttypes	0002_remove_content_type_name	2019-08-26 04:21:46.793137-04
7	auth	0002_alter_permission_name_max_length	2019-08-26 04:21:46.800202-04
8	auth	0003_alter_user_email_max_length	2019-08-26 04:21:46.810684-04
9	auth	0004_alter_user_username_opts	2019-08-26 04:21:46.823243-04
10	auth	0005_alter_user_last_login_null	2019-08-26 04:21:46.83394-04
11	auth	0006_require_contenttypes_0002	2019-08-26 04:21:46.837082-04
12	auth	0007_alter_validators_add_error_messages	2019-08-26 04:21:46.848727-04
13	auth	0008_alter_user_username_max_length	2019-08-26 04:21:46.864416-04
14	auth	0009_alter_user_last_name_max_length	2019-08-26 04:21:46.874477-04
15	auth	0010_alter_group_name_max_length	2019-08-26 04:21:46.885536-04
16	auth	0011_update_proxy_permissions	2019-08-26 04:21:46.896093-04
17	tenancy	0001_initial	2019-08-26 04:21:46.961233-04
18	tenancy	0002_tenant_group_optional	2019-08-26 04:21:47.01075-04
19	tenancy	0003_unicode_literals	2019-08-26 04:21:47.011817-04
20	taggit	0001_initial	2019-08-26 04:21:47.045933-04
21	taggit	0002_auto_20150616_2121	2019-08-26 04:21:47.102963-04
22	tenancy	0004_tags	2019-08-26 04:21:47.114373-04
23	tenancy	0005_change_logging	2019-08-26 04:21:47.207599-04
24	dcim	0001_initial	2019-08-26 04:21:47.529502-04
25	ipam	0001_initial	2019-08-26 04:21:47.850627-04
26	dcim	0002_auto_20160622_1821	2019-08-26 04:21:48.806183-04
27	dcim	0003_auto_20160628_1721	2019-08-26 04:21:48.807226-04
28	dcim	0004_auto_20160701_2049	2019-08-26 04:21:48.808162-04
29	dcim	0005_auto_20160706_1722	2019-08-26 04:21:48.809095-04
30	dcim	0006_add_device_primary_ip4_ip6	2019-08-26 04:21:48.810129-04
31	dcim	0007_device_copy_primary_ip	2019-08-26 04:21:48.811135-04
32	dcim	0008_device_remove_primary_ip	2019-08-26 04:21:48.812088-04
33	dcim	0009_site_32bit_asn_support	2019-08-26 04:21:48.813378-04
34	dcim	0010_devicebay_installed_device_set_null	2019-08-26 04:21:48.814922-04
35	dcim	0011_devicetype_part_number	2019-08-26 04:21:48.816081-04
36	dcim	0012_site_rack_device_add_tenant	2019-08-26 04:21:48.817033-04
37	dcim	0013_add_interface_form_factors	2019-08-26 04:21:48.817984-04
38	dcim	0014_rack_add_type_width	2019-08-26 04:21:48.818928-04
39	dcim	0015_rack_add_u_height_validator	2019-08-26 04:21:48.81987-04
40	dcim	0016_module_add_manufacturer	2019-08-26 04:21:48.820983-04
41	dcim	0017_rack_add_role	2019-08-26 04:21:48.82215-04
42	dcim	0018_device_add_asset_tag	2019-08-26 04:21:48.823212-04
43	dcim	0019_new_iface_form_factors	2019-08-26 04:21:48.824327-04
44	dcim	0020_rack_desc_units	2019-08-26 04:21:48.825546-04
45	dcim	0021_add_ff_flexstack	2019-08-26 04:21:48.826585-04
46	dcim	0022_color_names_to_rgb	2019-08-26 04:21:48.827547-04
47	extras	0001_initial	2019-08-26 04:21:49.337221-04
48	extras	0002_custom_fields	2019-08-26 04:21:49.338398-04
49	extras	0003_exporttemplate_add_description	2019-08-26 04:21:49.339437-04
50	extras	0004_topologymap_change_comma_to_semicolon	2019-08-26 04:21:49.340467-04
51	extras	0005_useraction_add_bulk_create	2019-08-26 04:21:49.341545-04
52	extras	0006_add_imageattachments	2019-08-26 04:21:49.342965-04
53	extras	0007_unicode_literals	2019-08-26 04:21:49.344145-04
54	extras	0008_reports	2019-08-26 04:21:49.345344-04
55	extras	0009_topologymap_type	2019-08-26 04:21:49.346464-04
56	extras	0010_customfield_filter_logic	2019-08-26 04:21:49.347485-04
57	extras	0011_django2	2019-08-26 04:21:49.559839-04
58	extras	0012_webhooks	2019-08-26 04:21:49.626708-04
59	extras	0013_objectchange	2019-08-26 04:21:49.692092-04
60	ipam	0002_vrf_add_enforce_unique	2019-08-26 04:21:50.650233-04
61	ipam	0003_ipam_add_vlangroups	2019-08-26 04:21:50.651336-04
62	ipam	0004_ipam_vlangroup_uniqueness	2019-08-26 04:21:50.652434-04
63	ipam	0005_auto_20160725_1842	2019-08-26 04:21:50.653511-04
64	ipam	0006_vrf_vlan_add_tenant	2019-08-26 04:21:50.654592-04
65	ipam	0007_prefix_ipaddress_add_tenant	2019-08-26 04:21:50.655921-04
66	ipam	0008_prefix_change_order	2019-08-26 04:21:50.657062-04
67	ipam	0009_ipaddress_add_status	2019-08-26 04:21:50.658128-04
68	ipam	0010_ipaddress_help_texts	2019-08-26 04:21:50.659165-04
69	ipam	0011_rir_add_is_private	2019-08-26 04:21:50.660167-04
70	ipam	0012_services	2019-08-26 04:21:50.661163-04
71	ipam	0013_prefix_add_is_pool	2019-08-26 04:21:50.662238-04
72	ipam	0014_ipaddress_status_add_deprecated	2019-08-26 04:21:50.663325-04
73	ipam	0015_global_vlans	2019-08-26 04:21:50.66434-04
74	ipam	0016_unicode_literals	2019-08-26 04:21:50.665454-04
75	ipam	0017_ipaddress_roles	2019-08-26 04:21:50.666538-04
76	ipam	0018_remove_service_uniqueness_constraint	2019-08-26 04:21:50.667576-04
77	dcim	0023_devicetype_comments	2019-08-26 04:21:52.668697-04
78	dcim	0024_site_add_contact_fields	2019-08-26 04:21:52.6699-04
79	dcim	0025_devicetype_add_interface_ordering	2019-08-26 04:21:52.670965-04
80	dcim	0026_add_rack_reservations	2019-08-26 04:21:52.672032-04
81	dcim	0027_device_add_site	2019-08-26 04:21:52.673143-04
82	dcim	0028_device_copy_rack_to_site	2019-08-26 04:21:52.67427-04
83	dcim	0029_allow_rackless_devices	2019-08-26 04:21:52.675481-04
84	dcim	0030_interface_add_lag	2019-08-26 04:21:52.6766-04
85	dcim	0031_regions	2019-08-26 04:21:52.677677-04
86	dcim	0032_device_increase_name_length	2019-08-26 04:21:52.67883-04
87	dcim	0033_rackreservation_rack_editable	2019-08-26 04:21:52.679891-04
88	dcim	0034_rename_module_to_inventoryitem	2019-08-26 04:21:52.68093-04
89	dcim	0035_device_expand_status_choices	2019-08-26 04:21:52.681996-04
90	dcim	0036_add_ff_juniper_vcp	2019-08-26 04:21:52.68306-04
91	dcim	0037_unicode_literals	2019-08-26 04:21:52.684115-04
92	dcim	0038_wireless_interfaces	2019-08-26 04:21:52.685269-04
93	dcim	0039_interface_add_enabled_mtu	2019-08-26 04:21:52.686382-04
94	dcim	0040_inventoryitem_add_asset_tag_description	2019-08-26 04:21:52.687442-04
95	dcim	0041_napalm_integration	2019-08-26 04:21:52.68862-04
96	dcim	0042_interface_ff_10ge_cx4	2019-08-26 04:21:52.689746-04
97	dcim	0043_device_component_name_lengths	2019-08-26 04:21:52.690817-04
98	virtualization	0001_virtualization	2019-08-26 04:21:52.898578-04
99	ipam	0019_virtualization	2019-08-26 04:21:53.17127-04
100	ipam	0020_ipaddress_add_role_carp	2019-08-26 04:21:53.172441-04
101	dcim	0044_virtualization	2019-08-26 04:21:54.02431-04
102	dcim	0045_devicerole_vm_role	2019-08-26 04:21:54.025765-04
103	dcim	0046_rack_lengthen_facility_id	2019-08-26 04:21:54.026944-04
104	dcim	0047_more_100ge_form_factors	2019-08-26 04:21:54.028042-04
105	dcim	0048_rack_serial	2019-08-26 04:21:54.02912-04
106	dcim	0049_rackreservation_change_user	2019-08-26 04:21:54.030233-04
107	dcim	0050_interface_vlan_tagging	2019-08-26 04:21:54.031301-04
108	dcim	0051_rackreservation_tenant	2019-08-26 04:21:54.032513-04
109	dcim	0052_virtual_chassis	2019-08-26 04:21:54.033714-04
110	dcim	0053_platform_manufacturer	2019-08-26 04:21:54.035033-04
111	dcim	0054_site_status_timezone_description	2019-08-26 04:21:54.036258-04
112	dcim	0055_virtualchassis_ordering	2019-08-26 04:21:54.037383-04
113	dcim	0056_django2	2019-08-26 04:21:54.184568-04
114	dcim	0057_tags	2019-08-26 04:21:54.789636-04
115	dcim	0058_relax_rack_naming_constraints	2019-08-26 04:21:54.863929-04
116	dcim	0059_site_latitude_longitude	2019-08-26 04:21:54.927542-04
117	dcim	0060_change_logging	2019-08-26 04:21:55.762507-04
118	dcim	0061_platform_napalm_args	2019-08-26 04:21:55.78682-04
119	extras	0014_configcontexts	2019-08-26 04:21:56.155007-04
120	extras	0015_remove_useraction	2019-08-26 04:21:56.385835-04
121	extras	0016_exporttemplate_add_cable	2019-08-26 04:21:56.439466-04
122	extras	0017_exporttemplate_mime_type_length	2019-08-26 04:21:56.454677-04
123	extras	0018_exporttemplate_add_jinja2	2019-08-26 04:21:56.549679-04
124	extras	0019_tag_taggeditem	2019-08-26 04:21:56.636617-04
125	dcim	0062_interface_mtu	2019-08-26 04:21:56.740633-04
126	dcim	0063_device_local_context_data	2019-08-26 04:21:56.873987-04
127	dcim	0064_remove_platform_rpc_client	2019-08-26 04:21:56.886579-04
128	dcim	0065_front_rear_ports	2019-08-26 04:21:57.366324-04
129	circuits	0001_initial	2019-08-26 04:21:57.608484-04
130	circuits	0002_auto_20160622_1821	2019-08-26 04:21:57.609743-04
131	circuits	0003_provider_32bit_asn_support	2019-08-26 04:21:57.610934-04
132	circuits	0004_circuit_add_tenant	2019-08-26 04:21:57.612059-04
133	circuits	0005_circuit_add_upstream_speed	2019-08-26 04:21:57.613195-04
134	circuits	0006_terminations	2019-08-26 04:21:57.61436-04
135	circuits	0007_circuit_add_description	2019-08-26 04:21:57.615549-04
136	circuits	0008_circuittermination_interface_protect_on_delete	2019-08-26 04:21:57.616733-04
137	circuits	0009_unicode_literals	2019-08-26 04:21:57.617856-04
138	circuits	0010_circuit_status	2019-08-26 04:21:57.618971-04
139	dcim	0066_cables	2019-08-26 04:21:59.543759-04
140	circuits	0011_tags	2019-08-26 04:21:59.720179-04
141	circuits	0012_change_logging	2019-08-26 04:21:59.921254-04
142	circuits	0013_cables	2019-08-26 04:22:00.285611-04
143	circuits	0014_circuittermination_description	2019-08-26 04:22:00.358969-04
144	circuits	0015_custom_tag_models	2019-08-26 04:22:00.505402-04
145	virtualization	0002_virtualmachine_add_status	2019-08-26 04:22:00.708602-04
146	virtualization	0003_cluster_add_site	2019-08-26 04:22:00.709841-04
147	virtualization	0004_virtualmachine_add_role	2019-08-26 04:22:00.710986-04
148	virtualization	0005_django2	2019-08-26 04:22:00.786637-04
149	virtualization	0006_tags	2019-08-26 04:22:01.005455-04
150	virtualization	0007_change_logging	2019-08-26 04:22:01.302674-04
151	virtualization	0008_virtualmachine_local_context_data	2019-08-26 04:22:01.343531-04
152	virtualization	0009_custom_tag_models	2019-08-26 04:22:01.467819-04
153	tenancy	0006_custom_tag_models	2019-08-26 04:22:01.530218-04
154	secrets	0001_initial	2019-08-26 04:22:01.983469-04
155	secrets	0002_userkey_add_session_key	2019-08-26 04:22:01.984821-04
156	secrets	0003_unicode_literals	2019-08-26 04:22:01.986129-04
157	secrets	0004_tags	2019-08-26 04:22:02.106222-04
158	secrets	0005_change_logging	2019-08-26 04:22:02.245921-04
159	secrets	0006_custom_tag_models	2019-08-26 04:22:02.309958-04
160	ipam	0021_vrf_ordering	2019-08-26 04:22:02.335968-04
161	ipam	0022_tags	2019-08-26 04:22:02.811111-04
162	ipam	0023_change_logging	2019-08-26 04:22:03.498258-04
163	ipam	0024_vrf_allow_null_rd	2019-08-26 04:22:03.550439-04
164	ipam	0025_custom_tag_models	2019-08-26 04:22:03.877944-04
165	dcim	0067_device_type_remove_qualifiers	2019-08-26 04:22:03.985307-04
166	dcim	0068_rack_new_fields	2019-08-26 04:22:04.968995-04
167	dcim	0069_deprecate_nullablecharfield	2019-08-26 04:22:05.104291-04
168	dcim	0070_custom_tag_models	2019-08-26 04:22:06.249816-04
169	extras	0020_tag_data	2019-08-26 04:22:06.541354-04
170	extras	0021_add_color_comments_changelog_to_tag	2019-08-26 04:22:06.903633-04
171	dcim	0071_device_components_add_description	2019-08-26 04:22:07.192934-04
172	dcim	0072_powerfeeds	2019-08-26 04:22:08.174318-04
173	dcim	0073_interface_form_factor_to_type	2019-08-26 04:22:08.439939-04
174	extras	0022_custom_links	2019-08-26 04:22:08.751168-04
175	extras	0023_fix_tag_sequences	2019-08-26 04:22:08.770469-04
176	ipam	0026_prefix_ordering_vrf_nulls_first	2019-08-26 04:22:08.824655-04
177	ipam	0027_ipaddress_add_dns_name	2019-08-26 04:22:08.896748-04
178	sessions	0001_initial	2019-08-26 04:22:08.916154-04
179	taggit	0003_taggeditem_add_unique_index	2019-08-26 04:22:08.956232-04
180	users	0001_api_tokens	2019-08-26 04:22:09.046752-04
181	users	0002_unicode_literals	2019-08-26 04:22:09.048077-04
182	users	0003_token_permissions	2019-08-26 04:22:09.082945-04
183	circuits	0001_initial_squashed_0010_circuit_status	2019-08-26 04:22:09.087682-04
184	dcim	0023_devicetype_comments_squashed_0043_device_component_name_lengths	2019-08-26 04:22:09.090859-04
185	dcim	0002_auto_20160622_1821_squashed_0022_color_names_to_rgb	2019-08-26 04:22:09.093877-04
186	dcim	0044_virtualization_squashed_0055_virtualchassis_ordering	2019-08-26 04:22:09.097376-04
187	ipam	0002_vrf_add_enforce_unique_squashed_0018_remove_service_uniqueness_constraint	2019-08-26 04:22:09.100501-04
188	ipam	0019_virtualization_squashed_0020_ipaddress_add_role_carp	2019-08-26 04:22:09.103704-04
189	extras	0001_initial_squashed_0010_customfield_filter_logic	2019-08-26 04:22:09.107118-04
190	secrets	0001_initial_squashed_0003_unicode_literals	2019-08-26 04:22:09.110109-04
191	tenancy	0002_tenant_group_optional_squashed_0003_unicode_literals	2019-08-26 04:22:09.113855-04
192	users	0001_api_tokens_squashed_0002_unicode_literals	2019-08-26 04:22:09.117096-04
193	virtualization	0002_virtualmachine_add_status_squashed_0004_virtualmachine_add_role	2019-08-26 04:22:09.120067-04
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 193, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
27ys89v93kwilo384lyodvtqte1ysmos	NmVkYzdkZWMwY2M0OWQ5YmNhZjE3MTdmNWU2NTRlODdhZDMyODA4Mjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvX2F1dGhfbGRhcC5iYWNrZW5kLkxEQVBCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjVjOGIyNjIzNGI5NTRmMWUxMmUwOTE3MTk5NzZjZWE4N2FhZDExMiJ9	2019-09-10 03:08:28.237304-04
\.


--
-- Data for Name: extras_configcontext; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_configcontext (id, name, weight, description, is_active, data) FROM stdin;
\.


--
-- Name: extras_configcontext_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_configcontext_id_seq', 1, false);


--
-- Data for Name: extras_configcontext_platforms; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_configcontext_platforms (id, configcontext_id, platform_id) FROM stdin;
\.


--
-- Name: extras_configcontext_platforms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_configcontext_platforms_id_seq', 1, false);


--
-- Data for Name: extras_configcontext_regions; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_configcontext_regions (id, configcontext_id, region_id) FROM stdin;
\.


--
-- Name: extras_configcontext_regions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_configcontext_regions_id_seq', 1, false);


--
-- Data for Name: extras_configcontext_roles; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_configcontext_roles (id, configcontext_id, devicerole_id) FROM stdin;
\.


--
-- Name: extras_configcontext_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_configcontext_roles_id_seq', 1, false);


--
-- Data for Name: extras_configcontext_sites; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_configcontext_sites (id, configcontext_id, site_id) FROM stdin;
\.


--
-- Name: extras_configcontext_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_configcontext_sites_id_seq', 1, false);


--
-- Data for Name: extras_configcontext_tenant_groups; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_configcontext_tenant_groups (id, configcontext_id, tenantgroup_id) FROM stdin;
\.


--
-- Name: extras_configcontext_tenant_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_configcontext_tenant_groups_id_seq', 1, false);


--
-- Data for Name: extras_configcontext_tenants; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_configcontext_tenants (id, configcontext_id, tenant_id) FROM stdin;
\.


--
-- Name: extras_configcontext_tenants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_configcontext_tenants_id_seq', 1, false);


--
-- Data for Name: extras_customfield; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_customfield (id, type, name, label, description, required, "default", weight, filter_logic) FROM stdin;
\.


--
-- Name: extras_customfield_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_customfield_id_seq', 1, true);


--
-- Data for Name: extras_customfield_obj_type; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_customfield_obj_type (id, customfield_id, contenttype_id) FROM stdin;
\.


--
-- Name: extras_customfield_obj_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_customfield_obj_type_id_seq', 3, true);


--
-- Data for Name: extras_customfieldchoice; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_customfieldchoice (id, value, weight, field_id) FROM stdin;
\.


--
-- Name: extras_customfieldchoice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_customfieldchoice_id_seq', 1, false);


--
-- Data for Name: extras_customfieldvalue; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_customfieldvalue (id, obj_id, serialized_value, field_id, obj_type_id) FROM stdin;
\.


--
-- Name: extras_customfieldvalue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_customfieldvalue_id_seq', 1, true);


--
-- Data for Name: extras_customlink; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_customlink (id, name, text, url, weight, group_name, button_class, new_window, content_type_id) FROM stdin;
\.


--
-- Name: extras_customlink_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_customlink_id_seq', 1, false);


--
-- Data for Name: extras_exporttemplate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_exporttemplate (id, name, template_code, mime_type, file_extension, content_type_id, description, template_language) FROM stdin;
\.


--
-- Name: extras_exporttemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_exporttemplate_id_seq', 1, false);


--
-- Data for Name: extras_graph; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_graph (id, type, weight, name, source, link) FROM stdin;
\.


--
-- Name: extras_graph_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_graph_id_seq', 1, false);


--
-- Data for Name: extras_imageattachment; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_imageattachment (id, object_id, image, image_height, image_width, name, created, content_type_id) FROM stdin;
\.


--
-- Name: extras_imageattachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_imageattachment_id_seq', 1, false);


--
-- Data for Name: extras_objectchange; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_objectchange (id, "time", user_name, request_id, action, changed_object_id, related_object_id, object_repr, object_data, changed_object_type_id, related_object_type_id, user_id) FROM stdin;
1	2019-08-26 04:34:55.169579-04	admin	60f3afb7-17dd-42ba-9b10-679a9d87448e	1	1	\N	H3	{"asn": null, "name": "H3", "slug": "h3", "tags": [], "region": null, "status": 1, "tenant": null, "created": "2019-08-26", "comments": "", "facility": "", "latitude": null, "longitude": null, "time_zone": "Asia/Ho_Chi_Minh", "description": "", "contact_name": "", "last_updated": "2019-08-26T08:34:55.145Z", "contact_email": "", "contact_phone": "", "custom_fields": {}, "physical_address": "", "shipping_address": ""}	31	\N	1
2	2019-08-26 04:41:41.935569-04	admin	7d46c6a6-3a14-45b7-a75c-6f1cd2f59b57	1	1	\N	ho--srv-ad	{"disk": 50, "name": "ho--srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 2, "created": "2019-08-26", "comments": "", "platform": null, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T08:41:41.913Z", "custom_fields": {}, "local_context_data": null}	79	\N	1
3	2019-08-26 04:42:50.719323-04	admin	e60eee0e-b69f-4dc7-92db-abc5030f4332	3	1	\N	ho--srv-ad	{"disk": 50, "name": "ho--srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 2, "created": "2019-08-26", "comments": "", "platform": null, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T08:41:41.913Z", "custom_fields": {}, "local_context_data": null}	79	\N	1
4	2019-08-26 04:48:22.888041-04	admin	7b5a55f7-3df5-4d08-a629-a0e3c8335770	1	2	\N	ho-srv-ad	{"disk": 50, "name": "ho-srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 2, "created": "2019-08-26", "comments": "", "platform": null, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T08:48:22.851Z", "custom_fields": {}, "local_context_data": null}	79	\N	1
5	2019-08-26 04:50:47.90313-04	admin	a1dc67ee-bf8a-4547-affa-ddddaf57c740	1	7	\N	Window Server 2012 R2	{"name": "Window Server 2012 R2", "slug": "window-server-2012-r2", "created": "2019-08-26", "napalm_args": null, "last_updated": "2019-08-26T08:50:47.897Z", "manufacturer": null, "napalm_driver": ""}	26	\N	1
6	2019-08-26 04:51:10.613797-04	admin	d7a8b3b7-1fbb-4064-aeba-395e7c461f4a	1	9	\N	Microsoft	{"name": "Microsoft", "slug": "microsoft", "created": "2019-08-26", "last_updated": "2019-08-26T08:51:10.607Z"}	25	\N	1
7	2019-08-26 04:51:18.847798-04	admin	e8d1c29b-bb0b-4bba-8a13-f67f22252c26	2	7	\N	Window Server 2012 R2	{"name": "Window Server 2012 R2", "slug": "window-server-2012-r2", "created": "2019-08-26", "napalm_args": null, "last_updated": "2019-08-26T08:51:18.837Z", "manufacturer": 9, "napalm_driver": ""}	26	\N	1
8	2019-08-26 04:53:45.63792-04	admin	8b3f3c09-1515-4a81-a8ef-dd965836625c	1	8	\N	Centos 7	{"name": "Centos 7", "slug": "centos-7", "created": "2019-08-26", "napalm_args": null, "last_updated": "2019-08-26T08:53:45.634Z", "manufacturer": null, "napalm_driver": ""}	26	\N	1
9	2019-08-26 04:53:50.51932-04	admin	adedeaf0-c9df-46c6-9583-c677a98a235d	1	9	\N	Ubuntu	{"name": "Ubuntu", "slug": "ubuntu", "created": "2019-08-26", "napalm_args": null, "last_updated": "2019-08-26T08:53:50.514Z", "manufacturer": null, "napalm_driver": ""}	26	\N	1
10	2019-08-26 04:55:48.284521-04	admin	1c0607af-69be-4080-9ef1-ed351de401eb	1	4	\N	VPS01	{"name": "VPS01", "site": 1, "tags": [], "type": 2, "group": 1, "created": "2019-08-26", "comments": "", "last_updated": "2019-08-26T08:55:48.268Z", "custom_fields": {}}	76	\N	1
11	2019-08-26 04:55:59.798157-04	admin	b5f5da78-4f35-4613-81b0-ad217b9d85f3	1	5	\N	VPS05	{"name": "VPS05", "site": 1, "tags": [], "type": 2, "group": 1, "created": "2019-08-26", "comments": "", "last_updated": "2019-08-26T08:55:59.777Z", "custom_fields": {}}	76	\N	1
12	2019-08-26 04:57:05.840417-04	admin	b8829cc9-6b3b-4627-8b6c-fdec8833742e	2	2	\N	ho-srv-ad	{"disk": 50, "name": "ho-srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T08:57:05.832Z", "custom_fields": {}, "local_context_data": null}	79	\N	1
13	2019-08-26 05:06:47.384008-04	admin	dc4b933f-cee9-4ea3-a623-10b5bb30fded	1	1	\N	10.1.1.10/24	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/24", "created": "2019-08-26", "dns_name": "10.1.1.20", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-26T09:06:47.340Z", "custom_fields": {}}	47	\N	1
14	2019-08-26 05:07:28.455519-04	admin	a2f897a5-6010-45e6-9a3d-2637398e9c54	3	1	\N	10.1.1.10/24	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/24", "created": "2019-08-26", "dns_name": "10.1.1.20", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-26T09:06:47.340Z", "custom_fields": {}}	47	\N	1
15	2019-08-26 05:08:11.267852-04	admin	d9eae796-3915-47ab-8a8c-39def0247402	1	1	\N	ITD	{"name": "ITD", "slug": "itd", "tags": [], "group": null, "created": "2019-08-26", "comments": "", "description": "", "last_updated": "2019-08-26T09:08:11.244Z", "custom_fields": {}}	73	\N	1
16	2019-08-26 05:08:27.417381-04	admin	4fbf96dd-fb0c-4998-a12c-16287800991b	2	2	\N	ho-srv-ad	{"disk": 50, "name": "ho-srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": 1, "cluster": 5, "created": "2019-08-26", "comments": "", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T09:08:27.404Z", "custom_fields": {}, "local_context_data": null}	79	\N	1
17	2019-08-26 05:11:26.517927-04	admin	97631291-75a1-4f33-b087-9a64dd94a9ee	1	1	\N	Rack01	{"name": "Rack01", "role": null, "site": 1, "tags": [], "type": null, "group": null, "width": 19, "serial": "", "status": 3, "tenant": null, "created": "2019-08-26", "comments": "", "u_height": 42, "asset_tag": null, "desc_units": false, "outer_unit": null, "facility_id": null, "outer_depth": null, "outer_width": null, "last_updated": "2019-08-26T09:11:26.492Z", "custom_fields": {}}	29	\N	1
18	2019-08-26 05:12:49.084574-04	admin	08928010-9132-4454-b418-9532671abe13	2	2	\N	ho-srv-ad	{"disk": 50, "name": "ho-srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T09:12:49.074Z", "custom_fields": {}, "local_context_data": null}	79	\N	1
19	2019-08-26 05:12:58.912257-04	admin	bc3fa428-ff6b-4594-9ee4-eb68a258c4c6	3	1	\N	ITD	{"name": "ITD", "slug": "itd", "tags": [], "group": null, "created": "2019-08-26", "comments": "", "description": "", "last_updated": "2019-08-26T09:08:11.244Z", "custom_fields": {}}	73	\N	1
20	2019-08-26 06:01:30.79148-04	admin	d1b8e97b-6890-4f4a-a398-4a80781505fd	2	2	\N	ho-srv-ad	{"disk": 50, "name": "ho-srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T10:01:30.751Z", "custom_fields": {"Field1": "New Feild"}, "local_context_data": null}	79	\N	1
21	2019-08-26 06:02:50.100682-04	admin	f3222624-2119-483d-9d03-296741b1a94c	2	2	\N	ho-srv-ad	{"disk": 50, "name": "ho-srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "This is comment", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-26T10:02:50.068Z", "custom_fields": {"Field1": "New Feild"}, "local_context_data": null}	79	\N	1
22	2019-08-26 06:23:32.297377-04	admin	1e28ae97-35a6-4f69-a2a0-447a2d578e9f	1	1	\N	20 (Vlan 20)	{"vid": 20, "name": "Vlan 20", "role": null, "site": 1, "tags": [], "group": null, "status": 1, "tenant": null, "created": "2019-08-26", "description": "", "last_updated": "2019-08-26T10:23:32.279Z", "custom_fields": {}}	51	\N	1
23	2019-08-26 06:24:45.401013-04	admin	81ec41cc-4116-46fd-a191-5834ff9bdc46	1	2	\N	24 (Vlan 24)	{"vid": 24, "name": "Vlan 24", "role": 1, "site": 1, "tags": [], "group": null, "status": 1, "tenant": null, "created": "2019-08-26", "description": "", "last_updated": "2019-08-26T10:24:45.377Z", "custom_fields": {}}	51	\N	1
24	2019-08-26 06:25:56.938879-04	admin	6c500c09-7726-4cbe-b3b8-6c859da3950d	2	1	\N	20 (Vlan 20)	{"vid": 20, "name": "Vlan 20", "role": 3, "site": 1, "tags": [], "group": null, "status": 1, "tenant": null, "created": "2019-08-26", "description": "", "last_updated": "2019-08-26T10:25:56.929Z", "custom_fields": {}}	51	\N	1
25	2019-08-26 06:27:45.424658-04	admin	25d21056-5f3f-4149-91b1-df0333772f91	1	6	\N	VPS02	{"name": "VPS02", "site": 1, "tags": [], "type": 2, "group": 1, "created": "2019-08-26", "comments": "", "last_updated": "2019-08-26T10:27:45.405Z", "custom_fields": {"Field1": "None"}}	76	\N	1
26	2019-08-26 06:27:56.099189-04	admin	6e0e056b-9c0a-4f75-a6d6-3b1f305762ef	1	7	\N	VPS03	{"name": "VPS03", "site": 1, "tags": [], "type": 2, "group": 1, "created": "2019-08-26", "comments": "", "last_updated": "2019-08-26T10:27:56.077Z", "custom_fields": {"Field1": "None"}}	76	\N	1
27	2019-08-26 06:28:13.545523-04	admin	18364b0c-0459-4242-99b6-e3ed648023dd	3	3	\N	Microsoft Azure	{"name": "Microsoft Azure", "site": null, "tags": [], "type": 1, "group": 1, "created": "2016-08-01", "comments": "", "last_updated": "2016-08-01T15:22:42.289Z", "custom_fields": {"Field1": "None"}}	76	\N	1
28	2019-08-26 06:28:13.552925-04	admin	18364b0c-0459-4242-99b6-e3ed648023dd	3	2	\N	Amazon EC2	{"name": "Amazon EC2", "site": null, "tags": [], "type": 1, "group": 1, "created": "2016-08-01", "comments": "", "last_updated": "2016-08-01T15:22:42.289Z", "custom_fields": {"Field1": "None"}}	76	\N	1
29	2019-08-26 06:28:13.557793-04	admin	18364b0c-0459-4242-99b6-e3ed648023dd	3	1	\N	Digital Ocean	{"name": "Digital Ocean", "site": null, "tags": [], "type": 1, "group": 1, "created": "2016-08-01", "comments": "", "last_updated": "2016-08-01T15:22:42.289Z", "custom_fields": {"Field1": "None"}}	76	\N	1
30	2019-08-26 06:28:25.41129-04	admin	883f72c0-08d2-469e-9eb4-f454645b76df	1	8	\N	VPS04	{"name": "VPS04", "site": 1, "tags": [], "type": 2, "group": 1, "created": "2019-08-26", "comments": "", "last_updated": "2019-08-26T10:28:25.390Z", "custom_fields": {"Field1": "None"}}	76	\N	1
31	2019-08-26 06:34:02.562971-04	admin	570c27fb-f82c-4df3-bad5-4c4af80fab72	1	1	\N	Server Room F4	{"name": "Server Room F4", "site": 1, "slug": "server-room-f4", "created": "2019-08-26", "last_updated": "2019-08-26T10:34:02.554Z"}	30	\N	1
32	2019-08-26 06:34:48.89836-04	admin	d9a3fbfe-da1b-4430-bda2-eafd1c6ad01c	2	1	\N	Rack01	{"name": "Rack01", "role": null, "site": 1, "tags": [], "type": null, "group": 1, "width": 19, "serial": "", "status": 3, "tenant": null, "created": "2019-08-26", "comments": "", "u_height": 42, "asset_tag": null, "desc_units": false, "outer_unit": null, "facility_id": null, "outer_depth": null, "outer_width": null, "last_updated": "2019-08-26T10:34:48.885Z", "custom_fields": {}}	29	\N	1
33	2019-08-26 06:35:28.053355-04	admin	b008aa96-e4b0-4c58-bbd8-21a514fbd622	1	2	\N	Rack02	{"name": "Rack02", "role": null, "site": 1, "tags": [], "type": null, "group": 1, "width": 19, "serial": "", "status": 3, "tenant": null, "created": "2019-08-26", "comments": "", "u_height": 42, "asset_tag": null, "desc_units": false, "outer_unit": null, "facility_id": null, "outer_depth": null, "outer_width": null, "last_updated": "2019-08-26T10:35:28.035Z", "custom_fields": {}}	29	\N	1
34	2019-08-26 06:36:54.91588-04	admin	c561dd87-d0e3-4608-97c7-0a42e818c369	1	1	\N	DL380 G9	{"slug": "dl380-g9", "tags": [], "model": "DL380 G9", "created": "2019-08-26", "comments": "", "u_height": 2, "part_number": "", "last_updated": "2019-08-26T10:36:54.897Z", "manufacturer": 4, "custom_fields": {}, "is_full_depth": true, "subdevice_role": null}	23	\N	1
35	2019-08-26 06:37:21.059345-04	admin	c20499d9-6f0f-4d0f-b36d-4090dec85590	1	1	\N	DL380 G9	{"face": 0, "name": null, "rack": 1, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-26", "comments": "", "platform": null, "position": 30, "asset_tag": null, "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-26T10:37:21.007Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
36	2019-08-26 20:53:51.512301-04	admin	a61c9819-e63a-4bfb-9b58-d39e574b1e96	2	1	\N	DL380 G9	{"face": 0, "name": null, "rack": 1, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-26", "comments": "", "platform": null, "position": 30, "asset_tag": "Server001", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T00:53:51.476Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
37	2019-08-26 20:54:12.723837-04	admin	8f3ef0f6-2743-4619-949e-66422a34379b	2	1	\N	DL380 G9	{"face": 0, "name": null, "rack": 1, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-26", "comments": "", "platform": null, "position": 30, "asset_tag": null, "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T00:54:12.702Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
38	2019-08-26 20:55:58.825257-04	admin	f4813544-76ab-40f8-8998-4a8c9aadd40c	1	1	1	VPS04	{"name": "VPS04", "tags": [], "device": 1, "parent": null, "serial": "SGH829WLKQ", "part_id": "719064-B21", "asset_tag": "Server005", "discovered": false, "description": "", "manufacturer": 4}	37	21	1
39	2019-08-26 20:56:51.607921-04	admin	2d3753aa-1877-43d1-bc7c-5bbd2870ff76	1	2	1	VPS02	{"name": "VPS02", "tags": [], "device": 1, "parent": null, "serial": "SGH625WANS", "part_id": "719064-B21", "asset_tag": "Server002", "discovered": false, "description": "", "manufacturer": 4}	37	21	1
40	2019-08-26 20:57:34.523969-04	admin	abef1403-8ec1-4c69-a061-ca5bbd2a9614	1	3	1	VPS03	{"name": "VPS03", "tags": [], "device": 1, "parent": null, "serial": "SGH708YBTY", "part_id": "719064-B21", "asset_tag": "Server004", "discovered": false, "description": "", "manufacturer": 4}	37	21	1
41	2019-08-26 20:59:03.850878-04	admin	17388313-e7fb-418e-96d6-9545e1a826a7	2	1	\N	DL380 G9	{"face": null, "name": null, "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-26", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T00:59:03.835Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
42	2019-08-26 21:00:08.066895-04	admin	7fc18d23-1598-457b-88a8-32fd5261c774	1	2	\N	DL380 G10	{"slug": "dl380-g10", "tags": [], "model": "DL380 G10", "created": "2019-08-27", "comments": "", "u_height": 2, "part_number": "", "last_updated": "2019-08-27T01:00:08.045Z", "manufacturer": 4, "custom_fields": {}, "is_full_depth": true, "subdevice_role": null}	23	\N	1
43	2019-08-26 21:00:55.487936-04	admin	03ed88a7-f2dd-44ac-b5e7-c2c92600872c	1	2	\N	DL380 G10	{"face": null, "name": null, "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T01:00:55.436Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
44	2019-08-26 21:18:33.621558-04	admin	6a995429-c429-4de0-816a-c11ab925e09f	1	1	\N	10.1.3.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": null, "family": 4, "prefix": "10.1.3.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:18:33.594Z", "custom_fields": {}}	48	\N	1
45	2019-08-26 21:19:41.962203-04	admin	fa17d63c-4680-4248-a2f3-982d6b1f71eb	1	3	\N	3 (Vlan 03)	{"vid": 3, "name": "Vlan 03", "role": 3, "site": 1, "tags": [], "group": null, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:19:41.806Z", "custom_fields": {}}	51	\N	1
46	2019-08-26 21:20:31.498716-04	admin	759831b2-6944-491c-9af5-aed267a8bc4a	2	1	\N	10.1.3.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": 3, "family": 4, "prefix": "10.1.3.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:20:31.485Z", "custom_fields": {}}	48	\N	1
47	2019-08-26 21:21:15.435927-04	admin	b91b5e65-c4bd-4b21-9172-7d1b3cb2c12b	2	2	\N	ho-srv-ad	{"disk": 50, "name": "ho-srv-ad", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "This is comment", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T01:21:15.410Z", "custom_fields": {"Field1": "New Feild"}, "local_context_data": null}	79	\N	1
48	2019-08-26 21:25:30.18944-04	admin	dc010680-8954-480d-9a5b-93c9d1071049	1	1	\N	ITD	{"name": "ITD", "slug": "itd", "created": "2019-08-27", "last_updated": "2019-08-27T01:25:30.182Z"}	74	\N	1
49	2019-08-26 21:25:34.620911-04	admin	e21d3cd6-5a56-461a-b4c5-30e4d7cab23c	1	2	\N	SDD	{"name": "SDD", "slug": "sdd", "created": "2019-08-27", "last_updated": "2019-08-27T01:25:34.616Z"}	74	\N	1
50	2019-08-26 21:25:57.367572-04	admin	8c7e35c1-30c1-4d8d-a29d-646dc1601dca	1	2	\N	vuongnx	{"name": "vuongnx", "slug": "vuongnx", "tags": [], "group": 1, "created": "2019-08-27", "comments": "", "description": "", "last_updated": "2019-08-27T01:25:57.347Z", "custom_fields": {}}	73	\N	1
51	2019-08-26 21:26:30.317179-04	admin	3ae85111-e582-4300-9c24-a270895d2caa	2	1	\N	10.1.3.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": 3, "family": 4, "prefix": "10.1.3.0/24", "status": 1, "tenant": 2, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:26:30.305Z", "custom_fields": {}}	48	\N	1
52	2019-08-26 21:26:50.113142-04	admin	9ec99da3-29f1-448a-9fe9-0070b3ada293	1	1	\N	ITD	{"name": "ITD", "site": 1, "slug": "itd", "created": "2019-08-27", "last_updated": "2019-08-27T01:26:50.105Z"}	53	\N	1
53	2019-08-26 21:26:54.253031-04	admin	6bb3aad6-cf0c-41a3-b5b3-8b63abd1c731	1	2	\N	SDD	{"name": "SDD", "site": 1, "slug": "sdd", "created": "2019-08-27", "last_updated": "2019-08-27T01:26:54.247Z"}	53	\N	1
54	2019-08-26 21:27:04.569245-04	admin	66fc864f-d5f6-4392-9257-8932e7401395	1	3	\N	DOC1	{"name": "DOC1", "site": 1, "slug": "doc1", "created": "2019-08-27", "last_updated": "2019-08-27T01:27:04.565Z"}	53	\N	1
55	2019-08-26 21:27:36.865536-04	admin	eca00b67-8ad6-4599-ae21-b50dd6d44c9f	2	1	\N	20 (Vlan 20)	{"vid": 20, "name": "Vlan 20", "role": 3, "site": 1, "tags": [], "group": 1, "status": 1, "tenant": null, "created": "2019-08-26", "description": "", "last_updated": "2019-08-27T01:27:36.850Z", "custom_fields": {}}	51	\N	1
56	2019-08-26 21:28:11.416707-04	admin	8f1b4f6b-c80b-4f2e-a905-3037f5b72be1	1	4	\N	Vlan Management ESXi	{"name": "Vlan Management ESXi", "site": 1, "slug": "vlan-management-esxi", "created": "2019-08-27", "last_updated": "2019-08-27T01:28:11.412Z"}	53	\N	1
57	2019-08-26 21:28:23.158919-04	admin	c78638cc-129b-43b5-b501-145a62d7c4f3	2	3	\N	3 (Vlan 03)	{"vid": 3, "name": "Vlan 03", "role": 3, "site": 1, "tags": [], "group": 4, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:28:23.144Z", "custom_fields": {}}	51	\N	1
58	2019-08-26 21:29:23.233533-04	admin	1e198a14-6140-4abc-b891-fba41c4afbad	1	2	\N	10.1.20.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": 1, "family": 4, "prefix": "10.1.20.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:29:23.214Z", "custom_fields": {}}	48	\N	1
59	2019-08-26 21:29:43.702354-04	admin	fe51d59e-a247-4296-add4-9d2a6b8240d7	2	1	\N	10.1.3.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": 3, "family": 4, "prefix": "10.1.3.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:29:43.691Z", "custom_fields": {}}	48	\N	1
60	2019-08-26 21:30:52.09238-04	admin	eb03ac84-777f-44ab-bcf7-7748dc9bb963	2	2	\N	HO-SRV-AD	{"disk": 50, "name": "HO-SRV-AD", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "This is comment", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T01:30:52.060Z", "custom_fields": {"Field1": "New Feild"}, "local_context_data": null}	79	\N	1
61	2019-08-26 21:31:35.245776-04	admin	2de0a390-5da0-45d1-91ef-a63135ddab3b	1	3	\N	10.1.24.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 2, "family": 4, "prefix": "10.1.24.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:31:35.229Z", "custom_fields": {}}	48	\N	1
62	2019-08-26 21:32:13.329642-04	admin	6ad2ce7f-62eb-4580-b616-9fe654361a45	2	3	\N	Pro1	{"name": "Pro1", "site": 1, "slug": "pro1", "created": "2019-08-27", "last_updated": "2019-08-27T01:32:13.324Z"}	53	\N	1
63	2019-08-26 21:33:01.778183-04	admin	c8228524-5b34-4ee7-b58c-8540702e85f3	3	3	\N	Pro1	{"name": "Pro1", "site": 1, "slug": "pro1", "created": "2019-08-27", "last_updated": "2019-08-27T01:32:13.324Z"}	53	\N	1
64	2019-08-26 21:33:16.329496-04	admin	b2a28664-d838-4581-b52a-e2877938e67d	1	5	\N	Pro1	{"name": "Pro1", "site": 1, "slug": "pro1", "created": "2019-08-27", "last_updated": "2019-08-27T01:33:16.325Z"}	53	\N	1
65	2019-08-26 21:33:37.130655-04	admin	022fad29-9584-49b8-8f96-d1db51686835	2	2	\N	24 (Vlan 24)	{"vid": 24, "name": "Vlan 24", "role": 1, "site": 1, "tags": [], "group": 5, "status": 1, "tenant": null, "created": "2019-08-26", "description": "", "last_updated": "2019-08-27T01:33:37.117Z", "custom_fields": {}}	51	\N	1
66	2019-08-26 21:34:47.845184-04	admin	08b60248-b0f2-4b70-9542-aa11c191cfea	1	3	\N	DOC1	{"name": "DOC1", "slug": "doc1", "created": "2019-08-27", "last_updated": "2019-08-27T01:34:47.840Z"}	74	\N	1
67	2019-08-26 21:34:53.336611-04	admin	89451983-adaf-4d26-8932-1bf18f9209f5	1	4	\N	DOC2	{"name": "DOC2", "slug": "doc2", "created": "2019-08-27", "last_updated": "2019-08-27T01:34:53.330Z"}	74	\N	1
68	2019-08-26 21:34:57.325299-04	admin	fe9438af-64af-4cd0-8b1e-87bfa167f48f	1	5	\N	TSA	{"name": "TSA", "slug": "tsa", "created": "2019-08-27", "last_updated": "2019-08-27T01:34:57.320Z"}	74	\N	1
69	2019-08-26 21:35:05.067567-04	admin	642586ca-be8a-4323-9172-f0d23eb29f50	1	6	\N	QMD	{"name": "QMD", "slug": "qmd", "created": "2019-08-27", "last_updated": "2019-08-27T01:35:05.061Z"}	74	\N	1
70	2019-08-26 21:36:11.520858-04	admin	5edadc1d-0e00-4a3a-b6a6-2496cbfe9b8d	1	4	\N	21 (Vlan 21)	{"vid": 21, "name": "Vlan 21", "role": 2, "site": 1, "tags": [], "group": 2, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:36:11.502Z", "custom_fields": {}}	51	\N	1
71	2019-08-26 21:36:37.904574-04	admin	6c3c73a3-16e7-46ac-8ed1-7df2e2b4ea02	1	4	\N	10.1.21.0/24	{"vrf": null, "role": 2, "site": 1, "tags": [], "vlan": 4, "family": 4, "prefix": "10.1.21.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:36:37.886Z", "custom_fields": {}}	48	\N	1
72	2019-08-26 21:43:10.242285-04	admin	af498860-edc9-4351-99f2-8680a8373df2	1	7	\N	OMD	{"name": "OMD", "slug": "omd", "created": "2019-08-27", "last_updated": "2019-08-27T01:43:10.239Z"}	74	\N	1
73	2019-08-26 21:43:38.073957-04	admin	a0952560-87bc-42d8-a6ff-e78f0f9a8e99	1	6	\N	Pro2	{"name": "Pro2", "site": 1, "slug": "pro2", "created": "2019-08-27", "last_updated": "2019-08-27T01:43:38.069Z"}	53	\N	1
74	2019-08-26 21:43:59.547742-04	admin	c79c50d1-cdf4-43d3-b8a7-ccac287304a4	1	7	\N	Pro3	{"name": "Pro3", "site": 1, "slug": "pro3", "created": "2019-08-27", "last_updated": "2019-08-27T01:43:59.542Z"}	53	\N	1
75	2019-08-26 21:44:09.768359-04	admin	7f3dc5c3-e18e-4364-90c9-72d7871b49f3	1	8	\N	QMD	{"name": "QMD", "site": 1, "slug": "qmd", "created": "2019-08-27", "last_updated": "2019-08-27T01:44:09.761Z"}	53	\N	1
76	2019-08-26 21:44:16.770029-04	admin	79255f3c-45c3-46bd-839f-1e691816d18f	1	9	\N	OMD	{"name": "OMD", "site": 1, "slug": "omd", "created": "2019-08-27", "last_updated": "2019-08-27T01:44:16.762Z"}	53	\N	1
77	2019-08-26 21:44:44.874706-04	admin	05a2971f-09dd-49d9-bc25-58ec2c83f949	1	10	\N	Vlan 27	{"name": "Vlan 27", "site": 1, "slug": "vlan-27", "created": "2019-08-27", "last_updated": "2019-08-27T01:44:44.868Z"}	53	\N	1
78	2019-08-26 21:45:18.033736-04	admin	2f3cce38-43f7-4f83-bbe5-c7a11048885c	1	5	\N	22 (Vlan 22)	{"vid": 22, "name": "Vlan 22", "role": 3, "site": 1, "tags": [], "group": 9, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:45:18.013Z", "custom_fields": {}}	51	\N	1
79	2019-08-26 21:46:04.373828-04	admin	43fa80f8-e060-465f-8ea1-fd20253594ea	1	5	\N	10.1.22.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": 5, "family": 4, "prefix": "10.1.22.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:46:04.354Z", "custom_fields": {}}	48	\N	1
80	2019-08-26 21:46:26.827969-04	admin	47fa33bb-8fdf-45de-b0fd-030bc529718d	1	6	\N	10.1.23.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": null, "family": 4, "prefix": "10.1.23.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:46:26.808Z", "custom_fields": {}}	48	\N	1
81	2019-08-26 21:47:06.915768-04	admin	3ea3907f-9b1f-4029-9cef-023119f9a68a	1	6	\N	23 (Vlan 23)	{"vid": 23, "name": "Vlan 23", "role": 3, "site": 1, "tags": [], "group": 8, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:47:06.897Z", "custom_fields": {}}	51	\N	1
82	2019-08-26 21:47:24.547385-04	admin	8800a05b-7110-47ad-abd0-f2253d3a0e2c	2	6	\N	10.1.23.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": 6, "family": 4, "prefix": "10.1.23.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:47:24.534Z", "custom_fields": {}}	48	\N	1
83	2019-08-26 21:48:49.979191-04	admin	eaf94f05-4831-4b2e-9bea-80971153a0d0	1	7	\N	25 (Vlan 25)	{"vid": 25, "name": "Vlan 25", "role": 1, "site": 1, "tags": [], "group": 6, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:48:49.961Z", "custom_fields": {}}	51	\N	1
84	2019-08-26 21:49:07.44531-04	admin	5d6a2fa2-a124-4abe-bc1d-5a8541dd13e2	1	8	\N	26 (Vlan 26)	{"vid": 26, "name": "Vlan 26", "role": 1, "site": 1, "tags": [], "group": 7, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:49:07.426Z", "custom_fields": {}}	51	\N	1
85	2019-08-26 21:49:26.78011-04	admin	fac1b677-f124-47b9-8783-0586b7fff357	1	9	\N	27 (Vlan 27)	{"vid": 27, "name": "Vlan 27", "role": 1, "site": 1, "tags": [], "group": 10, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:49:26.759Z", "custom_fields": {}}	51	\N	1
86	2019-08-26 21:50:05.360802-04	admin	85b67be1-618d-42fe-9664-da6e860623c6	1	7	\N	10.1.25.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 7, "family": 4, "prefix": "10.1.25.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:50:05.340Z", "custom_fields": {}}	48	\N	1
87	2019-08-26 21:50:23.299344-04	admin	eaccdb8a-9d36-46db-92cc-dd18c6c2e2d2	1	8	\N	10.1.26.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 8, "family": 4, "prefix": "10.1.26.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:50:23.276Z", "custom_fields": {}}	48	\N	1
88	2019-08-26 21:50:39.317928-04	admin	dc88b89d-3c5c-4eae-ac08-144734ef90e4	1	9	\N	10.1.27.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 9, "family": 4, "prefix": "10.1.27.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:50:39.298Z", "custom_fields": {}}	48	\N	1
89	2019-08-26 21:51:14.047404-04	admin	d662a4e4-ba60-4720-b56b-e04351033cf0	1	11	\N	DMZ	{"name": "DMZ", "site": 1, "slug": "dmz", "created": "2019-08-27", "last_updated": "2019-08-27T01:51:14.038Z"}	53	\N	1
90	2019-08-26 21:52:22.301615-04	admin	2efdad89-cefb-4bc0-b624-6267c039c742	1	5	\N	DMZ Zone	{"name": "DMZ Zone", "slug": "dmz-zone", "weight": 1000, "created": "2019-08-27", "last_updated": "2019-08-27T01:52:22.294Z"}	50	\N	1
91	2019-08-26 21:52:51.799929-04	admin	50c46b12-9fab-4555-bf56-8f7ff6498059	1	10	\N	172.16.1.0/24	{"vrf": null, "role": 5, "site": 1, "tags": [], "vlan": null, "family": 4, "prefix": "172.16.1.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:52:51.781Z", "custom_fields": {}}	48	\N	1
92	2019-08-26 21:53:19.05995-04	admin	a18d4f74-d59e-41c8-a5aa-c027f5f40ffe	1	10	\N	172 (Vlan DMZ)	{"vid": 172, "name": "Vlan DMZ", "role": 5, "site": 1, "tags": [], "group": 11, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:53:19.041Z", "custom_fields": {}}	51	\N	1
93	2019-08-26 21:53:36.942727-04	admin	7365eb09-fca3-4903-9a20-21e7701e257e	2	10	\N	172.16.1.0/24	{"vrf": null, "role": 5, "site": 1, "tags": [], "vlan": 10, "family": 4, "prefix": "172.16.1.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:53:36.930Z", "custom_fields": {}}	48	\N	1
94	2019-08-26 21:55:52.652278-04	admin	6ca79c83-102a-40ca-8eef-862a50131506	1	12	\N	SRV	{"name": "SRV", "site": 1, "slug": "srv", "created": "2019-08-27", "last_updated": "2019-08-27T01:55:52.646Z"}	53	\N	1
95	2019-08-26 21:56:17.537064-04	admin	257de3cc-6542-4414-8441-cd7df8694684	1	11	\N	1 (Vlan SRV)	{"vid": 1, "name": "Vlan SRV", "role": 1, "site": 1, "tags": [], "group": 12, "status": 1, "tenant": null, "created": "2019-08-27", "description": "", "last_updated": "2019-08-27T01:56:17.514Z", "custom_fields": {}}	51	\N	1
96	2019-08-26 21:56:43.430599-04	admin	f65e64e3-a2a8-4ebd-ba72-b5b74b5ac0e7	1	11	\N	10.1.1.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 11, "family": 4, "prefix": "10.1.1.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": false, "description": "", "last_updated": "2019-08-27T01:56:43.411Z", "custom_fields": {}}	48	\N	1
97	2019-08-26 21:59:42.137803-04	admin	fc2f1ec1-147f-4e79-8186-94d8d41e6089	2	9	\N	OMD - HRD	{"name": "OMD - HRD", "site": 1, "slug": "omd-hrd", "created": "2019-08-27", "last_updated": "2019-08-27T01:59:42.132Z"}	53	\N	1
98	2019-08-26 22:01:14.94238-04	admin	b6b5ead6-2b79-44b6-b7ef-a14f53092607	2	9	\N	10.1.27.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 9, "family": 4, "prefix": "10.1.27.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": true, "description": "", "last_updated": "2019-08-27T02:01:14.930Z", "custom_fields": {}}	48	\N	1
99	2019-08-26 22:05:01.419485-04	admin	468edbb1-920c-4936-a791-566f461be260	2	1	\N	H3	{"asn": null, "name": "H3", "slug": "h3", "tags": [], "region": null, "status": 1, "tenant": null, "created": "2019-08-26", "comments": "", "facility": "", "latitude": null, "longitude": null, "time_zone": "Asia/Ho_Chi_Minh", "description": "", "contact_name": "", "last_updated": "2019-08-27T02:05:01.401Z", "contact_email": "info@saigonbpo.vn", "contact_phone": "+84 28 7300 8184", "custom_fields": {}, "physical_address": "4th Floor, H3 Building, 384 Hoang Dieu, Ward 6, District 4, Ho Chi Minh City, Vietnam", "shipping_address": ""}	31	\N	1
100	2019-08-26 22:09:08.726356-04	admin	4ba1b1b7-c323-4b79-ae95-73068e2cc317	3	4	\N	Backup	{"name": "Backup", "slug": "backup", "weight": 1000, "created": null, "last_updated": null}	50	\N	1
101	2019-08-26 22:10:53.784241-04	admin	16029fc9-98ce-4f9f-90c9-8591c4871960	1	2	\N	10.1.1.10/24	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/24", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:10:53.753Z", "custom_fields": {}}	47	\N	1
102	2019-08-26 22:11:36.622983-04	admin	95e1e0a1-bcb7-4dbe-acf6-7ed437bc66cd	2	11	\N	10.1.1.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 11, "family": 4, "prefix": "10.1.1.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": true, "description": "", "last_updated": "2019-08-27T02:11:36.612Z", "custom_fields": {}}	48	\N	1
103	2019-08-26 22:12:28.833928-04	admin	80b7440d-5d5a-4570-984e-d795569cc60c	2	2	\N	10.1.1.10/24	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/24", "created": "2019-08-27", "dns_name": "saigonbpo.in", "interface": null, "nat_inside": 2, "description": "", "last_updated": "2019-08-27T02:12:28.819Z", "custom_fields": {}}	47	\N	1
104	2019-08-26 22:12:50.811693-04	admin	b49ee860-2d9f-4e38-a7e6-a93ab9a50bce	2	2	\N	10.1.1.10/24	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/24", "created": "2019-08-27", "dns_name": "saigonbpo.in", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:12:50.799Z", "custom_fields": {}}	47	\N	1
105	2019-08-26 22:24:10.861903-04	admin	4288c310-8657-4f47-af42-79d93c9ab622	2	2	\N	10.1.1.10/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/32", "created": "2019-08-27", "dns_name": "saigonbpo.in", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:24:10.847Z", "custom_fields": {}}	47	\N	1
106	2019-08-26 22:47:18.364306-04	admin	5d08be27-90de-4f4c-a205-85b7de7277a3	2	2	\N	10.1.1.10/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:47:18.345Z", "custom_fields": {}}	47	\N	1
107	2019-08-26 22:48:47.11127-04	admin	9d0dec23-f247-4cf1-a40e-5c001a98a0d2	1	3	\N	10.1.1.20/21	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.20/21", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:48:47.092Z", "custom_fields": {}}	47	\N	1
108	2019-08-26 22:49:16.507081-04	admin	626e2602-1054-47ca-8e51-94a8bdbecabe	2	3	\N	10.1.1.20/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.20/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:49:16.494Z", "custom_fields": {}}	47	\N	1
109	2019-08-27 00:19:20.966364-04	admin	103876aa-31f7-4db7-a522-c71a72b5389f	1	4	\N	10.1.27.1/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.1/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:19:20.932Z", "custom_fields": {}}	47	\N	1
110	2019-08-27 00:19:20.969993-04	admin	103876aa-31f7-4db7-a522-c71a72b5389f	1	1	\N	auto	{"name": "auto", "slug": "auto", "color": "9e9e9e", "created": "2019-08-27", "comments": "", "last_updated": "2019-08-27T04:19:20.940Z"}	66	\N	1
111	2019-08-27 00:19:21.040112-04	admin	844d3554-6c1d-4d2c-ba0c-a37214453205	1	5	\N	10.1.27.10/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.10/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:19:21.014Z", "custom_fields": {}}	47	\N	1
112	2019-08-27 00:19:21.108849-04	admin	c2fe0a20-2880-41c0-8e8f-17a8f2ab39fd	1	6	\N	10.1.27.100/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.100/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.085Z", "custom_fields": {}}	47	\N	1
113	2019-08-27 00:19:21.176001-04	admin	c417ec5c-a059-4667-9fcd-6fea469253fc	1	7	\N	10.1.27.111/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.111/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:19:21.152Z", "custom_fields": {}}	47	\N	1
114	2019-08-27 00:19:21.243654-04	admin	da259b5b-35db-4267-b8bf-a14fb3f9506a	1	8	\N	10.1.27.201/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.201/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.220Z", "custom_fields": {}}	47	\N	1
115	2019-08-27 00:19:21.312838-04	admin	31f6af3d-268e-4a35-9902-30797d4b8de8	1	9	\N	10.1.27.202/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.202/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.286Z", "custom_fields": {}}	47	\N	1
116	2019-08-27 00:19:21.381407-04	admin	d4dbed5b-94b1-42d0-9043-5e656102363f	1	10	\N	10.1.27.203/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.203/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.357Z", "custom_fields": {}}	47	\N	1
117	2019-08-27 00:19:21.448213-04	admin	418a57de-d751-4ec8-b84a-22b91c68ca27	1	11	\N	10.1.27.204/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.204/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.424Z", "custom_fields": {}}	47	\N	1
118	2019-08-27 00:19:21.518789-04	admin	72c3438b-e954-4fad-80ab-c0b60e02567d	1	12	\N	10.1.27.205/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.205/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.495Z", "custom_fields": {}}	47	\N	1
119	2019-08-27 00:19:21.587165-04	admin	6cf9de2d-eb3c-45fd-a0d3-801063fedc49	1	13	\N	10.1.27.206/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.206/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.562Z", "custom_fields": {}}	47	\N	1
120	2019-08-27 00:19:21.653292-04	admin	74c7b980-bf6e-4343-a7c9-fbd9ed133669	1	14	\N	10.1.27.207/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.207/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.629Z", "custom_fields": {}}	47	\N	1
121	2019-08-27 00:19:21.723224-04	admin	59844f3a-52c2-47ee-b6fd-df19b18a830b	1	15	\N	10.1.27.208/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.208/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.698Z", "custom_fields": {}}	47	\N	1
122	2019-08-27 00:19:21.793449-04	admin	64cbee00-6ad5-4173-9bb1-4b3ad3b55af9	1	16	\N	10.1.27.209/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.209/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.768Z", "custom_fields": {}}	47	\N	1
123	2019-08-27 00:19:21.869371-04	admin	a1fb4eed-7947-4fa7-bf5b-49667287a323	1	17	\N	10.1.27.210/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.210/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.845Z", "custom_fields": {}}	47	\N	1
124	2019-08-27 00:19:21.947396-04	admin	f8496cf6-02cf-4c2c-b9ef-9dd8f7b3e8a8	1	18	\N	10.1.27.211/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.211/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.921Z", "custom_fields": {}}	47	\N	1
125	2019-08-27 00:19:22.01806-04	admin	cbcf4e22-c238-46fe-9ae0-1c0c5fb0df74	1	19	\N	10.1.27.212/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.212/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.995Z", "custom_fields": {}}	47	\N	1
126	2019-08-27 00:19:22.085388-04	admin	0ee81843-741b-4076-a8a9-389095a42b34	1	20	\N	10.1.27.213/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.213/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.061Z", "custom_fields": {}}	47	\N	1
127	2019-08-27 00:19:22.15712-04	admin	d1a706e0-b7ba-489f-9400-898a8876b0c2	1	21	\N	10.1.27.214/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.214/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.134Z", "custom_fields": {}}	47	\N	1
128	2019-08-27 00:19:22.223518-04	admin	00812189-7bb3-4e23-941b-c66bb5b6124a	1	22	\N	10.1.27.215/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.215/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.200Z", "custom_fields": {}}	47	\N	1
129	2019-08-27 00:19:22.289966-04	admin	4178b445-e155-4ef6-b8f7-e22e3bd6fa4c	1	23	\N	10.1.27.216/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.216/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.266Z", "custom_fields": {}}	47	\N	1
130	2019-08-27 00:19:22.359194-04	admin	cec4de59-02b3-495a-aa27-97029a5ab758	1	24	\N	10.1.27.217/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.217/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.335Z", "custom_fields": {}}	47	\N	1
131	2019-08-27 00:19:22.426208-04	admin	af519e58-50b8-4f1b-90de-8d4c0fb4ecf9	1	25	\N	10.1.27.218/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.218/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.402Z", "custom_fields": {}}	47	\N	1
132	2019-08-27 00:19:22.494388-04	admin	cfa52921-cc4e-429e-8ae5-ffef3b67e44e	1	26	\N	10.1.27.219/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.219/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.470Z", "custom_fields": {}}	47	\N	1
133	2019-08-27 00:19:22.560274-04	admin	00b91d70-e496-420e-bd3a-d28fdda24ef0	1	27	\N	10.1.27.22/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.22/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.537Z", "custom_fields": {}}	47	\N	1
134	2019-08-27 00:19:22.631774-04	admin	b4e6a0dc-7096-459e-b80e-481e849d4bdc	1	28	\N	10.1.27.220/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.220/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.604Z", "custom_fields": {}}	47	\N	1
135	2019-08-27 00:19:22.698927-04	admin	fb60ea60-81b5-4590-ba82-52419c89c87a	1	29	\N	10.1.27.221/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.221/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.675Z", "custom_fields": {}}	47	\N	1
136	2019-08-27 00:19:22.767732-04	admin	306a3607-c009-4700-a124-e1bf06d6dc77	1	30	\N	10.1.27.222/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.222/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.744Z", "custom_fields": {}}	47	\N	1
137	2019-08-27 00:19:22.834099-04	admin	7e4c0408-4867-4793-a6bd-12e00fc6f170	1	31	\N	10.1.27.223/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.223/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.811Z", "custom_fields": {}}	47	\N	1
138	2019-08-27 00:19:22.901256-04	admin	0486ce59-2bcf-429e-9ba5-2d73f59f2ae7	1	32	\N	10.1.27.224/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.224/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.878Z", "custom_fields": {}}	47	\N	1
139	2019-08-27 00:19:22.969297-04	admin	f0158a59-a766-4c58-a53b-40b6c52f2b4e	1	33	\N	10.1.27.225/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.225/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.944Z", "custom_fields": {}}	47	\N	1
140	2019-08-27 00:19:23.035803-04	admin	c147dee7-4edf-40a1-9374-52e4177d437f	1	34	\N	10.1.27.226/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.226/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.012Z", "custom_fields": {}}	47	\N	1
141	2019-08-27 00:19:23.103581-04	admin	c6ffe1b2-46e6-4d7a-95ea-3e6417cf922e	1	35	\N	10.1.27.227/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.227/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.079Z", "custom_fields": {}}	47	\N	1
142	2019-08-27 00:19:23.172373-04	admin	69f6b94b-4715-48af-ac20-d94829fab9de	1	36	\N	10.1.27.228/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.228/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.148Z", "custom_fields": {}}	47	\N	1
143	2019-08-27 00:19:23.239436-04	admin	ee2d7c92-1687-4820-889c-cc2e03a9e5d0	1	37	\N	10.1.27.229/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.229/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.215Z", "custom_fields": {}}	47	\N	1
144	2019-08-27 00:19:23.306614-04	admin	2c562b60-608d-4c6f-bb4c-bff2fd52b1fd	1	38	\N	10.1.27.23/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.23/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.283Z", "custom_fields": {}}	47	\N	1
145	2019-08-27 00:19:23.373395-04	admin	cf703be8-57b8-464f-9ebc-1cb1ebcc3b28	1	39	\N	10.1.27.230/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.230/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.350Z", "custom_fields": {}}	47	\N	1
146	2019-08-27 00:19:23.441058-04	admin	37459f28-395b-41b7-a98c-fc0ca6879c42	1	40	\N	10.1.27.24/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.24/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.417Z", "custom_fields": {}}	47	\N	1
147	2019-08-27 00:19:23.509296-04	admin	5756e57d-2db3-43a5-aac3-831d6b261d84	1	41	\N	10.1.27.25/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.25/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.485Z", "custom_fields": {}}	47	\N	1
148	2019-08-27 00:19:23.578663-04	admin	cc0bb4e2-6549-4df1-943c-b1fa1fd303d0	1	42	\N	10.1.27.70/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.70/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:19:23.555Z", "custom_fields": {}}	47	\N	1
149	2019-08-27 00:19:23.651872-04	admin	61f80767-b787-4079-b70b-419e310f739b	1	43	\N	10.1.27.71/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.71/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.627Z", "custom_fields": {}}	47	\N	1
150	2019-08-27 00:19:23.722684-04	admin	1c019b01-a601-4e36-b47e-15888fd4aa47	1	44	\N	10.1.27.72/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.72/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.695Z", "custom_fields": {}}	47	\N	1
151	2019-08-27 00:19:23.794082-04	admin	86fc73cb-e3ba-40c3-9e1e-b161f397922b	1	45	\N	10.1.27.73/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.73/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:23.768Z", "custom_fields": {}}	47	\N	1
152	2019-08-27 00:19:23.861185-04	admin	fe2173e6-53f7-494f-833e-766070464c46	1	46	\N	10.1.27.74/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.74/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.837Z", "custom_fields": {}}	47	\N	1
153	2019-08-27 00:19:23.929992-04	admin	9ccc6a32-0fdf-43cd-aee6-ebb0e82c04e0	1	47	\N	10.1.27.75/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.75/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.905Z", "custom_fields": {}}	47	\N	1
154	2019-08-27 00:19:24.00041-04	admin	4b6b33f3-d659-4f40-8ce6-41e0c1fae805	1	48	\N	10.1.27.76/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.76/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.977Z", "custom_fields": {}}	47	\N	1
155	2019-08-27 00:19:24.066876-04	admin	231f1b33-5781-411a-9084-232169888323	1	49	\N	10.1.27.77/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.77/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.043Z", "custom_fields": {}}	47	\N	1
156	2019-08-27 00:19:24.135531-04	admin	0a8c276e-e180-4806-b620-75f5c3dbf58c	1	50	\N	10.1.27.78/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.78/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.110Z", "custom_fields": {}}	47	\N	1
157	2019-08-27 00:19:24.202159-04	admin	40bb5fee-25f0-495d-b239-5aed5f686451	1	51	\N	10.1.27.79/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.79/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:24.178Z", "custom_fields": {}}	47	\N	1
158	2019-08-27 00:19:24.270064-04	admin	d921f5d2-3e6c-4dfd-a4ee-7ca9471e8221	1	52	\N	10.1.27.80/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.80/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:24.246Z", "custom_fields": {}}	47	\N	1
159	2019-08-27 00:19:24.337207-04	admin	4770f316-12d1-47d8-9194-8475f9c75e5e	1	53	\N	10.1.27.81/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.81/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.313Z", "custom_fields": {}}	47	\N	1
160	2019-08-27 00:19:24.405945-04	admin	209b9ba8-2254-43c1-9019-092663fc7ce5	1	54	\N	10.1.27.82/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.82/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.383Z", "custom_fields": {}}	47	\N	1
161	2019-08-27 00:19:24.472551-04	admin	f992d8b1-e1c7-4c2c-8610-de46f89ceae6	1	55	\N	10.1.27.83/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.83/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.449Z", "custom_fields": {}}	47	\N	1
162	2019-08-27 00:19:24.538757-04	admin	04df2ad9-b974-483d-8574-9f36b3e71a76	1	56	\N	10.1.27.84/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.84/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.515Z", "custom_fields": {}}	47	\N	1
163	2019-08-27 00:19:24.607677-04	admin	197ccc2c-5206-40e1-8b89-3eeef51a24c2	1	57	\N	10.1.27.85/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.85/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.582Z", "custom_fields": {}}	47	\N	1
164	2019-08-27 00:19:24.675363-04	admin	da40c5cb-88ea-405e-a8c9-53b65eeb980b	1	58	\N	10.1.27.86/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.86/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.651Z", "custom_fields": {}}	47	\N	1
165	2019-08-27 00:19:24.742122-04	admin	7d68b9e8-d374-42fa-acb8-973ecd0d5b43	1	59	\N	10.1.27.87/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.87/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.718Z", "custom_fields": {}}	47	\N	1
166	2019-08-27 00:19:24.81049-04	admin	5f68852f-cb39-4c65-8bc2-db49b3e45e18	1	60	\N	10.1.27.88/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.88/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.787Z", "custom_fields": {}}	47	\N	1
167	2019-08-27 00:19:24.877198-04	admin	6a42d245-79b8-4a50-b008-9bc593413b5f	1	61	\N	10.1.27.89/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.89/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.854Z", "custom_fields": {}}	47	\N	1
168	2019-08-27 00:19:24.943892-04	admin	07274710-023a-4f45-a8aa-e52ed3e120d6	1	62	\N	10.1.27.90/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.90/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.920Z", "custom_fields": {}}	47	\N	1
169	2019-08-27 00:19:25.010432-04	admin	8513fe92-04fd-4162-9f1e-c9642ed5e6a2	1	63	\N	10.1.27.91/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.91/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:24.987Z", "custom_fields": {}}	47	\N	1
170	2019-08-27 00:19:25.077457-04	admin	df6a3e8a-1937-4818-b8d5-71a507f93346	1	64	\N	10.1.27.92/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.92/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:25.053Z", "custom_fields": {}}	47	\N	1
171	2019-08-27 00:19:25.148857-04	admin	d638dd1e-6a15-47bf-ae14-f91c0627a1f6	1	65	\N	10.1.27.93/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.93/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.123Z", "custom_fields": {}}	47	\N	1
172	2019-08-27 00:19:25.220063-04	admin	fddf132f-b501-4b36-b45c-4a967bdb5bab	1	66	\N	10.1.27.94/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.94/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.195Z", "custom_fields": {}}	47	\N	1
173	2019-08-27 00:19:25.287512-04	admin	dd1437a3-403d-4f05-a14c-91d18e4cf735	1	67	\N	10.1.27.95/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.95/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.264Z", "custom_fields": {}}	47	\N	1
174	2019-08-27 00:19:25.355786-04	admin	ccc11f3a-d5dc-4bd1-8cef-bf2303d83083	1	68	\N	10.1.27.96/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.96/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.331Z", "custom_fields": {}}	47	\N	1
175	2019-08-27 00:19:25.426502-04	admin	fda4e404-e4a7-439f-a610-2f31d259535b	1	69	\N	10.1.27.97/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.97/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.399Z", "custom_fields": {}}	47	\N	1
176	2019-08-27 00:19:25.495939-04	admin	2213f24d-16f8-471c-8e99-7e6c357b350c	1	70	\N	10.1.27.98/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.98/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.471Z", "custom_fields": {}}	47	\N	1
177	2019-08-27 00:19:25.562547-04	admin	8c15d5ff-fa63-48a8-8ded-6c33140b57ff	1	71	\N	10.1.27.99/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.99/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.539Z", "custom_fields": {}}	47	\N	1
178	2019-08-27 00:21:10.318906-04	admin	3bd11b0e-1398-4719-ac07-f440c8659530	3	3	\N	10.1.1.20/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.20/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:49:16.494Z", "custom_fields": {}}	47	\N	1
179	2019-08-27 00:21:10.324445-04	admin	3bd11b0e-1398-4719-ac07-f440c8659530	3	2	\N	10.1.1.10/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "", "last_updated": "2019-08-27T02:47:18.345Z", "custom_fields": {}}	47	\N	1
180	2019-08-27 00:23:52.872871-04	admin	c0b5dd72-8103-40a0-abb3-c563c944cd9e	2	36	\N	10.1.27.228/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.228/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:23:52.864Z", "custom_fields": {}}	47	\N	1
181	2019-08-27 00:23:53.30477-04	admin	36e3efdf-be8e-45f3-a14b-3a4922bc0792	2	45	\N	10.1.27.73/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.73/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:23:53.296Z", "custom_fields": {}}	47	\N	1
182	2019-08-27 00:23:53.483208-04	admin	6ce0c36a-fe72-4dc9-8de3-6eedc8b86a63	2	48	\N	10.1.27.76/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.76/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:23:53.475Z", "custom_fields": {}}	47	\N	1
183	2019-08-27 00:23:53.576462-04	admin	129e7912-4322-49ad-9bc1-1a73257a0422	2	49	\N	10.1.27.77/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.77/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:23:53.568Z", "custom_fields": {}}	47	\N	1
184	2019-08-27 00:23:53.713296-04	admin	0d8974e8-87a6-4cf4-be7f-59ed7ee5138e	2	51	\N	10.1.27.79/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.79/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:23:53.705Z", "custom_fields": {}}	47	\N	1
185	2019-08-27 00:26:19.164799-04	admin	1e9ec90a-ac5e-4c79-a076-bd1dac8cc11b	1	72	\N	10.1.24.1/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.1/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:26:19.139Z", "custom_fields": {}}	47	\N	1
186	2019-08-27 00:26:19.231907-04	admin	4ff30d19-8f31-49aa-a7ef-6e2c48902954	1	73	\N	10.1.24.100/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.100/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "linux.linux_kernel.2.6.13", "last_updated": "2019-08-27T04:26:19.208Z", "custom_fields": {}}	47	\N	1
187	2019-08-27 00:26:19.30067-04	admin	b8c20559-17d7-4c9b-950f-c41b47146d5c	1	74	\N	10.1.24.103/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.103/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.277Z", "custom_fields": {}}	47	\N	1
188	2019-08-27 00:26:19.367031-04	admin	af881014-bfc8-4f46-9bc9-d8e1eba787be	1	75	\N	10.1.24.107/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.107/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.343Z", "custom_fields": {}}	47	\N	1
189	2019-08-27 00:26:19.435812-04	admin	f0eea3d3-c3a0-4663-8d6e-752117e3afda	1	76	\N	10.1.24.108/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.108/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.411Z", "custom_fields": {}}	47	\N	1
190	2019-08-27 00:26:19.504274-04	admin	cc945841-a25f-4cec-8031-70ecaf1029eb	1	77	\N	10.1.24.109/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.109/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.481Z", "custom_fields": {}}	47	\N	1
191	2019-08-27 00:26:19.571469-04	admin	a07cef47-2d97-4515-97a4-ce745d2af491	1	78	\N	10.1.24.111/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.111/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:26:19.548Z", "custom_fields": {}}	47	\N	1
192	2019-08-27 00:26:19.639237-04	admin	3bddfab8-d634-466e-ae51-074eb2955ceb	1	79	\N	10.1.24.112/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.112/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.615Z", "custom_fields": {}}	47	\N	1
193	2019-08-27 00:26:19.710841-04	admin	963329f6-5163-4685-b294-c8b07d2ad49a	1	80	\N	10.1.24.113/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.113/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.687Z", "custom_fields": {}}	47	\N	1
194	2019-08-27 00:26:19.778769-04	admin	400ac211-ed0c-413b-b509-5d3bc50a8ca4	1	81	\N	10.1.24.115/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.115/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.755Z", "custom_fields": {}}	47	\N	1
195	2019-08-27 00:26:19.845767-04	admin	187772bb-1ad8-4f59-80d3-65af2aea2f74	1	82	\N	10.1.24.120/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.120/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.821Z", "custom_fields": {}}	47	\N	1
196	2019-08-27 00:26:19.912067-04	admin	14ce0f4a-0470-4808-9a12-686f82162200	1	83	\N	10.1.24.126/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.126/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.888Z", "custom_fields": {}}	47	\N	1
197	2019-08-27 00:26:19.978531-04	admin	11ef95d7-4789-4281-a856-4cb91fd2d558	1	84	\N	10.1.24.127/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.127/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.laserjet_cp4525.", "last_updated": "2019-08-27T04:26:19.955Z", "custom_fields": {}}	47	\N	1
198	2019-08-27 00:26:20.045451-04	admin	a224d043-c748-42e0-8695-49f7af8221af	1	85	\N	10.1.24.130/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.130/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.022Z", "custom_fields": {}}	47	\N	1
199	2019-08-27 00:26:20.113427-04	admin	fd0d2445-0e94-41ac-9a7b-42877a02fa9e	1	86	\N	10.1.24.132/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.132/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.090Z", "custom_fields": {}}	47	\N	1
200	2019-08-27 00:26:20.183376-04	admin	290a43bf-3426-4a25-bde9-acd9b80832fb	1	87	\N	10.1.24.134/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.134/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:20.157Z", "custom_fields": {}}	47	\N	1
201	2019-08-27 00:26:20.250861-04	admin	675fc8be-2099-480f-a9f0-2a1176ef54fd	1	88	\N	10.1.24.139/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.139/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.227Z", "custom_fields": {}}	47	\N	1
202	2019-08-27 00:26:20.319164-04	admin	0812ed6e-710d-4e96-9f68-b6c0f320e573	1	89	\N	10.1.24.142/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.142/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:20.294Z", "custom_fields": {}}	47	\N	1
203	2019-08-27 00:26:20.388024-04	admin	c504123a-fd1c-4dc9-99ef-62624b419c44	1	90	\N	10.1.24.144/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.144/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.363Z", "custom_fields": {}}	47	\N	1
204	2019-08-27 00:26:20.456046-04	admin	f4828900-e60c-4beb-85bf-718b98cd49d3	1	91	\N	10.1.24.145/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.145/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.432Z", "custom_fields": {}}	47	\N	1
205	2019-08-27 00:26:20.528308-04	admin	ce6c88fc-b89c-4a4f-a5cd-a8c3a53ed320	1	92	\N	10.1.24.146/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.146/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.504Z", "custom_fields": {}}	47	\N	1
206	2019-08-27 00:26:20.596084-04	admin	147a83c8-c265-450e-bd4c-7718f04d4cf0	1	93	\N	10.1.24.147/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.147/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.572Z", "custom_fields": {}}	47	\N	1
207	2019-08-27 00:26:20.664314-04	admin	6db0958c-3c7e-4d82-85ae-1b71498d2b00	1	94	\N	10.1.24.150/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.150/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.639Z", "custom_fields": {}}	47	\N	1
208	2019-08-27 00:26:20.733087-04	admin	9fb0a0d3-14cb-47a9-a925-6f801caf84c1	1	95	\N	10.1.24.152/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.152/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.708Z", "custom_fields": {}}	47	\N	1
209	2019-08-27 00:26:20.800601-04	admin	f5953244-14ba-4c4a-9bde-14421e51855d	1	96	\N	10.1.24.153/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.153/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.776Z", "custom_fields": {}}	47	\N	1
210	2019-08-27 00:26:20.867494-04	admin	1f3b9934-6a1b-43ef-a9a4-8699a107f6cb	1	97	\N	10.1.24.155/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.155/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.844Z", "custom_fields": {}}	47	\N	1
211	2019-08-27 00:26:20.937663-04	admin	9563ff20-ced2-4822-89b2-3bc207a6d3b8	1	98	\N	10.1.24.157/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.157/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.913Z", "custom_fields": {}}	47	\N	1
212	2019-08-27 00:26:21.008496-04	admin	fa9b1178-ba9a-4fef-9824-361c231d5db0	1	99	\N	10.1.24.161/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.161/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.984Z", "custom_fields": {}}	47	\N	1
213	2019-08-27 00:26:21.076709-04	admin	401f1d51-4de4-4091-8edd-e6e3d4d04847	1	100	\N	10.1.24.163/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.163/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:21.052Z", "custom_fields": {}}	47	\N	1
214	2019-08-27 00:26:21.1446-04	admin	e9ea7d08-62eb-4e78-8249-8020464e0657	1	101	\N	10.1.24.165/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.165/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.120Z", "custom_fields": {}}	47	\N	1
215	2019-08-27 00:26:21.213026-04	admin	d4bc5a3d-9207-4339-81b0-aee607387bfd	1	102	\N	10.1.24.175/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.175/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:21.189Z", "custom_fields": {}}	47	\N	1
216	2019-08-27 00:26:21.280867-04	admin	34af417e-3fc9-4ce2-8c01-9914374bd625	1	103	\N	10.1.24.177/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.177/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.257Z", "custom_fields": {}}	47	\N	1
217	2019-08-27 00:26:21.349825-04	admin	ffeba125-843b-4490-88c1-5bfa9ed29f0a	1	104	\N	10.1.24.180/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.180/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:21.326Z", "custom_fields": {}}	47	\N	1
218	2019-08-27 00:26:21.416838-04	admin	d9755934-8f8a-4d7d-b3ae-1957be754850	1	105	\N	10.1.24.182/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.182/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:21.393Z", "custom_fields": {}}	47	\N	1
219	2019-08-27 00:26:21.484332-04	admin	e1f689f1-ec9b-4421-b559-6451c141b2cc	1	106	\N	10.1.24.187/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.187/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.460Z", "custom_fields": {}}	47	\N	1
220	2019-08-27 00:26:21.552641-04	admin	969005c0-307d-4014-bac9-226068185c08	1	107	\N	10.1.24.188/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.188/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.528Z", "custom_fields": {}}	47	\N	1
221	2019-08-27 00:26:21.619885-04	admin	6821ccda-aeff-495e-8ab3-99f647449d12	1	108	\N	10.1.24.192/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.192/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:21.596Z", "custom_fields": {}}	47	\N	1
222	2019-08-27 00:26:21.688159-04	admin	9feaeb6e-4259-4904-87d1-8c284cabe690	1	109	\N	10.1.24.196/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.196/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.665Z", "custom_fields": {}}	47	\N	1
223	2019-08-27 00:26:21.757524-04	admin	5bc6dab6-e861-49d2-b6fc-5c52ad7770b6	1	110	\N	10.1.24.197/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.197/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:21.733Z", "custom_fields": {}}	47	\N	1
224	2019-08-27 00:26:21.825589-04	admin	992c1e52-f5d8-442a-a830-5d1d2d00a1cc	1	111	\N	10.1.24.20/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.20/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.laserjet_cp4525.", "last_updated": "2019-08-27T04:26:21.800Z", "custom_fields": {}}	47	\N	1
225	2019-08-27 00:26:21.892583-04	admin	470967ad-5ae3-4cee-b25a-ab0ae3d3194b	1	112	\N	10.1.24.204/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.204/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.868Z", "custom_fields": {}}	47	\N	1
226	2019-08-27 00:26:21.959285-04	admin	36f36681-6536-41be-a759-abda952ca8dd	1	113	\N	10.1.24.207/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.207/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:21.936Z", "custom_fields": {}}	47	\N	1
227	2019-08-27 00:26:22.046249-04	admin	cfd12b48-23c3-434f-858a-51a19da45d6b	1	114	\N	10.1.24.210/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.210/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.009Z", "custom_fields": {}}	47	\N	1
228	2019-08-27 00:26:22.114866-04	admin	ce2d28f7-0b77-43a4-9e21-c4274838f1cb	1	115	\N	10.1.24.216/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.216/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:22.090Z", "custom_fields": {}}	47	\N	1
229	2019-08-27 00:26:22.186677-04	admin	93a30815-8d1b-467d-bb96-d5deafcdb113	1	116	\N	10.1.24.218/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.218/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.162Z", "custom_fields": {}}	47	\N	1
230	2019-08-27 00:26:22.254684-04	admin	2b3232bc-e05d-4dea-97ac-225a1e0ea732	1	117	\N	10.1.24.219/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.219/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.230Z", "custom_fields": {}}	47	\N	1
231	2019-08-27 00:26:22.323731-04	admin	068e377a-5d86-4a53-8075-e9ae88626537	1	118	\N	10.1.24.220/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.220/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:22.299Z", "custom_fields": {}}	47	\N	1
232	2019-08-27 00:26:22.391965-04	admin	ceffc857-d027-4772-9606-1237d047af35	1	119	\N	10.1.24.226/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.226/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.368Z", "custom_fields": {}}	47	\N	1
233	2019-08-27 00:26:22.460473-04	admin	e0ab57b9-81e9-4d2e-9a3b-6cbfc3e1e4ea	1	120	\N	10.1.24.227/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.227/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.436Z", "custom_fields": {}}	47	\N	1
234	2019-08-27 00:26:22.528986-04	admin	7694d50b-e2cb-439a-a13b-4a55d03a65c4	1	121	\N	10.1.24.228/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.228/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.505Z", "custom_fields": {}}	47	\N	1
235	2019-08-27 00:26:22.600912-04	admin	73d85915-4b31-40f5-9561-ecb3c89a759f	1	122	\N	10.1.24.229/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.229/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.576Z", "custom_fields": {}}	47	\N	1
236	2019-08-27 00:26:22.671745-04	admin	a44fc5ec-f7fe-447b-bff6-f577360057dd	1	123	\N	10.1.24.23/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.23/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.645Z", "custom_fields": {}}	47	\N	1
237	2019-08-27 00:26:22.739826-04	admin	14feca55-6f27-4972-bcd9-5026ede1558d	1	124	\N	10.1.24.233/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.233/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.715Z", "custom_fields": {}}	47	\N	1
238	2019-08-27 00:26:22.809707-04	admin	d192669b-8864-4b7e-8f26-17a5d8aefa31	1	125	\N	10.1.24.24/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.24/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.785Z", "custom_fields": {}}	47	\N	1
239	2019-08-27 00:26:22.876651-04	admin	5978c506-dab6-4def-9914-4d0aa7fda00e	1	126	\N	10.1.24.241/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.241/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:22.852Z", "custom_fields": {}}	47	\N	1
240	2019-08-27 00:26:22.946427-04	admin	d41031ac-b970-424c-8768-96eb422651f3	1	127	\N	10.1.24.242/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.242/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:22.921Z", "custom_fields": {}}	47	\N	1
241	2019-08-27 00:26:23.019805-04	admin	f0882923-0a62-41b4-9775-e5fec3607942	1	128	\N	10.1.24.243/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.243/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:22.994Z", "custom_fields": {}}	47	\N	1
242	2019-08-27 00:26:23.089055-04	admin	4778a103-9df8-41b2-b3c7-168db9f276db	1	129	\N	10.1.24.244/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.244/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:23.064Z", "custom_fields": {}}	47	\N	1
243	2019-08-27 00:26:23.1578-04	admin	36382191-519c-4950-ae1e-c6deb96f40aa	1	130	\N	10.1.24.246/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.246/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows.", "last_updated": "2019-08-27T04:26:23.134Z", "custom_fields": {}}	47	\N	1
244	2019-08-27 00:26:23.226583-04	admin	a6acd644-3823-4915-8c52-a6ab9577d0c3	1	131	\N	10.1.24.248/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.248/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:23.202Z", "custom_fields": {}}	47	\N	1
245	2019-08-27 00:26:23.294658-04	admin	0f3a8ba0-1115-41f7-a126-18f4115b1905	1	132	\N	10.1.24.250/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.250/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:23.270Z", "custom_fields": {}}	47	\N	1
246	2019-08-27 00:26:23.360962-04	admin	bd700fcc-2ba7-4d75-9243-7c814368ebab	1	133	\N	10.1.24.251/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.251/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.337Z", "custom_fields": {}}	47	\N	1
247	2019-08-27 00:26:23.427999-04	admin	ea8e78a9-16c8-4da4-82a4-344440348c61	1	134	\N	10.1.24.252/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.252/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:23.404Z", "custom_fields": {}}	47	\N	1
248	2019-08-27 00:26:23.500972-04	admin	ed798c78-383f-46eb-9fac-6f38f2e05291	1	135	\N	10.1.24.253/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.253/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:23.475Z", "custom_fields": {}}	47	\N	1
249	2019-08-27 00:26:23.570015-04	admin	a64b9d8e-c36c-4a52-b959-59d173238fc1	1	136	\N	10.1.24.30/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.30/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.545Z", "custom_fields": {}}	47	\N	1
250	2019-08-27 00:26:23.637681-04	admin	67854de1-41fb-4cad-8818-31f5d8020702	1	137	\N	10.1.24.33/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.33/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:23.613Z", "custom_fields": {}}	47	\N	1
251	2019-08-27 00:26:23.705756-04	admin	0e7daa4a-d902-4154-8da9-d7d024b48e12	1	138	\N	10.1.24.36/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.36/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:23.682Z", "custom_fields": {}}	47	\N	1
252	2019-08-27 00:26:23.773934-04	admin	bf6a8633-2eba-4433-bf32-d25c61af2868	1	139	\N	10.1.24.42/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.42/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.749Z", "custom_fields": {}}	47	\N	1
253	2019-08-27 00:26:23.841511-04	admin	3fbf633a-913c-47fd-9671-3dc904e431e7	1	140	\N	10.1.24.43/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.43/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.817Z", "custom_fields": {}}	47	\N	1
254	2019-08-27 00:26:23.912067-04	admin	1fc62a09-0bf9-46f4-95fe-aa47b9858cf8	1	141	\N	10.1.24.45/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.45/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.887Z", "custom_fields": {}}	47	\N	1
255	2019-08-27 00:26:23.98154-04	admin	0b271ce7-dbfc-46e8-a293-6d67a58cc6af	1	142	\N	10.1.24.49/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.49/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.956Z", "custom_fields": {}}	47	\N	1
256	2019-08-27 00:26:24.051103-04	admin	64096e9d-8a75-4bd2-8493-83a5b4f66a17	1	143	\N	10.1.24.57/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.57/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:24.027Z", "custom_fields": {}}	47	\N	1
257	2019-08-27 00:26:24.118954-04	admin	2b83e5fd-a4fe-4941-9dad-26a74b79e862	1	144	\N	10.1.24.58/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.58/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.095Z", "custom_fields": {}}	47	\N	1
258	2019-08-27 00:26:24.186333-04	admin	0d6ecbb2-0875-49db-9550-6c1ba4738254	1	145	\N	10.1.24.6/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.6/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "linux.linux_kernel.2.6", "last_updated": "2019-08-27T04:26:24.162Z", "custom_fields": {}}	47	\N	1
259	2019-08-27 00:26:24.253233-04	admin	17bb1cd2-2c6f-4c57-b558-0e20e4e07d9b	1	146	\N	10.1.24.65/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.65/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:24.229Z", "custom_fields": {}}	47	\N	1
260	2019-08-27 00:26:24.324463-04	admin	ea9a8c9c-bc7e-4004-b4ad-9a06fb2d22a3	1	147	\N	10.1.24.69/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.69/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:24.298Z", "custom_fields": {}}	47	\N	1
261	2019-08-27 00:26:24.391924-04	admin	535bd4cf-89aa-449b-872a-1624df6926a1	1	148	\N	10.1.24.7/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.7/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "linux.linux_kernel.2.6", "last_updated": "2019-08-27T04:26:24.368Z", "custom_fields": {}}	47	\N	1
262	2019-08-27 00:26:24.459942-04	admin	ba4e164a-ab8c-4cb4-87a9-e12347544e1a	1	149	\N	10.1.24.70/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.70/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.436Z", "custom_fields": {}}	47	\N	1
263	2019-08-27 00:26:24.531129-04	admin	ac26dc7f-663a-4923-9f17-9fda56ebcf53	1	150	\N	10.1.24.71/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.71/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:24.506Z", "custom_fields": {}}	47	\N	1
264	2019-08-27 00:26:24.600418-04	admin	8b3fb08e-db84-4fa5-a0b0-6cd6084e1e30	1	151	\N	10.1.24.73/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.73/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.575Z", "custom_fields": {}}	47	\N	1
265	2019-08-27 00:26:24.667873-04	admin	e7ff0c66-62ad-403f-81e1-f4c678a12809	1	152	\N	10.1.24.74/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.74/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:24.644Z", "custom_fields": {}}	47	\N	1
266	2019-08-27 00:26:24.737649-04	admin	c500ee68-d309-4d7e-813d-0a4f9046c5da	1	153	\N	10.1.24.77/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.77/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.714Z", "custom_fields": {}}	47	\N	1
267	2019-08-27 00:26:24.805688-04	admin	7eb4cd27-7583-4426-8c0d-69fef62ae6a6	1	154	\N	10.1.24.79/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.79/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:24.781Z", "custom_fields": {}}	47	\N	1
268	2019-08-27 00:26:24.87193-04	admin	8e24cc6a-2e82-42d3-ac15-7d03df8f269d	1	155	\N	10.1.24.80/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.80/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.848Z", "custom_fields": {}}	47	\N	1
269	2019-08-27 00:26:24.939631-04	admin	017ea020-d24d-4215-80f1-42ebdefdfcae	1	156	\N	10.1.24.83/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.83/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:24.915Z", "custom_fields": {}}	47	\N	1
270	2019-08-27 00:26:25.009908-04	admin	893aa92c-86f6-4381-a9ee-ade16607a342	1	157	\N	10.1.24.86/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.86/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:24.984Z", "custom_fields": {}}	47	\N	1
271	2019-08-27 00:26:25.078207-04	admin	b51eaf20-d519-44dc-8c4f-3d5cf793ebca	1	158	\N	10.1.24.87/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.87/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:25.054Z", "custom_fields": {}}	47	\N	1
272	2019-08-27 00:26:25.150881-04	admin	e34d3127-7a91-4834-b9de-90b8314392db	1	159	\N	10.1.24.88/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.88/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:25.124Z", "custom_fields": {}}	47	\N	1
273	2019-08-27 00:26:25.218163-04	admin	b1f782f2-cce9-47ad-9a0c-a3ff9abf32fb	1	160	\N	10.1.24.97/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.97/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:25.194Z", "custom_fields": {}}	47	\N	1
274	2019-08-27 00:26:25.285635-04	admin	796c2673-a754-4f0d-a200-233ef1e81a00	1	161	\N	10.1.24.98/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.98/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:25.261Z", "custom_fields": {}}	47	\N	1
275	2019-08-27 00:26:35.9453-04	admin	12fb1d70-ab57-4a78-9073-36c4f7b22898	2	3	\N	10.1.24.0/24	{"vrf": null, "role": 1, "site": 1, "tags": [], "vlan": 2, "family": 4, "prefix": "10.1.24.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": true, "description": "", "last_updated": "2019-08-27T04:26:35.930Z", "custom_fields": {}}	48	\N	1
276	2019-08-27 00:28:08.283708-04	admin	b3048ebb-1866-45bf-9291-8e9a47e34e1c	2	89	\N	10.1.24.142/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.142/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:08.275Z", "custom_fields": {}}	47	\N	1
277	2019-08-27 00:28:08.807369-04	admin	a8b4b52b-7e38-4650-8f1e-934a4a0f5ba5	2	100	\N	10.1.24.163/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.163/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:08.799Z", "custom_fields": {}}	47	\N	1
278	2019-08-27 00:28:09.031085-04	admin	b2bdf0d9-00d6-4188-b57f-752750f4a275	2	104	\N	10.1.24.180/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.180/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:09.022Z", "custom_fields": {}}	47	\N	1
279	2019-08-27 00:28:09.120773-04	admin	b9daac6e-71c8-4858-ada2-c7f36453598d	2	105	\N	10.1.24.182/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.182/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:09.112Z", "custom_fields": {}}	47	\N	1
280	2019-08-27 00:28:09.878335-04	admin	daa632a1-7ac2-4559-9953-81059a985c6c	2	121	\N	10.1.24.228/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.228/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:09.870Z", "custom_fields": {}}	47	\N	1
281	2019-08-27 00:28:10.207439-04	admin	5993d6d1-5181-44df-822d-b4ad8e50fd91	2	127	\N	10.1.24.242/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.242/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:10.199Z", "custom_fields": {}}	47	\N	1
282	2019-08-27 00:28:10.297786-04	admin	28334d19-ddb4-4b91-a1d3-b8397ba090fc	2	128	\N	10.1.24.243/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.243/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:10.289Z", "custom_fields": {}}	47	\N	1
283	2019-08-27 00:28:11.005628-04	admin	160ff588-fbfe-4d31-97b4-9b17fc241c51	2	143	\N	10.1.24.57/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.57/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:10.997Z", "custom_fields": {}}	47	\N	1
284	2019-08-27 00:28:11.230476-04	admin	01bf311a-4aaa-4814-94b6-4ec45c2f2182	2	147	\N	10.1.24.69/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.69/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:11.222Z", "custom_fields": {}}	47	\N	1
285	2019-08-27 00:28:11.446472-04	admin	e5da2124-0408-4433-8391-32155a90010d	2	151	\N	10.1.24.73/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.73/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:11.438Z", "custom_fields": {}}	47	\N	1
286	2019-08-27 00:28:11.537159-04	admin	4fca99f4-9cbb-455d-91d3-ce6e28f17f50	2	152	\N	10.1.24.74/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.74/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:11.528Z", "custom_fields": {}}	47	\N	1
287	2019-08-27 00:28:11.98035-04	admin	384d87f6-4770-4f8c-a024-1ae0e63bbc8f	2	161	\N	10.1.24.98/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.98/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:11.972Z", "custom_fields": {}}	47	\N	1
288	2019-08-27 00:30:55.088736-04	admin	7ecb73b0-b055-4dd0-a521-42079dd1e34e	2	2	\N	HO-SRV-AD	{"disk": 50, "name": "HO-SRV-AD", "role": 8, "tags": ["auto"], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "This is comment", "platform": 7, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:30:55.056Z", "custom_fields": {"Field1": "New Feild"}, "local_context_data": null}	79	\N	1
289	2019-08-27 00:31:31.46029-04	admin	6fa9ae7f-85db-49a7-9188-535b71f892f1	1	162	1	10.1.1.10/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/32", "created": "2019-08-27", "dns_name": "", "interface": 1, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:31:31.434Z", "custom_fields": {}}	47	5	1
290	2019-08-27 00:31:31.468183-04	admin	6fa9ae7f-85db-49a7-9188-535b71f892f1	2	2	\N	HO-SRV-AD	{"disk": 50, "name": "HO-SRV-AD", "role": 8, "tags": ["auto"], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "This is comment", "platform": 7, "primary_ip4": 162, "primary_ip6": null, "last_updated": "2019-08-27T04:31:31.446Z", "custom_fields": {"Field1": "New Feild"}, "local_context_data": null}	79	\N	1
291	2019-08-27 00:33:19.555061-04	admin	ffdd5c8a-817f-46c3-87e6-02c7d795914d	1	3	\N	HO-SRV-autoFillCRC01	{"disk": 50, "name": "HO-SRV-autoFillCRC01", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": null, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:33:19.532Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
292	2019-08-27 00:33:42.571935-04	admin	21a89520-160f-4f85-a6a6-6dc29a0feafe	1	10	\N	Window 7 Pro	{"name": "Window 7 Pro", "slug": "window-7-pro", "created": "2019-08-27", "napalm_args": null, "last_updated": "2019-08-27T04:33:42.567Z", "manufacturer": 9, "napalm_driver": ""}	26	\N	1
293	2019-08-27 00:33:56.178348-04	admin	51a83f8b-7cf2-4d68-97ac-7669464e89af	2	3	\N	HO-SRV-autoFillCRC01	{"disk": 50, "name": "HO-SRV-autoFillCRC01", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:33:56.164Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
294	2019-08-27 00:34:11.247853-04	admin	3d509bf0-fd7c-4e89-b20f-63e3e616df65	1	2	3	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 3, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
295	2019-08-27 00:34:30.000415-04	admin	abab1f99-ad56-4d47-a613-bb128e27a55b	1	163	2	10.1.27.201/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.201/32", "created": "2019-08-27", "dns_name": "", "interface": 2, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:34:29.979Z", "custom_fields": {}}	47	5	1
296	2019-08-27 00:34:43.842877-04	admin	142539f9-207a-49c2-b7a5-aca771a77d24	2	3	\N	HO-SRV-autoFillCRC01	{"disk": 50, "name": "HO-SRV-autoFillCRC01", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 163, "primary_ip6": null, "last_updated": "2019-08-27T04:34:43.827Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
297	2019-08-27 00:39:47.320497-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	4	\N	HO-SRV-autoFillCRC02	{"disk": 50, "name": "HO-SRV-autoFillCRC02", "role": 8, "tags": [], "vcpus": 2, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:46.966Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
298	2019-08-27 00:39:47.329947-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	5	\N	HO-SRV-autoFillCRC03	{"disk": 50, "name": "HO-SRV-autoFillCRC03", "role": 8, "tags": [], "vcpus": 1, "memory": 3072, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:46.979Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
299	2019-08-27 00:39:47.33994-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	6	\N	HO-SRV-autoFillCRC04	{"disk": 50, "name": "HO-SRV-autoFillCRC04", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:46.989Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
300	2019-08-27 00:39:47.349387-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	7	\N	HO-SRV-autoFillCRC05	{"disk": 50, "name": "HO-SRV-autoFillCRC05", "role": 8, "tags": [], "vcpus": 1, "memory": 3072, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.003Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
301	2019-08-27 00:39:47.360264-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	8	\N	HO-SRV-autoFillCRC06	{"disk": 50, "name": "HO-SRV-autoFillCRC06", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.014Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
302	2019-08-27 00:39:47.370763-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	9	\N	HO-SRV-autoFillCRC07	{"disk": 50, "name": "HO-SRV-autoFillCRC07", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.025Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
303	2019-08-27 00:39:47.380612-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	10	\N	HO-SRV-autoFillCRC08	{"disk": 50, "name": "HO-SRV-autoFillCRC08", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.033Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
304	2019-08-27 00:39:47.390559-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	11	\N	HO-SRV-autoFillCRC09	{"disk": 50, "name": "HO-SRV-autoFillCRC09", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.042Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
305	2019-08-27 00:39:47.401717-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	12	\N	HO-SRV-autoFillCRC10	{"disk": 50, "name": "HO-SRV-autoFillCRC10", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.053Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
306	2019-08-27 00:39:47.411802-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	13	\N	HO-SRV-autoFillCRC11	{"disk": 50, "name": "HO-SRV-autoFillCRC11", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.063Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
307	2019-08-27 00:39:47.422033-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	14	\N	HO-SRV-autoFillCRC12	{"disk": 50, "name": "HO-SRV-autoFillCRC12", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.073Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
308	2019-08-27 00:39:47.432353-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	15	\N	HO-SRV-autoFillCRC13	{"disk": 50, "name": "HO-SRV-autoFillCRC13", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.082Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
309	2019-08-27 00:39:47.441252-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	16	\N	HO-SRV-autoFillCRC14	{"disk": 50, "name": "HO-SRV-autoFillCRC14", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.093Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
310	2019-08-27 00:39:47.450242-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	17	\N	HO-SRV-autoFillCRC15	{"disk": 50, "name": "HO-SRV-autoFillCRC15", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.101Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
311	2019-08-27 00:39:47.461426-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	18	\N	HO-SRV-autoFillCRC16	{"disk": 50, "name": "HO-SRV-autoFillCRC16", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.109Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
312	2019-08-27 00:39:47.470382-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	19	\N	HO-SRV-autoFillCRC17	{"disk": 50, "name": "HO-SRV-autoFillCRC17", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.117Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
313	2019-08-27 00:39:47.480303-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	20	\N	HO-SRV-autoFillCRC18	{"disk": 50, "name": "HO-SRV-autoFillCRC18", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.127Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
314	2019-08-27 00:39:47.490186-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	21	\N	HO-SRV-autoFillCRC19	{"disk": 50, "name": "HO-SRV-autoFillCRC19", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.134Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
315	2019-08-27 00:39:47.502026-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	22	\N	HO-SRV-autoFillCRC20	{"disk": 50, "name": "HO-SRV-autoFillCRC20", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.142Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
316	2019-08-27 00:39:47.510946-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	23	\N	HO-SRV-autoFillCRC21	{"disk": 50, "name": "HO-SRV-autoFillCRC21", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.150Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
317	2019-08-27 00:39:47.52174-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	24	\N	HO-SRV-autoFillCRC22	{"disk": 50, "name": "HO-SRV-autoFillCRC22", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.160Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
318	2019-08-27 00:39:47.531769-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	25	\N	HO-SRV-autoFillCRC23	{"disk": 50, "name": "HO-SRV-autoFillCRC23", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.167Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
319	2019-08-27 00:39:47.541683-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	26	\N	HO-SRV-autoFillCRC24	{"disk": 50, "name": "HO-SRV-autoFillCRC24", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.176Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
320	2019-08-27 00:39:47.551966-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	27	\N	HO-SRV-autoFillCRC25	{"disk": 50, "name": "HO-SRV-autoFillCRC25", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.184Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
321	2019-08-27 00:39:47.561809-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	28	\N	HO-SRV-autoFillCRC26	{"disk": 50, "name": "HO-SRV-autoFillCRC26", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.193Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
322	2019-08-27 00:39:47.571587-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	29	\N	HO-SRV-autoFillCRC27	{"disk": 50, "name": "HO-SRV-autoFillCRC27", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.201Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
323	2019-08-27 00:39:47.581507-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	30	\N	HO-SRV-autoFillCRC28	{"disk": 50, "name": "HO-SRV-autoFillCRC28", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.210Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
324	2019-08-27 00:39:47.589962-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	31	\N	HO-SRV-autoFillCRC29	{"disk": 50, "name": "HO-SRV-autoFillCRC29", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.219Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
325	2019-08-27 00:39:47.599155-04	admin	061281f4-01b8-46e7-a6a5-3622dc52e8ce	1	32	\N	HO-SRV-autoFillCRC30	{"disk": 50, "name": "HO-SRV-autoFillCRC30", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:39:47.227Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
326	2019-08-27 00:40:26.897586-04	admin	c3b87681-8403-4fe0-8420-746d8bf465cf	1	3	4	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 4, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
327	2019-08-27 00:40:52.218582-04	admin	1f44229a-e88c-4935-abdd-41b5b13a736b	1	164	3	10.1.27.202/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.202/32", "created": "2019-08-27", "dns_name": "", "interface": 3, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:40:52.189Z", "custom_fields": {}}	47	5	1
328	2019-08-27 00:40:52.233217-04	admin	1f44229a-e88c-4935-abdd-41b5b13a736b	2	4	\N	HO-SRV-autoFillCRC02	{"disk": 50, "name": "HO-SRV-autoFillCRC02", "role": 8, "tags": [], "vcpus": 2, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 164, "primary_ip6": null, "last_updated": "2019-08-27T04:40:52.201Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
329	2019-08-27 00:41:15.407879-04	admin	6333f718-7196-4242-9a40-c73d42cbc4bc	3	2	\N	HO-SRV-AD	{"disk": 50, "name": "HO-SRV-AD", "role": 8, "tags": ["auto"], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-26", "comments": "This is comment", "platform": 7, "primary_ip4": 162, "primary_ip6": null, "last_updated": "2019-08-27T04:31:31.446Z", "custom_fields": {"Field1": "New Feild"}, "local_context_data": null}	79	\N	1
330	2019-08-27 00:41:15.416949-04	admin	6333f718-7196-4242-9a40-c73d42cbc4bc	3	1	\N	Interface1	{"lag": null, "mtu": 1500, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "Description Interface1", "mac_address": null, "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 2, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	\N	1
331	2019-08-27 00:41:15.424538-04	admin	6333f718-7196-4242-9a40-c73d42cbc4bc	3	162	\N	10.1.1.10/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.10/32", "created": "2019-08-27", "dns_name": "", "interface": 1, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:31:31.434Z", "custom_fields": {}}	47	\N	1
332	2019-08-27 00:42:26.650138-04	admin	5bdee6cf-41ab-4edb-961c-0679f04820f3	1	9	\N	VPS06	{"name": "VPS06", "site": 1, "tags": [], "type": 2, "group": 1, "created": "2019-08-27", "comments": "", "last_updated": "2019-08-27T04:42:26.628Z", "custom_fields": {"Field1": "None"}}	76	\N	1
333	2019-08-27 00:42:55.923912-04	admin	b6fbe3d9-c2e6-4dc3-9abf-4623aab415f6	2	8	\N	HO-SRV-autoFillCRC06	{"disk": 50, "name": "HO-SRV-autoFillCRC06", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:42:55.906Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
334	2019-08-27 00:43:28.953436-04	admin	a6ffccd6-2e95-4182-96f1-f17d448c2f11	2	26	\N	HO-SRV-autoFillCRC24	{"disk": 50, "name": "HO-SRV-autoFillCRC24", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 4, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:43:28.937Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
335	2019-08-27 00:44:09.336747-04	admin	a86b3ca8-955c-4b02-94cf-120e60386297	2	13	\N	HO-SRV-autoFillCRC11	{"disk": 50, "name": "HO-SRV-autoFillCRC11", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 7, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:44:09.311Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
336	2019-08-27 00:44:09.34474-04	admin	a86b3ca8-955c-4b02-94cf-120e60386297	2	21	\N	HO-SRV-autoFillCRC19	{"disk": 50, "name": "HO-SRV-autoFillCRC19", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 7, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:44:09.320Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
337	2019-08-27 00:44:09.351901-04	admin	a86b3ca8-955c-4b02-94cf-120e60386297	2	25	\N	HO-SRV-autoFillCRC23	{"disk": 50, "name": "HO-SRV-autoFillCRC23", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 7, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T04:44:09.327Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
338	2019-08-27 00:44:32.114037-04	admin	6dd34bb7-c21b-4dcd-84e4-015e65af9640	1	4	5	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 5, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
339	2019-08-27 00:44:47.692547-04	admin	6a7de0fd-6f50-445e-b1b5-960549430c8f	1	165	4	10.1.27.203/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.203/32", "created": "2019-08-27", "dns_name": "", "interface": 4, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:44:47.663Z", "custom_fields": {}}	47	5	1
340	2019-08-27 00:44:47.700186-04	admin	6a7de0fd-6f50-445e-b1b5-960549430c8f	2	5	\N	HO-SRV-autoFillCRC03	{"disk": 50, "name": "HO-SRV-autoFillCRC03", "role": 8, "tags": [], "vcpus": 1, "memory": 3072, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 165, "primary_ip6": null, "last_updated": "2019-08-27T04:44:47.677Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
341	2019-08-27 00:45:13.117082-04	admin	193beb62-649b-4d4d-b14e-61a0bdf36e1a	1	5	6	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 6, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
342	2019-08-27 00:45:26.331167-04	admin	6785fd9c-0f30-4a93-8e3e-fb7ee7776f47	1	166	5	10.1.27.204/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.204/32", "created": "2019-08-27", "dns_name": "", "interface": 5, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:45:26.304Z", "custom_fields": {}}	47	5	1
343	2019-08-27 00:45:26.34093-04	admin	6785fd9c-0f30-4a93-8e3e-fb7ee7776f47	2	6	\N	HO-SRV-autoFillCRC04	{"disk": 50, "name": "HO-SRV-autoFillCRC04", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 166, "primary_ip6": null, "last_updated": "2019-08-27T04:45:26.318Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
344	2019-08-27 00:45:35.527856-04	admin	3ef5964d-1df2-4c49-8f5d-ad35afa8c3f7	1	6	7	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 7, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
345	2019-08-27 00:45:46.561186-04	admin	3fddd0e2-bc05-4224-8199-847103c9ee50	1	167	6	10.1.27.205/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.205/32", "created": "2019-08-27", "dns_name": "", "interface": 6, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:45:46.535Z", "custom_fields": {}}	47	5	1
346	2019-08-27 00:45:46.568937-04	admin	3fddd0e2-bc05-4224-8199-847103c9ee50	2	7	\N	HO-SRV-autoFillCRC05	{"disk": 50, "name": "HO-SRV-autoFillCRC05", "role": 8, "tags": [], "vcpus": 1, "memory": 3072, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 167, "primary_ip6": null, "last_updated": "2019-08-27T04:45:46.546Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
347	2019-08-27 00:46:57.632548-04	admin	d5697d83-e7a5-43f3-89a9-720182335c28	1	7	8	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 8, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
348	2019-08-27 00:47:05.464481-04	admin	a838ca7d-78f6-4b2d-b148-70158ddc35c7	1	168	7	10.1.27.206/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.206/32", "created": "2019-08-27", "dns_name": "", "interface": 7, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:47:05.439Z", "custom_fields": {}}	47	5	1
349	2019-08-27 00:47:05.47304-04	admin	a838ca7d-78f6-4b2d-b148-70158ddc35c7	2	8	\N	HO-SRV-autoFillCRC06	{"disk": 50, "name": "HO-SRV-autoFillCRC06", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 168, "primary_ip6": null, "last_updated": "2019-08-27T04:47:05.451Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
350	2019-08-27 00:47:42.529592-04	admin	9c58f2fe-0982-47a9-8405-cb385573efbe	1	8	9	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 9, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
351	2019-08-27 00:47:50.920537-04	admin	3d5732fa-9200-41b8-ab55-7f08e2036da6	1	169	8	10.1.27.207/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.207/32", "created": "2019-08-27", "dns_name": "", "interface": 8, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:47:50.894Z", "custom_fields": {}}	47	5	1
352	2019-08-27 00:47:50.927979-04	admin	3d5732fa-9200-41b8-ab55-7f08e2036da6	2	9	\N	HO-SRV-autoFillCRC07	{"disk": 50, "name": "HO-SRV-autoFillCRC07", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 169, "primary_ip6": null, "last_updated": "2019-08-27T04:47:50.906Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
353	2019-08-27 00:48:05.358469-04	admin	b2010da4-acb9-44ed-a7b3-993d4b0e6f85	1	9	10	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 10, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
354	2019-08-27 00:48:19.058562-04	admin	7cc5ff1c-e00d-458a-a34e-f8ea02984a02	1	170	9	10.1.27.208/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.208/32", "created": "2019-08-27", "dns_name": "", "interface": 9, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:48:19.032Z", "custom_fields": {}}	47	5	1
355	2019-08-27 00:48:19.066074-04	admin	7cc5ff1c-e00d-458a-a34e-f8ea02984a02	2	10	\N	HO-SRV-autoFillCRC08	{"disk": 50, "name": "HO-SRV-autoFillCRC08", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 170, "primary_ip6": null, "last_updated": "2019-08-27T04:48:19.043Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
356	2019-08-27 00:49:46.564857-04	admin	ce16d931-8f69-41fa-bc83-b074572500b0	1	10	11	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 11, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
357	2019-08-27 00:50:02.850114-04	admin	47398a0c-dcfa-45a5-b793-161680783ce0	1	171	10	10.1.27.209/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.209/32", "created": "2019-08-27", "dns_name": "", "interface": 10, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:50:02.826Z", "custom_fields": {}}	47	5	1
358	2019-08-27 00:50:02.858106-04	admin	47398a0c-dcfa-45a5-b793-161680783ce0	2	11	\N	HO-SRV-autoFillCRC09	{"disk": 50, "name": "HO-SRV-autoFillCRC09", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 171, "primary_ip6": null, "last_updated": "2019-08-27T04:50:02.838Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
359	2019-08-27 00:52:07.895786-04	admin	1b77e57f-33d8-4290-a400-291d9dd84ff5	1	11	12	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 12, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
360	2019-08-27 00:52:19.26246-04	admin	760271e8-bbe3-4e3d-98e9-b2442d84ce3d	1	172	11	10.1.27.210/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.210/32", "created": "2019-08-27", "dns_name": "", "interface": 11, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:52:19.239Z", "custom_fields": {}}	47	5	1
361	2019-08-27 00:52:19.269466-04	admin	760271e8-bbe3-4e3d-98e9-b2442d84ce3d	2	12	\N	HO-SRV-autoFillCRC10	{"disk": 50, "name": "HO-SRV-autoFillCRC10", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 172, "primary_ip6": null, "last_updated": "2019-08-27T04:52:19.250Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
362	2019-08-27 00:53:56.048687-04	admin	ef684f8f-5d45-4830-b9d6-6bb1a7ac45eb	1	12	13	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 13, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
363	2019-08-27 00:54:03.173955-04	admin	0f50744b-c6be-4da7-b9d9-4c09ff8a28c8	1	173	12	10.1.27.211/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.211/32", "created": "2019-08-27", "dns_name": "", "interface": 12, "nat_inside": null, "description": "", "last_updated": "2019-08-27T04:54:03.140Z", "custom_fields": {}}	47	5	1
364	2019-08-27 00:54:03.184879-04	admin	0f50744b-c6be-4da7-b9d9-4c09ff8a28c8	2	13	\N	HO-SRV-autoFillCRC11	{"disk": 50, "name": "HO-SRV-autoFillCRC11", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 7, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 173, "primary_ip6": null, "last_updated": "2019-08-27T04:54:03.155Z", "custom_fields": {"Field1": "None"}, "local_context_data": null}	79	\N	1
365	2019-08-27 00:56:30.848415-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	161	\N	10.1.24.98/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.98/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:11.972Z", "custom_fields": {}}	47	\N	1
366	2019-08-27 00:56:30.856594-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	160	\N	10.1.24.97/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.97/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:25.194Z", "custom_fields": {}}	47	\N	1
367	2019-08-27 00:56:30.861894-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	159	\N	10.1.24.88/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.88/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:25.124Z", "custom_fields": {}}	47	\N	1
368	2019-08-27 00:56:30.86676-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	158	\N	10.1.24.87/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.87/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:25.054Z", "custom_fields": {}}	47	\N	1
369	2019-08-27 00:56:30.872617-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	157	\N	10.1.24.86/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.86/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:24.984Z", "custom_fields": {}}	47	\N	1
370	2019-08-27 00:56:30.877722-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	156	\N	10.1.24.83/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.83/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:24.915Z", "custom_fields": {}}	47	\N	1
371	2019-08-27 00:56:30.884999-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	155	\N	10.1.24.80/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.80/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.848Z", "custom_fields": {}}	47	\N	1
372	2019-08-27 00:56:30.889903-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	154	\N	10.1.24.79/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.79/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:24.781Z", "custom_fields": {}}	47	\N	1
373	2019-08-27 00:56:30.894937-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	153	\N	10.1.24.77/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.77/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.714Z", "custom_fields": {}}	47	\N	1
374	2019-08-27 00:56:30.899713-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	152	\N	10.1.24.74/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.74/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:11.528Z", "custom_fields": {}}	47	\N	1
375	2019-08-27 00:56:30.905527-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	151	\N	10.1.24.73/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.73/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:11.438Z", "custom_fields": {}}	47	\N	1
376	2019-08-27 00:56:30.910309-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	150	\N	10.1.24.71/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.71/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:24.506Z", "custom_fields": {}}	47	\N	1
377	2019-08-27 00:56:30.916293-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	149	\N	10.1.24.70/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.70/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.436Z", "custom_fields": {}}	47	\N	1
378	2019-08-27 00:56:30.922863-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	148	\N	10.1.24.7/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.7/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "linux.linux_kernel.2.6", "last_updated": "2019-08-27T04:26:24.368Z", "custom_fields": {}}	47	\N	1
379	2019-08-27 00:56:30.927818-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	147	\N	10.1.24.69/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.69/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:11.222Z", "custom_fields": {}}	47	\N	1
380	2019-08-27 00:56:30.933053-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	146	\N	10.1.24.65/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.65/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:24.229Z", "custom_fields": {}}	47	\N	1
381	2019-08-27 00:56:30.93864-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	145	\N	10.1.24.6/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.6/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "linux.linux_kernel.2.6", "last_updated": "2019-08-27T04:26:24.162Z", "custom_fields": {}}	47	\N	1
382	2019-08-27 00:56:30.944365-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	144	\N	10.1.24.58/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.58/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:24.095Z", "custom_fields": {}}	47	\N	1
383	2019-08-27 00:56:30.951159-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	143	\N	10.1.24.57/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.57/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:10.997Z", "custom_fields": {}}	47	\N	1
384	2019-08-27 00:56:30.956271-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	142	\N	10.1.24.49/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.49/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.956Z", "custom_fields": {}}	47	\N	1
385	2019-08-27 00:56:30.96157-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	141	\N	10.1.24.45/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.45/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.887Z", "custom_fields": {}}	47	\N	1
386	2019-08-27 00:56:30.967174-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	140	\N	10.1.24.43/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.43/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.817Z", "custom_fields": {}}	47	\N	1
387	2019-08-27 00:56:30.972129-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	139	\N	10.1.24.42/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.42/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.749Z", "custom_fields": {}}	47	\N	1
388	2019-08-27 00:56:30.978508-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	138	\N	10.1.24.36/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.36/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:23.682Z", "custom_fields": {}}	47	\N	1
389	2019-08-27 00:56:30.983353-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	137	\N	10.1.24.33/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.33/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:23.613Z", "custom_fields": {}}	47	\N	1
390	2019-08-27 00:56:30.98897-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	136	\N	10.1.24.30/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.30/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.545Z", "custom_fields": {}}	47	\N	1
391	2019-08-27 00:56:30.993881-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	135	\N	10.1.24.253/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.253/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:23.475Z", "custom_fields": {}}	47	\N	1
392	2019-08-27 00:56:31.000962-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	134	\N	10.1.24.252/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.252/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:23.404Z", "custom_fields": {}}	47	\N	1
393	2019-08-27 00:56:31.009761-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	133	\N	10.1.24.251/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.251/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:23.337Z", "custom_fields": {}}	47	\N	1
394	2019-08-27 00:56:31.014673-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	132	\N	10.1.24.250/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.250/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:23.270Z", "custom_fields": {}}	47	\N	1
395	2019-08-27 00:56:31.020258-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	131	\N	10.1.24.248/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.248/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:23.202Z", "custom_fields": {}}	47	\N	1
396	2019-08-27 00:56:31.025191-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	130	\N	10.1.24.246/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.246/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows.", "last_updated": "2019-08-27T04:26:23.134Z", "custom_fields": {}}	47	\N	1
397	2019-08-27 00:56:31.031159-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	129	\N	10.1.24.244/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.244/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:23.064Z", "custom_fields": {}}	47	\N	1
398	2019-08-27 00:56:31.037073-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	128	\N	10.1.24.243/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.243/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:10.289Z", "custom_fields": {}}	47	\N	1
399	2019-08-27 00:56:31.042023-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	127	\N	10.1.24.242/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.242/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:10.199Z", "custom_fields": {}}	47	\N	1
400	2019-08-27 00:56:31.046772-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	126	\N	10.1.24.241/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.241/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:22.852Z", "custom_fields": {}}	47	\N	1
401	2019-08-27 00:56:31.05275-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	125	\N	10.1.24.24/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.24/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.785Z", "custom_fields": {}}	47	\N	1
402	2019-08-27 00:56:31.057651-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	124	\N	10.1.24.233/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.233/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.715Z", "custom_fields": {}}	47	\N	1
403	2019-08-27 00:56:31.063347-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	123	\N	10.1.24.23/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.23/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.645Z", "custom_fields": {}}	47	\N	1
404	2019-08-27 00:56:31.070108-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	122	\N	10.1.24.229/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.229/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.576Z", "custom_fields": {}}	47	\N	1
405	2019-08-27 00:56:31.075048-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	121	\N	10.1.24.228/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.228/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:09.870Z", "custom_fields": {}}	47	\N	1
406	2019-08-27 00:56:31.080882-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	120	\N	10.1.24.227/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.227/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.436Z", "custom_fields": {}}	47	\N	1
407	2019-08-27 00:56:31.085884-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	119	\N	10.1.24.226/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.226/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.368Z", "custom_fields": {}}	47	\N	1
408	2019-08-27 00:56:31.091089-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	118	\N	10.1.24.220/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.220/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:22.299Z", "custom_fields": {}}	47	\N	1
409	2019-08-27 00:56:31.096778-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	117	\N	10.1.24.219/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.219/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.230Z", "custom_fields": {}}	47	\N	1
410	2019-08-27 00:56:31.103262-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	116	\N	10.1.24.218/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.218/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:22.162Z", "custom_fields": {}}	47	\N	1
411	2019-08-27 00:56:31.10791-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	115	\N	10.1.24.216/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.216/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:22.090Z", "custom_fields": {}}	47	\N	1
412	2019-08-27 00:56:31.113825-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	114	\N	10.1.24.210/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.210/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:22.009Z", "custom_fields": {}}	47	\N	1
413	2019-08-27 00:56:31.11853-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	113	\N	10.1.24.207/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.207/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:21.936Z", "custom_fields": {}}	47	\N	1
414	2019-08-27 00:56:31.123349-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	112	\N	10.1.24.204/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.204/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.868Z", "custom_fields": {}}	47	\N	1
415	2019-08-27 00:56:31.129494-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	111	\N	10.1.24.20/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.20/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.laserjet_cp4525.", "last_updated": "2019-08-27T04:26:21.800Z", "custom_fields": {}}	47	\N	1
416	2019-08-27 00:56:31.13488-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	110	\N	10.1.24.197/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.197/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:21.733Z", "custom_fields": {}}	47	\N	1
417	2019-08-27 00:56:31.139731-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	109	\N	10.1.24.196/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.196/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.665Z", "custom_fields": {}}	47	\N	1
418	2019-08-27 00:56:31.14507-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	108	\N	10.1.24.192/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.192/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:26:21.596Z", "custom_fields": {}}	47	\N	1
419	2019-08-27 00:56:31.150498-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	107	\N	10.1.24.188/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.188/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.528Z", "custom_fields": {}}	47	\N	1
420	2019-08-27 00:56:31.155305-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	106	\N	10.1.24.187/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.187/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.460Z", "custom_fields": {}}	47	\N	1
421	2019-08-27 00:56:31.161551-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	105	\N	10.1.24.182/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.182/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:09.112Z", "custom_fields": {}}	47	\N	1
422	2019-08-27 00:56:31.166415-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	104	\N	10.1.24.180/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.180/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:09.022Z", "custom_fields": {}}	47	\N	1
423	2019-08-27 00:56:31.172011-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	103	\N	10.1.24.177/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.177/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.257Z", "custom_fields": {}}	47	\N	1
424	2019-08-27 00:56:31.176842-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	102	\N	10.1.24.175/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.175/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:21.189Z", "custom_fields": {}}	47	\N	1
425	2019-08-27 00:56:31.182935-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	101	\N	10.1.24.165/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.165/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:21.120Z", "custom_fields": {}}	47	\N	1
426	2019-08-27 00:56:31.188634-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	100	\N	10.1.24.163/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.163/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:28:08.799Z", "custom_fields": {}}	47	\N	1
427	2019-08-27 00:56:31.193872-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	99	\N	10.1.24.161/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.161/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.984Z", "custom_fields": {}}	47	\N	1
428	2019-08-27 00:56:31.198492-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	98	\N	10.1.24.157/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.157/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.913Z", "custom_fields": {}}	47	\N	1
429	2019-08-27 00:56:31.204253-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	97	\N	10.1.24.155/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.155/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.844Z", "custom_fields": {}}	47	\N	1
430	2019-08-27 00:56:31.209227-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	96	\N	10.1.24.153/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.153/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.776Z", "custom_fields": {}}	47	\N	1
431	2019-08-27 00:56:31.218875-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	95	\N	10.1.24.152/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.152/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.708Z", "custom_fields": {}}	47	\N	1
432	2019-08-27 00:56:31.224635-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	94	\N	10.1.24.150/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.150/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.639Z", "custom_fields": {}}	47	\N	1
433	2019-08-27 00:56:31.230634-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	93	\N	10.1.24.147/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.147/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.572Z", "custom_fields": {}}	47	\N	1
434	2019-08-27 00:56:31.236179-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	92	\N	10.1.24.146/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.146/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.504Z", "custom_fields": {}}	47	\N	1
435	2019-08-27 00:56:31.241019-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	91	\N	10.1.24.145/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.145/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.432Z", "custom_fields": {}}	47	\N	1
436	2019-08-27 00:56:31.246739-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	90	\N	10.1.24.144/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.144/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.363Z", "custom_fields": {}}	47	\N	1
437	2019-08-27 00:56:31.253705-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	89	\N	10.1.24.142/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.142/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:28:08.275Z", "custom_fields": {}}	47	\N	1
438	2019-08-27 00:56:31.258336-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	88	\N	10.1.24.139/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.139/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:20.227Z", "custom_fields": {}}	47	\N	1
439	2019-08-27 00:56:31.264276-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	87	\N	10.1.24.134/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.134/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:26:20.157Z", "custom_fields": {}}	47	\N	1
440	2019-08-27 00:56:31.269084-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	86	\N	10.1.24.132/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.132/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.090Z", "custom_fields": {}}	47	\N	1
441	2019-08-27 00:56:31.27376-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	85	\N	10.1.24.130/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.130/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:20.022Z", "custom_fields": {}}	47	\N	1
442	2019-08-27 00:56:31.278541-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	84	\N	10.1.24.127/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.127/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.laserjet_cp4525.", "last_updated": "2019-08-27T04:26:19.955Z", "custom_fields": {}}	47	\N	1
443	2019-08-27 00:56:31.285315-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	83	\N	10.1.24.126/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.126/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.888Z", "custom_fields": {}}	47	\N	1
444	2019-08-27 00:56:31.377996-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	82	\N	10.1.24.120/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.120/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.821Z", "custom_fields": {}}	47	\N	1
445	2019-08-27 00:56:31.383264-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	81	\N	10.1.24.115/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.115/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.755Z", "custom_fields": {}}	47	\N	1
446	2019-08-27 00:56:31.390758-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	80	\N	10.1.24.113/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.113/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.687Z", "custom_fields": {}}	47	\N	1
447	2019-08-27 00:56:31.396693-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	79	\N	10.1.24.112/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.112/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.615Z", "custom_fields": {}}	47	\N	1
448	2019-08-27 00:56:31.405054-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	78	\N	10.1.24.111/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.111/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:26:19.548Z", "custom_fields": {}}	47	\N	1
449	2019-08-27 00:56:31.411102-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	77	\N	10.1.24.109/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.109/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.481Z", "custom_fields": {}}	47	\N	1
450	2019-08-27 00:56:31.416963-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	76	\N	10.1.24.108/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.108/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.411Z", "custom_fields": {}}	47	\N	1
451	2019-08-27 00:56:31.423995-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	75	\N	10.1.24.107/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.107/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:26:19.343Z", "custom_fields": {}}	47	\N	1
452	2019-08-27 00:56:31.429927-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	74	\N	10.1.24.103/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.103/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:26:19.277Z", "custom_fields": {}}	47	\N	1
453	2019-08-27 00:56:31.438703-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	73	\N	10.1.24.100/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.100/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "linux.linux_kernel.2.6.13", "last_updated": "2019-08-27T04:26:19.208Z", "custom_fields": {}}	47	\N	1
454	2019-08-27 00:56:31.44541-04	admin	23086093-5b65-46c8-95c0-80dfd7744227	3	72	\N	10.1.24.1/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.24.1/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:26:19.139Z", "custom_fields": {}}	47	\N	1
455	2019-08-27 00:57:18.698997-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	71	\N	10.1.27.99/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.99/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.539Z", "custom_fields": {}}	47	\N	1
456	2019-08-27 00:57:18.708457-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	70	\N	10.1.27.98/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.98/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.471Z", "custom_fields": {}}	47	\N	1
457	2019-08-27 00:57:18.714659-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	69	\N	10.1.27.97/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.97/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.399Z", "custom_fields": {}}	47	\N	1
458	2019-08-27 00:57:18.722264-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	68	\N	10.1.27.96/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.96/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.331Z", "custom_fields": {}}	47	\N	1
459	2019-08-27 00:57:18.728201-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	67	\N	10.1.27.95/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.95/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.264Z", "custom_fields": {}}	47	\N	1
460	2019-08-27 00:57:18.735507-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	66	\N	10.1.27.94/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.94/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.195Z", "custom_fields": {}}	47	\N	1
461	2019-08-27 00:57:18.741816-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	65	\N	10.1.27.93/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.93/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:25.123Z", "custom_fields": {}}	47	\N	1
462	2019-08-27 00:57:18.749265-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	64	\N	10.1.27.92/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.92/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:25.053Z", "custom_fields": {}}	47	\N	1
463	2019-08-27 00:57:18.756321-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	63	\N	10.1.27.91/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.91/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:24.987Z", "custom_fields": {}}	47	\N	1
464	2019-08-27 00:57:18.761949-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	62	\N	10.1.27.90/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.90/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.920Z", "custom_fields": {}}	47	\N	1
465	2019-08-27 00:57:18.769854-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	61	\N	10.1.27.89/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.89/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.854Z", "custom_fields": {}}	47	\N	1
466	2019-08-27 00:57:18.775474-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	60	\N	10.1.27.88/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.88/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.787Z", "custom_fields": {}}	47	\N	1
467	2019-08-27 00:57:18.782768-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	59	\N	10.1.27.87/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.87/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.718Z", "custom_fields": {}}	47	\N	1
468	2019-08-27 00:57:18.788796-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	58	\N	10.1.27.86/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.86/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.651Z", "custom_fields": {}}	47	\N	1
469	2019-08-27 00:57:18.794031-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	57	\N	10.1.27.85/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.85/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.582Z", "custom_fields": {}}	47	\N	1
470	2019-08-27 00:57:18.801362-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	56	\N	10.1.27.84/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.84/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.515Z", "custom_fields": {}}	47	\N	1
471	2019-08-27 00:57:18.806381-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	55	\N	10.1.27.83/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.83/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.449Z", "custom_fields": {}}	47	\N	1
472	2019-08-27 00:57:18.811371-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	54	\N	10.1.27.82/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.82/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.383Z", "custom_fields": {}}	47	\N	1
473	2019-08-27 00:57:18.818105-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	53	\N	10.1.27.81/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.81/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.313Z", "custom_fields": {}}	47	\N	1
474	2019-08-27 00:57:18.823255-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	52	\N	10.1.27.80/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.80/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:19:24.246Z", "custom_fields": {}}	47	\N	1
475	2019-08-27 00:57:18.828891-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	51	\N	10.1.27.79/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.79/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:23:53.705Z", "custom_fields": {}}	47	\N	1
476	2019-08-27 00:57:18.835101-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	50	\N	10.1.27.78/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.78/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:24.110Z", "custom_fields": {}}	47	\N	1
477	2019-08-27 00:57:18.842008-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	49	\N	10.1.27.77/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.77/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:23:53.568Z", "custom_fields": {}}	47	\N	1
478	2019-08-27 00:57:18.846818-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	48	\N	10.1.27.76/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.76/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:23:53.475Z", "custom_fields": {}}	47	\N	1
479	2019-08-27 00:57:18.852926-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	47	\N	10.1.27.75/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.75/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.905Z", "custom_fields": {}}	47	\N	1
480	2019-08-27 00:57:18.857866-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	46	\N	10.1.27.74/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.74/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.837Z", "custom_fields": {}}	47	\N	1
481	2019-08-27 00:57:18.864379-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	45	\N	10.1.27.73/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.73/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:23:53.296Z", "custom_fields": {}}	47	\N	1
482	2019-08-27 00:57:18.870175-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	44	\N	10.1.27.72/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.72/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.695Z", "custom_fields": {}}	47	\N	1
483	2019-08-27 00:57:18.87536-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	43	\N	10.1.27.71/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.71/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.627Z", "custom_fields": {}}	47	\N	1
484	2019-08-27 00:57:18.880314-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	42	\N	10.1.27.70/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.70/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "netgear.dg834g.", "last_updated": "2019-08-27T04:19:23.555Z", "custom_fields": {}}	47	\N	1
485	2019-08-27 00:57:18.886138-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	41	\N	10.1.27.25/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.25/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.485Z", "custom_fields": {}}	47	\N	1
486	2019-08-27 00:57:18.89107-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	40	\N	10.1.27.24/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.24/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.417Z", "custom_fields": {}}	47	\N	1
487	2019-08-27 00:57:18.897592-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	39	\N	10.1.27.230/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.230/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.350Z", "custom_fields": {}}	47	\N	1
488	2019-08-27 00:57:18.904535-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	38	\N	10.1.27.23/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.23/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.283Z", "custom_fields": {}}	47	\N	1
489	2019-08-27 00:57:18.909737-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	37	\N	10.1.27.229/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.229/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.215Z", "custom_fields": {}}	47	\N	1
490	2019-08-27 00:57:18.915891-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	36	\N	10.1.27.228/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.228/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_server_2008.", "last_updated": "2019-08-27T04:23:52.864Z", "custom_fields": {}}	47	\N	1
491	2019-08-27 00:57:18.921554-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	35	\N	10.1.27.227/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.227/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.079Z", "custom_fields": {}}	47	\N	1
492	2019-08-27 00:57:18.927917-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	34	\N	10.1.27.226/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.226/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:23.012Z", "custom_fields": {}}	47	\N	1
493	2019-08-27 00:57:18.935807-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	33	\N	10.1.27.225/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.225/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.944Z", "custom_fields": {}}	47	\N	1
494	2019-08-27 00:57:18.940752-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	32	\N	10.1.27.224/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.224/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.878Z", "custom_fields": {}}	47	\N	1
495	2019-08-27 00:57:18.94689-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	31	\N	10.1.27.223/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.223/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.811Z", "custom_fields": {}}	47	\N	1
496	2019-08-27 00:57:18.951917-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	30	\N	10.1.27.222/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.222/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.744Z", "custom_fields": {}}	47	\N	1
497	2019-08-27 00:57:18.956997-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	29	\N	10.1.27.221/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.221/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.675Z", "custom_fields": {}}	47	\N	1
498	2019-08-27 00:57:18.963972-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	28	\N	10.1.27.220/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.220/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.604Z", "custom_fields": {}}	47	\N	1
499	2019-08-27 00:57:18.96976-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	27	\N	10.1.27.22/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.22/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.537Z", "custom_fields": {}}	47	\N	1
500	2019-08-27 00:57:18.974643-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	26	\N	10.1.27.219/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.219/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.470Z", "custom_fields": {}}	47	\N	1
501	2019-08-27 00:57:18.98064-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	25	\N	10.1.27.218/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.218/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.402Z", "custom_fields": {}}	47	\N	1
502	2019-08-27 00:57:18.985533-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	24	\N	10.1.27.217/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.217/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.335Z", "custom_fields": {}}	47	\N	1
503	2019-08-27 00:57:18.991983-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	23	\N	10.1.27.216/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.216/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.266Z", "custom_fields": {}}	47	\N	1
504	2019-08-27 00:57:18.99681-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	22	\N	10.1.27.215/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.215/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.200Z", "custom_fields": {}}	47	\N	1
505	2019-08-27 00:57:19.003817-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	21	\N	10.1.27.214/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.214/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.134Z", "custom_fields": {}}	47	\N	1
506	2019-08-27 00:57:19.008781-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	20	\N	10.1.27.213/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.213/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:22.061Z", "custom_fields": {}}	47	\N	1
507	2019-08-27 00:57:19.014925-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	19	\N	10.1.27.212/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.212/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.995Z", "custom_fields": {}}	47	\N	1
508	2019-08-27 00:57:19.020458-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	18	\N	10.1.27.211/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.211/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.921Z", "custom_fields": {}}	47	\N	1
509	2019-08-27 00:57:19.026204-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	17	\N	10.1.27.210/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.210/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.845Z", "custom_fields": {}}	47	\N	1
510	2019-08-27 00:57:19.031032-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	16	\N	10.1.27.209/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.209/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.768Z", "custom_fields": {}}	47	\N	1
511	2019-08-27 00:57:19.036731-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	15	\N	10.1.27.208/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.208/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.698Z", "custom_fields": {}}	47	\N	1
512	2019-08-27 00:57:19.041909-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	14	\N	10.1.27.207/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.207/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.629Z", "custom_fields": {}}	47	\N	1
513	2019-08-27 00:57:19.048353-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	13	\N	10.1.27.206/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.206/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.562Z", "custom_fields": {}}	47	\N	1
514	2019-08-27 00:57:19.054863-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	12	\N	10.1.27.205/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.205/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.495Z", "custom_fields": {}}	47	\N	1
515	2019-08-27 00:57:19.0597-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	11	\N	10.1.27.204/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.204/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.424Z", "custom_fields": {}}	47	\N	1
516	2019-08-27 00:57:19.064614-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	10	\N	10.1.27.203/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.203/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.357Z", "custom_fields": {}}	47	\N	1
517	2019-08-27 00:57:19.070754-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	9	\N	10.1.27.202/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.202/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.286Z", "custom_fields": {}}	47	\N	1
518	2019-08-27 00:57:19.07696-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	8	\N	10.1.27.201/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.201/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.220Z", "custom_fields": {}}	47	\N	1
519	2019-08-27 00:57:19.084275-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	7	\N	10.1.27.111/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.111/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:19:21.152Z", "custom_fields": {}}	47	\N	1
520	2019-08-27 00:57:19.090858-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	6	\N	10.1.27.100/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.100/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "microsoft.windows_7.", "last_updated": "2019-08-27T04:19:21.085Z", "custom_fields": {}}	47	\N	1
521	2019-08-27 00:57:19.096832-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	5	\N	10.1.27.10/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.10/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "unknown host", "last_updated": "2019-08-27T04:19:21.014Z", "custom_fields": {}}	47	\N	1
522	2019-08-27 00:57:19.102685-04	admin	0c304dc5-cbe9-4c91-8503-e85bcab78cf1	3	4	\N	10.1.27.1/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.1/32", "created": "2019-08-27", "dns_name": "", "interface": null, "nat_inside": null, "description": "hp.switch_e3500yl.", "last_updated": "2019-08-27T04:19:20.932Z", "custom_fields": {}}	47	\N	1
523	2019-08-27 02:13:21.68182-04	admin	52d4bc42-8ef7-4993-8639-59ce3f4bed9d	3	2	1	VPS02	{"name": "VPS02", "tags": [], "device": 1, "parent": null, "serial": "SGH625WANS", "part_id": "719064-B21", "asset_tag": "Server002", "discovered": false, "description": "", "manufacturer": 4}	37	21	1
524	2019-08-27 02:13:23.780721-04	admin	e5f5c8fa-242b-4948-b0ca-ec143e11fe34	3	3	1	VPS03	{"name": "VPS03", "tags": [], "device": 1, "parent": null, "serial": "SGH708YBTY", "part_id": "719064-B21", "asset_tag": "Server004", "discovered": false, "description": "", "manufacturer": 4}	37	21	1
525	2019-08-27 02:13:27.990592-04	admin	8ddc3d1f-f6b9-44bd-8b7d-c26046d25f50	3	1	1	VPS04	{"name": "VPS04", "tags": [], "device": 1, "parent": null, "serial": "SGH829WLKQ", "part_id": "719064-B21", "asset_tag": "Server005", "discovered": false, "description": "", "manufacturer": 4}	37	21	1
526	2019-08-27 02:13:52.455968-04	admin	8379a7dc-52ab-4b6c-82cd-99a3d93a164c	2	1	\N	Server001	{"face": null, "name": "Server001", "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-26", "comments": "", "platform": null, "position": null, "asset_tag": "Server001", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:13:52.437Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
527	2019-08-27 02:14:13.388671-04	admin	de042908-a431-4acb-a5c5-883e7b62ae77	2	1	\N	Server001	{"face": null, "name": "Server001", "rack": null, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-26", "comments": "", "platform": null, "position": null, "asset_tag": "Server001", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:14:13.355Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
528	2019-08-27 02:15:43.160513-04	admin	2f3cf07f-3306-406e-b9dc-e21097733407	2	2	\N	Server006	{"face": null, "name": "Server006", "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": null, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:15:43.140Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
529	2019-08-27 02:17:17.559613-04	admin	efe81884-f516-4102-a3e0-9a161fe48c82	1	11	\N	ESXi 6.0	{"name": "ESXi 6.0", "slug": "esxi-6-0", "created": "2019-08-27", "napalm_args": null, "last_updated": "2019-08-27T06:17:17.554Z", "manufacturer": null, "napalm_driver": ""}	26	\N	1
530	2019-08-27 02:17:45.447777-04	admin	a1a7c558-fed6-4c8b-a29f-103df1ce0936	2	2	\N	Server006	{"face": null, "name": "Server006", "rack": null, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:17:45.424Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
531	2019-08-27 02:20:22.251134-04	admin	3db29ada-2b2a-42a0-8a5f-da4c14bca511	2	1	\N	Server001	{"face": 0, "name": "Server001", "rack": 1, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-26", "comments": "", "platform": 11, "position": null, "asset_tag": "Server001", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:20:22.232Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
532	2019-08-27 02:21:06.729481-04	admin	f63697f3-fe9e-4175-af00-0634d6a562b0	2	2	\N	Server006	{"face": 0, "name": "Server006", "rack": 1, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:21:06.707Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
533	2019-08-27 02:23:57.661673-04	admin	749a2569-5401-4e88-8f3b-0f3d5568b763	2	2	\N	Server006	{"face": 0, "name": "Server006", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH850TH90", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:23:57.644Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
534	2019-08-27 02:27:02.710582-04	admin	2605c487-0a62-4db7-8524-ba66743aaee0	2	1	\N	Server001	{"face": 0, "name": "Server001", "rack": 1, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": 4, "created": "2019-08-26", "comments": "", "platform": 11, "position": null, "asset_tag": "Server001", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:27:02.694Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
535	2019-08-27 02:29:56.401655-04	admin	adb54584-4f71-49ea-83cf-fee07a972a2a	2	2	\N	Server006	{"face": 0, "name": "Server006", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH850TH90", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:29:56.377Z", "custom_fields": {}, "virtual_chassis": 1, "local_context_data": null}	21	\N	1
536	2019-08-27 02:29:56.4093-04	admin	adb54584-4f71-49ea-83cf-fee07a972a2a	1	1	\N	Server006	{"tags": [], "domain": "", "master": 2, "created": "2019-08-27", "last_updated": "2019-08-27T06:29:56.370Z"}	38	\N	1
537	2019-08-27 02:29:56.415687-04	admin	adb54584-4f71-49ea-83cf-fee07a972a2a	2	2	\N	Server006	{"face": 0, "name": "Server006", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH850TH90", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": 1, "vc_priority": 1, "last_updated": "2019-08-27T06:29:56.388Z", "custom_fields": {}, "virtual_chassis": 1, "local_context_data": null}	21	\N	1
538	2019-08-27 02:30:08.469668-04	admin	ff571ac2-7979-44ee-a327-2463b97be380	3	1	\N	Server006	{"tags": [], "domain": "", "master": 2, "created": "2019-08-27", "last_updated": "2019-08-27T06:29:56.370Z"}	38	\N	1
539	2019-08-27 02:30:08.480454-04	admin	5f5b00e5-ee4a-42b1-8bfd-68af91972c5c	2	2	\N	Server006	{"face": 0, "name": "Server006", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH850TH90", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:30:08.457Z", "custom_fields": {}, "virtual_chassis": 1, "local_context_data": null}	21	\N	1
540	2019-08-27 02:31:54.392757-04	admin	9d4acb5a-000d-4582-a6cd-f84947bc1373	1	10	\N	Sophos	{"name": "Sophos", "slug": "sophos", "created": "2019-08-27", "last_updated": "2019-08-27T06:31:54.386Z"}	25	\N	1
541	2019-08-27 02:32:39.472035-04	admin	00305ecb-100f-4238-9703-74681d307c67	1	3	\N	SG310	{"slug": "sg310", "tags": [], "model": "SG310", "created": "2019-08-27", "comments": "", "u_height": 1, "part_number": "", "last_updated": "2019-08-27T06:32:39.453Z", "manufacturer": 10, "custom_fields": {}, "is_full_depth": true, "subdevice_role": null}	23	\N	1
542	2019-08-27 02:33:10.48246-04	admin	9671d207-7c2e-4318-b325-a2799b25769d	1	3	\N	Firewall001	{"face": null, "name": "Firewall001", "rack": null, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": null, "position": null, "asset_tag": "Firewall001", "device_role": 6, "device_type": 3, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:33:10.431Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
543	2019-08-27 02:33:28.059529-04	admin	93fda30b-4fbc-4f34-ba70-50e02fc02036	2	3	\N	Firewall001	{"face": 0, "name": "Firewall001", "rack": 2, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": null, "position": null, "asset_tag": "Firewall001", "device_role": 6, "device_type": 3, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:33:28.039Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
544	2019-08-27 02:33:49.995051-04	admin	7faaa794-d59e-41ae-a709-b531f9343837	2	8	\N	Server	{"name": "Server", "slug": "server", "color": "4caf50", "created": null, "vm_role": true, "last_updated": "2019-08-27T06:33:49.991Z"}	22	\N	1
545	2019-08-27 02:33:57.112715-04	admin	8e559a2a-5914-4c69-a63f-4f3299bc6d31	2	1	\N	Console Server	{"name": "Console Server", "slug": "console-server", "color": "cddc39", "created": null, "vm_role": true, "last_updated": "2019-08-27T06:33:57.106Z"}	22	\N	1
546	2019-08-27 02:36:17.60765-04	admin	101d3d07-86f4-41e6-8260-98a850fb7713	1	13	3	Interface1	{"lag": null, "mtu": null, "mode": 100, "name": "Interface1", "tags": [], "type": 1000, "cable": null, "device": 3, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	1
547	2019-08-27 02:36:59.939241-04	admin	8d46298d-dc4c-4625-9a48-31d0013643e3	1	174	13	10.1.127.1/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.127.1/32", "created": "2019-08-27", "dns_name": "", "interface": 13, "nat_inside": null, "description": "4680", "last_updated": "2019-08-27T06:36:59.900Z", "custom_fields": {}}	47	5	1
548	2019-08-27 02:36:59.94755-04	admin	8d46298d-dc4c-4625-9a48-31d0013643e3	2	3	\N	Firewall001	{"face": 0, "name": "Firewall001", "rack": 2, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": null, "position": null, "asset_tag": "Firewall001", "device_role": 6, "device_type": 3, "primary_ip4": 174, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:36:59.919Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
549	2019-08-27 02:38:33.31959-04	admin	16a7b6b2-d70e-4c92-9333-e3a66fa485dd	2	174	13	10.1.127.1/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.127.1/32", "created": "2019-08-27", "dns_name": "", "interface": 13, "nat_inside": null, "description": "4680", "last_updated": "2019-08-27T06:38:33.292Z", "custom_fields": {}}	47	5	1
550	2019-08-27 02:38:33.326289-04	admin	16a7b6b2-d70e-4c92-9333-e3a66fa485dd	2	3	\N	Firewall001	{"face": 0, "name": "Firewall001", "rack": 2, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": null, "position": null, "asset_tag": "Firewall001", "device_role": 6, "device_type": 3, "primary_ip4": 174, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:38:33.304Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
551	2019-08-27 02:43:18.050797-04	admin	19c3c0b4-dc4b-42a5-a628-c513782dd312	1	14	1	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 800, "cable": null, "device": 1, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	1
552	2019-08-27 02:43:31.741095-04	admin	82e97651-9b9b-409d-8c61-d4b7a647358d	2	14	1	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": ["auto"], "type": 800, "cable": null, "device": 1, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	1
553	2019-08-27 02:44:12.872666-04	admin	7253c915-f5ed-4c82-9ecc-1ba8ef0de91d	1	175	14	10.1.3.21/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.21/32", "created": "2019-08-27", "dns_name": "", "interface": 14, "nat_inside": null, "description": "", "last_updated": "2019-08-27T06:44:12.837Z", "custom_fields": {}}	47	5	1
554	2019-08-27 02:44:12.88019-04	admin	7253c915-f5ed-4c82-9ecc-1ba8ef0de91d	2	1	\N	Server001	{"face": 0, "name": "Server001", "rack": 1, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": 4, "created": "2019-08-26", "comments": "", "platform": 11, "position": null, "asset_tag": "Server001", "device_role": 8, "device_type": 1, "primary_ip4": 175, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T06:44:12.853Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	1
555	2019-08-27 02:49:43.953103-04	admin	9168c088-7c63-4f24-9b3b-ec79fdc2c205	1	15	14	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 14, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	1
556	2019-08-27 02:50:07.746172-04	admin	34617111-75c0-452a-9808-abf80d3b6532	1	176	15	10.1.27.212/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.212/32", "created": "2019-08-27", "dns_name": "", "interface": 15, "nat_inside": null, "description": "", "last_updated": "2019-08-27T06:50:07.715Z", "custom_fields": {}}	47	5	1
557	2019-08-27 02:50:07.753665-04	admin	34617111-75c0-452a-9808-abf80d3b6532	2	14	\N	HO-SRV-autoFillCRC12	{"disk": 50, "name": "HO-SRV-autoFillCRC12", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 176, "primary_ip6": null, "last_updated": "2019-08-27T06:50:07.732Z", "custom_fields": {}, "local_context_data": null}	79	\N	1
558	2019-08-27 03:17:21.047614-04	vuongnx	c6781266-903d-47cc-94b1-9f99f0a6df8e	1	16	15	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 15, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
559	2019-08-27 03:17:32.004341-04	vuongnx	8cf82791-ceda-4b55-8369-6250b27717d9	1	177	16	10.1.27.213/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.213/32", "created": "2019-08-27", "dns_name": "", "interface": 16, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:17:31.969Z", "custom_fields": {}}	47	5	2
560	2019-08-27 03:17:32.012879-04	vuongnx	8cf82791-ceda-4b55-8369-6250b27717d9	2	15	\N	HO-SRV-autoFillCRC13	{"disk": 50, "name": "HO-SRV-autoFillCRC13", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 177, "primary_ip6": null, "last_updated": "2019-08-27T07:17:31.982Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
561	2019-08-27 03:17:43.859301-04	vuongnx	ee9f74b5-948e-4915-968d-499f0bc37121	1	17	16	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 16, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
562	2019-08-27 03:17:52.407476-04	vuongnx	1fb54bdf-5499-4bf7-8b9c-18411b952cc0	1	178	17	10.1.27.214/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.214/32", "created": "2019-08-27", "dns_name": "", "interface": 17, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:17:52.379Z", "custom_fields": {}}	47	5	2
563	2019-08-27 03:17:52.415443-04	vuongnx	1fb54bdf-5499-4bf7-8b9c-18411b952cc0	2	16	\N	HO-SRV-autoFillCRC14	{"disk": 50, "name": "HO-SRV-autoFillCRC14", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 178, "primary_ip6": null, "last_updated": "2019-08-27T07:17:52.393Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
564	2019-08-27 03:18:12.850032-04	vuongnx	26599451-9a70-4787-82cb-7c8af4b6a95e	1	18	17	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 17, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
565	2019-08-27 03:18:22.945572-04	vuongnx	d8f63331-6aee-45a5-8435-13cea2acbd56	1	179	18	10.1.27.215/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.215/32", "created": "2019-08-27", "dns_name": "", "interface": 18, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:18:22.917Z", "custom_fields": {}}	47	5	2
566	2019-08-27 03:18:22.952808-04	vuongnx	d8f63331-6aee-45a5-8435-13cea2acbd56	2	17	\N	HO-SRV-autoFillCRC15	{"disk": 50, "name": "HO-SRV-autoFillCRC15", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 179, "primary_ip6": null, "last_updated": "2019-08-27T07:18:22.932Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
567	2019-08-27 03:18:53.631518-04	vuongnx	f6a8dc14-9494-4a5f-9b8b-5e8b259f7492	2	8	\N	Server	{"name": "Server", "slug": "server", "color": "ffeb3b", "created": null, "vm_role": true, "last_updated": "2019-08-27T07:18:53.626Z"}	22	\N	2
568	2019-08-27 03:19:07.856843-04	vuongnx	638ff532-cee8-45e9-96b1-dedab39e486a	2	8	\N	Server	{"name": "Server", "slug": "server", "color": "ff9800", "created": null, "vm_role": true, "last_updated": "2019-08-27T07:19:07.851Z"}	22	\N	2
569	2019-08-27 03:19:18.251902-04	vuongnx	07733e52-131c-4dde-a806-fbd20183f781	2	8	\N	Server	{"name": "Server", "slug": "server", "color": "c0c0c0", "created": null, "vm_role": true, "last_updated": "2019-08-27T07:19:18.248Z"}	22	\N	2
570	2019-08-27 03:19:32.54468-04	vuongnx	b55e0b44-6516-46e5-90c9-e233ec8356dd	1	19	18	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 18, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
571	2019-08-27 03:19:40.717692-04	vuongnx	5ee4c446-ae5f-4612-933a-3b0819fbd592	1	180	19	10.1.27.216/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.216/32", "created": "2019-08-27", "dns_name": "", "interface": 19, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:19:40.692Z", "custom_fields": {}}	47	5	2
572	2019-08-27 03:19:40.72635-04	vuongnx	5ee4c446-ae5f-4612-933a-3b0819fbd592	2	18	\N	HO-SRV-autoFillCRC16	{"disk": 50, "name": "HO-SRV-autoFillCRC16", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 180, "primary_ip6": null, "last_updated": "2019-08-27T07:19:40.703Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
573	2019-08-27 03:19:59.479399-04	vuongnx	adaba2a0-094e-43e3-8054-f3cf58e56fde	1	20	19	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 19, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
574	2019-08-27 03:20:14.528842-04	vuongnx	142bbde1-3048-497d-b6ef-bbfaa16b9bb8	1	181	20	10.1.27.217/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.217/32", "created": "2019-08-27", "dns_name": "", "interface": 20, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:20:14.503Z", "custom_fields": {}}	47	5	2
575	2019-08-27 03:20:14.535255-04	vuongnx	142bbde1-3048-497d-b6ef-bbfaa16b9bb8	2	19	\N	HO-SRV-autoFillCRC17	{"disk": 50, "name": "HO-SRV-autoFillCRC17", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 181, "primary_ip6": null, "last_updated": "2019-08-27T07:20:14.516Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
576	2019-08-27 03:22:21.033253-04	vuongnx	7c9f6b33-35e9-4e73-ad7b-95a8fa181eed	1	21	20	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 20, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
577	2019-08-27 03:22:38.881988-04	vuongnx	83e7b5da-7314-47f8-b768-1ee1f493e1d1	1	182	21	10.1.27.218/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.218/32", "created": "2019-08-27", "dns_name": "", "interface": 21, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:22:38.856Z", "custom_fields": {}}	47	5	2
578	2019-08-27 03:22:38.891313-04	vuongnx	83e7b5da-7314-47f8-b768-1ee1f493e1d1	2	20	\N	HO-SRV-autoFillCRC18	{"disk": 50, "name": "HO-SRV-autoFillCRC18", "role": 8, "tags": [], "vcpus": 1, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 182, "primary_ip6": null, "last_updated": "2019-08-27T07:22:38.868Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
579	2019-08-27 03:23:26.911426-04	vuongnx	47bebc55-322d-45fb-978d-ccfccedeb3a6	1	22	21	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 21, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
580	2019-08-27 03:23:41.440143-04	vuongnx	dd6f335a-b306-44d5-aea3-86fae93b9773	1	183	22	10.1.27.219/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.219/32", "created": "2019-08-27", "dns_name": "", "interface": 22, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:23:41.409Z", "custom_fields": {}}	47	5	2
581	2019-08-27 03:23:41.448655-04	vuongnx	dd6f335a-b306-44d5-aea3-86fae93b9773	2	21	\N	HO-SRV-autoFillCRC19	{"disk": 50, "name": "HO-SRV-autoFillCRC19", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 7, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 183, "primary_ip6": null, "last_updated": "2019-08-27T07:23:41.426Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
582	2019-08-27 03:23:50.797861-04	vuongnx	ab139d52-e43c-488a-b4f4-0ff839a1a57c	1	23	22	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 22, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
583	2019-08-27 03:23:59.411726-04	vuongnx	757fdbcc-6d3e-4221-95e4-04ae2074160b	1	184	23	10.1.27.220/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.220/32", "created": "2019-08-27", "dns_name": "", "interface": 23, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:23:59.385Z", "custom_fields": {}}	47	5	2
584	2019-08-27 03:23:59.419443-04	vuongnx	757fdbcc-6d3e-4221-95e4-04ae2074160b	2	22	\N	HO-SRV-autoFillCRC20	{"disk": 50, "name": "HO-SRV-autoFillCRC20", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 184, "primary_ip6": null, "last_updated": "2019-08-27T07:23:59.397Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
585	2019-08-27 03:24:58.078976-04	vuongnx	28e3f420-6723-4c50-b220-c3c8187ce4bb	1	24	23	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 23, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
586	2019-08-27 03:25:06.196755-04	vuongnx	418633a2-1756-4a19-ba0d-de99de9461af	1	185	24	10.1.27.221/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.221/32", "created": "2019-08-27", "dns_name": "", "interface": 24, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:25:06.172Z", "custom_fields": {}}	47	5	2
587	2019-08-27 03:25:06.206587-04	vuongnx	418633a2-1756-4a19-ba0d-de99de9461af	2	23	\N	HO-SRV-autoFillCRC21	{"disk": 50, "name": "HO-SRV-autoFillCRC21", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 185, "primary_ip6": null, "last_updated": "2019-08-27T07:25:06.182Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
588	2019-08-27 03:25:18.08559-04	vuongnx	3a730fca-ca64-4176-8f57-5c67246e3fb7	1	25	24	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 24, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
589	2019-08-27 03:25:25.725963-04	vuongnx	e2fa5e45-3bc5-479b-a89d-6d12aa7fa271	1	186	25	10.1.27.222/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.222/32", "created": "2019-08-27", "dns_name": "", "interface": 25, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:25:25.696Z", "custom_fields": {}}	47	5	2
590	2019-08-27 03:25:25.732886-04	vuongnx	e2fa5e45-3bc5-479b-a89d-6d12aa7fa271	2	24	\N	HO-SRV-autoFillCRC22	{"disk": 50, "name": "HO-SRV-autoFillCRC22", "role": 8, "tags": [], "vcpus": 1, "memory": 4096, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 186, "primary_ip6": null, "last_updated": "2019-08-27T07:25:25.711Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
591	2019-08-27 03:25:52.011059-04	vuongnx	daefaee7-26a6-445c-baa7-1c1d712b94a8	1	26	25	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 25, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
592	2019-08-27 03:26:01.249569-04	vuongnx	d154dbe7-df3e-4978-aa8b-76616783b284	1	187	26	10.1.27.223/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.223/32", "created": "2019-08-27", "dns_name": "", "interface": 26, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:26:01.226Z", "custom_fields": {}}	47	5	2
593	2019-08-27 03:26:01.257982-04	vuongnx	d154dbe7-df3e-4978-aa8b-76616783b284	2	25	\N	HO-SRV-autoFillCRC23	{"disk": 50, "name": "HO-SRV-autoFillCRC23", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 7, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 187, "primary_ip6": null, "last_updated": "2019-08-27T07:26:01.237Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
594	2019-08-27 03:26:17.041518-04	vuongnx	73bb5cf6-ddba-4516-ba79-a3497fb3907b	1	27	26	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 26, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
595	2019-08-27 03:26:25.327027-04	vuongnx	d4fd57b7-8763-416b-8869-a3082b62d7a8	1	188	27	10.1.27.224/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.224/32", "created": "2019-08-27", "dns_name": "", "interface": 27, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:26:25.298Z", "custom_fields": {}}	47	5	2
596	2019-08-27 03:26:25.335383-04	vuongnx	d4fd57b7-8763-416b-8869-a3082b62d7a8	2	26	\N	HO-SRV-autoFillCRC24	{"disk": 50, "name": "HO-SRV-autoFillCRC24", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 4, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 188, "primary_ip6": null, "last_updated": "2019-08-27T07:26:25.310Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
597	2019-08-27 03:26:35.375013-04	vuongnx	c2b1eb13-4688-4c40-a166-581bb2a40ef1	1	28	27	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 27, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
598	2019-08-27 03:26:42.574133-04	vuongnx	b77cf8de-85ea-4158-b7f2-38df1d7031f5	1	189	28	10.1.27.22/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.22/32", "created": "2019-08-27", "dns_name": "", "interface": 28, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:26:42.546Z", "custom_fields": {}}	47	5	2
599	2019-08-27 03:26:42.581148-04	vuongnx	b77cf8de-85ea-4158-b7f2-38df1d7031f5	2	27	\N	HO-SRV-autoFillCRC25	{"disk": 50, "name": "HO-SRV-autoFillCRC25", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 189, "primary_ip6": null, "last_updated": "2019-08-27T07:26:42.559Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
600	2019-08-27 03:26:55.914555-04	vuongnx	ec48febe-0ad7-4220-990a-7363bc2a939a	2	189	28	10.1.27.225/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.225/32", "created": "2019-08-27", "dns_name": "", "interface": 28, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:26:55.894Z", "custom_fields": {}}	47	5	2
601	2019-08-27 03:26:55.922073-04	vuongnx	ec48febe-0ad7-4220-990a-7363bc2a939a	2	27	\N	HO-SRV-autoFillCRC25	{"disk": 50, "name": "HO-SRV-autoFillCRC25", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 189, "primary_ip6": null, "last_updated": "2019-08-27T07:26:55.905Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
602	2019-08-27 03:27:03.903058-04	vuongnx	157c0ba6-f075-4880-aa2d-fd9e8146152e	1	29	28	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 28, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
603	2019-08-27 03:27:12.22896-04	vuongnx	4a1ae2ab-4ea0-4837-a76c-4fa61544ae69	1	190	29	10.1.27.226/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.226/32", "created": "2019-08-27", "dns_name": "", "interface": 29, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:27:12.206Z", "custom_fields": {}}	47	5	2
604	2019-08-27 03:27:12.235875-04	vuongnx	4a1ae2ab-4ea0-4837-a76c-4fa61544ae69	2	28	\N	HO-SRV-autoFillCRC26	{"disk": 50, "name": "HO-SRV-autoFillCRC26", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 190, "primary_ip6": null, "last_updated": "2019-08-27T07:27:12.217Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
605	2019-08-27 03:27:21.601032-04	vuongnx	97bc6c70-e4ca-4ee5-8725-81d877b8b254	1	30	29	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 29, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
606	2019-08-27 03:27:29.928648-04	vuongnx	2b5a26bd-2b68-4078-93c1-d64e6e56d0e0	1	191	30	10.1.27.227/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.227/32", "created": "2019-08-27", "dns_name": "", "interface": 30, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:27:29.903Z", "custom_fields": {}}	47	5	2
607	2019-08-27 03:27:29.937225-04	vuongnx	2b5a26bd-2b68-4078-93c1-d64e6e56d0e0	2	29	\N	HO-SRV-autoFillCRC27	{"disk": 50, "name": "HO-SRV-autoFillCRC27", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 191, "primary_ip6": null, "last_updated": "2019-08-27T07:27:29.913Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
608	2019-08-27 03:27:45.208526-04	vuongnx	8b7639a2-071e-4f45-8acf-62bcf4dde4fe	1	31	30	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 30, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
609	2019-08-27 03:27:53.108394-04	vuongnx	3824a7d3-de59-4e73-bba8-746dc2f386f7	1	192	31	10.1.27.228/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.228/32", "created": "2019-08-27", "dns_name": "", "interface": 31, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:27:53.080Z", "custom_fields": {}}	47	5	2
610	2019-08-27 03:27:53.11513-04	vuongnx	3824a7d3-de59-4e73-bba8-746dc2f386f7	2	30	\N	HO-SRV-autoFillCRC28	{"disk": 50, "name": "HO-SRV-autoFillCRC28", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 192, "primary_ip6": null, "last_updated": "2019-08-27T07:27:53.093Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
611	2019-08-27 03:28:02.372739-04	vuongnx	52ef14a0-8fff-4a8f-b8c9-57cba6a101c6	1	32	31	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 31, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
612	2019-08-27 03:28:12.111904-04	vuongnx	b4e7a859-a71c-4de9-9399-5c7aa09c7f17	1	193	32	10.1.27.229/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.229/32", "created": "2019-08-27", "dns_name": "", "interface": 32, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:28:12.079Z", "custom_fields": {}}	47	5	2
613	2019-08-27 03:28:12.119048-04	vuongnx	b4e7a859-a71c-4de9-9399-5c7aa09c7f17	2	31	\N	HO-SRV-autoFillCRC29	{"disk": 50, "name": "HO-SRV-autoFillCRC29", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 193, "primary_ip6": null, "last_updated": "2019-08-27T07:28:12.098Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
614	2019-08-27 03:28:25.300377-04	vuongnx	7d69727e-91fe-4bcf-98e4-893dfa8d4fb1	1	33	32	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 32, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
615	2019-08-27 03:28:35.972309-04	vuongnx	d10edd63-ac1c-40ad-bcc7-ab628d921c67	1	194	33	10.1.27.230/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.230/32", "created": "2019-08-27", "dns_name": "", "interface": 33, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:28:35.940Z", "custom_fields": {}}	47	5	2
616	2019-08-27 03:28:35.98041-04	vuongnx	d10edd63-ac1c-40ad-bcc7-ab628d921c67	2	32	\N	HO-SRV-autoFillCRC30	{"disk": 50, "name": "HO-SRV-autoFillCRC30", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 8, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 194, "primary_ip6": null, "last_updated": "2019-08-27T07:28:35.957Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
617	2019-08-27 03:31:02.755333-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	33	\N	HO-SRV-AutoFillPre01	{"disk": 50, "name": "HO-SRV-AutoFillPre01", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.396Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
618	2019-08-27 03:31:02.764673-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	34	\N	HO-SRV-AutoFillPre02	{"disk": 50, "name": "HO-SRV-AutoFillPre02", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.407Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
619	2019-08-27 03:31:02.773818-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	35	\N	HO-SRV-AutoFillPre03	{"disk": 50, "name": "HO-SRV-AutoFillPre03", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.416Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
620	2019-08-27 03:31:02.784113-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	36	\N	HO-SRV-AutoFillPre04	{"disk": 50, "name": "HO-SRV-AutoFillPre04", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.429Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
621	2019-08-27 03:31:02.793795-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	37	\N	HO-SRV-AutoFillPre05	{"disk": 50, "name": "HO-SRV-AutoFillPre05", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.441Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
622	2019-08-27 03:31:02.802757-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	38	\N	HO-SRV-AutoFillPre06	{"disk": 50, "name": "HO-SRV-AutoFillPre06", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.450Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
623	2019-08-27 03:31:02.812474-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	39	\N	HO-SRV-AutoFillPre07	{"disk": 50, "name": "HO-SRV-AutoFillPre07", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.463Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
624	2019-08-27 03:31:02.823274-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	40	\N	HO-SRV-AutoFillPre08	{"disk": 50, "name": "HO-SRV-AutoFillPre08", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.472Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
625	2019-08-27 03:31:02.835568-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	41	\N	HO-SRV-AutoFillPre09	{"disk": 50, "name": "HO-SRV-AutoFillPre09", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.482Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
626	2019-08-27 03:31:02.843908-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	42	\N	HO-SRV-AutoFillPre10	{"disk": 50, "name": "HO-SRV-AutoFillPre10", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.492Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
627	2019-08-27 03:31:02.854709-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	43	\N	HO-SRV-AutoFillPre11	{"disk": 50, "name": "HO-SRV-AutoFillPre11", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.503Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
628	2019-08-27 03:31:02.864651-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	44	\N	HO-SRV-AutoFillPre12	{"disk": 50, "name": "HO-SRV-AutoFillPre12", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.512Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
629	2019-08-27 03:31:02.87443-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	45	\N	HO-SRV-AutoFillPre13	{"disk": 50, "name": "HO-SRV-AutoFillPre13", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.522Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
630	2019-08-27 03:31:02.88633-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	46	\N	HO-SRV-AutoFillPre14	{"disk": 50, "name": "HO-SRV-AutoFillPre14", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.530Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
631	2019-08-27 03:31:02.894883-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	47	\N	HO-SRV-AutoFillPre15	{"disk": 50, "name": "HO-SRV-AutoFillPre15", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.538Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
632	2019-08-27 03:31:02.903826-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	48	\N	HO-SRV-AutoFillPre16	{"disk": 50, "name": "HO-SRV-AutoFillPre16", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.547Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
681	2019-08-27 23:08:33.578149-04	vuongnx	d9488395-62bd-4e7a-aa9a-0301791b70ce	1	203	42	10.1.3.24/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.24/32", "created": "2019-08-28", "dns_name": "", "interface": 42, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:08:33.548Z", "custom_fields": {}}	47	5	2
633	2019-08-27 03:31:02.913516-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	49	\N	HO-SRV-AutoFillPre17	{"disk": 50, "name": "HO-SRV-AutoFillPre17", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.554Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
634	2019-08-27 03:31:02.92403-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	50	\N	HO-SRV-AutoFillPre18	{"disk": 50, "name": "HO-SRV-AutoFillPre18", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.561Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
635	2019-08-27 03:31:02.940899-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	51	\N	HO-SRV-AutoFillPre19	{"disk": 50, "name": "HO-SRV-AutoFillPre19", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.571Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
636	2019-08-27 03:31:02.952444-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	52	\N	HO-SRV-AutoFillPre20	{"disk": 50, "name": "HO-SRV-AutoFillPre20", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.581Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
637	2019-08-27 03:31:02.961755-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	53	\N	HO-SRV-AutoFillPre21	{"disk": 50, "name": "HO-SRV-AutoFillPre21", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.590Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
638	2019-08-27 03:31:02.97149-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	54	\N	HO-SRV-AutoFillPre22	{"disk": 50, "name": "HO-SRV-AutoFillPre22", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.599Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
639	2019-08-27 03:31:02.980425-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	55	\N	HO-SRV-AutoFillPre23	{"disk": 50, "name": "HO-SRV-AutoFillPre23", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.607Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
640	2019-08-27 03:31:02.989435-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	56	\N	HO-SRV-AutoFillPre24	{"disk": 50, "name": "HO-SRV-AutoFillPre24", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.615Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
641	2019-08-27 03:31:02.99926-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	57	\N	HO-SRV-AutoFillPre25	{"disk": 50, "name": "HO-SRV-AutoFillPre25", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.623Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
642	2019-08-27 03:31:03.008746-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	58	\N	HO-SRV-AutoFillPre26	{"disk": 50, "name": "HO-SRV-AutoFillPre26", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.631Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
643	2019-08-27 03:31:03.017084-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	59	\N	HO-SRV-AutoFillPre27	{"disk": 50, "name": "HO-SRV-AutoFillPre27", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.640Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
644	2019-08-27 03:31:03.026115-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	60	\N	HO-SRV-AutoFillPre28	{"disk": 50, "name": "HO-SRV-AutoFillPre28", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.647Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
645	2019-08-27 03:31:03.037611-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	61	\N	HO-SRV-AutoFillPre29	{"disk": 50, "name": "HO-SRV-AutoFillPre29", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.655Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
646	2019-08-27 03:31:03.047391-04	vuongnx	eb0db587-e5ee-46fe-9ff2-49b0e7ed3ab4	1	62	\N	HO-SRV-AutoFillPre30	{"disk": 50, "name": "HO-SRV-AutoFillPre30", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": null, "primary_ip6": null, "last_updated": "2019-08-27T07:31:02.664Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
647	2019-08-27 03:31:44.78832-04	vuongnx	e183bd5f-1d06-44b4-9b4d-da3c81bc407e	1	34	33	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 0, "cable": null, "device": null, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": 33, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	79	2
648	2019-08-27 03:31:57.421787-04	vuongnx	96f97270-5160-41cb-87b5-06925ac0058e	1	195	34	10.1.27.71/32	{"vrf": null, "role": null, "tags": ["auto"], "family": 4, "status": 1, "tenant": null, "address": "10.1.27.71/32", "created": "2019-08-27", "dns_name": "", "interface": 34, "nat_inside": null, "description": "", "last_updated": "2019-08-27T07:31:57.392Z", "custom_fields": {}}	47	5	2
649	2019-08-27 03:31:57.429644-04	vuongnx	96f97270-5160-41cb-87b5-06925ac0058e	2	33	\N	HO-SRV-AutoFillPre01	{"disk": 50, "name": "HO-SRV-AutoFillPre01", "role": 8, "tags": [], "vcpus": 2, "memory": 2048, "status": 1, "tenant": null, "cluster": 9, "created": "2019-08-27", "comments": "", "platform": 10, "primary_ip4": 195, "primary_ip6": null, "last_updated": "2019-08-27T07:31:57.409Z", "custom_fields": {}, "local_context_data": null}	79	\N	2
650	2019-08-27 06:37:57.701148-04	vuongnx	d7e769ac-ee9e-4bbe-adf8-e54e5a490f55	2	1	\N	VPS01	{"face": 0, "name": "VPS01", "rack": 1, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": 4, "created": "2019-08-26", "comments": "", "platform": 11, "position": null, "asset_tag": "Server001", "device_role": 8, "device_type": 1, "primary_ip4": 175, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T10:37:57.680Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
651	2019-08-27 06:38:18.794703-04	vuongnx	c24acf60-1240-4676-884e-bd0f2530c259	2	2	\N	VPS05	{"face": 0, "name": "VPS05", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH850TH90", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T10:38:18.777Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
652	2019-08-27 06:39:16.682999-04	vuongnx	b94b2724-4e67-48cb-9e03-23fb1de995ce	1	35	1	iLo	{"lag": null, "mtu": null, "mode": null, "name": "iLo", "tags": [], "type": 0, "cable": null, "device": 1, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
653	2019-08-27 06:39:28.321355-04	vuongnx	ce6236e0-86c7-4762-94b0-e65360102f11	1	196	35	10.1.3.101/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.101/32", "created": "2019-08-27", "dns_name": "", "interface": 35, "nat_inside": null, "description": "", "last_updated": "2019-08-27T10:39:28.301Z", "custom_fields": {}}	47	5	2
654	2019-08-27 06:40:26.064495-04	vuongnx	c8ded247-60d9-4c94-8745-93e814aab7f9	1	36	2	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 0, "cable": null, "device": 2, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
655	2019-08-27 06:40:39.373056-04	vuongnx	4c2c9207-49f3-4141-a2c4-d13436eeadc3	1	197	36	10.1.3.25/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.25/32", "created": "2019-08-27", "dns_name": "", "interface": 36, "nat_inside": null, "description": "", "last_updated": "2019-08-27T10:40:39.341Z", "custom_fields": {}}	47	5	2
656	2019-08-27 06:40:39.381195-04	vuongnx	4c2c9207-49f3-4141-a2c4-d13436eeadc3	2	2	\N	VPS05	{"face": 0, "name": "VPS05", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH850TH90", "status": 1, "tenant": null, "cluster": 5, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server006", "device_role": 8, "device_type": 2, "primary_ip4": 197, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T10:40:39.353Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
657	2019-08-27 06:40:44.738699-04	vuongnx	3f43e9bd-c10b-4a73-a592-72e37c684390	1	37	2	iLo	{"lag": null, "mtu": null, "mode": null, "name": "iLo", "tags": [], "type": 0, "cable": null, "device": 2, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
658	2019-08-27 06:40:54.72912-04	vuongnx	65efc22a-6c4a-4bab-9c20-f952003b296a	1	198	37	10.1.3.105/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.105/32", "created": "2019-08-27", "dns_name": "", "interface": 37, "nat_inside": null, "description": "", "last_updated": "2019-08-27T10:40:54.703Z", "custom_fields": {}}	47	5	2
659	2019-08-27 06:41:20.680899-04	vuongnx	33082700-7a9d-45f2-b6b4-6042ffc0342a	2	36	2	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 1000, "cable": null, "device": 2, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
660	2019-08-27 06:41:42.466199-04	vuongnx	d62e1eca-23a6-4bf6-9e08-53fc1689f4d5	2	198	37	10.1.3.105/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.105/32", "created": "2019-08-27", "dns_name": "", "interface": 37, "nat_inside": null, "description": "", "last_updated": "2019-08-27T10:41:42.448Z", "custom_fields": {}}	47	5	2
661	2019-08-27 06:43:14.185373-04	vuongnx	f5295c09-e91b-4487-aac4-321b094820f5	1	4	\N	VPS02	{"face": null, "name": "VPS02", "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server002", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T10:43:14.128Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
662	2019-08-27 06:43:23.479837-04	vuongnx	d444704e-96cf-45e6-869a-16296855f83d	1	38	4	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 1000, "cable": null, "device": 4, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
663	2019-08-27 06:43:36.115628-04	vuongnx	271b76cc-5014-442a-b111-ac8a23bfd05c	1	199	38	10.1.2.22/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.2.22/32", "created": "2019-08-27", "dns_name": "", "interface": 38, "nat_inside": null, "description": "", "last_updated": "2019-08-27T10:43:36.087Z", "custom_fields": {}}	47	5	2
664	2019-08-27 06:43:36.126106-04	vuongnx	271b76cc-5014-442a-b111-ac8a23bfd05c	2	4	\N	VPS02	{"face": null, "name": "VPS02", "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server002", "device_role": 8, "device_type": 1, "primary_ip4": 199, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T10:43:36.098Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
665	2019-08-27 06:43:47.146462-04	vuongnx	2ba14c18-0993-4d30-8fc5-c084df719c7a	1	39	4	iLo	{"lag": null, "mtu": null, "mode": null, "name": "iLo", "tags": [], "type": 0, "cable": null, "device": 4, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
666	2019-08-27 06:43:57.549547-04	vuongnx	31bc4662-e45e-428a-90bb-872edda092fc	1	200	39	10.1.2.102/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.2.102/32", "created": "2019-08-27", "dns_name": "", "interface": 39, "nat_inside": null, "description": "", "last_updated": "2019-08-27T10:43:57.528Z", "custom_fields": {}}	47	5	2
667	2019-08-27 06:44:10.614541-04	vuongnx	b7a5784c-2a66-4278-8c1e-b2f82f40d76f	2	4	\N	VPS02	{"face": null, "name": "VPS02", "rack": 1, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server002", "device_role": 8, "device_type": 1, "primary_ip4": 199, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-27T10:44:10.594Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
668	2019-08-27 23:02:13.405155-04	vuongnx	defb0217-faff-4411-beb0-c5d3f02b3161	2	1	\N	10.1.3.0/24	{"vrf": null, "role": 3, "site": 1, "tags": [], "vlan": 3, "family": 4, "prefix": "10.1.3.0/24", "status": 1, "tenant": null, "created": "2019-08-27", "is_pool": true, "description": "", "last_updated": "2019-08-28T03:02:13.370Z", "custom_fields": {}}	48	\N	2
669	2019-08-27 23:04:21.803338-04	vuongnx	1c0bd514-2d6d-4f40-8734-ebd689210338	1	5	\N	VPS03	{"face": 0, "name": "VPS03", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH708YBTY", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": 11, "position": null, "asset_tag": "Server004", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:04:21.719Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
670	2019-08-27 23:04:41.695842-04	vuongnx	436f0209-450c-43b9-afa1-6f7b1c56d9f1	1	40	5	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 1000, "cable": null, "device": 5, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
671	2019-08-27 23:04:55.422602-04	vuongnx	55107646-2fc9-454e-b761-13bcb545099d	1	201	40	10.1.3.23/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.23/32", "created": "2019-08-28", "dns_name": "", "interface": 40, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:04:55.384Z", "custom_fields": {}}	47	5	2
672	2019-08-27 23:04:55.430149-04	vuongnx	55107646-2fc9-454e-b761-13bcb545099d	2	5	\N	VPS03	{"face": 0, "name": "VPS03", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH708YBTY", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": 11, "position": null, "asset_tag": "Server004", "device_role": 8, "device_type": 1, "primary_ip4": 201, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:04:55.398Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
673	2019-08-27 23:05:04.371049-04	vuongnx	72567880-0a3e-4cb7-9c7b-36c972b9104a	1	41	5	iLo	{"lag": null, "mtu": null, "mode": null, "name": "iLo", "tags": [], "type": 0, "cable": null, "device": 5, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
674	2019-08-27 23:05:24.789685-04	vuongnx	cfaec5a6-e276-49c1-8e3a-b14d3b4a34e4	1	202	41	10.1.3.103/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.103/32", "created": "2019-08-28", "dns_name": "", "interface": 41, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:05:24.763Z", "custom_fields": {}}	47	5	2
675	2019-08-27 23:06:48.301167-04	vuongnx	04334ec2-9810-4b6c-b247-83d60dcd5989	2	199	38	10.1.3.22/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.22/32", "created": "2019-08-27", "dns_name": "", "interface": 38, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:06:48.272Z", "custom_fields": {}}	47	5	2
676	2019-08-27 23:06:48.310272-04	vuongnx	04334ec2-9810-4b6c-b247-83d60dcd5989	2	4	\N	VPS02	{"face": null, "name": "VPS02", "rack": 1, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": 11, "position": null, "asset_tag": "Server002", "device_role": 8, "device_type": 1, "primary_ip4": 199, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:06:48.285Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
677	2019-08-27 23:07:03.472995-04	vuongnx	12be4aa3-6586-4c7e-89f7-735adb01b2b4	2	200	39	10.1.3.102/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.102/32", "created": "2019-08-27", "dns_name": "", "interface": 39, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:07:03.453Z", "custom_fields": {}}	47	5	2
678	2019-08-27 23:08:03.502638-04	vuongnx	96ba34da-33b3-4679-8dde-bd4c94944a90	1	6	\N	VPS04	{"face": null, "name": "VPS04", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH829WLKQ", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": "Server005", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:08:03.460Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
679	2019-08-27 23:08:15.36784-04	vuongnx	be7760eb-322f-488d-ad14-4eccff978bee	1	42	6	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 1000, "cable": null, "device": 6, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
680	2019-08-27 23:08:20.653454-04	vuongnx	150d7f29-a38b-4e1c-8120-14c8c02ac0de	1	43	6	iLo	{"lag": null, "mtu": null, "mode": null, "name": "iLo", "tags": [], "type": 0, "cable": null, "device": 6, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
682	2019-08-27 23:08:33.586161-04	vuongnx	d9488395-62bd-4e7a-aa9a-0301791b70ce	2	6	\N	VPS04	{"face": null, "name": "VPS04", "rack": 1, "site": 1, "tags": ["auto"], "serial": "SGH829WLKQ", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": "Server005", "device_role": 8, "device_type": 1, "primary_ip4": 203, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:08:33.561Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
683	2019-08-27 23:08:42.043389-04	vuongnx	5fb24819-9a90-4433-af29-955d5b5cd3fc	1	204	43	10.1.3.104/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.104/32", "created": "2019-08-28", "dns_name": "", "interface": 43, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:08:42.016Z", "custom_fields": {}}	47	5	2
684	2019-08-27 23:09:32.393362-04	vuongnx	f3a49867-c7ac-42e4-ade4-17bcf6d4cab4	1	7	\N	VPS06	{"face": 0, "name": "VPS06", "rack": 1, "site": 1, "tags": [], "serial": "SGH849SDD5", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": "Server007", "device_role": 8, "device_type": 2, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:09:32.337Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
685	2019-08-27 23:10:05.395095-04	vuongnx	e969e5ef-3acd-4dba-9a2b-75305783e398	1	205	44	10.1.3.26/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.26/32", "created": "2019-08-28", "dns_name": "", "interface": 44, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:10:05.361Z", "custom_fields": {}}	47	5	2
686	2019-08-27 23:10:05.401972-04	vuongnx	e969e5ef-3acd-4dba-9a2b-75305783e398	2	7	\N	VPS06	{"face": 0, "name": "VPS06", "rack": 1, "site": 1, "tags": [], "serial": "SGH849SDD5", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": "Server007", "device_role": 8, "device_type": 2, "primary_ip4": 205, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:10:05.374Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
687	2019-08-27 23:10:13.099044-04	vuongnx	3c493d04-abc9-45e5-b796-91924290dd90	1	45	7	iLo	{"lag": null, "mtu": null, "mode": null, "name": "iLo", "tags": [], "type": 0, "cable": null, "device": 7, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
688	2019-08-27 23:10:24.140264-04	vuongnx	add8dc94-6c66-4dd9-8e2c-31aae5185aec	1	206	45	10.1.3.106/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.106/32", "created": "2019-08-28", "dns_name": "", "interface": 45, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:10:24.116Z", "custom_fields": {}}	47	5	2
689	2019-08-27 23:13:06.56592-04	vuongnx	bdc58143-a796-49ad-b691-08f201f2a280	1	8	\N	Firewall002	{"face": null, "name": "Firewall002", "rack": 2, "site": 1, "tags": ["auto"], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": "Firewall002", "device_role": 6, "device_type": 3, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:13:06.504Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
690	2019-08-27 23:13:52.475714-04	vuongnx	c88ae2b4-c691-4f7d-aec8-b608d305b4b3	2	3	\N	Firewall001	{"face": 0, "name": "Firewall001", "rack": 2, "site": 1, "tags": ["auto"], "serial": "S300067841CE287", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-27", "comments": "", "platform": null, "position": null, "asset_tag": "Firewall001", "device_role": 6, "device_type": 3, "primary_ip4": 174, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:13:52.458Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
691	2019-08-27 23:14:20.038228-04	vuongnx	f2e6ab5a-6cb0-461a-8db2-ac1ee3dd85cf	2	8	\N	Firewall002	{"face": null, "name": "Firewall002", "rack": 2, "site": 1, "tags": ["auto"], "serial": "S30055199EAED8F", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": "Firewall002", "device_role": 6, "device_type": 3, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:14:20.021Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
692	2019-08-27 23:14:46.955182-04	vuongnx	ad091f39-88fd-44ac-8f83-f5429c61cbd9	3	6	\N	Opengear	{"name": "Opengear", "slug": "opengear", "created": null, "napalm_args": null, "last_updated": null, "manufacturer": null, "napalm_driver": ""}	26	\N	2
693	2019-08-27 23:14:46.956631-04	vuongnx	ad091f39-88fd-44ac-8f83-f5429c61cbd9	3	4	\N	Arista EOS	{"name": "Arista EOS", "slug": "arista-eos", "created": null, "napalm_args": null, "last_updated": null, "manufacturer": null, "napalm_driver": ""}	26	\N	2
694	2019-08-27 23:14:46.957706-04	vuongnx	ad091f39-88fd-44ac-8f83-f5429c61cbd9	3	3	\N	Juniper Junos	{"name": "Juniper Junos", "slug": "juniper-junos", "created": null, "napalm_args": null, "last_updated": null, "manufacturer": null, "napalm_driver": ""}	26	\N	2
695	2019-08-27 23:14:46.95926-04	vuongnx	ad091f39-88fd-44ac-8f83-f5429c61cbd9	3	2	\N	Cisco NX-OS	{"name": "Cisco NX-OS", "slug": "cisco-nx-os", "created": null, "napalm_args": null, "last_updated": null, "manufacturer": null, "napalm_driver": ""}	26	\N	2
696	2019-08-27 23:14:46.960309-04	vuongnx	ad091f39-88fd-44ac-8f83-f5429c61cbd9	3	1	\N	Cisco IOS	{"name": "Cisco IOS", "slug": "cisco-ios", "created": null, "napalm_args": null, "last_updated": null, "manufacturer": null, "napalm_driver": ""}	26	\N	2
697	2019-08-27 23:15:01.410028-04	vuongnx	fce21485-fde5-4d98-9b09-df0641da6f43	2	9	\N	Ubuntu Server	{"name": "Ubuntu Server", "slug": "ubuntu-server", "created": "2019-08-26", "napalm_args": null, "last_updated": "2019-08-28T03:15:01.399Z", "manufacturer": null, "napalm_driver": ""}	26	\N	2
698	2019-08-27 23:17:55.040434-04	vuongnx	5e881160-8e5c-44cd-8621-216608518d0a	1	9	\N	Database	{"face": null, "name": "Database", "rack": 1, "site": 1, "tags": [], "serial": "SGH651SFPK", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": 8, "position": null, "asset_tag": "Server003", "device_role": 8, "device_type": 1, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:17:55.007Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
699	2019-08-27 23:18:09.850651-04	vuongnx	b8c833aa-31b7-40f4-bdf4-c7b0846bae81	1	46	9	Interface1	{"lag": null, "mtu": null, "mode": null, "name": "Interface1", "tags": [], "type": 1000, "cable": null, "device": 9, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
700	2019-08-27 23:18:24.51267-04	vuongnx	86933d1d-8e1d-449e-a3d2-915387d10c78	1	207	46	10.1.1.3/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.1.3/32", "created": "2019-08-28", "dns_name": "", "interface": 46, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:18:24.484Z", "custom_fields": {}}	47	5	2
701	2019-08-27 23:18:24.520774-04	vuongnx	86933d1d-8e1d-449e-a3d2-915387d10c78	2	9	\N	Database	{"face": null, "name": "Database", "rack": 1, "site": 1, "tags": [], "serial": "SGH651SFPK", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": 8, "position": null, "asset_tag": "Server003", "device_role": 8, "device_type": 1, "primary_ip4": 207, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:18:24.497Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
702	2019-08-27 23:20:13.961433-04	vuongnx	d21f9452-0152-4a2c-92a9-0da53d1ea9d7	1	4	\N	HP 1820-24G-PoE+ (185W) Switch	{"slug": "hp-1820-24g-poe-185w-switch", "tags": [], "model": "HP 1820-24G-PoE+ (185W) Switch", "created": "2019-08-28", "comments": "", "u_height": 1, "part_number": "J9983A", "last_updated": "2019-08-28T03:20:13.941Z", "manufacturer": 4, "custom_fields": {}, "is_full_depth": true, "subdevice_role": null}	23	\N	2
703	2019-08-27 23:20:27.086081-04	vuongnx	72c4d6c0-5731-48c9-aca1-65e7cb27b884	2	4	\N	HPE	{"name": "HPE", "slug": "hpe", "created": null, "last_updated": "2019-08-28T03:20:27.077Z"}	25	\N	2
704	2019-08-27 23:20:48.547896-04	vuongnx	46d07606-44eb-44b8-b21a-2c108ba49843	3	8	\N	Super Micro	{"name": "Super Micro", "slug": "super-micro", "created": null, "last_updated": null}	25	\N	2
705	2019-08-27 23:20:48.55245-04	vuongnx	46d07606-44eb-44b8-b21a-2c108ba49843	3	7	\N	Opengear	{"name": "Opengear", "slug": "opengear", "created": null, "last_updated": null}	25	\N	2
706	2019-08-27 23:20:48.555314-04	vuongnx	46d07606-44eb-44b8-b21a-2c108ba49843	3	6	\N	Arista	{"name": "Arista", "slug": "arista", "created": null, "last_updated": null}	25	\N	2
707	2019-08-27 23:23:58.379133-04	vuongnx	81d0a260-6abe-4e6e-8485-e99bb84f01e4	1	10	\N	Swich Border	{"face": 0, "name": "Swich Border", "rack": 2, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 4, "device_type": 4, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:23:58.332Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
708	2019-08-27 23:24:12.098776-04	vuongnx	00636e81-d923-422f-86fb-c81a127a9f9e	1	47	10	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 1000, "cable": null, "device": 10, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
709	2019-08-27 23:24:40.454608-04	vuongnx	3b03d598-d223-4ff6-9f24-be79905d16e3	1	208	47	192.168.168.2/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "192.168.168.2/32", "created": "2019-08-28", "dns_name": "", "interface": 47, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:24:40.424Z", "custom_fields": {}}	47	5	2
710	2019-08-27 23:24:40.461327-04	vuongnx	3b03d598-d223-4ff6-9f24-be79905d16e3	2	10	\N	Swich Border	{"face": 0, "name": "Swich Border", "rack": 2, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 4, "device_type": 4, "primary_ip4": 208, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:24:40.437Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
711	2019-08-27 23:26:53.481485-04	vuongnx	15d144fa-65f8-4336-814f-700eceeda6ae	1	5	\N	HP J9850A Switch 5406Rzl2	{"slug": "hp-j9850a-switch-5406rzl2", "tags": [], "model": "HP J9850A Switch 5406Rzl2", "created": "2019-08-28", "comments": "", "u_height": 4, "part_number": "J9850A", "last_updated": "2019-08-28T03:26:53.459Z", "manufacturer": 4, "custom_fields": {}, "is_full_depth": true, "subdevice_role": null}	23	\N	2
712	2019-08-27 23:28:12.374399-04	vuongnx	db5a831b-b135-4f01-9b38-3324f456ef81	1	11	\N	ho-sw-core-01	{"face": 0, "name": "ho-sw-core-01", "rack": 2, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 2, "device_type": 5, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:28:12.322Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
713	2019-08-27 23:28:20.404571-04	vuongnx	ed6c0211-006e-4cdf-bdd9-169bc9680e43	1	48	11	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 1000, "cable": null, "device": 11, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
714	2019-08-27 23:28:30.376537-04	vuongnx	24f884ae-82bb-42e8-aa89-8f1d725d94b7	1	209	48	10.1.3.1/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.1/32", "created": "2019-08-28", "dns_name": "", "interface": 48, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:28:30.346Z", "custom_fields": {}}	47	5	2
715	2019-08-27 23:28:30.386623-04	vuongnx	24f884ae-82bb-42e8-aa89-8f1d725d94b7	2	11	\N	ho-sw-core-01	{"face": 0, "name": "ho-sw-core-01", "rack": 2, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 2, "device_type": 5, "primary_ip4": 209, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:28:30.358Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
716	2019-08-27 23:29:11.780781-04	vuongnx	980e1690-a465-4495-910f-b37a8c31230d	1	6	\N	HP J9726A 2920-24G Switch	{"slug": "hp-j9726a-2920-24g-switch", "tags": [], "model": "HP J9726A 2920-24G Switch", "created": "2019-08-28", "comments": "", "u_height": 1, "part_number": "J9726A", "last_updated": "2019-08-28T03:29:11.763Z", "manufacturer": 4, "custom_fields": {}, "is_full_depth": true, "subdevice_role": null}	23	\N	2
717	2019-08-27 23:29:53.721536-04	vuongnx	e28887b9-8e39-4f28-aadf-a6ffe3575464	1	12	\N	ho-sw-central-01	{"face": null, "name": "ho-sw-central-01", "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 3, "device_type": 6, "primary_ip4": null, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:29:53.668Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
718	2019-08-27 23:30:01.322574-04	vuongnx	f9c90960-d4aa-4e02-a588-4372d6757094	1	49	12	Management	{"lag": null, "mtu": null, "mode": null, "name": "Management", "tags": [], "type": 1000, "cable": null, "device": 12, "enabled": true, "mgmt_only": false, "description": "", "mac_address": "", "tagged_vlans": [], "untagged_vlan": null, "virtual_machine": null, "connection_status": null, "_connected_interface": null, "_connected_circuittermination": null}	5	21	2
719	2019-08-27 23:30:14.883006-04	vuongnx	5076d02b-f195-4407-83ea-49c06efeca1f	1	210	49	10.1.3.111/32	{"vrf": null, "role": null, "tags": [], "family": 4, "status": 1, "tenant": null, "address": "10.1.3.111/32", "created": "2019-08-28", "dns_name": "", "interface": 49, "nat_inside": null, "description": "", "last_updated": "2019-08-28T03:30:14.853Z", "custom_fields": {}}	47	5	2
720	2019-08-27 23:30:14.88968-04	vuongnx	5076d02b-f195-4407-83ea-49c06efeca1f	2	12	\N	ho-sw-central-01	{"face": null, "name": "ho-sw-central-01", "rack": null, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 3, "device_type": 6, "primary_ip4": 210, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:30:14.866Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
721	2019-08-27 23:30:27.149778-04	vuongnx	dcffb41d-f211-44e0-a170-455deb2696ef	2	12	\N	ho-sw-central-01	{"face": null, "name": "ho-sw-central-01", "rack": 2, "site": 1, "tags": [], "serial": "", "status": 1, "tenant": null, "cluster": null, "created": "2019-08-28", "comments": "", "platform": null, "position": null, "asset_tag": null, "device_role": 3, "device_type": 6, "primary_ip4": 210, "primary_ip6": null, "vc_position": null, "vc_priority": null, "last_updated": "2019-08-28T03:30:27.131Z", "custom_fields": {}, "virtual_chassis": null, "local_context_data": null}	21	\N	2
\.


--
-- Name: extras_objectchange_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_objectchange_id_seq', 721, true);


--
-- Data for Name: extras_reportresult; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_reportresult (id, report, created, failed, data, user_id) FROM stdin;
\.


--
-- Name: extras_reportresult_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_reportresult_id_seq', 1, false);


--
-- Data for Name: extras_tag; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_tag (id, name, slug, color, comments, created, last_updated) FROM stdin;
1	auto	auto	9e9e9e		2019-08-27	2019-08-27 00:19:20.940786-04
\.


--
-- Name: extras_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_tag_id_seq', 1, true);


--
-- Data for Name: extras_taggeditem; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_taggeditem (id, object_id, content_type_id, tag_id) FROM stdin;
160	1	21	1
161	2	21	1
162	3	21	1
163	174	47	1
164	14	5	1
165	175	47	1
166	176	47	1
167	193	47	1
168	195	47	1
169	5	21	1
170	6	21	1
171	8	21	1
\.


--
-- Name: extras_taggeditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_taggeditem_id_seq', 171, true);


--
-- Data for Name: extras_topologymap; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_topologymap (id, name, slug, device_patterns, description, site_id, type) FROM stdin;
1	Network	network	Rack01		1	1
\.


--
-- Name: extras_topologymap_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_topologymap_id_seq', 1, true);


--
-- Data for Name: extras_webhook; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_webhook (id, name, type_create, type_update, type_delete, payload_url, http_content_type, secret, enabled, ssl_verification) FROM stdin;
\.


--
-- Name: extras_webhook_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_webhook_id_seq', 1, false);


--
-- Data for Name: extras_webhook_obj_type; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.extras_webhook_obj_type (id, webhook_id, contenttype_id) FROM stdin;
\.


--
-- Name: extras_webhook_obj_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.extras_webhook_obj_type_id_seq', 1, false);


--
-- Data for Name: ipam_aggregate; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_aggregate (id, created, last_updated, family, prefix, date_added, description, rir_id) FROM stdin;
1	2016-08-01	2016-08-01 11:22:20.938-04	4	10.0.0.0/8	\N	Private IPv4 space	6
2	2016-08-01	2016-08-01 11:22:32.679-04	4	172.16.0.0/12	\N	Private IPv4 space	6
3	2016-08-01	2016-08-01 11:22:42.289-04	4	192.168.0.0/16	\N	Private IPv4 space	6
\.


--
-- Name: ipam_aggregate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_aggregate_id_seq', 3, true);


--
-- Data for Name: ipam_ipaddress; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_ipaddress (id, created, last_updated, family, address, description, interface_id, nat_inside_id, vrf_id, tenant_id, status, role, dns_name) FROM stdin;
174	2019-08-27	2019-08-27 02:38:33.292249-04	4	10.1.127.1	4680	13	\N	\N	\N	1	\N	
176	2019-08-27	2019-08-27 02:50:07.715975-04	4	10.1.27.212		15	\N	\N	\N	1	\N	
178	2019-08-27	2019-08-27 03:17:52.379605-04	4	10.1.27.214		17	\N	\N	\N	1	\N	
180	2019-08-27	2019-08-27 03:19:40.692114-04	4	10.1.27.216		19	\N	\N	\N	1	\N	
182	2019-08-27	2019-08-27 03:22:38.856583-04	4	10.1.27.218		21	\N	\N	\N	1	\N	
184	2019-08-27	2019-08-27 03:23:59.385272-04	4	10.1.27.220		23	\N	\N	\N	1	\N	
186	2019-08-27	2019-08-27 03:25:25.696165-04	4	10.1.27.222		25	\N	\N	\N	1	\N	
188	2019-08-27	2019-08-27 03:26:25.298512-04	4	10.1.27.224		27	\N	\N	\N	1	\N	
190	2019-08-27	2019-08-27 03:27:12.20679-04	4	10.1.27.226		29	\N	\N	\N	1	\N	
192	2019-08-27	2019-08-27 03:27:53.080992-04	4	10.1.27.228		31	\N	\N	\N	1	\N	
194	2019-08-27	2019-08-27 03:28:35.940712-04	4	10.1.27.230		33	\N	\N	\N	1	\N	
196	2019-08-27	2019-08-27 06:39:28.301677-04	4	10.1.3.101		35	\N	\N	\N	1	\N	
198	2019-08-27	2019-08-27 06:41:42.448349-04	4	10.1.3.105		37	\N	\N	\N	1	\N	
201	2019-08-28	2019-08-27 23:04:55.3845-04	4	10.1.3.23		40	\N	\N	\N	1	\N	
200	2019-08-27	2019-08-27 23:07:03.453066-04	4	10.1.3.102		39	\N	\N	\N	1	\N	
203	2019-08-28	2019-08-27 23:08:33.548885-04	4	10.1.3.24		42	\N	\N	\N	1	\N	
205	2019-08-28	2019-08-27 23:10:05.361329-04	4	10.1.3.26		44	\N	\N	\N	1	\N	
207	2019-08-28	2019-08-27 23:18:24.484912-04	4	10.1.1.3		46	\N	\N	\N	1	\N	
209	2019-08-28	2019-08-27 23:28:30.346988-04	4	10.1.3.1		48	\N	\N	\N	1	\N	
175	2019-08-27	2019-08-27 02:44:12.837741-04	4	10.1.3.21		14	\N	\N	\N	1	\N	
177	2019-08-27	2019-08-27 03:17:31.969732-04	4	10.1.27.213		16	\N	\N	\N	1	\N	
179	2019-08-27	2019-08-27 03:18:22.917345-04	4	10.1.27.215		18	\N	\N	\N	1	\N	
181	2019-08-27	2019-08-27 03:20:14.503272-04	4	10.1.27.217		20	\N	\N	\N	1	\N	
183	2019-08-27	2019-08-27 03:23:41.409024-04	4	10.1.27.219		22	\N	\N	\N	1	\N	
185	2019-08-27	2019-08-27 03:25:06.172116-04	4	10.1.27.221		24	\N	\N	\N	1	\N	
187	2019-08-27	2019-08-27 03:26:01.226013-04	4	10.1.27.223		26	\N	\N	\N	1	\N	
189	2019-08-27	2019-08-27 03:26:55.894835-04	4	10.1.27.225		28	\N	\N	\N	1	\N	
191	2019-08-27	2019-08-27 03:27:29.903232-04	4	10.1.27.227		30	\N	\N	\N	1	\N	
193	2019-08-27	2019-08-27 03:28:12.079353-04	4	10.1.27.229		32	\N	\N	\N	1	\N	
195	2019-08-27	2019-08-27 03:31:57.392609-04	4	10.1.27.71		34	\N	\N	\N	1	\N	
197	2019-08-27	2019-08-27 06:40:39.341445-04	4	10.1.3.25		36	\N	\N	\N	1	\N	
202	2019-08-28	2019-08-27 23:05:24.763722-04	4	10.1.3.103		41	\N	\N	\N	1	\N	
199	2019-08-27	2019-08-27 23:06:48.272669-04	4	10.1.3.22		38	\N	\N	\N	1	\N	
204	2019-08-28	2019-08-27 23:08:42.016187-04	4	10.1.3.104		43	\N	\N	\N	1	\N	
206	2019-08-28	2019-08-27 23:10:24.11688-04	4	10.1.3.106		45	\N	\N	\N	1	\N	
208	2019-08-28	2019-08-27 23:24:40.424283-04	4	192.168.168.2		47	\N	\N	\N	1	\N	
210	2019-08-28	2019-08-27 23:30:14.853288-04	4	10.1.3.111		49	\N	\N	\N	1	\N	
163	2019-08-27	2019-08-27 00:34:29.979218-04	4	10.1.27.201		2	\N	\N	\N	1	\N	
164	2019-08-27	2019-08-27 00:40:52.189446-04	4	10.1.27.202		3	\N	\N	\N	1	\N	
165	2019-08-27	2019-08-27 00:44:47.663192-04	4	10.1.27.203		4	\N	\N	\N	1	\N	
166	2019-08-27	2019-08-27 00:45:26.304347-04	4	10.1.27.204		5	\N	\N	\N	1	\N	
167	2019-08-27	2019-08-27 00:45:46.535726-04	4	10.1.27.205		6	\N	\N	\N	1	\N	
168	2019-08-27	2019-08-27 00:47:05.43975-04	4	10.1.27.206		7	\N	\N	\N	1	\N	
169	2019-08-27	2019-08-27 00:47:50.894733-04	4	10.1.27.207		8	\N	\N	\N	1	\N	
170	2019-08-27	2019-08-27 00:48:19.032152-04	4	10.1.27.208		9	\N	\N	\N	1	\N	
171	2019-08-27	2019-08-27 00:50:02.826014-04	4	10.1.27.209		10	\N	\N	\N	1	\N	
172	2019-08-27	2019-08-27 00:52:19.239523-04	4	10.1.27.210		11	\N	\N	\N	1	\N	
173	2019-08-27	2019-08-27 00:54:03.140163-04	4	10.1.27.211		12	\N	\N	\N	1	\N	
\.


--
-- Name: ipam_ipaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_ipaddress_id_seq', 210, true);


--
-- Data for Name: ipam_prefix; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_prefix (id, created, last_updated, family, prefix, status, description, role_id, site_id, vlan_id, vrf_id, tenant_id, is_pool) FROM stdin;
2	2019-08-27	2019-08-26 21:29:23.214017-04	4	10.1.20.0/24	1		3	1	1	\N	\N	f
4	2019-08-27	2019-08-26 21:36:37.88602-04	4	10.1.21.0/24	1		2	1	4	\N	\N	f
5	2019-08-27	2019-08-26 21:46:04.354704-04	4	10.1.22.0/24	1		3	1	5	\N	\N	f
6	2019-08-27	2019-08-26 21:47:24.534836-04	4	10.1.23.0/24	1		3	1	6	\N	\N	f
7	2019-08-27	2019-08-26 21:50:05.340874-04	4	10.1.25.0/24	1		1	1	7	\N	\N	f
8	2019-08-27	2019-08-26 21:50:23.276667-04	4	10.1.26.0/24	1		1	1	8	\N	\N	f
10	2019-08-27	2019-08-26 21:53:36.930608-04	4	172.16.1.0/24	1		5	1	10	\N	\N	f
9	2019-08-27	2019-08-26 22:01:14.930676-04	4	10.1.27.0/24	1		1	1	9	\N	\N	t
11	2019-08-27	2019-08-26 22:11:36.612142-04	4	10.1.1.0/24	1		1	1	11	\N	\N	t
3	2019-08-27	2019-08-27 00:26:35.930202-04	4	10.1.24.0/24	1		1	1	2	\N	\N	t
1	2019-08-27	2019-08-27 23:02:13.370959-04	4	10.1.3.0/24	1		3	1	3	\N	\N	t
\.


--
-- Name: ipam_prefix_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_prefix_id_seq', 11, true);


--
-- Data for Name: ipam_rir; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_rir (id, name, slug, is_private, created, last_updated) FROM stdin;
1	ARIN	arin	f	\N	\N
2	RIPE	ripe	f	\N	\N
3	APNIC	apnic	f	\N	\N
4	LACNIC	lacnic	f	\N	\N
5	AFRINIC	afrinic	f	\N	\N
6	RFC 1918	rfc-1918	t	\N	\N
\.


--
-- Name: ipam_rir_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_rir_id_seq', 6, true);


--
-- Data for Name: ipam_role; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_role (id, name, slug, weight, created, last_updated) FROM stdin;
1	Production	production	1000	\N	\N
2	Development	development	1000	\N	\N
3	Management	management	1000	\N	\N
5	DMZ Zone	dmz-zone	1000	2019-08-27	2019-08-26 21:52:22.294859-04
\.


--
-- Name: ipam_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_role_id_seq', 5, true);


--
-- Data for Name: ipam_service; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_service (id, created, last_updated, name, protocol, port, description, device_id, virtual_machine_id) FROM stdin;
\.


--
-- Name: ipam_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_service_id_seq', 1, false);


--
-- Data for Name: ipam_service_ipaddresses; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_service_ipaddresses (id, service_id, ipaddress_id) FROM stdin;
\.


--
-- Name: ipam_service_ipaddresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_service_ipaddresses_id_seq', 1, false);


--
-- Data for Name: ipam_vlan; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_vlan (id, created, last_updated, vid, name, status, role_id, site_id, group_id, description, tenant_id) FROM stdin;
1	2019-08-26	2019-08-26 21:27:36.850562-04	20	Vlan 20	1	3	1	1		\N
3	2019-08-27	2019-08-26 21:28:23.144746-04	3	Vlan 03	1	3	1	4		\N
2	2019-08-26	2019-08-26 21:33:37.117198-04	24	Vlan 24	1	1	1	5		\N
4	2019-08-27	2019-08-26 21:36:11.502095-04	21	Vlan 21	1	2	1	2		\N
5	2019-08-27	2019-08-26 21:45:18.013288-04	22	Vlan 22	1	3	1	9		\N
6	2019-08-27	2019-08-26 21:47:06.897096-04	23	Vlan 23	1	3	1	8		\N
7	2019-08-27	2019-08-26 21:48:49.961369-04	25	Vlan 25	1	1	1	6		\N
8	2019-08-27	2019-08-26 21:49:07.426339-04	26	Vlan 26	1	1	1	7		\N
9	2019-08-27	2019-08-26 21:49:26.759623-04	27	Vlan 27	1	1	1	10		\N
10	2019-08-27	2019-08-26 21:53:19.041234-04	172	Vlan DMZ	1	5	1	11		\N
11	2019-08-27	2019-08-26 21:56:17.514162-04	1	Vlan SRV	1	1	1	12		\N
\.


--
-- Name: ipam_vlan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_vlan_id_seq', 11, true);


--
-- Data for Name: ipam_vlangroup; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_vlangroup (id, name, slug, site_id, created, last_updated) FROM stdin;
1	ITD	itd	1	2019-08-27	2019-08-26 21:26:50.105728-04
2	SDD	sdd	1	2019-08-27	2019-08-26 21:26:54.247071-04
4	Vlan Management ESXi	vlan-management-esxi	1	2019-08-27	2019-08-26 21:28:11.4122-04
5	Pro1	pro1	1	2019-08-27	2019-08-26 21:33:16.325022-04
6	Pro2	pro2	1	2019-08-27	2019-08-26 21:43:38.069572-04
7	Pro3	pro3	1	2019-08-27	2019-08-26 21:43:59.542115-04
8	QMD	qmd	1	2019-08-27	2019-08-26 21:44:09.761665-04
10	Vlan 27	vlan-27	1	2019-08-27	2019-08-26 21:44:44.868218-04
11	DMZ	dmz	1	2019-08-27	2019-08-26 21:51:14.038876-04
12	SRV	srv	1	2019-08-27	2019-08-26 21:55:52.646236-04
9	OMD - HRD	omd-hrd	1	2019-08-27	2019-08-26 21:59:42.132992-04
\.


--
-- Name: ipam_vlangroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_vlangroup_id_seq', 12, true);


--
-- Data for Name: ipam_vrf; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.ipam_vrf (id, created, last_updated, name, rd, description, enforce_unique, tenant_id) FROM stdin;
\.


--
-- Name: ipam_vrf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.ipam_vrf_id_seq', 1, false);


--
-- Data for Name: secrets_secret; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.secrets_secret (id, created, last_updated, name, ciphertext, hash, device_id, role_id) FROM stdin;
\.


--
-- Name: secrets_secret_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.secrets_secret_id_seq', 1, false);


--
-- Data for Name: secrets_secretrole; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.secrets_secretrole (id, name, slug, created, last_updated) FROM stdin;
1	Login Credentials	login-credentials	\N	\N
2	RADIUS Key	radius-key	\N	\N
3	SNMPv2 Community	snmpv2-community	\N	\N
4	SNMPv3 Credentials	snmpv3-credentials	\N	\N
\.


--
-- Data for Name: secrets_secretrole_groups; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.secrets_secretrole_groups (id, secretrole_id, group_id) FROM stdin;
\.


--
-- Name: secrets_secretrole_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.secrets_secretrole_groups_id_seq', 1, false);


--
-- Name: secrets_secretrole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.secrets_secretrole_id_seq', 4, true);


--
-- Data for Name: secrets_secretrole_users; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.secrets_secretrole_users (id, secretrole_id, user_id) FROM stdin;
\.


--
-- Name: secrets_secretrole_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.secrets_secretrole_users_id_seq', 1, false);


--
-- Data for Name: secrets_sessionkey; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.secrets_sessionkey (id, cipher, hash, created, userkey_id) FROM stdin;
\.


--
-- Name: secrets_sessionkey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.secrets_sessionkey_id_seq', 1, false);


--
-- Data for Name: secrets_userkey; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.secrets_userkey (id, created, last_updated, public_key, master_key_cipher, user_id) FROM stdin;
\.


--
-- Name: secrets_userkey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.secrets_userkey_id_seq', 1, false);


--
-- Data for Name: taggit_tag; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.taggit_tag (id, name, slug) FROM stdin;
\.


--
-- Name: taggit_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.taggit_tag_id_seq', 1, false);


--
-- Data for Name: taggit_taggeditem; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.taggit_taggeditem (id, object_id, content_type_id, tag_id) FROM stdin;
\.


--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.taggit_taggeditem_id_seq', 1, false);


--
-- Data for Name: tenancy_tenant; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.tenancy_tenant (id, created, last_updated, name, slug, description, comments, group_id) FROM stdin;
2	2019-08-27	2019-08-26 21:25:57.347108-04	vuongnx	vuongnx			1
\.


--
-- Name: tenancy_tenant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.tenancy_tenant_id_seq', 2, true);


--
-- Data for Name: tenancy_tenantgroup; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.tenancy_tenantgroup (id, name, slug, created, last_updated) FROM stdin;
1	ITD	itd	2019-08-27	2019-08-26 21:25:30.182988-04
2	SDD	sdd	2019-08-27	2019-08-26 21:25:34.616704-04
3	DOC1	doc1	2019-08-27	2019-08-26 21:34:47.840397-04
4	DOC2	doc2	2019-08-27	2019-08-26 21:34:53.330868-04
5	TSA	tsa	2019-08-27	2019-08-26 21:34:57.320634-04
6	QMD	qmd	2019-08-27	2019-08-26 21:35:05.061528-04
7	OMD	omd	2019-08-27	2019-08-26 21:43:10.239181-04
\.


--
-- Name: tenancy_tenantgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.tenancy_tenantgroup_id_seq', 7, true);


--
-- Data for Name: users_token; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.users_token (id, created, expires, key, write_enabled, description, user_id) FROM stdin;
1	2019-08-26 05:23:14.891458-04	2019-08-27 07:00:00-04	6d4219408f10a1b8567451abd776c5bfc5a97c71	t		1
\.


--
-- Name: users_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.users_token_id_seq', 1, true);


--
-- Data for Name: virtualization_cluster; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.virtualization_cluster (id, created, last_updated, name, comments, group_id, type_id, site_id) FROM stdin;
4	2019-08-26	2019-08-26 04:55:48.268212-04	VPS01		1	2	1
5	2019-08-26	2019-08-26 04:55:59.77714-04	VPS05		1	2	1
6	2019-08-26	2019-08-26 06:27:45.405641-04	VPS02		1	2	1
7	2019-08-26	2019-08-26 06:27:56.077282-04	VPS03		1	2	1
8	2019-08-26	2019-08-26 06:28:25.390041-04	VPS04		1	2	1
9	2019-08-27	2019-08-27 00:42:26.628726-04	VPS06		1	2	1
\.


--
-- Name: virtualization_cluster_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.virtualization_cluster_id_seq', 9, true);


--
-- Data for Name: virtualization_clustergroup; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.virtualization_clustergroup (id, name, slug, created, last_updated) FROM stdin;
1	VM Host	vm-host	\N	\N
\.


--
-- Name: virtualization_clustergroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.virtualization_clustergroup_id_seq', 1, true);


--
-- Data for Name: virtualization_clustertype; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.virtualization_clustertype (id, name, slug, created, last_updated) FROM stdin;
1	Public Cloud	public-cloud	\N	\N
2	vSphere	vsphere	\N	\N
3	Hyper-V	hyper-v	\N	\N
4	libvirt	libvirt	\N	\N
5	LXD	lxd	\N	\N
6	Docker	docker	\N	\N
\.


--
-- Name: virtualization_clustertype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.virtualization_clustertype_id_seq', 6, true);


--
-- Data for Name: virtualization_virtualmachine; Type: TABLE DATA; Schema: public; Owner: netbox
--

COPY public.virtualization_virtualmachine (id, created, last_updated, name, vcpus, memory, disk, comments, cluster_id, platform_id, primary_ip4_id, primary_ip6_id, tenant_id, status, role_id, local_context_data) FROM stdin;
24	2019-08-27	2019-08-27 03:25:25.711173-04	HO-SRV-autoFillCRC22	1	4096	50		8	10	186	\N	\N	1	8	\N
25	2019-08-27	2019-08-27 03:26:01.237423-04	HO-SRV-autoFillCRC23	2	2048	50		7	10	187	\N	\N	1	8	\N
26	2019-08-27	2019-08-27 03:26:25.310504-04	HO-SRV-autoFillCRC24	2	2048	50		4	10	188	\N	\N	1	8	\N
3	2019-08-27	2019-08-27 00:34:43.827046-04	HO-SRV-autoFillCRC01	1	4096	50		8	10	163	\N	\N	1	8	\N
4	2019-08-27	2019-08-27 00:40:52.201962-04	HO-SRV-autoFillCRC02	2	4096	50		8	10	164	\N	\N	1	8	\N
5	2019-08-27	2019-08-27 00:44:47.677143-04	HO-SRV-autoFillCRC03	1	3072	50		8	10	165	\N	\N	1	8	\N
6	2019-08-27	2019-08-27 00:45:26.31839-04	HO-SRV-autoFillCRC04	1	2048	50		8	10	166	\N	\N	1	8	\N
7	2019-08-27	2019-08-27 00:45:46.546665-04	HO-SRV-autoFillCRC05	1	3072	50		8	10	167	\N	\N	1	8	\N
8	2019-08-27	2019-08-27 00:47:05.451767-04	HO-SRV-autoFillCRC06	1	4096	50		9	10	168	\N	\N	1	8	\N
9	2019-08-27	2019-08-27 00:47:50.906914-04	HO-SRV-autoFillCRC07	1	4096	50		8	10	169	\N	\N	1	8	\N
10	2019-08-27	2019-08-27 00:48:19.043967-04	HO-SRV-autoFillCRC08	1	4096	50		8	10	170	\N	\N	1	8	\N
11	2019-08-27	2019-08-27 00:50:02.838276-04	HO-SRV-autoFillCRC09	1	4096	50		8	10	171	\N	\N	1	8	\N
12	2019-08-27	2019-08-27 00:52:19.250429-04	HO-SRV-autoFillCRC10	1	4096	50		8	10	172	\N	\N	1	8	\N
13	2019-08-27	2019-08-27 00:54:03.155441-04	HO-SRV-autoFillCRC11	1	4096	50		7	10	173	\N	\N	1	8	\N
14	2019-08-27	2019-08-27 02:50:07.732519-04	HO-SRV-autoFillCRC12	1	4096	50		8	10	176	\N	\N	1	8	\N
15	2019-08-27	2019-08-27 03:17:31.982416-04	HO-SRV-autoFillCRC13	1	4096	50		8	10	177	\N	\N	1	8	\N
16	2019-08-27	2019-08-27 03:17:52.393924-04	HO-SRV-autoFillCRC14	1	4096	50		8	10	178	\N	\N	1	8	\N
17	2019-08-27	2019-08-27 03:18:22.93249-04	HO-SRV-autoFillCRC15	1	4096	50		8	10	179	\N	\N	1	8	\N
18	2019-08-27	2019-08-27 03:19:40.703995-04	HO-SRV-autoFillCRC16	1	2048	50		8	10	180	\N	\N	1	8	\N
19	2019-08-27	2019-08-27 03:20:14.516683-04	HO-SRV-autoFillCRC17	1	2048	50		8	10	181	\N	\N	1	8	\N
20	2019-08-27	2019-08-27 03:22:38.86855-04	HO-SRV-autoFillCRC18	1	2048	50		8	10	182	\N	\N	1	8	\N
21	2019-08-27	2019-08-27 03:23:41.426881-04	HO-SRV-autoFillCRC19	1	4096	50		7	10	183	\N	\N	1	8	\N
22	2019-08-27	2019-08-27 03:23:59.397873-04	HO-SRV-autoFillCRC20	1	4096	50		8	10	184	\N	\N	1	8	\N
23	2019-08-27	2019-08-27 03:25:06.182988-04	HO-SRV-autoFillCRC21	2	2048	50		8	10	185	\N	\N	1	8	\N
27	2019-08-27	2019-08-27 03:26:55.90501-04	HO-SRV-autoFillCRC25	2	2048	50		8	10	189	\N	\N	1	8	\N
28	2019-08-27	2019-08-27 03:27:12.217308-04	HO-SRV-autoFillCRC26	2	2048	50		8	10	190	\N	\N	1	8	\N
29	2019-08-27	2019-08-27 03:27:29.913623-04	HO-SRV-autoFillCRC27	2	2048	50		8	10	191	\N	\N	1	8	\N
30	2019-08-27	2019-08-27 03:27:53.093716-04	HO-SRV-autoFillCRC28	2	2048	50		8	10	192	\N	\N	1	8	\N
31	2019-08-27	2019-08-27 03:28:12.098776-04	HO-SRV-autoFillCRC29	2	2048	50		8	10	193	\N	\N	1	8	\N
32	2019-08-27	2019-08-27 03:28:35.95712-04	HO-SRV-autoFillCRC30	2	2048	50		8	10	194	\N	\N	1	8	\N
34	2019-08-27	2019-08-27 03:31:02.407116-04	HO-SRV-AutoFillPre02	2	2048	50		9	10	\N	\N	\N	1	8	\N
35	2019-08-27	2019-08-27 03:31:02.416855-04	HO-SRV-AutoFillPre03	2	2048	50		9	10	\N	\N	\N	1	8	\N
36	2019-08-27	2019-08-27 03:31:02.429337-04	HO-SRV-AutoFillPre04	2	2048	50		9	10	\N	\N	\N	1	8	\N
37	2019-08-27	2019-08-27 03:31:02.441232-04	HO-SRV-AutoFillPre05	2	2048	50		9	10	\N	\N	\N	1	8	\N
38	2019-08-27	2019-08-27 03:31:02.450351-04	HO-SRV-AutoFillPre06	2	2048	50		9	10	\N	\N	\N	1	8	\N
39	2019-08-27	2019-08-27 03:31:02.463114-04	HO-SRV-AutoFillPre07	2	2048	50		9	10	\N	\N	\N	1	8	\N
40	2019-08-27	2019-08-27 03:31:02.472464-04	HO-SRV-AutoFillPre08	2	2048	50		9	10	\N	\N	\N	1	8	\N
41	2019-08-27	2019-08-27 03:31:02.482862-04	HO-SRV-AutoFillPre09	2	2048	50		9	10	\N	\N	\N	1	8	\N
42	2019-08-27	2019-08-27 03:31:02.492838-04	HO-SRV-AutoFillPre10	2	2048	50		9	10	\N	\N	\N	1	8	\N
43	2019-08-27	2019-08-27 03:31:02.503459-04	HO-SRV-AutoFillPre11	2	2048	50		9	10	\N	\N	\N	1	8	\N
44	2019-08-27	2019-08-27 03:31:02.51225-04	HO-SRV-AutoFillPre12	2	2048	50		9	10	\N	\N	\N	1	8	\N
45	2019-08-27	2019-08-27 03:31:02.522024-04	HO-SRV-AutoFillPre13	2	2048	50		9	10	\N	\N	\N	1	8	\N
46	2019-08-27	2019-08-27 03:31:02.530828-04	HO-SRV-AutoFillPre14	2	2048	50		9	10	\N	\N	\N	1	8	\N
47	2019-08-27	2019-08-27 03:31:02.53871-04	HO-SRV-AutoFillPre15	2	2048	50		9	10	\N	\N	\N	1	8	\N
48	2019-08-27	2019-08-27 03:31:02.54736-04	HO-SRV-AutoFillPre16	2	2048	50		9	10	\N	\N	\N	1	8	\N
49	2019-08-27	2019-08-27 03:31:02.554733-04	HO-SRV-AutoFillPre17	2	2048	50		9	10	\N	\N	\N	1	8	\N
50	2019-08-27	2019-08-27 03:31:02.561895-04	HO-SRV-AutoFillPre18	2	2048	50		9	10	\N	\N	\N	1	8	\N
51	2019-08-27	2019-08-27 03:31:02.571526-04	HO-SRV-AutoFillPre19	2	2048	50		9	10	\N	\N	\N	1	8	\N
52	2019-08-27	2019-08-27 03:31:02.581564-04	HO-SRV-AutoFillPre20	2	2048	50		9	10	\N	\N	\N	1	8	\N
53	2019-08-27	2019-08-27 03:31:02.590613-04	HO-SRV-AutoFillPre21	2	2048	50		9	10	\N	\N	\N	1	8	\N
54	2019-08-27	2019-08-27 03:31:02.599024-04	HO-SRV-AutoFillPre22	2	2048	50		9	10	\N	\N	\N	1	8	\N
55	2019-08-27	2019-08-27 03:31:02.607641-04	HO-SRV-AutoFillPre23	2	2048	50		9	10	\N	\N	\N	1	8	\N
56	2019-08-27	2019-08-27 03:31:02.615054-04	HO-SRV-AutoFillPre24	2	2048	50		9	10	\N	\N	\N	1	8	\N
57	2019-08-27	2019-08-27 03:31:02.623545-04	HO-SRV-AutoFillPre25	2	2048	50		9	10	\N	\N	\N	1	8	\N
58	2019-08-27	2019-08-27 03:31:02.631776-04	HO-SRV-AutoFillPre26	2	2048	50		9	10	\N	\N	\N	1	8	\N
59	2019-08-27	2019-08-27 03:31:02.640177-04	HO-SRV-AutoFillPre27	2	2048	50		9	10	\N	\N	\N	1	8	\N
60	2019-08-27	2019-08-27 03:31:02.647222-04	HO-SRV-AutoFillPre28	2	2048	50		9	10	\N	\N	\N	1	8	\N
61	2019-08-27	2019-08-27 03:31:02.655407-04	HO-SRV-AutoFillPre29	2	2048	50		9	10	\N	\N	\N	1	8	\N
62	2019-08-27	2019-08-27 03:31:02.664565-04	HO-SRV-AutoFillPre30	2	2048	50		9	10	\N	\N	\N	1	8	\N
33	2019-08-27	2019-08-27 03:31:57.40935-04	HO-SRV-AutoFillPre01	2	2048	50		9	10	195	\N	\N	1	8	\N
\.


--
-- Name: virtualization_virtualmachine_id_seq; Type: SEQUENCE SET; Schema: public; Owner: netbox
--

SELECT pg_catalog.setval('public.virtualization_virtualmachine_id_seq', 62, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: circuits_circuit circuits_circuit_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuit
    ADD CONSTRAINT circuits_circuit_pkey PRIMARY KEY (id);


--
-- Name: circuits_circuit circuits_circuit_provider_id_cid_b6f29862_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuit
    ADD CONSTRAINT circuits_circuit_provider_id_cid_b6f29862_uniq UNIQUE (provider_id, cid);


--
-- Name: circuits_circuittermination circuits_circuittermination_circuit_id_term_side_b13efd0e_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination
    ADD CONSTRAINT circuits_circuittermination_circuit_id_term_side_b13efd0e_uniq UNIQUE (circuit_id, term_side);


--
-- Name: circuits_circuittermination circuits_circuittermination_connected_endpoint_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination
    ADD CONSTRAINT circuits_circuittermination_connected_endpoint_id_key UNIQUE (connected_endpoint_id);


--
-- Name: circuits_circuittermination circuits_circuittermination_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination
    ADD CONSTRAINT circuits_circuittermination_pkey PRIMARY KEY (id);


--
-- Name: circuits_circuittype circuits_circuittype_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittype
    ADD CONSTRAINT circuits_circuittype_name_key UNIQUE (name);


--
-- Name: circuits_circuittype circuits_circuittype_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittype
    ADD CONSTRAINT circuits_circuittype_pkey PRIMARY KEY (id);


--
-- Name: circuits_circuittype circuits_circuittype_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittype
    ADD CONSTRAINT circuits_circuittype_slug_key UNIQUE (slug);


--
-- Name: circuits_provider circuits_provider_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_provider
    ADD CONSTRAINT circuits_provider_name_key UNIQUE (name);


--
-- Name: circuits_provider circuits_provider_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_provider
    ADD CONSTRAINT circuits_provider_pkey PRIMARY KEY (id);


--
-- Name: circuits_provider circuits_provider_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_provider
    ADD CONSTRAINT circuits_provider_slug_key UNIQUE (slug);


--
-- Name: dcim_cable dcim_cable_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_cable
    ADD CONSTRAINT dcim_cable_pkey PRIMARY KEY (id);


--
-- Name: dcim_cable dcim_cable_termination_a_type_id_termination_a_id_e9d24bad_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_cable
    ADD CONSTRAINT dcim_cable_termination_a_type_id_termination_a_id_e9d24bad_uniq UNIQUE (termination_a_type_id, termination_a_id);


--
-- Name: dcim_cable dcim_cable_termination_b_type_id_termination_b_id_057fc21f_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_cable
    ADD CONSTRAINT dcim_cable_termination_b_type_id_termination_b_id_057fc21f_uniq UNIQUE (termination_b_type_id, termination_b_id);


--
-- Name: dcim_consoleport dcim_consoleport_cs_port_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleport
    ADD CONSTRAINT dcim_consoleport_cs_port_id_key UNIQUE (connected_endpoint_id);


--
-- Name: dcim_consoleport dcim_consoleport_device_id_name_293786b6_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleport
    ADD CONSTRAINT dcim_consoleport_device_id_name_293786b6_uniq UNIQUE (device_id, name);


--
-- Name: dcim_consoleport dcim_consoleport_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleport
    ADD CONSTRAINT dcim_consoleport_pkey PRIMARY KEY (id);


--
-- Name: dcim_consoleporttemplate dcim_consoleporttemplate_device_type_id_name_8208f9ca_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleporttemplate
    ADD CONSTRAINT dcim_consoleporttemplate_device_type_id_name_8208f9ca_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_consoleporttemplate dcim_consoleporttemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleporttemplate
    ADD CONSTRAINT dcim_consoleporttemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_consoleserverport dcim_consoleserverport_device_id_name_fb1c5999_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverport
    ADD CONSTRAINT dcim_consoleserverport_device_id_name_fb1c5999_uniq UNIQUE (device_id, name);


--
-- Name: dcim_consoleserverport dcim_consoleserverport_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverport
    ADD CONSTRAINT dcim_consoleserverport_pkey PRIMARY KEY (id);


--
-- Name: dcim_consoleserverporttemplate dcim_consoleserverportte_device_type_id_name_a05c974d_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverporttemplate
    ADD CONSTRAINT dcim_consoleserverportte_device_type_id_name_a05c974d_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_consoleserverporttemplate dcim_consoleserverporttemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverporttemplate
    ADD CONSTRAINT dcim_consoleserverporttemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_device dcim_device_asset_tag_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_asset_tag_key UNIQUE (asset_tag);


--
-- Name: dcim_device dcim_device_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_name_key UNIQUE (name);


--
-- Name: dcim_device dcim_device_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_pkey PRIMARY KEY (id);


--
-- Name: dcim_device dcim_device_primary_ip4_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_primary_ip4_id_key UNIQUE (primary_ip4_id);


--
-- Name: dcim_device dcim_device_primary_ip6_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_primary_ip6_id_key UNIQUE (primary_ip6_id);


--
-- Name: dcim_device dcim_device_rack_id_position_face_43208a79_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_rack_id_position_face_43208a79_uniq UNIQUE (rack_id, "position", face);


--
-- Name: dcim_device dcim_device_virtual_chassis_id_vc_position_efea7133_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_virtual_chassis_id_vc_position_efea7133_uniq UNIQUE (virtual_chassis_id, vc_position);


--
-- Name: dcim_devicebay dcim_devicebay_device_id_name_2475a67b_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebay
    ADD CONSTRAINT dcim_devicebay_device_id_name_2475a67b_uniq UNIQUE (device_id, name);


--
-- Name: dcim_devicebay dcim_devicebay_installed_device_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebay
    ADD CONSTRAINT dcim_devicebay_installed_device_id_key UNIQUE (installed_device_id);


--
-- Name: dcim_devicebay dcim_devicebay_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebay
    ADD CONSTRAINT dcim_devicebay_pkey PRIMARY KEY (id);


--
-- Name: dcim_devicebaytemplate dcim_devicebaytemplate_device_type_id_name_8f4899fe_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebaytemplate
    ADD CONSTRAINT dcim_devicebaytemplate_device_type_id_name_8f4899fe_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_devicebaytemplate dcim_devicebaytemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebaytemplate
    ADD CONSTRAINT dcim_devicebaytemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_devicerole dcim_devicerole_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicerole
    ADD CONSTRAINT dcim_devicerole_name_key UNIQUE (name);


--
-- Name: dcim_devicerole dcim_devicerole_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicerole
    ADD CONSTRAINT dcim_devicerole_pkey PRIMARY KEY (id);


--
-- Name: dcim_devicerole dcim_devicerole_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicerole
    ADD CONSTRAINT dcim_devicerole_slug_key UNIQUE (slug);


--
-- Name: dcim_devicetype dcim_devicetype_manufacturer_id_model_17948c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicetype
    ADD CONSTRAINT dcim_devicetype_manufacturer_id_model_17948c0c_uniq UNIQUE (manufacturer_id, model);


--
-- Name: dcim_devicetype dcim_devicetype_manufacturer_id_slug_a0b931cb_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicetype
    ADD CONSTRAINT dcim_devicetype_manufacturer_id_slug_a0b931cb_uniq UNIQUE (manufacturer_id, slug);


--
-- Name: dcim_devicetype dcim_devicetype_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicetype
    ADD CONSTRAINT dcim_devicetype_pkey PRIMARY KEY (id);


--
-- Name: dcim_frontport dcim_frontport_device_id_name_235b7af2_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontport
    ADD CONSTRAINT dcim_frontport_device_id_name_235b7af2_uniq UNIQUE (device_id, name);


--
-- Name: dcim_frontport dcim_frontport_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontport
    ADD CONSTRAINT dcim_frontport_pkey PRIMARY KEY (id);


--
-- Name: dcim_frontport dcim_frontport_rear_port_id_rear_port_position_8b0bf7ca_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontport
    ADD CONSTRAINT dcim_frontport_rear_port_id_rear_port_position_8b0bf7ca_uniq UNIQUE (rear_port_id, rear_port_position);


--
-- Name: dcim_frontporttemplate dcim_frontporttemplate_device_type_id_name_0a0a0e05_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontporttemplate
    ADD CONSTRAINT dcim_frontporttemplate_device_type_id_name_0a0a0e05_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_frontporttemplate dcim_frontporttemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontporttemplate
    ADD CONSTRAINT dcim_frontporttemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_frontporttemplate dcim_frontporttemplate_rear_port_id_rear_port_p_401fe927_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontporttemplate
    ADD CONSTRAINT dcim_frontporttemplate_rear_port_id_rear_port_p_401fe927_uniq UNIQUE (rear_port_id, rear_port_position);


--
-- Name: dcim_interface dcim_interface__connected_circuittermination_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface__connected_circuittermination_id_key UNIQUE (_connected_circuittermination_id);


--
-- Name: dcim_interface dcim_interface__connected_interface_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface__connected_interface_id_key UNIQUE (_connected_interface_id);


--
-- Name: dcim_interface dcim_interface_device_id_name_bffc4ec4_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface_device_id_name_bffc4ec4_uniq UNIQUE (device_id, name);


--
-- Name: dcim_interface dcim_interface_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface_pkey PRIMARY KEY (id);


--
-- Name: dcim_interface_tagged_vlans dcim_interface_tagged_vlans_interface_id_vlan_id_0d55c576_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface_tagged_vlans
    ADD CONSTRAINT dcim_interface_tagged_vlans_interface_id_vlan_id_0d55c576_uniq UNIQUE (interface_id, vlan_id);


--
-- Name: dcim_interface_tagged_vlans dcim_interface_tagged_vlans_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface_tagged_vlans
    ADD CONSTRAINT dcim_interface_tagged_vlans_pkey PRIMARY KEY (id);


--
-- Name: dcim_interfacetemplate dcim_interfacetemplate_device_type_id_name_3a847237_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interfacetemplate
    ADD CONSTRAINT dcim_interfacetemplate_device_type_id_name_3a847237_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_interfacetemplate dcim_interfacetemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interfacetemplate
    ADD CONSTRAINT dcim_interfacetemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_inventoryitem dcim_inventoryitem_asset_tag_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_inventoryitem
    ADD CONSTRAINT dcim_inventoryitem_asset_tag_key UNIQUE (asset_tag);


--
-- Name: dcim_manufacturer dcim_manufacturer_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_manufacturer
    ADD CONSTRAINT dcim_manufacturer_name_key UNIQUE (name);


--
-- Name: dcim_manufacturer dcim_manufacturer_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_manufacturer
    ADD CONSTRAINT dcim_manufacturer_pkey PRIMARY KEY (id);


--
-- Name: dcim_manufacturer dcim_manufacturer_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_manufacturer
    ADD CONSTRAINT dcim_manufacturer_slug_key UNIQUE (slug);


--
-- Name: dcim_inventoryitem dcim_module_device_id_parent_id_name_4d8292af_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_inventoryitem
    ADD CONSTRAINT dcim_module_device_id_parent_id_name_4d8292af_uniq UNIQUE (device_id, parent_id, name);


--
-- Name: dcim_inventoryitem dcim_module_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_inventoryitem
    ADD CONSTRAINT dcim_module_pkey PRIMARY KEY (id);


--
-- Name: dcim_platform dcim_platform_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_platform
    ADD CONSTRAINT dcim_platform_name_key UNIQUE (name);


--
-- Name: dcim_platform dcim_platform_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_platform
    ADD CONSTRAINT dcim_platform_pkey PRIMARY KEY (id);


--
-- Name: dcim_platform dcim_platform_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_platform
    ADD CONSTRAINT dcim_platform_slug_key UNIQUE (slug);


--
-- Name: dcim_powerfeed dcim_powerfeed_connected_endpoint_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed
    ADD CONSTRAINT dcim_powerfeed_connected_endpoint_id_key UNIQUE (connected_endpoint_id);


--
-- Name: dcim_powerfeed dcim_powerfeed_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed
    ADD CONSTRAINT dcim_powerfeed_pkey PRIMARY KEY (id);


--
-- Name: dcim_powerfeed dcim_powerfeed_power_panel_id_name_0fbaae9f_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed
    ADD CONSTRAINT dcim_powerfeed_power_panel_id_name_0fbaae9f_uniq UNIQUE (power_panel_id, name);


--
-- Name: dcim_poweroutlet dcim_poweroutlet_device_id_name_981b00c1_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlet
    ADD CONSTRAINT dcim_poweroutlet_device_id_name_981b00c1_uniq UNIQUE (device_id, name);


--
-- Name: dcim_poweroutlet dcim_poweroutlet_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlet
    ADD CONSTRAINT dcim_poweroutlet_pkey PRIMARY KEY (id);


--
-- Name: dcim_poweroutlettemplate dcim_poweroutlettemplate_device_type_id_name_eafbb07d_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlettemplate
    ADD CONSTRAINT dcim_poweroutlettemplate_device_type_id_name_eafbb07d_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_poweroutlettemplate dcim_poweroutlettemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlettemplate
    ADD CONSTRAINT dcim_poweroutlettemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_powerpanel dcim_powerpanel_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerpanel
    ADD CONSTRAINT dcim_powerpanel_pkey PRIMARY KEY (id);


--
-- Name: dcim_powerpanel dcim_powerpanel_site_id_name_804df4c0_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerpanel
    ADD CONSTRAINT dcim_powerpanel_site_id_name_804df4c0_uniq UNIQUE (site_id, name);


--
-- Name: dcim_powerport dcim_powerport__connected_powerfeed_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport__connected_powerfeed_id_key UNIQUE (_connected_powerfeed_id);


--
-- Name: dcim_powerport dcim_powerport_device_id_name_948af82c_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport_device_id_name_948af82c_uniq UNIQUE (device_id, name);


--
-- Name: dcim_powerport dcim_powerport_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport_pkey PRIMARY KEY (id);


--
-- Name: dcim_powerport dcim_powerport_power_outlet_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport_power_outlet_id_key UNIQUE (_connected_poweroutlet_id);


--
-- Name: dcim_powerporttemplate dcim_powerporttemplate_device_type_id_name_b4e9689f_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerporttemplate
    ADD CONSTRAINT dcim_powerporttemplate_device_type_id_name_b4e9689f_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_powerporttemplate dcim_powerporttemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerporttemplate
    ADD CONSTRAINT dcim_powerporttemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_rack dcim_rack_asset_tag_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_asset_tag_key UNIQUE (asset_tag);


--
-- Name: dcim_rack dcim_rack_group_id_facility_id_f16a53ae_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_group_id_facility_id_f16a53ae_uniq UNIQUE (group_id, facility_id);


--
-- Name: dcim_rack dcim_rack_group_id_name_846f3826_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_group_id_name_846f3826_uniq UNIQUE (group_id, name);


--
-- Name: dcim_rack dcim_rack_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_pkey PRIMARY KEY (id);


--
-- Name: dcim_rackgroup dcim_rackgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackgroup
    ADD CONSTRAINT dcim_rackgroup_pkey PRIMARY KEY (id);


--
-- Name: dcim_rackgroup dcim_rackgroup_site_id_name_c9bd921f_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackgroup
    ADD CONSTRAINT dcim_rackgroup_site_id_name_c9bd921f_uniq UNIQUE (site_id, name);


--
-- Name: dcim_rackgroup dcim_rackgroup_site_id_slug_7fbfd118_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackgroup
    ADD CONSTRAINT dcim_rackgroup_site_id_slug_7fbfd118_uniq UNIQUE (site_id, slug);


--
-- Name: dcim_rackreservation dcim_rackreservation_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackreservation
    ADD CONSTRAINT dcim_rackreservation_pkey PRIMARY KEY (id);


--
-- Name: dcim_rackrole dcim_rackrole_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackrole
    ADD CONSTRAINT dcim_rackrole_name_key UNIQUE (name);


--
-- Name: dcim_rackrole dcim_rackrole_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackrole
    ADD CONSTRAINT dcim_rackrole_pkey PRIMARY KEY (id);


--
-- Name: dcim_rackrole dcim_rackrole_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackrole
    ADD CONSTRAINT dcim_rackrole_slug_key UNIQUE (slug);


--
-- Name: dcim_rearport dcim_rearport_device_id_name_4b14dde6_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearport
    ADD CONSTRAINT dcim_rearport_device_id_name_4b14dde6_uniq UNIQUE (device_id, name);


--
-- Name: dcim_rearport dcim_rearport_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearport
    ADD CONSTRAINT dcim_rearport_pkey PRIMARY KEY (id);


--
-- Name: dcim_rearporttemplate dcim_rearporttemplate_device_type_id_name_9bdddb29_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearporttemplate
    ADD CONSTRAINT dcim_rearporttemplate_device_type_id_name_9bdddb29_uniq UNIQUE (device_type_id, name);


--
-- Name: dcim_rearporttemplate dcim_rearporttemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearporttemplate
    ADD CONSTRAINT dcim_rearporttemplate_pkey PRIMARY KEY (id);


--
-- Name: dcim_region dcim_region_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_region
    ADD CONSTRAINT dcim_region_name_key UNIQUE (name);


--
-- Name: dcim_region dcim_region_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_region
    ADD CONSTRAINT dcim_region_pkey PRIMARY KEY (id);


--
-- Name: dcim_region dcim_region_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_region
    ADD CONSTRAINT dcim_region_slug_key UNIQUE (slug);


--
-- Name: dcim_site dcim_site_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_site
    ADD CONSTRAINT dcim_site_name_key UNIQUE (name);


--
-- Name: dcim_site dcim_site_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_site
    ADD CONSTRAINT dcim_site_pkey PRIMARY KEY (id);


--
-- Name: dcim_site dcim_site_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_site
    ADD CONSTRAINT dcim_site_slug_key UNIQUE (slug);


--
-- Name: dcim_virtualchassis dcim_virtualchassis_master_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_virtualchassis
    ADD CONSTRAINT dcim_virtualchassis_master_id_key UNIQUE (master_id);


--
-- Name: dcim_virtualchassis dcim_virtualchassis_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_virtualchassis
    ADD CONSTRAINT dcim_virtualchassis_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: extras_configcontext extras_configcontext_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext
    ADD CONSTRAINT extras_configcontext_name_key UNIQUE (name);


--
-- Name: extras_configcontext extras_configcontext_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext
    ADD CONSTRAINT extras_configcontext_pkey PRIMARY KEY (id);


--
-- Name: extras_configcontext_platforms extras_configcontext_pla_configcontext_id_platfor_3c67c104_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_platforms
    ADD CONSTRAINT extras_configcontext_pla_configcontext_id_platfor_3c67c104_uniq UNIQUE (configcontext_id, platform_id);


--
-- Name: extras_configcontext_platforms extras_configcontext_platforms_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_platforms
    ADD CONSTRAINT extras_configcontext_platforms_pkey PRIMARY KEY (id);


--
-- Name: extras_configcontext_regions extras_configcontext_reg_configcontext_id_region__d4a1d77f_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_regions
    ADD CONSTRAINT extras_configcontext_reg_configcontext_id_region__d4a1d77f_uniq UNIQUE (configcontext_id, region_id);


--
-- Name: extras_configcontext_regions extras_configcontext_regions_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_regions
    ADD CONSTRAINT extras_configcontext_regions_pkey PRIMARY KEY (id);


--
-- Name: extras_configcontext_roles extras_configcontext_rol_configcontext_id_devicer_4d8dbb50_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_roles
    ADD CONSTRAINT extras_configcontext_rol_configcontext_id_devicer_4d8dbb50_uniq UNIQUE (configcontext_id, devicerole_id);


--
-- Name: extras_configcontext_roles extras_configcontext_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_roles
    ADD CONSTRAINT extras_configcontext_roles_pkey PRIMARY KEY (id);


--
-- Name: extras_configcontext_sites extras_configcontext_sit_configcontext_id_site_id_a4fe5f4f_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_sites
    ADD CONSTRAINT extras_configcontext_sit_configcontext_id_site_id_a4fe5f4f_uniq UNIQUE (configcontext_id, site_id);


--
-- Name: extras_configcontext_sites extras_configcontext_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_sites
    ADD CONSTRAINT extras_configcontext_sites_pkey PRIMARY KEY (id);


--
-- Name: extras_configcontext_tenants extras_configcontext_ten_configcontext_id_tenant__aefb257d_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenants
    ADD CONSTRAINT extras_configcontext_ten_configcontext_id_tenant__aefb257d_uniq UNIQUE (configcontext_id, tenant_id);


--
-- Name: extras_configcontext_tenant_groups extras_configcontext_ten_configcontext_id_tenantg_d6afc6f5_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenant_groups
    ADD CONSTRAINT extras_configcontext_ten_configcontext_id_tenantg_d6afc6f5_uniq UNIQUE (configcontext_id, tenantgroup_id);


--
-- Name: extras_configcontext_tenant_groups extras_configcontext_tenant_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenant_groups
    ADD CONSTRAINT extras_configcontext_tenant_groups_pkey PRIMARY KEY (id);


--
-- Name: extras_configcontext_tenants extras_configcontext_tenants_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenants
    ADD CONSTRAINT extras_configcontext_tenants_pkey PRIMARY KEY (id);


--
-- Name: extras_customfield extras_customfield_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield
    ADD CONSTRAINT extras_customfield_name_key UNIQUE (name);


--
-- Name: extras_customfield_obj_type extras_customfield_obj_t_customfield_id_contentty_77878958_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield_obj_type
    ADD CONSTRAINT extras_customfield_obj_t_customfield_id_contentty_77878958_uniq UNIQUE (customfield_id, contenttype_id);


--
-- Name: extras_customfield_obj_type extras_customfield_obj_type_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield_obj_type
    ADD CONSTRAINT extras_customfield_obj_type_pkey PRIMARY KEY (id);


--
-- Name: extras_customfield extras_customfield_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield
    ADD CONSTRAINT extras_customfield_pkey PRIMARY KEY (id);


--
-- Name: extras_customfieldchoice extras_customfieldchoice_field_id_value_f959a108_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldchoice
    ADD CONSTRAINT extras_customfieldchoice_field_id_value_f959a108_uniq UNIQUE (field_id, value);


--
-- Name: extras_customfieldchoice extras_customfieldchoice_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldchoice
    ADD CONSTRAINT extras_customfieldchoice_pkey PRIMARY KEY (id);


--
-- Name: extras_customfieldvalue extras_customfieldvalue_field_id_obj_type_id_obj_876f6d9c_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldvalue
    ADD CONSTRAINT extras_customfieldvalue_field_id_obj_type_id_obj_876f6d9c_uniq UNIQUE (field_id, obj_type_id, obj_id);


--
-- Name: extras_customfieldvalue extras_customfieldvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldvalue
    ADD CONSTRAINT extras_customfieldvalue_pkey PRIMARY KEY (id);


--
-- Name: extras_customlink extras_customlink_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customlink
    ADD CONSTRAINT extras_customlink_name_key UNIQUE (name);


--
-- Name: extras_customlink extras_customlink_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customlink
    ADD CONSTRAINT extras_customlink_pkey PRIMARY KEY (id);


--
-- Name: extras_exporttemplate extras_exporttemplate_content_type_id_name_edca9b9b_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_exporttemplate
    ADD CONSTRAINT extras_exporttemplate_content_type_id_name_edca9b9b_uniq UNIQUE (content_type_id, name);


--
-- Name: extras_exporttemplate extras_exporttemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_exporttemplate
    ADD CONSTRAINT extras_exporttemplate_pkey PRIMARY KEY (id);


--
-- Name: extras_graph extras_graph_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_graph
    ADD CONSTRAINT extras_graph_pkey PRIMARY KEY (id);


--
-- Name: extras_imageattachment extras_imageattachment_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_imageattachment
    ADD CONSTRAINT extras_imageattachment_pkey PRIMARY KEY (id);


--
-- Name: extras_objectchange extras_objectchange_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_objectchange
    ADD CONSTRAINT extras_objectchange_pkey PRIMARY KEY (id);


--
-- Name: extras_reportresult extras_reportresult_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_reportresult
    ADD CONSTRAINT extras_reportresult_pkey PRIMARY KEY (id);


--
-- Name: extras_reportresult extras_reportresult_report_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_reportresult
    ADD CONSTRAINT extras_reportresult_report_key UNIQUE (report);


--
-- Name: extras_tag extras_tag_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_tag
    ADD CONSTRAINT extras_tag_name_key UNIQUE (name);


--
-- Name: extras_tag extras_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_tag
    ADD CONSTRAINT extras_tag_pkey PRIMARY KEY (id);


--
-- Name: extras_tag extras_tag_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_tag
    ADD CONSTRAINT extras_tag_slug_key UNIQUE (slug);


--
-- Name: extras_taggeditem extras_taggeditem_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_taggeditem
    ADD CONSTRAINT extras_taggeditem_pkey PRIMARY KEY (id);


--
-- Name: extras_topologymap extras_topologymap_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_topologymap
    ADD CONSTRAINT extras_topologymap_name_key UNIQUE (name);


--
-- Name: extras_topologymap extras_topologymap_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_topologymap
    ADD CONSTRAINT extras_topologymap_pkey PRIMARY KEY (id);


--
-- Name: extras_topologymap extras_topologymap_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_topologymap
    ADD CONSTRAINT extras_topologymap_slug_key UNIQUE (slug);


--
-- Name: extras_webhook extras_webhook_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook
    ADD CONSTRAINT extras_webhook_name_key UNIQUE (name);


--
-- Name: extras_webhook_obj_type extras_webhook_obj_type_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook_obj_type
    ADD CONSTRAINT extras_webhook_obj_type_pkey PRIMARY KEY (id);


--
-- Name: extras_webhook_obj_type extras_webhook_obj_type_webhook_id_contenttype_id_99b8b9c3_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook_obj_type
    ADD CONSTRAINT extras_webhook_obj_type_webhook_id_contenttype_id_99b8b9c3_uniq UNIQUE (webhook_id, contenttype_id);


--
-- Name: extras_webhook extras_webhook_payload_url_type_create__dd332134_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook
    ADD CONSTRAINT extras_webhook_payload_url_type_create__dd332134_uniq UNIQUE (payload_url, type_create, type_update, type_delete);


--
-- Name: extras_webhook extras_webhook_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook
    ADD CONSTRAINT extras_webhook_pkey PRIMARY KEY (id);


--
-- Name: ipam_aggregate ipam_aggregate_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_aggregate
    ADD CONSTRAINT ipam_aggregate_pkey PRIMARY KEY (id);


--
-- Name: ipam_ipaddress ipam_ipaddress_nat_inside_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_ipaddress
    ADD CONSTRAINT ipam_ipaddress_nat_inside_id_key UNIQUE (nat_inside_id);


--
-- Name: ipam_ipaddress ipam_ipaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_ipaddress
    ADD CONSTRAINT ipam_ipaddress_pkey PRIMARY KEY (id);


--
-- Name: ipam_prefix ipam_prefix_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_prefix
    ADD CONSTRAINT ipam_prefix_pkey PRIMARY KEY (id);


--
-- Name: ipam_rir ipam_rir_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_rir
    ADD CONSTRAINT ipam_rir_name_key UNIQUE (name);


--
-- Name: ipam_rir ipam_rir_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_rir
    ADD CONSTRAINT ipam_rir_pkey PRIMARY KEY (id);


--
-- Name: ipam_rir ipam_rir_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_rir
    ADD CONSTRAINT ipam_rir_slug_key UNIQUE (slug);


--
-- Name: ipam_role ipam_role_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_role
    ADD CONSTRAINT ipam_role_name_key UNIQUE (name);


--
-- Name: ipam_role ipam_role_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_role
    ADD CONSTRAINT ipam_role_pkey PRIMARY KEY (id);


--
-- Name: ipam_role ipam_role_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_role
    ADD CONSTRAINT ipam_role_slug_key UNIQUE (slug);


--
-- Name: ipam_service_ipaddresses ipam_service_ipaddresses_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service_ipaddresses
    ADD CONSTRAINT ipam_service_ipaddresses_pkey PRIMARY KEY (id);


--
-- Name: ipam_service_ipaddresses ipam_service_ipaddresses_service_id_ipaddress_id_d019a805_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service_ipaddresses
    ADD CONSTRAINT ipam_service_ipaddresses_service_id_ipaddress_id_d019a805_uniq UNIQUE (service_id, ipaddress_id);


--
-- Name: ipam_service ipam_service_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service
    ADD CONSTRAINT ipam_service_pkey PRIMARY KEY (id);


--
-- Name: ipam_vlan ipam_vlan_group_id_name_e53919df_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan
    ADD CONSTRAINT ipam_vlan_group_id_name_e53919df_uniq UNIQUE (group_id, name);


--
-- Name: ipam_vlan ipam_vlan_group_id_vid_5ca4cc47_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan
    ADD CONSTRAINT ipam_vlan_group_id_vid_5ca4cc47_uniq UNIQUE (group_id, vid);


--
-- Name: ipam_vlan ipam_vlan_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan
    ADD CONSTRAINT ipam_vlan_pkey PRIMARY KEY (id);


--
-- Name: ipam_vlangroup ipam_vlangroup_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlangroup
    ADD CONSTRAINT ipam_vlangroup_pkey PRIMARY KEY (id);


--
-- Name: ipam_vlangroup ipam_vlangroup_site_id_name_a38e981b_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlangroup
    ADD CONSTRAINT ipam_vlangroup_site_id_name_a38e981b_uniq UNIQUE (site_id, name);


--
-- Name: ipam_vlangroup ipam_vlangroup_site_id_slug_6372a304_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlangroup
    ADD CONSTRAINT ipam_vlangroup_site_id_slug_6372a304_uniq UNIQUE (site_id, slug);


--
-- Name: ipam_vrf ipam_vrf_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vrf
    ADD CONSTRAINT ipam_vrf_pkey PRIMARY KEY (id);


--
-- Name: ipam_vrf ipam_vrf_rd_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vrf
    ADD CONSTRAINT ipam_vrf_rd_key UNIQUE (rd);


--
-- Name: secrets_secret secrets_secret_device_id_role_id_name_f8acc218_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secret
    ADD CONSTRAINT secrets_secret_device_id_role_id_name_f8acc218_uniq UNIQUE (device_id, role_id, name);


--
-- Name: secrets_secret secrets_secret_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secret
    ADD CONSTRAINT secrets_secret_pkey PRIMARY KEY (id);


--
-- Name: secrets_secretrole_groups secrets_secretrole_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_groups
    ADD CONSTRAINT secrets_secretrole_groups_pkey PRIMARY KEY (id);


--
-- Name: secrets_secretrole_groups secrets_secretrole_groups_secretrole_id_group_id_1c7f7ee5_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_groups
    ADD CONSTRAINT secrets_secretrole_groups_secretrole_id_group_id_1c7f7ee5_uniq UNIQUE (secretrole_id, group_id);


--
-- Name: secrets_secretrole secrets_secretrole_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole
    ADD CONSTRAINT secrets_secretrole_name_key UNIQUE (name);


--
-- Name: secrets_secretrole secrets_secretrole_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole
    ADD CONSTRAINT secrets_secretrole_pkey PRIMARY KEY (id);


--
-- Name: secrets_secretrole secrets_secretrole_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole
    ADD CONSTRAINT secrets_secretrole_slug_key UNIQUE (slug);


--
-- Name: secrets_secretrole_users secrets_secretrole_users_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_users
    ADD CONSTRAINT secrets_secretrole_users_pkey PRIMARY KEY (id);


--
-- Name: secrets_secretrole_users secrets_secretrole_users_secretrole_id_user_id_41832d38_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_users
    ADD CONSTRAINT secrets_secretrole_users_secretrole_id_user_id_41832d38_uniq UNIQUE (secretrole_id, user_id);


--
-- Name: secrets_sessionkey secrets_sessionkey_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_sessionkey
    ADD CONSTRAINT secrets_sessionkey_pkey PRIMARY KEY (id);


--
-- Name: secrets_sessionkey secrets_sessionkey_userkey_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_sessionkey
    ADD CONSTRAINT secrets_sessionkey_userkey_id_key UNIQUE (userkey_id);


--
-- Name: secrets_userkey secrets_userkey_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_userkey
    ADD CONSTRAINT secrets_userkey_pkey PRIMARY KEY (id);


--
-- Name: secrets_userkey secrets_userkey_user_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_userkey
    ADD CONSTRAINT secrets_userkey_user_id_key UNIQUE (user_id);


--
-- Name: taggit_tag taggit_tag_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_name_key UNIQUE (name);


--
-- Name: taggit_tag taggit_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_pkey PRIMARY KEY (id);


--
-- Name: taggit_tag taggit_tag_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_slug_key UNIQUE (slug);


--
-- Name: taggit_taggeditem taggit_taggeditem_content_type_id_object_i_4bb97a8e_uniq; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_content_type_id_object_i_4bb97a8e_uniq UNIQUE (content_type_id, object_id, tag_id);


--
-- Name: taggit_taggeditem taggit_taggeditem_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_pkey PRIMARY KEY (id);


--
-- Name: tenancy_tenant tenancy_tenant_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenant
    ADD CONSTRAINT tenancy_tenant_name_key UNIQUE (name);


--
-- Name: tenancy_tenant tenancy_tenant_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenant
    ADD CONSTRAINT tenancy_tenant_pkey PRIMARY KEY (id);


--
-- Name: tenancy_tenant tenancy_tenant_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenant
    ADD CONSTRAINT tenancy_tenant_slug_key UNIQUE (slug);


--
-- Name: tenancy_tenantgroup tenancy_tenantgroup_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenantgroup
    ADD CONSTRAINT tenancy_tenantgroup_name_key UNIQUE (name);


--
-- Name: tenancy_tenantgroup tenancy_tenantgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenantgroup
    ADD CONSTRAINT tenancy_tenantgroup_pkey PRIMARY KEY (id);


--
-- Name: tenancy_tenantgroup tenancy_tenantgroup_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenantgroup
    ADD CONSTRAINT tenancy_tenantgroup_slug_key UNIQUE (slug);


--
-- Name: users_token users_token_key_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.users_token
    ADD CONSTRAINT users_token_key_key UNIQUE (key);


--
-- Name: users_token users_token_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.users_token
    ADD CONSTRAINT users_token_pkey PRIMARY KEY (id);


--
-- Name: virtualization_cluster virtualization_cluster_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_cluster
    ADD CONSTRAINT virtualization_cluster_name_key UNIQUE (name);


--
-- Name: virtualization_cluster virtualization_cluster_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_cluster
    ADD CONSTRAINT virtualization_cluster_pkey PRIMARY KEY (id);


--
-- Name: virtualization_clustergroup virtualization_clustergroup_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustergroup
    ADD CONSTRAINT virtualization_clustergroup_name_key UNIQUE (name);


--
-- Name: virtualization_clustergroup virtualization_clustergroup_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustergroup
    ADD CONSTRAINT virtualization_clustergroup_pkey PRIMARY KEY (id);


--
-- Name: virtualization_clustergroup virtualization_clustergroup_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustergroup
    ADD CONSTRAINT virtualization_clustergroup_slug_key UNIQUE (slug);


--
-- Name: virtualization_clustertype virtualization_clustertype_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustertype
    ADD CONSTRAINT virtualization_clustertype_name_key UNIQUE (name);


--
-- Name: virtualization_clustertype virtualization_clustertype_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustertype
    ADD CONSTRAINT virtualization_clustertype_pkey PRIMARY KEY (id);


--
-- Name: virtualization_clustertype virtualization_clustertype_slug_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_clustertype
    ADD CONSTRAINT virtualization_clustertype_slug_key UNIQUE (slug);


--
-- Name: virtualization_virtualmachine virtualization_virtualmachine_name_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtualmachine_name_key UNIQUE (name);


--
-- Name: virtualization_virtualmachine virtualization_virtualmachine_pkey; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtualmachine_pkey PRIMARY KEY (id);


--
-- Name: virtualization_virtualmachine virtualization_virtualmachine_primary_ip4_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtualmachine_primary_ip4_id_key UNIQUE (primary_ip4_id);


--
-- Name: virtualization_virtualmachine virtualization_virtualmachine_primary_ip6_id_key; Type: CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtualmachine_primary_ip6_id_key UNIQUE (primary_ip6_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: circuits_circuit_provider_id_d9195418; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuit_provider_id_d9195418 ON public.circuits_circuit USING btree (provider_id);


--
-- Name: circuits_circuit_tenant_id_812508a5; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuit_tenant_id_812508a5 ON public.circuits_circuit USING btree (tenant_id);


--
-- Name: circuits_circuit_type_id_1b9f485a; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuit_type_id_1b9f485a ON public.circuits_circuit USING btree (type_id);


--
-- Name: circuits_circuittermination_cable_id_35e9f703; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuittermination_cable_id_35e9f703 ON public.circuits_circuittermination USING btree (cable_id);


--
-- Name: circuits_circuittermination_circuit_id_257e87e7; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuittermination_circuit_id_257e87e7 ON public.circuits_circuittermination USING btree (circuit_id);


--
-- Name: circuits_circuittermination_site_id_e6fe5652; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuittermination_site_id_e6fe5652 ON public.circuits_circuittermination USING btree (site_id);


--
-- Name: circuits_circuittype_name_8256ea9a_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuittype_name_8256ea9a_like ON public.circuits_circuittype USING btree (name varchar_pattern_ops);


--
-- Name: circuits_circuittype_slug_9b4b3cf9_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_circuittype_slug_9b4b3cf9_like ON public.circuits_circuittype USING btree (slug varchar_pattern_ops);


--
-- Name: circuits_provider_name_8f2514f5_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_provider_name_8f2514f5_like ON public.circuits_provider USING btree (name varchar_pattern_ops);


--
-- Name: circuits_provider_slug_c3c0aa10_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX circuits_provider_slug_c3c0aa10_like ON public.circuits_provider USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_cable_termination_a_type_id_a614bab8; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_cable_termination_a_type_id_a614bab8 ON public.dcim_cable USING btree (termination_a_type_id);


--
-- Name: dcim_cable_termination_b_type_id_a91595d0; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_cable_termination_b_type_id_a91595d0 ON public.dcim_cable USING btree (termination_b_type_id);


--
-- Name: dcim_consoleport_cable_id_a9ae5465; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_consoleport_cable_id_a9ae5465 ON public.dcim_consoleport USING btree (cable_id);


--
-- Name: dcim_consoleport_device_id_f2d90d3c; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_consoleport_device_id_f2d90d3c ON public.dcim_consoleport USING btree (device_id);


--
-- Name: dcim_consoleporttemplate_device_type_id_075d4015; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_consoleporttemplate_device_type_id_075d4015 ON public.dcim_consoleporttemplate USING btree (device_type_id);


--
-- Name: dcim_consoleserverport_cable_id_f2940dfd; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_consoleserverport_cable_id_f2940dfd ON public.dcim_consoleserverport USING btree (cable_id);


--
-- Name: dcim_consoleserverport_device_id_d9866581; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_consoleserverport_device_id_d9866581 ON public.dcim_consoleserverport USING btree (device_id);


--
-- Name: dcim_consoleserverporttemplate_device_type_id_579bdc86; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_consoleserverporttemplate_device_type_id_579bdc86 ON public.dcim_consoleserverporttemplate USING btree (device_type_id);


--
-- Name: dcim_device_asset_tag_8dac1079_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_asset_tag_8dac1079_like ON public.dcim_device USING btree (asset_tag varchar_pattern_ops);


--
-- Name: dcim_device_cluster_id_cf852f78; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_cluster_id_cf852f78 ON public.dcim_device USING btree (cluster_id);


--
-- Name: dcim_device_device_role_id_682e8188; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_device_role_id_682e8188 ON public.dcim_device USING btree (device_role_id);


--
-- Name: dcim_device_device_type_id_d61b4086; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_device_type_id_d61b4086 ON public.dcim_device USING btree (device_type_id);


--
-- Name: dcim_device_name_cfa61dd8_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_name_cfa61dd8_like ON public.dcim_device USING btree (name varchar_pattern_ops);


--
-- Name: dcim_device_platform_id_468138f1; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_platform_id_468138f1 ON public.dcim_device USING btree (platform_id);


--
-- Name: dcim_device_rack_id_23bde71f; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_rack_id_23bde71f ON public.dcim_device USING btree (rack_id);


--
-- Name: dcim_device_site_id_ff897cf6; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_site_id_ff897cf6 ON public.dcim_device USING btree (site_id);


--
-- Name: dcim_device_tenant_id_dcea7969; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_tenant_id_dcea7969 ON public.dcim_device USING btree (tenant_id);


--
-- Name: dcim_device_virtual_chassis_id_aed51693; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_device_virtual_chassis_id_aed51693 ON public.dcim_device USING btree (virtual_chassis_id);


--
-- Name: dcim_devicebay_device_id_0c8a1218; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_devicebay_device_id_0c8a1218 ON public.dcim_devicebay USING btree (device_id);


--
-- Name: dcim_devicebaytemplate_device_type_id_f4b24a29; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_devicebaytemplate_device_type_id_f4b24a29 ON public.dcim_devicebaytemplate USING btree (device_type_id);


--
-- Name: dcim_devicerole_name_1c813306_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_devicerole_name_1c813306_like ON public.dcim_devicerole USING btree (name varchar_pattern_ops);


--
-- Name: dcim_devicerole_slug_7952643b_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_devicerole_slug_7952643b_like ON public.dcim_devicerole USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_devicetype_manufacturer_id_a3e8029e; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_devicetype_manufacturer_id_a3e8029e ON public.dcim_devicetype USING btree (manufacturer_id);


--
-- Name: dcim_devicetype_slug_448745bd; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_devicetype_slug_448745bd ON public.dcim_devicetype USING btree (slug);


--
-- Name: dcim_devicetype_slug_448745bd_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_devicetype_slug_448745bd_like ON public.dcim_devicetype USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_frontport_cable_id_04ff8aab; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_frontport_cable_id_04ff8aab ON public.dcim_frontport USING btree (cable_id);


--
-- Name: dcim_frontport_device_id_950557b5; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_frontport_device_id_950557b5 ON public.dcim_frontport USING btree (device_id);


--
-- Name: dcim_frontport_rear_port_id_78df2532; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_frontport_rear_port_id_78df2532 ON public.dcim_frontport USING btree (rear_port_id);


--
-- Name: dcim_frontporttemplate_device_type_id_f088b952; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_frontporttemplate_device_type_id_f088b952 ON public.dcim_frontporttemplate USING btree (device_type_id);


--
-- Name: dcim_frontporttemplate_rear_port_id_9775411b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_frontporttemplate_rear_port_id_9775411b ON public.dcim_frontporttemplate USING btree (rear_port_id);


--
-- Name: dcim_interface_cable_id_1b264edb; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interface_cable_id_1b264edb ON public.dcim_interface USING btree (cable_id);


--
-- Name: dcim_interface_device_id_359c6115; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interface_device_id_359c6115 ON public.dcim_interface USING btree (device_id);


--
-- Name: dcim_interface_lag_id_ea1a1d12; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interface_lag_id_ea1a1d12 ON public.dcim_interface USING btree (lag_id);


--
-- Name: dcim_interface_tagged_vlans_interface_id_5870c9e9; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interface_tagged_vlans_interface_id_5870c9e9 ON public.dcim_interface_tagged_vlans USING btree (interface_id);


--
-- Name: dcim_interface_tagged_vlans_vlan_id_e027005c; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interface_tagged_vlans_vlan_id_e027005c ON public.dcim_interface_tagged_vlans USING btree (vlan_id);


--
-- Name: dcim_interface_untagged_vlan_id_838dc7be; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interface_untagged_vlan_id_838dc7be ON public.dcim_interface USING btree (untagged_vlan_id);


--
-- Name: dcim_interface_virtual_machine_id_2afd2d50; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interface_virtual_machine_id_2afd2d50 ON public.dcim_interface USING btree (virtual_machine_id);


--
-- Name: dcim_interfacetemplate_device_type_id_4bfcbfab; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_interfacetemplate_device_type_id_4bfcbfab ON public.dcim_interfacetemplate USING btree (device_type_id);


--
-- Name: dcim_inventoryitem_asset_tag_d3289273_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_inventoryitem_asset_tag_d3289273_like ON public.dcim_inventoryitem USING btree (asset_tag varchar_pattern_ops);


--
-- Name: dcim_manufacturer_name_841fcd92_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_manufacturer_name_841fcd92_like ON public.dcim_manufacturer USING btree (name varchar_pattern_ops);


--
-- Name: dcim_manufacturer_slug_00430749_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_manufacturer_slug_00430749_like ON public.dcim_manufacturer USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_module_device_id_53cfd5be; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_module_device_id_53cfd5be ON public.dcim_inventoryitem USING btree (device_id);


--
-- Name: dcim_module_manufacturer_id_95322cbb; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_module_manufacturer_id_95322cbb ON public.dcim_inventoryitem USING btree (manufacturer_id);


--
-- Name: dcim_module_parent_id_bb5d0341; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_module_parent_id_bb5d0341 ON public.dcim_inventoryitem USING btree (parent_id);


--
-- Name: dcim_platform_manufacturer_id_83f72d3d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_platform_manufacturer_id_83f72d3d ON public.dcim_platform USING btree (manufacturer_id);


--
-- Name: dcim_platform_name_c2f04255_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_platform_name_c2f04255_like ON public.dcim_platform USING btree (name varchar_pattern_ops);


--
-- Name: dcim_platform_slug_b0908ae4_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_platform_slug_b0908ae4_like ON public.dcim_platform USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_powerfeed_cable_id_ec44c4f8; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerfeed_cable_id_ec44c4f8 ON public.dcim_powerfeed USING btree (cable_id);


--
-- Name: dcim_powerfeed_power_panel_id_32bde3be; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerfeed_power_panel_id_32bde3be ON public.dcim_powerfeed USING btree (power_panel_id);


--
-- Name: dcim_powerfeed_rack_id_7abba090; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerfeed_rack_id_7abba090 ON public.dcim_powerfeed USING btree (rack_id);


--
-- Name: dcim_poweroutlet_cable_id_8dbea1ec; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_poweroutlet_cable_id_8dbea1ec ON public.dcim_poweroutlet USING btree (cable_id);


--
-- Name: dcim_poweroutlet_device_id_286351d7; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_poweroutlet_device_id_286351d7 ON public.dcim_poweroutlet USING btree (device_id);


--
-- Name: dcim_poweroutlet_power_port_id_9bdf4163; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_poweroutlet_power_port_id_9bdf4163 ON public.dcim_poweroutlet USING btree (power_port_id);


--
-- Name: dcim_poweroutlettemplate_device_type_id_26b2316c; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_poweroutlettemplate_device_type_id_26b2316c ON public.dcim_poweroutlettemplate USING btree (device_type_id);


--
-- Name: dcim_poweroutlettemplate_power_port_id_c0fb0c42; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_poweroutlettemplate_power_port_id_c0fb0c42 ON public.dcim_poweroutlettemplate USING btree (power_port_id);


--
-- Name: dcim_powerpanel_rack_group_id_76467cc9; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerpanel_rack_group_id_76467cc9 ON public.dcim_powerpanel USING btree (rack_group_id);


--
-- Name: dcim_powerpanel_site_id_c430bc89; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerpanel_site_id_c430bc89 ON public.dcim_powerpanel USING btree (site_id);


--
-- Name: dcim_powerport_cable_id_c9682ba2; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerport_cable_id_c9682ba2 ON public.dcim_powerport USING btree (cable_id);


--
-- Name: dcim_powerport_device_id_ef7185ae; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerport_device_id_ef7185ae ON public.dcim_powerport USING btree (device_id);


--
-- Name: dcim_powerporttemplate_device_type_id_1ddfbfcc; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_powerporttemplate_device_type_id_1ddfbfcc ON public.dcim_powerporttemplate USING btree (device_type_id);


--
-- Name: dcim_rack_asset_tag_f88408e5_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rack_asset_tag_f88408e5_like ON public.dcim_rack USING btree (asset_tag varchar_pattern_ops);


--
-- Name: dcim_rack_group_id_44e90ea9; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rack_group_id_44e90ea9 ON public.dcim_rack USING btree (group_id);


--
-- Name: dcim_rack_role_id_62d6919e; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rack_role_id_62d6919e ON public.dcim_rack USING btree (role_id);


--
-- Name: dcim_rack_site_id_403c7b3a; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rack_site_id_403c7b3a ON public.dcim_rack USING btree (site_id);


--
-- Name: dcim_rack_tenant_id_7cdf3725; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rack_tenant_id_7cdf3725 ON public.dcim_rack USING btree (tenant_id);


--
-- Name: dcim_rackgroup_site_id_13520e89; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackgroup_site_id_13520e89 ON public.dcim_rackgroup USING btree (site_id);


--
-- Name: dcim_rackgroup_slug_3f4582a7; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackgroup_slug_3f4582a7 ON public.dcim_rackgroup USING btree (slug);


--
-- Name: dcim_rackgroup_slug_3f4582a7_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackgroup_slug_3f4582a7_like ON public.dcim_rackgroup USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_rackreservation_rack_id_1ebbaa9b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackreservation_rack_id_1ebbaa9b ON public.dcim_rackreservation USING btree (rack_id);


--
-- Name: dcim_rackreservation_tenant_id_eb5e045f; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackreservation_tenant_id_eb5e045f ON public.dcim_rackreservation USING btree (tenant_id);


--
-- Name: dcim_rackreservation_user_id_0785a527; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackreservation_user_id_0785a527 ON public.dcim_rackreservation USING btree (user_id);


--
-- Name: dcim_rackrole_name_9077cfcc_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackrole_name_9077cfcc_like ON public.dcim_rackrole USING btree (name varchar_pattern_ops);


--
-- Name: dcim_rackrole_slug_40bbcd3a_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rackrole_slug_40bbcd3a_like ON public.dcim_rackrole USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_rearport_cable_id_42c0e4e7; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rearport_cable_id_42c0e4e7 ON public.dcim_rearport USING btree (cable_id);


--
-- Name: dcim_rearport_device_id_0bdfe9c0; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rearport_device_id_0bdfe9c0 ON public.dcim_rearport USING btree (device_id);


--
-- Name: dcim_rearporttemplate_device_type_id_6a02fd01; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_rearporttemplate_device_type_id_6a02fd01 ON public.dcim_rearporttemplate USING btree (device_type_id);


--
-- Name: dcim_region_level_2cee781b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_region_level_2cee781b ON public.dcim_region USING btree (level);


--
-- Name: dcim_region_lft_923d059c; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_region_lft_923d059c ON public.dcim_region USING btree (lft);


--
-- Name: dcim_region_name_ba5a7082_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_region_name_ba5a7082_like ON public.dcim_region USING btree (name varchar_pattern_ops);


--
-- Name: dcim_region_parent_id_2486f5d4; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_region_parent_id_2486f5d4 ON public.dcim_region USING btree (parent_id);


--
-- Name: dcim_region_rght_20f888e3; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_region_rght_20f888e3 ON public.dcim_region USING btree (rght);


--
-- Name: dcim_region_slug_ff078a66_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_region_slug_ff078a66_like ON public.dcim_region USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_region_tree_id_a09ea9a7; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_region_tree_id_a09ea9a7 ON public.dcim_region USING btree (tree_id);


--
-- Name: dcim_site_name_8fe66c76_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_site_name_8fe66c76_like ON public.dcim_site USING btree (name varchar_pattern_ops);


--
-- Name: dcim_site_region_id_45210932; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_site_region_id_45210932 ON public.dcim_site USING btree (region_id);


--
-- Name: dcim_site_slug_4412c762_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_site_slug_4412c762_like ON public.dcim_site USING btree (slug varchar_pattern_ops);


--
-- Name: dcim_site_tenant_id_15e7df63; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX dcim_site_tenant_id_15e7df63 ON public.dcim_site USING btree (tenant_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: extras_configcontext_name_4bbfe25d_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_name_4bbfe25d_like ON public.extras_configcontext USING btree (name varchar_pattern_ops);


--
-- Name: extras_configcontext_platforms_configcontext_id_2a516699; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_platforms_configcontext_id_2a516699 ON public.extras_configcontext_platforms USING btree (configcontext_id);


--
-- Name: extras_configcontext_platforms_platform_id_3fdfedc0; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_platforms_platform_id_3fdfedc0 ON public.extras_configcontext_platforms USING btree (platform_id);


--
-- Name: extras_configcontext_regions_configcontext_id_73003dbc; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_regions_configcontext_id_73003dbc ON public.extras_configcontext_regions USING btree (configcontext_id);


--
-- Name: extras_configcontext_regions_region_id_35c6ba9d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_regions_region_id_35c6ba9d ON public.extras_configcontext_regions USING btree (region_id);


--
-- Name: extras_configcontext_roles_configcontext_id_59b67386; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_roles_configcontext_id_59b67386 ON public.extras_configcontext_roles USING btree (configcontext_id);


--
-- Name: extras_configcontext_roles_devicerole_id_d3a84813; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_roles_devicerole_id_d3a84813 ON public.extras_configcontext_roles USING btree (devicerole_id);


--
-- Name: extras_configcontext_sites_configcontext_id_8c54feb9; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_sites_configcontext_id_8c54feb9 ON public.extras_configcontext_sites USING btree (configcontext_id);


--
-- Name: extras_configcontext_sites_site_id_cbb76c96; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_sites_site_id_cbb76c96 ON public.extras_configcontext_sites USING btree (site_id);


--
-- Name: extras_configcontext_tenant_groups_configcontext_id_92f68345; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_tenant_groups_configcontext_id_92f68345 ON public.extras_configcontext_tenant_groups USING btree (configcontext_id);


--
-- Name: extras_configcontext_tenant_groups_tenantgroup_id_0909688d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_tenant_groups_tenantgroup_id_0909688d ON public.extras_configcontext_tenant_groups USING btree (tenantgroup_id);


--
-- Name: extras_configcontext_tenants_configcontext_id_b53552a6; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_tenants_configcontext_id_b53552a6 ON public.extras_configcontext_tenants USING btree (configcontext_id);


--
-- Name: extras_configcontext_tenants_tenant_id_8d0aa28e; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_configcontext_tenants_tenant_id_8d0aa28e ON public.extras_configcontext_tenants USING btree (tenant_id);


--
-- Name: extras_customfield_name_2fe72707_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customfield_name_2fe72707_like ON public.extras_customfield USING btree (name varchar_pattern_ops);


--
-- Name: extras_customfield_obj_type_contenttype_id_6890b714; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customfield_obj_type_contenttype_id_6890b714 ON public.extras_customfield_obj_type USING btree (contenttype_id);


--
-- Name: extras_customfield_obj_type_customfield_id_82480f86; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customfield_obj_type_customfield_id_82480f86 ON public.extras_customfield_obj_type USING btree (customfield_id);


--
-- Name: extras_customfieldchoice_field_id_35006739; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customfieldchoice_field_id_35006739 ON public.extras_customfieldchoice USING btree (field_id);


--
-- Name: extras_customfieldvalue_field_id_1a461f0d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customfieldvalue_field_id_1a461f0d ON public.extras_customfieldvalue USING btree (field_id);


--
-- Name: extras_customfieldvalue_obj_type_id_b750b07b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customfieldvalue_obj_type_id_b750b07b ON public.extras_customfieldvalue USING btree (obj_type_id);


--
-- Name: extras_customlink_content_type_id_4d35b063; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customlink_content_type_id_4d35b063 ON public.extras_customlink USING btree (content_type_id);


--
-- Name: extras_customlink_name_daed2d18_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_customlink_name_daed2d18_like ON public.extras_customlink USING btree (name varchar_pattern_ops);


--
-- Name: extras_exporttemplate_content_type_id_59737e21; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_exporttemplate_content_type_id_59737e21 ON public.extras_exporttemplate USING btree (content_type_id);


--
-- Name: extras_imageattachment_content_type_id_90e0643d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_imageattachment_content_type_id_90e0643d ON public.extras_imageattachment USING btree (content_type_id);


--
-- Name: extras_objectchange_changed_object_type_id_b755bb60; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_objectchange_changed_object_type_id_b755bb60 ON public.extras_objectchange USING btree (changed_object_type_id);


--
-- Name: extras_objectchange_related_object_type_id_fe6e521f; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_objectchange_related_object_type_id_fe6e521f ON public.extras_objectchange USING btree (related_object_type_id);


--
-- Name: extras_objectchange_user_id_7fdf8186; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_objectchange_user_id_7fdf8186 ON public.extras_objectchange USING btree (user_id);


--
-- Name: extras_reportresult_report_3575dd21_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_reportresult_report_3575dd21_like ON public.extras_reportresult USING btree (report varchar_pattern_ops);


--
-- Name: extras_reportresult_user_id_0df55b95; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_reportresult_user_id_0df55b95 ON public.extras_reportresult USING btree (user_id);


--
-- Name: extras_tag_name_9550b3d9_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_tag_name_9550b3d9_like ON public.extras_tag USING btree (name varchar_pattern_ops);


--
-- Name: extras_tag_slug_aaa5b7e9_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_tag_slug_aaa5b7e9_like ON public.extras_tag USING btree (slug varchar_pattern_ops);


--
-- Name: extras_taggeditem_content_type_id_ba5562ed; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_taggeditem_content_type_id_ba5562ed ON public.extras_taggeditem USING btree (content_type_id);


--
-- Name: extras_taggeditem_content_type_id_object_id_80e28e23_idx; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_taggeditem_content_type_id_object_id_80e28e23_idx ON public.extras_taggeditem USING btree (content_type_id, object_id);


--
-- Name: extras_taggeditem_object_id_31b2aa77; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_taggeditem_object_id_31b2aa77 ON public.extras_taggeditem USING btree (object_id);


--
-- Name: extras_taggeditem_tag_id_d48af7c7; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_taggeditem_tag_id_d48af7c7 ON public.extras_taggeditem USING btree (tag_id);


--
-- Name: extras_topologymap_name_f377ebf1_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_topologymap_name_f377ebf1_like ON public.extras_topologymap USING btree (name varchar_pattern_ops);


--
-- Name: extras_topologymap_site_id_b56b3ceb; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_topologymap_site_id_b56b3ceb ON public.extras_topologymap USING btree (site_id);


--
-- Name: extras_topologymap_slug_9ba3d31e_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_topologymap_slug_9ba3d31e_like ON public.extras_topologymap USING btree (slug varchar_pattern_ops);


--
-- Name: extras_webhook_name_82cf60b5_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_webhook_name_82cf60b5_like ON public.extras_webhook USING btree (name varchar_pattern_ops);


--
-- Name: extras_webhook_obj_type_contenttype_id_85c7693b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_webhook_obj_type_contenttype_id_85c7693b ON public.extras_webhook_obj_type USING btree (contenttype_id);


--
-- Name: extras_webhook_obj_type_webhook_id_c7bed170; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX extras_webhook_obj_type_webhook_id_c7bed170 ON public.extras_webhook_obj_type USING btree (webhook_id);


--
-- Name: ipam_aggregate_rir_id_ef7a27bd; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_aggregate_rir_id_ef7a27bd ON public.ipam_aggregate USING btree (rir_id);


--
-- Name: ipam_ipaddress_interface_id_91e71d9d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_ipaddress_interface_id_91e71d9d ON public.ipam_ipaddress USING btree (interface_id);


--
-- Name: ipam_ipaddress_tenant_id_ac55acfd; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_ipaddress_tenant_id_ac55acfd ON public.ipam_ipaddress USING btree (tenant_id);


--
-- Name: ipam_ipaddress_vrf_id_51fcc59b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_ipaddress_vrf_id_51fcc59b ON public.ipam_ipaddress USING btree (vrf_id);


--
-- Name: ipam_prefix_role_id_0a98d415; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_prefix_role_id_0a98d415 ON public.ipam_prefix USING btree (role_id);


--
-- Name: ipam_prefix_site_id_0b20df05; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_prefix_site_id_0b20df05 ON public.ipam_prefix USING btree (site_id);


--
-- Name: ipam_prefix_tenant_id_7ba1fcc4; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_prefix_tenant_id_7ba1fcc4 ON public.ipam_prefix USING btree (tenant_id);


--
-- Name: ipam_prefix_vlan_id_1db91bff; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_prefix_vlan_id_1db91bff ON public.ipam_prefix USING btree (vlan_id);


--
-- Name: ipam_prefix_vrf_id_34f78ed0; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_prefix_vrf_id_34f78ed0 ON public.ipam_prefix USING btree (vrf_id);


--
-- Name: ipam_rir_name_64a71982_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_rir_name_64a71982_like ON public.ipam_rir USING btree (name varchar_pattern_ops);


--
-- Name: ipam_rir_slug_ff1a369a_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_rir_slug_ff1a369a_like ON public.ipam_rir USING btree (slug varchar_pattern_ops);


--
-- Name: ipam_role_name_13784849_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_role_name_13784849_like ON public.ipam_role USING btree (name varchar_pattern_ops);


--
-- Name: ipam_role_slug_309ca14c_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_role_slug_309ca14c_like ON public.ipam_role USING btree (slug varchar_pattern_ops);


--
-- Name: ipam_service_device_id_b4d2bb9c; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_service_device_id_b4d2bb9c ON public.ipam_service USING btree (device_id);


--
-- Name: ipam_service_ipaddresses_ipaddress_id_b4138c6d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_service_ipaddresses_ipaddress_id_b4138c6d ON public.ipam_service_ipaddresses USING btree (ipaddress_id);


--
-- Name: ipam_service_ipaddresses_service_id_ae26b9ab; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_service_ipaddresses_service_id_ae26b9ab ON public.ipam_service_ipaddresses USING btree (service_id);


--
-- Name: ipam_service_virtual_machine_id_e8b53562; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_service_virtual_machine_id_e8b53562 ON public.ipam_service USING btree (virtual_machine_id);


--
-- Name: ipam_vlan_group_id_88cbfa62; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vlan_group_id_88cbfa62 ON public.ipam_vlan USING btree (group_id);


--
-- Name: ipam_vlan_role_id_f5015962; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vlan_role_id_f5015962 ON public.ipam_vlan USING btree (role_id);


--
-- Name: ipam_vlan_site_id_a59334e3; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vlan_site_id_a59334e3 ON public.ipam_vlan USING btree (site_id);


--
-- Name: ipam_vlan_tenant_id_71a8290d; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vlan_tenant_id_71a8290d ON public.ipam_vlan USING btree (tenant_id);


--
-- Name: ipam_vlangroup_site_id_264f36f6; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vlangroup_site_id_264f36f6 ON public.ipam_vlangroup USING btree (site_id);


--
-- Name: ipam_vlangroup_slug_40abcf6b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vlangroup_slug_40abcf6b ON public.ipam_vlangroup USING btree (slug);


--
-- Name: ipam_vlangroup_slug_40abcf6b_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vlangroup_slug_40abcf6b_like ON public.ipam_vlangroup USING btree (slug varchar_pattern_ops);


--
-- Name: ipam_vrf_rd_0ac1bde1_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vrf_rd_0ac1bde1_like ON public.ipam_vrf USING btree (rd varchar_pattern_ops);


--
-- Name: ipam_vrf_tenant_id_498b0051; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX ipam_vrf_tenant_id_498b0051 ON public.ipam_vrf USING btree (tenant_id);


--
-- Name: secrets_secret_device_id_c7c13124; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secret_device_id_c7c13124 ON public.secrets_secret USING btree (device_id);


--
-- Name: secrets_secret_role_id_39d9347f; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secret_role_id_39d9347f ON public.secrets_secret USING btree (role_id);


--
-- Name: secrets_secretrole_groups_group_id_a687dd10; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secretrole_groups_group_id_a687dd10 ON public.secrets_secretrole_groups USING btree (group_id);


--
-- Name: secrets_secretrole_groups_secretrole_id_3cf0338b; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secretrole_groups_secretrole_id_3cf0338b ON public.secrets_secretrole_groups USING btree (secretrole_id);


--
-- Name: secrets_secretrole_name_7b6ee7a4_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secretrole_name_7b6ee7a4_like ON public.secrets_secretrole USING btree (name varchar_pattern_ops);


--
-- Name: secrets_secretrole_slug_a06c885e_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secretrole_slug_a06c885e_like ON public.secrets_secretrole USING btree (slug varchar_pattern_ops);


--
-- Name: secrets_secretrole_users_secretrole_id_d2eac298; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secretrole_users_secretrole_id_d2eac298 ON public.secrets_secretrole_users USING btree (secretrole_id);


--
-- Name: secrets_secretrole_users_user_id_25be95ad; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX secrets_secretrole_users_user_id_25be95ad ON public.secrets_secretrole_users USING btree (user_id);


--
-- Name: taggit_tag_name_58eb2ed9_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX taggit_tag_name_58eb2ed9_like ON public.taggit_tag USING btree (name varchar_pattern_ops);


--
-- Name: taggit_tag_slug_6be58b2c_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX taggit_tag_slug_6be58b2c_like ON public.taggit_tag USING btree (slug varchar_pattern_ops);


--
-- Name: taggit_taggeditem_content_type_id_9957a03c; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX taggit_taggeditem_content_type_id_9957a03c ON public.taggit_taggeditem USING btree (content_type_id);


--
-- Name: taggit_taggeditem_content_type_id_object_id_196cc965_idx; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX taggit_taggeditem_content_type_id_object_id_196cc965_idx ON public.taggit_taggeditem USING btree (content_type_id, object_id);


--
-- Name: taggit_taggeditem_object_id_e2d7d1df; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX taggit_taggeditem_object_id_e2d7d1df ON public.taggit_taggeditem USING btree (object_id);


--
-- Name: taggit_taggeditem_tag_id_f4f5b767; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX taggit_taggeditem_tag_id_f4f5b767 ON public.taggit_taggeditem USING btree (tag_id);


--
-- Name: tenancy_tenant_group_id_7daef6f4; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX tenancy_tenant_group_id_7daef6f4 ON public.tenancy_tenant USING btree (group_id);


--
-- Name: tenancy_tenant_name_f6e5b2f5_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX tenancy_tenant_name_f6e5b2f5_like ON public.tenancy_tenant USING btree (name varchar_pattern_ops);


--
-- Name: tenancy_tenant_slug_0716575e_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX tenancy_tenant_slug_0716575e_like ON public.tenancy_tenant USING btree (slug varchar_pattern_ops);


--
-- Name: tenancy_tenantgroup_name_53363199_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX tenancy_tenantgroup_name_53363199_like ON public.tenancy_tenantgroup USING btree (name varchar_pattern_ops);


--
-- Name: tenancy_tenantgroup_slug_e2af1cb6_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX tenancy_tenantgroup_slug_e2af1cb6_like ON public.tenancy_tenantgroup USING btree (slug varchar_pattern_ops);


--
-- Name: users_token_key_820deccd_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX users_token_key_820deccd_like ON public.users_token USING btree (key varchar_pattern_ops);


--
-- Name: users_token_user_id_af964690; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX users_token_user_id_af964690 ON public.users_token USING btree (user_id);


--
-- Name: virtualization_cluster_group_id_de379828; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_cluster_group_id_de379828 ON public.virtualization_cluster USING btree (group_id);


--
-- Name: virtualization_cluster_name_1b59a61b_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_cluster_name_1b59a61b_like ON public.virtualization_cluster USING btree (name varchar_pattern_ops);


--
-- Name: virtualization_cluster_site_id_4d5af282; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_cluster_site_id_4d5af282 ON public.virtualization_cluster USING btree (site_id);


--
-- Name: virtualization_cluster_type_id_4efafb0a; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_cluster_type_id_4efafb0a ON public.virtualization_cluster USING btree (type_id);


--
-- Name: virtualization_clustergroup_name_4fcd26b4_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_clustergroup_name_4fcd26b4_like ON public.virtualization_clustergroup USING btree (name varchar_pattern_ops);


--
-- Name: virtualization_clustergroup_slug_57ca1d23_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_clustergroup_slug_57ca1d23_like ON public.virtualization_clustergroup USING btree (slug varchar_pattern_ops);


--
-- Name: virtualization_clustertype_name_ea854d3d_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_clustertype_name_ea854d3d_like ON public.virtualization_clustertype USING btree (name varchar_pattern_ops);


--
-- Name: virtualization_clustertype_slug_8ee4d0e0_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_clustertype_slug_8ee4d0e0_like ON public.virtualization_clustertype USING btree (slug varchar_pattern_ops);


--
-- Name: virtualization_virtualmachine_cluster_id_6c9f9047; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_virtualmachine_cluster_id_6c9f9047 ON public.virtualization_virtualmachine USING btree (cluster_id);


--
-- Name: virtualization_virtualmachine_name_266f6cdc_like; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_virtualmachine_name_266f6cdc_like ON public.virtualization_virtualmachine USING btree (name varchar_pattern_ops);


--
-- Name: virtualization_virtualmachine_platform_id_a6c5ccb2; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_virtualmachine_platform_id_a6c5ccb2 ON public.virtualization_virtualmachine USING btree (platform_id);


--
-- Name: virtualization_virtualmachine_role_id_0cc898f9; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_virtualmachine_role_id_0cc898f9 ON public.virtualization_virtualmachine USING btree (role_id);


--
-- Name: virtualization_virtualmachine_tenant_id_d00d1d77; Type: INDEX; Schema: public; Owner: netbox
--

CREATE INDEX virtualization_virtualmachine_tenant_id_d00d1d77 ON public.virtualization_virtualmachine USING btree (tenant_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: circuits_circuit circuits_circuit_provider_id_d9195418_fk_circuits_provider_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuit
    ADD CONSTRAINT circuits_circuit_provider_id_d9195418_fk_circuits_provider_id FOREIGN KEY (provider_id) REFERENCES public.circuits_provider(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: circuits_circuit circuits_circuit_tenant_id_812508a5_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuit
    ADD CONSTRAINT circuits_circuit_tenant_id_812508a5_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: circuits_circuit circuits_circuit_type_id_1b9f485a_fk_circuits_circuittype_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuit
    ADD CONSTRAINT circuits_circuit_type_id_1b9f485a_fk_circuits_circuittype_id FOREIGN KEY (type_id) REFERENCES public.circuits_circuittype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: circuits_circuittermination circuits_circuitterm_circuit_id_257e87e7_fk_circuits_; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination
    ADD CONSTRAINT circuits_circuitterm_circuit_id_257e87e7_fk_circuits_ FOREIGN KEY (circuit_id) REFERENCES public.circuits_circuit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: circuits_circuittermination circuits_circuitterm_connected_endpoint_i_eb10be43_fk_dcim_inte; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination
    ADD CONSTRAINT circuits_circuitterm_connected_endpoint_i_eb10be43_fk_dcim_inte FOREIGN KEY (connected_endpoint_id) REFERENCES public.dcim_interface(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: circuits_circuittermination circuits_circuittermination_cable_id_35e9f703_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination
    ADD CONSTRAINT circuits_circuittermination_cable_id_35e9f703_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: circuits_circuittermination circuits_circuittermination_site_id_e6fe5652_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.circuits_circuittermination
    ADD CONSTRAINT circuits_circuittermination_site_id_e6fe5652_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_cable dcim_cable_termination_a_type_i_a614bab8_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_cable
    ADD CONSTRAINT dcim_cable_termination_a_type_i_a614bab8_fk_django_co FOREIGN KEY (termination_a_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_cable dcim_cable_termination_b_type_i_a91595d0_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_cable
    ADD CONSTRAINT dcim_cable_termination_b_type_i_a91595d0_fk_django_co FOREIGN KEY (termination_b_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_consoleport dcim_consoleport_cable_id_a9ae5465_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleport
    ADD CONSTRAINT dcim_consoleport_cable_id_a9ae5465_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_consoleport dcim_consoleport_connected_endpoint_i_efe0a825_fk_dcim_cons; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleport
    ADD CONSTRAINT dcim_consoleport_connected_endpoint_i_efe0a825_fk_dcim_cons FOREIGN KEY (connected_endpoint_id) REFERENCES public.dcim_consoleserverport(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_consoleport dcim_consoleport_device_id_f2d90d3c_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleport
    ADD CONSTRAINT dcim_consoleport_device_id_f2d90d3c_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_consoleporttemplate dcim_consoleporttemp_device_type_id_075d4015_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleporttemplate
    ADD CONSTRAINT dcim_consoleporttemp_device_type_id_075d4015_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_consoleserverporttemplate dcim_consoleserverpo_device_type_id_579bdc86_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverporttemplate
    ADD CONSTRAINT dcim_consoleserverpo_device_type_id_579bdc86_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_consoleserverport dcim_consoleserverport_cable_id_f2940dfd_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverport
    ADD CONSTRAINT dcim_consoleserverport_cable_id_f2940dfd_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_consoleserverport dcim_consoleserverport_device_id_d9866581_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_consoleserverport
    ADD CONSTRAINT dcim_consoleserverport_device_id_d9866581_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_cluster_id_cf852f78_fk_virtualization_cluster_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_cluster_id_cf852f78_fk_virtualization_cluster_id FOREIGN KEY (cluster_id) REFERENCES public.virtualization_cluster(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_device_role_id_682e8188_fk_dcim_devicerole_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_device_role_id_682e8188_fk_dcim_devicerole_id FOREIGN KEY (device_role_id) REFERENCES public.dcim_devicerole(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_device_type_id_d61b4086_fk_dcim_devicetype_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_device_type_id_d61b4086_fk_dcim_devicetype_id FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_platform_id_468138f1_fk_dcim_platform_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_platform_id_468138f1_fk_dcim_platform_id FOREIGN KEY (platform_id) REFERENCES public.dcim_platform(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_primary_ip4_id_2ccd943a_fk_ipam_ipaddress_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_primary_ip4_id_2ccd943a_fk_ipam_ipaddress_id FOREIGN KEY (primary_ip4_id) REFERENCES public.ipam_ipaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_primary_ip6_id_d180fe91_fk_ipam_ipaddress_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_primary_ip6_id_d180fe91_fk_ipam_ipaddress_id FOREIGN KEY (primary_ip6_id) REFERENCES public.ipam_ipaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_rack_id_23bde71f_fk_dcim_rack_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_rack_id_23bde71f_fk_dcim_rack_id FOREIGN KEY (rack_id) REFERENCES public.dcim_rack(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_site_id_ff897cf6_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_site_id_ff897cf6_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_tenant_id_dcea7969_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_tenant_id_dcea7969_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_device dcim_device_virtual_chassis_id_aed51693_fk_dcim_virt; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_device
    ADD CONSTRAINT dcim_device_virtual_chassis_id_aed51693_fk_dcim_virt FOREIGN KEY (virtual_chassis_id) REFERENCES public.dcim_virtualchassis(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_devicebay dcim_devicebay_device_id_0c8a1218_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebay
    ADD CONSTRAINT dcim_devicebay_device_id_0c8a1218_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_devicebay dcim_devicebay_installed_device_id_04618112_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebay
    ADD CONSTRAINT dcim_devicebay_installed_device_id_04618112_fk_dcim_device_id FOREIGN KEY (installed_device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_devicebaytemplate dcim_devicebaytempla_device_type_id_f4b24a29_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicebaytemplate
    ADD CONSTRAINT dcim_devicebaytempla_device_type_id_f4b24a29_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_devicetype dcim_devicetype_manufacturer_id_a3e8029e_fk_dcim_manu; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_devicetype
    ADD CONSTRAINT dcim_devicetype_manufacturer_id_a3e8029e_fk_dcim_manu FOREIGN KEY (manufacturer_id) REFERENCES public.dcim_manufacturer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_frontport dcim_frontport_cable_id_04ff8aab_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontport
    ADD CONSTRAINT dcim_frontport_cable_id_04ff8aab_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_frontport dcim_frontport_device_id_950557b5_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontport
    ADD CONSTRAINT dcim_frontport_device_id_950557b5_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_frontport dcim_frontport_rear_port_id_78df2532_fk_dcim_rearport_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontport
    ADD CONSTRAINT dcim_frontport_rear_port_id_78df2532_fk_dcim_rearport_id FOREIGN KEY (rear_port_id) REFERENCES public.dcim_rearport(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_frontporttemplate dcim_frontporttempla_device_type_id_f088b952_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontporttemplate
    ADD CONSTRAINT dcim_frontporttempla_device_type_id_f088b952_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_frontporttemplate dcim_frontporttempla_rear_port_id_9775411b_fk_dcim_rear; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_frontporttemplate
    ADD CONSTRAINT dcim_frontporttempla_rear_port_id_9775411b_fk_dcim_rear FOREIGN KEY (rear_port_id) REFERENCES public.dcim_rearporttemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface dcim_interface__connected_circuitte_be36a3a3_fk_circuits_; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface__connected_circuitte_be36a3a3_fk_circuits_ FOREIGN KEY (_connected_circuittermination_id) REFERENCES public.circuits_circuittermination(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface dcim_interface__connected_interface_3dfcd87c_fk_dcim_inte; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface__connected_interface_3dfcd87c_fk_dcim_inte FOREIGN KEY (_connected_interface_id) REFERENCES public.dcim_interface(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface dcim_interface_cable_id_1b264edb_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface_cable_id_1b264edb_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface dcim_interface_device_id_359c6115_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface_device_id_359c6115_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface dcim_interface_lag_id_ea1a1d12_fk_dcim_interface_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface_lag_id_ea1a1d12_fk_dcim_interface_id FOREIGN KEY (lag_id) REFERENCES public.dcim_interface(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface_tagged_vlans dcim_interface_tagge_interface_id_5870c9e9_fk_dcim_inte; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface_tagged_vlans
    ADD CONSTRAINT dcim_interface_tagge_interface_id_5870c9e9_fk_dcim_inte FOREIGN KEY (interface_id) REFERENCES public.dcim_interface(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface_tagged_vlans dcim_interface_tagged_vlans_vlan_id_e027005c_fk_ipam_vlan_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface_tagged_vlans
    ADD CONSTRAINT dcim_interface_tagged_vlans_vlan_id_e027005c_fk_ipam_vlan_id FOREIGN KEY (vlan_id) REFERENCES public.ipam_vlan(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface dcim_interface_untagged_vlan_id_838dc7be_fk_ipam_vlan_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface_untagged_vlan_id_838dc7be_fk_ipam_vlan_id FOREIGN KEY (untagged_vlan_id) REFERENCES public.ipam_vlan(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interface dcim_interface_virtual_machine_id_2afd2d50_fk_virtualiz; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interface
    ADD CONSTRAINT dcim_interface_virtual_machine_id_2afd2d50_fk_virtualiz FOREIGN KEY (virtual_machine_id) REFERENCES public.virtualization_virtualmachine(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_interfacetemplate dcim_interfacetempla_device_type_id_4bfcbfab_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_interfacetemplate
    ADD CONSTRAINT dcim_interfacetempla_device_type_id_4bfcbfab_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_inventoryitem dcim_inventoryitem_device_id_033d83f8_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_inventoryitem
    ADD CONSTRAINT dcim_inventoryitem_device_id_033d83f8_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_inventoryitem dcim_inventoryitem_manufacturer_id_dcd1b78a_fk_dcim_manu; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_inventoryitem
    ADD CONSTRAINT dcim_inventoryitem_manufacturer_id_dcd1b78a_fk_dcim_manu FOREIGN KEY (manufacturer_id) REFERENCES public.dcim_manufacturer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_inventoryitem dcim_inventoryitem_parent_id_7ebcd457_fk_dcim_inventoryitem_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_inventoryitem
    ADD CONSTRAINT dcim_inventoryitem_parent_id_7ebcd457_fk_dcim_inventoryitem_id FOREIGN KEY (parent_id) REFERENCES public.dcim_inventoryitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_platform dcim_platform_manufacturer_id_83f72d3d_fk_dcim_manufacturer_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_platform
    ADD CONSTRAINT dcim_platform_manufacturer_id_83f72d3d_fk_dcim_manufacturer_id FOREIGN KEY (manufacturer_id) REFERENCES public.dcim_manufacturer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerfeed dcim_powerfeed_cable_id_ec44c4f8_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed
    ADD CONSTRAINT dcim_powerfeed_cable_id_ec44c4f8_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerfeed dcim_powerfeed_connected_endpoint_i_6ad0aad2_fk_dcim_powe; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed
    ADD CONSTRAINT dcim_powerfeed_connected_endpoint_i_6ad0aad2_fk_dcim_powe FOREIGN KEY (connected_endpoint_id) REFERENCES public.dcim_powerport(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerfeed dcim_powerfeed_power_panel_id_32bde3be_fk_dcim_powerpanel_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed
    ADD CONSTRAINT dcim_powerfeed_power_panel_id_32bde3be_fk_dcim_powerpanel_id FOREIGN KEY (power_panel_id) REFERENCES public.dcim_powerpanel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerfeed dcim_powerfeed_rack_id_7abba090_fk_dcim_rack_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerfeed
    ADD CONSTRAINT dcim_powerfeed_rack_id_7abba090_fk_dcim_rack_id FOREIGN KEY (rack_id) REFERENCES public.dcim_rack(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_poweroutlet dcim_poweroutlet_cable_id_8dbea1ec_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlet
    ADD CONSTRAINT dcim_poweroutlet_cable_id_8dbea1ec_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_poweroutlet dcim_poweroutlet_device_id_286351d7_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlet
    ADD CONSTRAINT dcim_poweroutlet_device_id_286351d7_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_poweroutlet dcim_poweroutlet_power_port_id_9bdf4163_fk_dcim_powerport_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlet
    ADD CONSTRAINT dcim_poweroutlet_power_port_id_9bdf4163_fk_dcim_powerport_id FOREIGN KEY (power_port_id) REFERENCES public.dcim_powerport(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_poweroutlettemplate dcim_poweroutlettemp_device_type_id_26b2316c_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlettemplate
    ADD CONSTRAINT dcim_poweroutlettemp_device_type_id_26b2316c_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_poweroutlettemplate dcim_poweroutlettemp_power_port_id_c0fb0c42_fk_dcim_powe; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_poweroutlettemplate
    ADD CONSTRAINT dcim_poweroutlettemp_power_port_id_c0fb0c42_fk_dcim_powe FOREIGN KEY (power_port_id) REFERENCES public.dcim_powerporttemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerpanel dcim_powerpanel_rack_group_id_76467cc9_fk_dcim_rackgroup_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerpanel
    ADD CONSTRAINT dcim_powerpanel_rack_group_id_76467cc9_fk_dcim_rackgroup_id FOREIGN KEY (rack_group_id) REFERENCES public.dcim_rackgroup(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerpanel dcim_powerpanel_site_id_c430bc89_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerpanel
    ADD CONSTRAINT dcim_powerpanel_site_id_c430bc89_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerport dcim_powerport__connected_powerfeed_8f5230a3_fk_dcim_powe; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport__connected_powerfeed_8f5230a3_fk_dcim_powe FOREIGN KEY (_connected_powerfeed_id) REFERENCES public.dcim_powerfeed(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerport dcim_powerport__connected_poweroutl_6c3ea413_fk_dcim_powe; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport__connected_poweroutl_6c3ea413_fk_dcim_powe FOREIGN KEY (_connected_poweroutlet_id) REFERENCES public.dcim_poweroutlet(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerport dcim_powerport_cable_id_c9682ba2_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport_cable_id_c9682ba2_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerport dcim_powerport_device_id_ef7185ae_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerport
    ADD CONSTRAINT dcim_powerport_device_id_ef7185ae_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_powerporttemplate dcim_powerporttempla_device_type_id_1ddfbfcc_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_powerporttemplate
    ADD CONSTRAINT dcim_powerporttempla_device_type_id_1ddfbfcc_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rack dcim_rack_group_id_44e90ea9_fk_dcim_rackgroup_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_group_id_44e90ea9_fk_dcim_rackgroup_id FOREIGN KEY (group_id) REFERENCES public.dcim_rackgroup(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rack dcim_rack_role_id_62d6919e_fk_dcim_rackrole_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_role_id_62d6919e_fk_dcim_rackrole_id FOREIGN KEY (role_id) REFERENCES public.dcim_rackrole(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rack dcim_rack_site_id_403c7b3a_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_site_id_403c7b3a_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rack dcim_rack_tenant_id_7cdf3725_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rack
    ADD CONSTRAINT dcim_rack_tenant_id_7cdf3725_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rackgroup dcim_rackgroup_site_id_13520e89_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackgroup
    ADD CONSTRAINT dcim_rackgroup_site_id_13520e89_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rackreservation dcim_rackreservation_rack_id_1ebbaa9b_fk_dcim_rack_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackreservation
    ADD CONSTRAINT dcim_rackreservation_rack_id_1ebbaa9b_fk_dcim_rack_id FOREIGN KEY (rack_id) REFERENCES public.dcim_rack(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rackreservation dcim_rackreservation_tenant_id_eb5e045f_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackreservation
    ADD CONSTRAINT dcim_rackreservation_tenant_id_eb5e045f_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rackreservation dcim_rackreservation_user_id_0785a527_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rackreservation
    ADD CONSTRAINT dcim_rackreservation_user_id_0785a527_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rearport dcim_rearport_cable_id_42c0e4e7_fk_dcim_cable_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearport
    ADD CONSTRAINT dcim_rearport_cable_id_42c0e4e7_fk_dcim_cable_id FOREIGN KEY (cable_id) REFERENCES public.dcim_cable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rearport dcim_rearport_device_id_0bdfe9c0_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearport
    ADD CONSTRAINT dcim_rearport_device_id_0bdfe9c0_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_rearporttemplate dcim_rearporttemplat_device_type_id_6a02fd01_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_rearporttemplate
    ADD CONSTRAINT dcim_rearporttemplat_device_type_id_6a02fd01_fk_dcim_devi FOREIGN KEY (device_type_id) REFERENCES public.dcim_devicetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_region dcim_region_parent_id_2486f5d4_fk_dcim_region_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_region
    ADD CONSTRAINT dcim_region_parent_id_2486f5d4_fk_dcim_region_id FOREIGN KEY (parent_id) REFERENCES public.dcim_region(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_site dcim_site_region_id_45210932_fk_dcim_region_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_site
    ADD CONSTRAINT dcim_site_region_id_45210932_fk_dcim_region_id FOREIGN KEY (region_id) REFERENCES public.dcim_region(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_site dcim_site_tenant_id_15e7df63_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_site
    ADD CONSTRAINT dcim_site_tenant_id_15e7df63_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dcim_virtualchassis dcim_virtualchassis_master_id_ab54cfc6_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.dcim_virtualchassis
    ADD CONSTRAINT dcim_virtualchassis_master_id_ab54cfc6_fk_dcim_device_id FOREIGN KEY (master_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_platforms extras_configcontext_configcontext_id_2a516699_fk_extras_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_platforms
    ADD CONSTRAINT extras_configcontext_configcontext_id_2a516699_fk_extras_co FOREIGN KEY (configcontext_id) REFERENCES public.extras_configcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_roles extras_configcontext_configcontext_id_59b67386_fk_extras_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_roles
    ADD CONSTRAINT extras_configcontext_configcontext_id_59b67386_fk_extras_co FOREIGN KEY (configcontext_id) REFERENCES public.extras_configcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_regions extras_configcontext_configcontext_id_73003dbc_fk_extras_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_regions
    ADD CONSTRAINT extras_configcontext_configcontext_id_73003dbc_fk_extras_co FOREIGN KEY (configcontext_id) REFERENCES public.extras_configcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_sites extras_configcontext_configcontext_id_8c54feb9_fk_extras_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_sites
    ADD CONSTRAINT extras_configcontext_configcontext_id_8c54feb9_fk_extras_co FOREIGN KEY (configcontext_id) REFERENCES public.extras_configcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_tenant_groups extras_configcontext_configcontext_id_92f68345_fk_extras_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenant_groups
    ADD CONSTRAINT extras_configcontext_configcontext_id_92f68345_fk_extras_co FOREIGN KEY (configcontext_id) REFERENCES public.extras_configcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_tenants extras_configcontext_configcontext_id_b53552a6_fk_extras_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenants
    ADD CONSTRAINT extras_configcontext_configcontext_id_b53552a6_fk_extras_co FOREIGN KEY (configcontext_id) REFERENCES public.extras_configcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_roles extras_configcontext_devicerole_id_d3a84813_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_roles
    ADD CONSTRAINT extras_configcontext_devicerole_id_d3a84813_fk_dcim_devi FOREIGN KEY (devicerole_id) REFERENCES public.dcim_devicerole(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_platforms extras_configcontext_platform_id_3fdfedc0_fk_dcim_plat; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_platforms
    ADD CONSTRAINT extras_configcontext_platform_id_3fdfedc0_fk_dcim_plat FOREIGN KEY (platform_id) REFERENCES public.dcim_platform(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_regions extras_configcontext_region_id_35c6ba9d_fk_dcim_regi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_regions
    ADD CONSTRAINT extras_configcontext_region_id_35c6ba9d_fk_dcim_regi FOREIGN KEY (region_id) REFERENCES public.dcim_region(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_sites extras_configcontext_sites_site_id_cbb76c96_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_sites
    ADD CONSTRAINT extras_configcontext_sites_site_id_cbb76c96_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_tenants extras_configcontext_tenant_id_8d0aa28e_fk_tenancy_t; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenants
    ADD CONSTRAINT extras_configcontext_tenant_id_8d0aa28e_fk_tenancy_t FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_configcontext_tenant_groups extras_configcontext_tenantgroup_id_0909688d_fk_tenancy_t; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_configcontext_tenant_groups
    ADD CONSTRAINT extras_configcontext_tenantgroup_id_0909688d_fk_tenancy_t FOREIGN KEY (tenantgroup_id) REFERENCES public.tenancy_tenantgroup(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_customfield_obj_type extras_customfield_o_contenttype_id_6890b714_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield_obj_type
    ADD CONSTRAINT extras_customfield_o_contenttype_id_6890b714_fk_django_co FOREIGN KEY (contenttype_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_customfield_obj_type extras_customfield_o_customfield_id_82480f86_fk_extras_cu; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfield_obj_type
    ADD CONSTRAINT extras_customfield_o_customfield_id_82480f86_fk_extras_cu FOREIGN KEY (customfield_id) REFERENCES public.extras_customfield(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_customfieldchoice extras_customfieldch_field_id_35006739_fk_extras_cu; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldchoice
    ADD CONSTRAINT extras_customfieldch_field_id_35006739_fk_extras_cu FOREIGN KEY (field_id) REFERENCES public.extras_customfield(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_customfieldvalue extras_customfieldva_field_id_1a461f0d_fk_extras_cu; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldvalue
    ADD CONSTRAINT extras_customfieldva_field_id_1a461f0d_fk_extras_cu FOREIGN KEY (field_id) REFERENCES public.extras_customfield(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_customfieldvalue extras_customfieldva_obj_type_id_b750b07b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customfieldvalue
    ADD CONSTRAINT extras_customfieldva_obj_type_id_b750b07b_fk_django_co FOREIGN KEY (obj_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_customlink extras_customlink_content_type_id_4d35b063_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_customlink
    ADD CONSTRAINT extras_customlink_content_type_id_4d35b063_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_exporttemplate extras_exporttemplat_content_type_id_59737e21_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_exporttemplate
    ADD CONSTRAINT extras_exporttemplat_content_type_id_59737e21_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_imageattachment extras_imageattachme_content_type_id_90e0643d_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_imageattachment
    ADD CONSTRAINT extras_imageattachme_content_type_id_90e0643d_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_objectchange extras_objectchange_changed_object_type__b755bb60_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_objectchange
    ADD CONSTRAINT extras_objectchange_changed_object_type__b755bb60_fk_django_co FOREIGN KEY (changed_object_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_objectchange extras_objectchange_related_object_type__fe6e521f_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_objectchange
    ADD CONSTRAINT extras_objectchange_related_object_type__fe6e521f_fk_django_co FOREIGN KEY (related_object_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_objectchange extras_objectchange_user_id_7fdf8186_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_objectchange
    ADD CONSTRAINT extras_objectchange_user_id_7fdf8186_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_reportresult extras_reportresult_user_id_0df55b95_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_reportresult
    ADD CONSTRAINT extras_reportresult_user_id_0df55b95_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_taggeditem extras_taggeditem_content_type_id_ba5562ed_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_taggeditem
    ADD CONSTRAINT extras_taggeditem_content_type_id_ba5562ed_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_taggeditem extras_taggeditem_tag_id_d48af7c7_fk_extras_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_taggeditem
    ADD CONSTRAINT extras_taggeditem_tag_id_d48af7c7_fk_extras_tag_id FOREIGN KEY (tag_id) REFERENCES public.extras_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_topologymap extras_topologymap_site_id_b56b3ceb_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_topologymap
    ADD CONSTRAINT extras_topologymap_site_id_b56b3ceb_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_webhook_obj_type extras_webhook_obj_t_contenttype_id_85c7693b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook_obj_type
    ADD CONSTRAINT extras_webhook_obj_t_contenttype_id_85c7693b_fk_django_co FOREIGN KEY (contenttype_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: extras_webhook_obj_type extras_webhook_obj_t_webhook_id_c7bed170_fk_extras_we; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.extras_webhook_obj_type
    ADD CONSTRAINT extras_webhook_obj_t_webhook_id_c7bed170_fk_extras_we FOREIGN KEY (webhook_id) REFERENCES public.extras_webhook(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_aggregate ipam_aggregate_rir_id_ef7a27bd_fk_ipam_rir_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_aggregate
    ADD CONSTRAINT ipam_aggregate_rir_id_ef7a27bd_fk_ipam_rir_id FOREIGN KEY (rir_id) REFERENCES public.ipam_rir(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_ipaddress ipam_ipaddress_interface_id_91e71d9d_fk_dcim_interface_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_ipaddress
    ADD CONSTRAINT ipam_ipaddress_interface_id_91e71d9d_fk_dcim_interface_id FOREIGN KEY (interface_id) REFERENCES public.dcim_interface(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_ipaddress ipam_ipaddress_nat_inside_id_a45fb7c5_fk_ipam_ipaddress_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_ipaddress
    ADD CONSTRAINT ipam_ipaddress_nat_inside_id_a45fb7c5_fk_ipam_ipaddress_id FOREIGN KEY (nat_inside_id) REFERENCES public.ipam_ipaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_ipaddress ipam_ipaddress_tenant_id_ac55acfd_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_ipaddress
    ADD CONSTRAINT ipam_ipaddress_tenant_id_ac55acfd_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_ipaddress ipam_ipaddress_vrf_id_51fcc59b_fk_ipam_vrf_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_ipaddress
    ADD CONSTRAINT ipam_ipaddress_vrf_id_51fcc59b_fk_ipam_vrf_id FOREIGN KEY (vrf_id) REFERENCES public.ipam_vrf(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_prefix ipam_prefix_role_id_0a98d415_fk_ipam_role_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_prefix
    ADD CONSTRAINT ipam_prefix_role_id_0a98d415_fk_ipam_role_id FOREIGN KEY (role_id) REFERENCES public.ipam_role(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_prefix ipam_prefix_site_id_0b20df05_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_prefix
    ADD CONSTRAINT ipam_prefix_site_id_0b20df05_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_prefix ipam_prefix_tenant_id_7ba1fcc4_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_prefix
    ADD CONSTRAINT ipam_prefix_tenant_id_7ba1fcc4_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_prefix ipam_prefix_vlan_id_1db91bff_fk_ipam_vlan_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_prefix
    ADD CONSTRAINT ipam_prefix_vlan_id_1db91bff_fk_ipam_vlan_id FOREIGN KEY (vlan_id) REFERENCES public.ipam_vlan(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_prefix ipam_prefix_vrf_id_34f78ed0_fk_ipam_vrf_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_prefix
    ADD CONSTRAINT ipam_prefix_vrf_id_34f78ed0_fk_ipam_vrf_id FOREIGN KEY (vrf_id) REFERENCES public.ipam_vrf(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_service ipam_service_device_id_b4d2bb9c_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service
    ADD CONSTRAINT ipam_service_device_id_b4d2bb9c_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_service_ipaddresses ipam_service_ipaddre_ipaddress_id_b4138c6d_fk_ipam_ipad; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service_ipaddresses
    ADD CONSTRAINT ipam_service_ipaddre_ipaddress_id_b4138c6d_fk_ipam_ipad FOREIGN KEY (ipaddress_id) REFERENCES public.ipam_ipaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_service_ipaddresses ipam_service_ipaddresses_service_id_ae26b9ab_fk_ipam_service_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service_ipaddresses
    ADD CONSTRAINT ipam_service_ipaddresses_service_id_ae26b9ab_fk_ipam_service_id FOREIGN KEY (service_id) REFERENCES public.ipam_service(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_service ipam_service_virtual_machine_id_e8b53562_fk_virtualiz; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_service
    ADD CONSTRAINT ipam_service_virtual_machine_id_e8b53562_fk_virtualiz FOREIGN KEY (virtual_machine_id) REFERENCES public.virtualization_virtualmachine(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_vlan ipam_vlan_group_id_88cbfa62_fk_ipam_vlangroup_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan
    ADD CONSTRAINT ipam_vlan_group_id_88cbfa62_fk_ipam_vlangroup_id FOREIGN KEY (group_id) REFERENCES public.ipam_vlangroup(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_vlan ipam_vlan_role_id_f5015962_fk_ipam_role_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan
    ADD CONSTRAINT ipam_vlan_role_id_f5015962_fk_ipam_role_id FOREIGN KEY (role_id) REFERENCES public.ipam_role(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_vlan ipam_vlan_site_id_a59334e3_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan
    ADD CONSTRAINT ipam_vlan_site_id_a59334e3_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_vlan ipam_vlan_tenant_id_71a8290d_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlan
    ADD CONSTRAINT ipam_vlan_tenant_id_71a8290d_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_vlangroup ipam_vlangroup_site_id_264f36f6_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vlangroup
    ADD CONSTRAINT ipam_vlangroup_site_id_264f36f6_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ipam_vrf ipam_vrf_tenant_id_498b0051_fk_tenancy_tenant_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.ipam_vrf
    ADD CONSTRAINT ipam_vrf_tenant_id_498b0051_fk_tenancy_tenant_id FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_secret secrets_secret_device_id_c7c13124_fk_dcim_device_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secret
    ADD CONSTRAINT secrets_secret_device_id_c7c13124_fk_dcim_device_id FOREIGN KEY (device_id) REFERENCES public.dcim_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_secret secrets_secret_role_id_39d9347f_fk_secrets_secretrole_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secret
    ADD CONSTRAINT secrets_secret_role_id_39d9347f_fk_secrets_secretrole_id FOREIGN KEY (role_id) REFERENCES public.secrets_secretrole(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_secretrole_groups secrets_secretrole_g_secretrole_id_3cf0338b_fk_secrets_s; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_groups
    ADD CONSTRAINT secrets_secretrole_g_secretrole_id_3cf0338b_fk_secrets_s FOREIGN KEY (secretrole_id) REFERENCES public.secrets_secretrole(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_secretrole_groups secrets_secretrole_groups_group_id_a687dd10_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_groups
    ADD CONSTRAINT secrets_secretrole_groups_group_id_a687dd10_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_secretrole_users secrets_secretrole_u_secretrole_id_d2eac298_fk_secrets_s; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_users
    ADD CONSTRAINT secrets_secretrole_u_secretrole_id_d2eac298_fk_secrets_s FOREIGN KEY (secretrole_id) REFERENCES public.secrets_secretrole(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_secretrole_users secrets_secretrole_users_user_id_25be95ad_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_secretrole_users
    ADD CONSTRAINT secrets_secretrole_users_user_id_25be95ad_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_sessionkey secrets_sessionkey_userkey_id_3ca6176b_fk_secrets_userkey_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_sessionkey
    ADD CONSTRAINT secrets_sessionkey_userkey_id_3ca6176b_fk_secrets_userkey_id FOREIGN KEY (userkey_id) REFERENCES public.secrets_userkey(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secrets_userkey secrets_userkey_user_id_13ada46b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.secrets_userkey
    ADD CONSTRAINT secrets_userkey_user_id_13ada46b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taggit_taggeditem taggit_taggeditem_content_type_id_9957a03c_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_content_type_id_9957a03c_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taggit_taggeditem taggit_taggeditem_tag_id_f4f5b767_fk_taggit_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_tag_id_f4f5b767_fk_taggit_tag_id FOREIGN KEY (tag_id) REFERENCES public.taggit_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tenancy_tenant tenancy_tenant_group_id_7daef6f4_fk_tenancy_tenantgroup_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.tenancy_tenant
    ADD CONSTRAINT tenancy_tenant_group_id_7daef6f4_fk_tenancy_tenantgroup_id FOREIGN KEY (group_id) REFERENCES public.tenancy_tenantgroup(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_token users_token_user_id_af964690_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.users_token
    ADD CONSTRAINT users_token_user_id_af964690_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_cluster virtualization_clust_group_id_de379828_fk_virtualiz; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_cluster
    ADD CONSTRAINT virtualization_clust_group_id_de379828_fk_virtualiz FOREIGN KEY (group_id) REFERENCES public.virtualization_clustergroup(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_cluster virtualization_clust_type_id_4efafb0a_fk_virtualiz; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_cluster
    ADD CONSTRAINT virtualization_clust_type_id_4efafb0a_fk_virtualiz FOREIGN KEY (type_id) REFERENCES public.virtualization_clustertype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_cluster virtualization_cluster_site_id_4d5af282_fk_dcim_site_id; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_cluster
    ADD CONSTRAINT virtualization_cluster_site_id_4d5af282_fk_dcim_site_id FOREIGN KEY (site_id) REFERENCES public.dcim_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_virtualmachine virtualization_virtu_cluster_id_6c9f9047_fk_virtualiz; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtu_cluster_id_6c9f9047_fk_virtualiz FOREIGN KEY (cluster_id) REFERENCES public.virtualization_cluster(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_virtualmachine virtualization_virtu_platform_id_a6c5ccb2_fk_dcim_plat; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtu_platform_id_a6c5ccb2_fk_dcim_plat FOREIGN KEY (platform_id) REFERENCES public.dcim_platform(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_virtualmachine virtualization_virtu_primary_ip4_id_942e42ae_fk_ipam_ipad; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtu_primary_ip4_id_942e42ae_fk_ipam_ipad FOREIGN KEY (primary_ip4_id) REFERENCES public.ipam_ipaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_virtualmachine virtualization_virtu_primary_ip6_id_b7904e73_fk_ipam_ipad; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtu_primary_ip6_id_b7904e73_fk_ipam_ipad FOREIGN KEY (primary_ip6_id) REFERENCES public.ipam_ipaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_virtualmachine virtualization_virtu_role_id_0cc898f9_fk_dcim_devi; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtu_role_id_0cc898f9_fk_dcim_devi FOREIGN KEY (role_id) REFERENCES public.dcim_devicerole(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: virtualization_virtualmachine virtualization_virtu_tenant_id_d00d1d77_fk_tenancy_t; Type: FK CONSTRAINT; Schema: public; Owner: netbox
--

ALTER TABLE ONLY public.virtualization_virtualmachine
    ADD CONSTRAINT virtualization_virtu_tenant_id_d00d1d77_fk_tenancy_t FOREIGN KEY (tenant_id) REFERENCES public.tenancy_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

